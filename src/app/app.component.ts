import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { PrehomePage } from '../pages/prehome/prehome';
import { CrucigramaPage } from '../pages/juegos/crucigrama/crucigrama';
import { DiscosPage } from '../pages/juegos/discos/discos';
import { DragPage } from '../pages/juegos/drag/drag';
import { InterpretePage } from '../pages/juegos/interprete/interprete';
import { MemoryPage } from '../pages/juegos/memory/memory';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  language: string;
  txtHome: string;
  txtMemoria: string;
  txtCrucigrama: string;
  txtDrag: string;
  txtInterprete: string;
  txtDiscos: string;
  selec: string;
  AudioEffec: string;
  Onanimated : string;
  PlayMusic: string;
  rootPage: any = PrehomePage; 

  pages: Array<{title: string, component: any, icon: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    
    this.PlayMusic = localStorage.getItem('Audio');
    if (this.PlayMusic == undefined || this.PlayMusic == null|| this.PlayMusic == ''){
      localStorage.setItem('Audio', '0');
      localStorage.setItem('IntroHome', 'false');
    }  
    this.AudioEffec = localStorage.getItem('sound-acert');
    if (this.AudioEffec == undefined || this.AudioEffec == null|| this.AudioEffec == ''){
      localStorage.setItem('sound-acert', '0');
    }  
    this.Onanimated = localStorage.getItem('Animated');
    if (this.Onanimated == undefined || this.Onanimated == null|| this.Onanimated == ''){
      localStorage.setItem('Animated', "true");
    }
    this.language = localStorage.getItem('selectedLang');
    if (this.language == undefined || this.language == null|| this.language == '' || this.language == 'es'){
      this.language = "Esp"; 
      localStorage.setItem('selectedLang', this.language);
    }
    console.log("PlayMusic =" , this.PlayMusic);
    console.log("AudioEffec =" , this.AudioEffec);
    console.log("Onanimated =" , this.Onanimated);
    console.log("language =" , this.language);
    
    
    this.ChangeLanguages();  
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: this.txtHome , component: HomePage, icon: "home"  },
      { title: this.txtHome , component: PrehomePage, icon: "home"  },
      { title: this.txtMemoria , component: MemoryPage, icon: "school"  },
      { title: this.txtCrucigrama , component: CrucigramaPage, icon: "grid" },
      { title: this.txtDrag , component: DragPage, icon: "move" },
      { title: this.txtInterprete , component: InterpretePage, icon: "help-circle" },
      { title: this.txtDiscos , component: DiscosPage, icon: "locate" },

    ];

  }
  initializeApp() {
    
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  ChangeLanguages() {
    if (this.language == "Eng") {
      this.txtHome = "Home";
      this.txtMemoria = "Memory";
      this.txtCrucigrama = "Crossword";
      this.txtDrag = "Drag";
      this.txtInterprete = "Output interpreter";
      this.txtDiscos = "Disc";
      this.selec = "¡Select a game!";
    }else if (this.language == "Por"){
      this.txtHome = "Home";
      this.txtMemoria = "Memória";
      this.txtCrucigrama = "Palavras cruzadas";
      this.txtDrag = "Drag";
      this.txtInterprete = "Interprete de saída";
      this.txtDiscos = "Discos";
      this.selec = "¡Selecione um jogo!";
    }else{
      this.txtHome = "Home";
      this.txtMemoria = "Memoria";
      this.txtCrucigrama = "Crucigrama";
      this.txtDrag = "Drag";
      this.txtInterprete = "Interprete la Salida";
      this.txtDiscos = "Discos";
      this.selec = "¡Seleccione un Juego!";
        }
    }
}
