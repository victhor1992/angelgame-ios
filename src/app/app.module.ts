import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PrehomePage } from '../pages/prehome/prehome';
import { CrucigramaPage } from '../pages/juegos/crucigrama/crucigrama';
import { DiscosPage } from '../pages/juegos/discos/discos';
import { DragPage } from '../pages/juegos/drag/drag';
import { InterpretePage } from '../pages/juegos/interprete/interprete';
import { MemoryPage } from '../pages/juegos/memory/memory';
import { AboutPage } from '../pages/about/about';
import { SettingPage } from '../pages/setting/setting';
import { InfoPage } from '../pages/info/info';
import { LoginPage } from '../pages/login/login';
import { ConceptosPage } from '../pages/juegos/conceptos/conceptos';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Instagram } from '@ionic-native/instagram';
import { UserService } from '../providers/user-service/user-service';
import { FacebookServiceProvider } from '../providers/user-service/facebook-service';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { Device } from '@ionic-native/device';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
var firebaseConfig = {
  apiKey: "AIzaSyBW8c3lEZveovUhW5DxY7j2z-_tLBoL_vc",
  authDomain: "angel-game.firebaseapp.com",
  databaseURL: "https://angel-game.firebaseio.com",
  projectId: "angel-game",
  storageBucket: "angel-game.appspot.com",
  messagingSenderId: "508243660318",
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PrehomePage,
    CrucigramaPage,
    DiscosPage,
    DragPage,
    InterpretePage,
    MemoryPage,
    SettingPage,
    AboutPage,
    InfoPage,
    LoginPage,
    ConceptosPage
  ],
  imports: [
    HttpClientModule,
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PrehomePage,
    CrucigramaPage,
    DiscosPage,
    DragPage,
    InterpretePage,
    MemoryPage,
    SettingPage,
    AboutPage,
    InfoPage,
    LoginPage,
    ConceptosPage,
  ],
  providers: [
    StatusBar,
    InAppBrowser,
    SplashScreen,
    Instagram,
    Device,
    SocialSharing,
    FacebookServiceProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: UserService, useClass: UserService},
  ]
})
export class AppModule {}
