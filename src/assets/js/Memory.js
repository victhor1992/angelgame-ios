var questionArray = [// Arreglo que concontienes las denominadas preguntas
"FTP",
"SSH",
"Telnet",
"SMTP",
"DNS",
"HTTP",
"TFTP",
"POP3",
"NTP",
"IMAP",
"BGP",
"HTTPS",
"XWindow",
"Echo",
"qotd",////////nuevas
"tacacs",
"mysql",
"sip",
"finger",
"kerberos",
"bootp",
"gopher",
"nntp",
"SNMP",
"LDAP",
"Syslog",
"RIP",
"RIPNG",
"EPP",
"RSYNC",
"OpenVPN",
"STUN",
];
var answerArray =[// Arreglo que concontienes las denominadas respuestas
"20/21",
"22",
"23",
"25",
"53",
"80",
"69",
"110",
"123",
"143",
"179",
"443",
"6000",
"7",
"17",////////nuevas
"49",
"3306",
"5060",
"79",
"88",
"67",
"70",
"119",
"161",
"389",
"514",
"520",
"521",
"700",
"873",
"1194",
"3478",
 ];
var perdida;
// window.localStorage.setItem('MemoriaStarEasy', '0');
var level1 = Number(localStorage.getItem('level1')); 
var level2 = Number(localStorage.getItem('level2')); 
var level3 = Number(localStorage.getItem('level3')); 
var level4 = Number(localStorage.getItem('level4'));
var level5 = Number(localStorage.getItem('level5')); 
var level6 = Number(localStorage.getItem('level6')); 
var level7 = Number(localStorage.getItem('level7')); 
var level8 = Number(localStorage.getItem('level8')); 
var level9 = Number(localStorage.getItem('level9'));
var level10 = Number(localStorage.getItem('level10'));
var level11 = Number(localStorage.getItem('level10'));
var level12 = Number(localStorage.getItem('level10'));
var level13 = Number(localStorage.getItem('level10'));
var level14 = Number(localStorage.getItem('level10'));
var level15 = Number(localStorage.getItem('level10'));
var level16 = Number(localStorage.getItem('level10'));
var level17 = Number(localStorage.getItem('level10'));
var level18 = Number(localStorage.getItem('level10'));
var level19 = Number(localStorage.getItem('level10'));
var level20 = Number(localStorage.getItem('level10'));
var ContStar = Number(localStorage.getItem('contStarMemoria')); 
var PlayMusica = localStorage.getItem('Audio');
window.localStorage.setItem('audio', PlayMusica); 
var PlayMusica2 = localStorage.getItem('audio');
var AudioEffect = localStorage.getItem('sound-acert');
window.localStorage.setItem('#sound-acert', AudioEffect);
var audioElement = document.createElement('audio');
var ttl = Number(window.localStorage.getItem('ttl'));
var coin = Number(window.localStorage.getItem('coin'));
var Onanimated = localStorage.getItem('Animated');
var language = localStorage.getItem('selectedLang');
var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
var arrayOrder=[];
var nivel =0;
var timeLevel = 0;
fin= false;
var punto=0;
var Aceptar
var text1
var text2
var noCoin
if(language=='Eng'){
	Aceptar='Accept'
	text1='You need to win the level'
	text2='with at least 1 Star to unlock this level'
	noCoin="You don't have enough <img src='imgs/coin50.png' alt='' class='coin'> to use the wild card"
}else if(language=='Por'){
	Aceptar='Aceitar'
	text1='Você precisa vencer o nível'
	text2='com pelo menos 1 estrela para desbloquear este nível'
	noCoin='Você não tem <img src="imgs/coin50.png" alt="" class="coin"> suficiente para usar o curinga'
}else {
	Aceptar='Aceptar'
	text1='Necesita ganar el nivel'
	text2='con al menos 1 Estrella para desbloquear este nivel'
	noCoin='No posee suficiente <img src="imgs/coin50.png" alt="" class="coin"> para utilizar el comodín'
}
// html modal aviso
var htmlAviso ='<div id="BoxAviso" class="">'
    htmlAviso+='    <h1 class="titAviso">'+text1+' <span class="nv"></span> '+text2+'</h1>'
    htmlAviso+='    <button class="btnComprar" onclick="cerrarModal()">'+Aceptar+'</button>' 
    htmlAviso+='</div>'

// console.log('PlayMusica2 ',PlayMusica2 );
$(window).resize(function(){
	var WindowW = $(window).innerWidth();
	var WindowH = $(window).innerHeight();

	$('.memoria').css({
		'height':WindowH + 'px',
		'width':WindowW + 'px'
	});

})
$(document).ready(function(){
	$("#levelModal .modal-content").append(infoLink)
	$('#ModalAviso').html(htmlAviso);  
	var elemClic= document.getElementById('btnNext')
	elemClic.setAttribute("onclick","nextLvl();");
	var elembackwin= document.getElementById('btnbackwin')
	elembackwin.setAttribute("onclick","reload();");
	var elembacklose= document.getElementById('btnbacklose')
	elembacklose.setAttribute("onclick","reload();");
	$('#nextLVL').css({'display':'none'})

	showTuto('memoria');
	blockMemoria()
	var WindowW = $(window).innerWidth();
	var WindowH = $(window).innerHeight();
	if(WindowH <= 560){	
		$('#imagenes').addClass('overCart');
	}

	if(language=='Eng'){
		$('.lev').text('Level');
		$('.txt1').text('');
		$('.txt2').text('cards to choose.');
		$('.titLvl').text('Choose a level of difficulty!');
	}else if(language=='Por'){
		$('.lev').text('Nível');
		$('.txt1').text('Haverá doze');
		$('.txt2').text('cartas para escolher.');
		$('.titLvl').text('Escolha um nível de dificuldade!');
	}else {
		$('.lev').text('Nivel');
		$('.txt1').text('Habrán');
		$('.txt2').text('tarjetas para elegir.');
		$('.titLvl').text('¡Elige un nivel de dificultad!');
	}
	
  $('.fb-share-button').attr('data-href', window.location.href);
  $('<div class="fb-share-button" data-href="" data-layout="button" data-mobile-iframe="true" ></div>').appendTo(".text-center")
	$('.memoria').css({
		'min-height':WindowH + 'px',
		'width':WindowW + 'px'
	});

	if(localStorage.getItem("Next")== 'si') 
	{

		if(localStorage.getItem("nivelNext")==2){

			nivel= 2;
			nivel1 = nivel;
			$('#lvlN').text(1);
			if ($('#cmbTime').val()==''){
			timeLevel = 30;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());		
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==3){

			nivel= 3;
			nivel1 = nivel;
			$('#lvlN').text(2);
			if ($('#cmbTime').val()==''){
			   timeLevel = 40;
			   $('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());	
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==4){

			nivel= 4;
			nivel1 = nivel;
			$('#lvlN').text(3);
			if ($('#cmbTime').val()==''){
				timeLevel = 50;
				$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());	
			StarGame();
			Timer();
		}else if(localStorage.getItem("nivelNext")==5){

			nivel= 5;
			nivel2 = nivel;
			$('#lvlN').text(4);
			if ($('#cmbTime').val()==''){
				timeLevel = 60;
				$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());		 
			StarGame();
			Timer();

	    }else if(localStorage.getItem("nivelNext")==6){

			nivel= 6;
			nivel3 = nivel;
			$('#lvlN').text(5);
			if ($('#cmbTime').val()==''){
				timeLevel = 70;
				$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==7){

			nivel= 7;
			nivel3 = nivel;
			$('#lvlN').text(6);
			if ($('#cmbTime').val()==''){
			timeLevel = 80;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==8){

			nivel= 8;
			nivel3 = nivel;
			$('#lvlN').text(7);
			if ($('#cmbTime').val()==''){
			timeLevel = 90;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==9){

			nivel= 9;
			nivel3 = nivel;
			$('#lvlN').text(8);
			if ($('#cmbTime').val()==''){
			timeLevel = 100;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==10){

			nivel= 10;
			nivel3 = nivel;
			$('#lvlN').text(9);
			if ($('#cmbTime').val()==''){
			timeLevel = 110;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();

		}else if(localStorage.getItem("nivelNext")==11){

			nivel= 11;
			nivel3 = nivel;
			$('#lvlN').text(10);
			if ($('#cmbTime').val()==''){
			timeLevel = 120;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();
		}

		localStorage.removeItem('intentar');

	}else{

		$('#levelModal').modal('show');
		$('#levelModal .modal-body button').click(function(){
			$('#levelModal').modal('hide');
		});
		if(ttl == 0){
			contadorMinutos();
			blockTTL();			
		}else if(ttl <= 4){
			contadorMinutos();
		}
		Level();
		star();
	}
	PlayMusic(window.localStorage.getItem('audio'));
});
//funcion para mostrar el las imagenes por idioma
function tutorial(language, juego){	
		if (language == "Eng") {
			$('.titAvisoTuto').text('Hello, welcome to the Memory game, we will explain how to play.');
			$('.tutoBtn').text('Close');
			$('.txtCheck').text('Do not show again');
		}else if (language == "Por"){		
			$('.titAvisoTuto').text('Olá, bem-vindo ao jogo da Memória, vamos explicar como jogar.');
			$('.tutoBtn').text('Fechar');
			$('.txtCheck').text('Não mostrar novamente');
		}else{
			$('.titAvisoTuto').text('Hola, bienvenidos al juego de Memoria, te explicaremos cómo jugar.');
			$('.tutoBtn').text('Cerrar');
			$('.txtCheck').text('No volver a mostrar');			
		}

		$('.img1').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'-'+language+'.jpg');
		$('.img2').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'2-'+language+'.jpg');
		$('.img3').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'3-'+language+'.jpg');
}
//funcion para colocar estrellas en modal win
function star(){
		if (level1 == '3'){				
				document.getElementById("star1level1").src = 'imgs/star.png';
				document.getElementById("star2level1").src = 'imgs/star.png';
				document.getElementById("star3level1").src = 'imgs/star.png';
			}else if(level1 == '2'){
				document.getElementById("star1level1").src = 'imgs/star.png';
				document.getElementById("star2level1").src = 'imgs/star.png';
			}else if(level1 == '1'){
				document.getElementById("star1level1").src = 'imgs/star.png';
		}
		if (level2 == '3'){				
				document.getElementById("star1level2").src = 'imgs/star.png';
				document.getElementById("star2level2").src = 'imgs/star.png';
				document.getElementById("star3level2").src = 'imgs/star.png';
			}else if(level2 == '2'){
				document.getElementById("star1level2").src = 'imgs/star.png';
				document.getElementById("star2level2").src = 'imgs/star.png';
			}else if(level2 == '1'){
				document.getElementById("star1level2").src = 'imgs/star.png';
		}
		if (level3 == '3'){				
				document.getElementById("star1level3").src = 'imgs/star.png';
				document.getElementById("star2level3").src = 'imgs/star.png';
				document.getElementById("star3level3").src = 'imgs/star.png';
			}else if(level3 == '2'){
				document.getElementById("star1level3").src = 'imgs/star.png';
				document.getElementById("star2level3").src = 'imgs/star.png';
			}else if(level3 == '1'){
				document.getElementById("star1level3").src = 'imgs/star.png';
		}
		if (level4 == '3'){				
				document.getElementById("star1level4").src = 'imgs/star.png';
				document.getElementById("star2level4").src = 'imgs/star.png';
				document.getElementById("star3level4").src = 'imgs/star.png';
			}else if(level4 == '2'){
				document.getElementById("star1level4").src = 'imgs/star.png';
				document.getElementById("star2level4").src = 'imgs/star.png';
			}else if(level4 == '1'){
				document.getElementById("star1level4").src = 'imgs/star.png';
		}
		if (level5 == '3'){				
				document.getElementById("star1level5").src = 'imgs/star.png';
				document.getElementById("star2level5").src = 'imgs/star.png';
				document.getElementById("star3level5").src = 'imgs/star.png';
			}else if(level5 == '2'){
				document.getElementById("star1level5").src = 'imgs/star.png';
				document.getElementById("star2level5").src = 'imgs/star.png';
			}else if(level5 == '1'){
				document.getElementById("star1level5").src = 'imgs/star.png';
		}
		if (level6 == '3'){				
				document.getElementById("star1level6").src = 'imgs/star.png';
				document.getElementById("star2level6").src = 'imgs/star.png';
				document.getElementById("star3level6").src = 'imgs/star.png';
			}else if(level6 == '2'){
				document.getElementById("star1level6").src = 'imgs/star.png';
				document.getElementById("star2level6").src = 'imgs/star.png';
			}else if(level6 == '1'){
				document.getElementById("star1level6").src = 'imgs/star.png';
		}
		if (level7 == '3'){				
				document.getElementById("star1level7").src = 'imgs/star.png';
				document.getElementById("star2level7").src = 'imgs/star.png';
				document.getElementById("star3level7").src = 'imgs/star.png';
			}else if(level7 == '2'){
				document.getElementById("star1level7").src = 'imgs/star.png';
				document.getElementById("star2level7").src = 'imgs/star.png';
			}else if(level7 == '1'){
				document.getElementById("star1level7").src = 'imgs/star.png';
		}
		if (level8 == '3'){				
				document.getElementById("star1level8").src = 'imgs/star.png';
				document.getElementById("star2level8").src = 'imgs/star.png';
				document.getElementById("star3level8").src = 'imgs/star.png';
			}else if(level8 == '2'){
				document.getElementById("star1level8").src = 'imgs/star.png';
				document.getElementById("star2level8").src = 'imgs/star.png';
			}else if(level8 == '1'){
				document.getElementById("star1level8").src = 'imgs/star.png';
		}
		if (level9 == '3'){				
				document.getElementById("star1level9").src = 'imgs/star.png';
				document.getElementById("star2level9").src = 'imgs/star.png';
				document.getElementById("star3level9").src = 'imgs/star.png';
			}else if(level9 == '2'){
				document.getElementById("star1level9").src = 'imgs/star.png';
				document.getElementById("star2level9").src = 'imgs/star.png';
			}else if(level9 == '1'){
				document.getElementById("star1level9").src = 'imgs/star.png';
		}
		if (level10 == '3'){				
				document.getElementById("star1level10").src = 'imgs/star.png';
				document.getElementById("star2level10").src = 'imgs/star.png';
				document.getElementById("star3level10").src = 'imgs/star.png';
			}else if(level10 == '2'){
				document.getElementById("star1level10").src = 'imgs/star.png';
				document.getElementById("star2level10").src = 'imgs/star.png';
			}else if(level10 == '1'){
				document.getElementById("star1level10").src = 'imgs/star.png';
		}
		if (level11 == '3'){				
			document.getElementById("star1level11").src = 'imgs/star.png';
			document.getElementById("star2level11").src = 'imgs/star.png';
			document.getElementById("star3level11").src = 'imgs/star.png';
		}else if(level11 == '2'){
			document.getElementById("star1level11").src = 'imgs/star.png';
			document.getElementById("star2level11").src = 'imgs/star.png';
		}else if(level11 == '1'){
			document.getElementById("star1level11").src = 'imgs/star.png';
	}
	if (level12 == '3'){				
			document.getElementById("star1level12").src = 'imgs/star.png';
			document.getElementById("star2level12").src = 'imgs/star.png';
			document.getElementById("star3level12").src = 'imgs/star.png';
		}else if(level12 == '2'){
			document.getElementById("star1level12").src = 'imgs/star.png';
			document.getElementById("star2level12").src = 'imgs/star.png';
		}else if(level12 == '1'){
			document.getElementById("star1level12").src = 'imgs/star.png';
	}
	if (level13 == '3'){				
			document.getElementById("star1level13").src = 'imgs/star.png';
			document.getElementById("star2level13").src = 'imgs/star.png';
			document.getElementById("star3level13").src = 'imgs/star.png';
		}else if(level13 == '2'){
			document.getElementById("star1level13").src = 'imgs/star.png';
			document.getElementById("star2level13").src = 'imgs/star.png';
		}else if(level13 == '1'){
			document.getElementById("star1level13").src = 'imgs/star.png';
	}
	if (level14 == '3'){				
			document.getElementById("star1level14").src = 'imgs/star.png';
			document.getElementById("star2level14").src = 'imgs/star.png';
			document.getElementById("star3level14").src = 'imgs/star.png';
		}else if(level14 == '2'){
			document.getElementById("star1level14").src = 'imgs/star.png';
			document.getElementById("star2level14").src = 'imgs/star.png';
		}else if(level14 == '1'){
			document.getElementById("star1level14").src = 'imgs/star.png';
	}
	if (level15 == '3'){				
			document.getElementById("star1level15").src = 'imgs/star.png';
			document.getElementById("star2level15").src = 'imgs/star.png';
			document.getElementById("star3level15").src = 'imgs/star.png';
		}else if(level15 == '2'){
			document.getElementById("star1level15").src = 'imgs/star.png';
			document.getElementById("star2level15").src = 'imgs/star.png';
		}else if(level15 == '1'){
			document.getElementById("star1level15").src = 'imgs/star.png';
	}
	if (level16 == '3'){				
			document.getElementById("star1level16").src = 'imgs/star.png';
			document.getElementById("star2level16").src = 'imgs/star.png';
			document.getElementById("star3level16").src = 'imgs/star.png';
		}else if(level16 == '2'){
			document.getElementById("star1level16").src = 'imgs/star.png';
			document.getElementById("star2level16").src = 'imgs/star.png';
		}else if(level16 == '1'){
			document.getElementById("star1level16").src = 'imgs/star.png';
	}
	if (level17 == '3'){				
			document.getElementById("star1level17").src = 'imgs/star.png';
			document.getElementById("star2level17").src = 'imgs/star.png';
			document.getElementById("star3level17").src = 'imgs/star.png';
		}else if(level17 == '2'){
			document.getElementById("star1level17").src = 'imgs/star.png';
			document.getElementById("star2level17").src = 'imgs/star.png';
		}else if(level17 == '1'){
			document.getElementById("star1level17").src = 'imgs/star.png';
	}
	if (level18 == '3'){				
			document.getElementById("star1level18").src = 'imgs/star.png';
			document.getElementById("star2level18").src = 'imgs/star.png';
			document.getElementById("star3level18").src = 'imgs/star.png';
		}else if(level18 == '2'){
			document.getElementById("star1level18").src = 'imgs/star.png';
			document.getElementById("star2level18").src = 'imgs/star.png';
		}else if(level18 == '1'){
			document.getElementById("star1level18").src = 'imgs/star.png';
	}
	if (level19 == '3'){				
			document.getElementById("star1level19").src = 'imgs/star.png';
			document.getElementById("star2level19").src = 'imgs/star.png';
			document.getElementById("star3level19").src = 'imgs/star.png';
		}else if(level19 == '2'){
			document.getElementById("star1level19").src = 'imgs/star.png';
			document.getElementById("star2level19").src = 'imgs/star.png';
		}else if(level19 == '1'){
			document.getElementById("star1level19").src = 'imgs/star.png';
	}
	if (level20 == '3'){				
			document.getElementById("star1level20").src = 'imgs/star.png';
			document.getElementById("star2level20").src = 'imgs/star.png';
			document.getElementById("star3level20").src = 'imgs/star.png';
		}else if(level20 == '2'){
			document.getElementById("star1level20").src = 'imgs/star.png';
			document.getElementById("star2level20").src = 'imgs/star.png';
		}else if(level20 == '1'){
			document.getElementById("star1level20").src = 'imgs/star.png';
	}
}	
function PlayMusic(active){	
	//console.log(window.localStorage.setItem('audio', active))
	if (active == "1"){
		audioElement.setAttribute('id', 'sonido');
		audioElement.setAttribute('src', 'sounds/world2.mp3');
    	audioElement.setAttribute('autoplay', 'autoplay');
    	audioElement.addEventListener("ended", function() {
	        this.currentTime = 0;
	        this.play();
	    }, false);
	}
	else {
		audioElement.pause();
		active=0;
	}
}
function EffectMusic(active){
	// var Active= window.localStorage.getItem('audio');
	// if (Active=="1") {
	// 	$('.play').toggleClass("active");
	// 	//active="1"
	// }
	// else{
	// 	$('.stop').toggleClass("active");
	// 	//console.log(active +" stop");
	// }
	//console.log(active);
	window.localStorage.setItem('#sound-acert', active);
	if (active=="1") {
		$('#sound-acert').each(function(){
			this.play();
		});
	}else{
		active=0;
	}

}
function blockTTL(){
	 $('#levelModal').modal('hide');
	 $('#TTLModal').modal('show');
}
function nextLvl(){
	var nextNivel = nivel + 1;
	localStorage.setItem("Next", 'si') 
	localStorage.setItem("nivelNext", nextNivel) 
	location.reload();
}
//Determina el Nivel en que se va a iniciar
function Level(){
	//Nivel  aparecen 4 cartas
	$('#level1').click(function(){
		nivel= 2;
		nivel1 = nivel;
		$('#lvlN').text(1);
		if ($('#cmbTime').val()==''){
		   timeLevel = 30;
		   $('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());		
		StarGame();
		Timer();
   	});
	//Nivel  aparecen 6 cartas x3
	$('#level2').click(function(){
		nivel= 3;
		nivel1 = nivel;
		$('#lvlN').text(2);
		if ($('#cmbTime').val()==''){
		   timeLevel = 40;
		   $('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());	
		StarGame();
		Timer();
   	});
		//Nivel  aparecen 8 cartas
	$('#level3').click(function(){
		 nivel= 4;
		 nivel1 = nivel;
		 $('#lvlN').text(3);
		 if ($('#cmbTime').val()==''){
			timeLevel = 45;
			$('#cmbTime').val(timeLevel);
		 }
		 else
			 timeLevel = parseInt($('#cmbTime').val());	
		 StarGame();
		 Timer();
	});
		//Nivel  aparecen 12 cartas
	$('#level4').click(function(){
		 nivel= 6;
		 nivel2 = nivel;
		 $('#lvlN').text(4);
		 if ($('#cmbTime').val()==''){
			timeLevel = 55;
			$('#cmbTime').val(timeLevel);
		 }
		 else
			 timeLevel = parseInt($('#cmbTime').val());		 
		 StarGame();
		 Timer();
	});
		//Nivel  aparecen 16 cartas
	$('#level5').click(function(){
		 nivel= 8;
		 nivel3 = nivel;
		 $('#lvlN').text(5);
		 if ($('#cmbTime').val()==''){
			timeLevel = 65;
			$('#cmbTime').val(timeLevel);
		 }
		 else
			 timeLevel = parseInt($('#cmbTime').val());
		 StarGame();
		 Timer();
	});
		//Nivel  aparecen 18 cartas x3
	$('#level6').click(function(){
			nivel= 9;
			nivel3 = nivel;
			$('#lvlN').text(6);
			if ($('#cmbTime').val()==''){
			timeLevel = 75;
			$('#cmbTime').val(timeLevel);
			}
			else
				timeLevel = parseInt($('#cmbTime').val());
			StarGame();
			Timer();
	});
			//Nivel  aparecen 20 cartas
	$('#level7').click(function(){
		nivel= 10;
		nivel3 = nivel;
		$('#lvlN').text(7);
		if ($('#cmbTime').val()==''){
		timeLevel = 85;
		$('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());
		StarGame();
		Timer();
	});
			//Nivel  aparecen 24 cartas
	$('#level8').click(function(){
		nivel= 12;
		nivel3 = nivel;
		$('#lvlN').text(8);
		if ($('#cmbTime').val()==''){
		timeLevel = 95;
		$('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());
		StarGame();
		Timer();
	});
			//Nivel  aparecen 28 cartas
	$('#level9').click(function(){
		nivel= 14;
		nivel3 = nivel;
		$('#lvlN').text(9);
		if ($('#cmbTime').val()==''){
		timeLevel = 105;
		$('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());
		StarGame();
		Timer();
	});
			//Nivel  aparecen 30 cartas x3
	$('#level10').click(function(){
		nivel= 15;
		nivel3 = nivel;
		$('#lvlN').text(10);
		if ($('#cmbTime').val()==''){
		timeLevel = 115;
		$('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());
		StarGame();
		Timer();
	});
	//Nivel  aparecen 32 cartas
	$('#level11').click(function(){
		nivel= 16;
		nivel1 = nivel;
		$('#lvlN').text(11);
		if ($('#cmbTime').val()==''){
		   timeLevel = 115;
		   $('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());		
		StarGame();
		Timer();
   	});
	//Nivel  aparecen 36 cartas
	$('#level12').click(function(){
		nivel= 18;
		nivel1 = nivel;
		$('#lvlN').text(12);
		$('#imagenes').addClass('overFlw');
		if ($('#cmbTime').val()==''){
		   timeLevel = 120;
		   $('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());	
		StarGame();
		Timer();
   	});
		//Nivel  aparecen 40 cartas
	$('#level13').click(function(){
		 nivel= 20;
		 nivel1 = nivel;
		 $('#lvlN').text(13);
		 $('#imagenes').addClass('overFlw');
		 if ($('#cmbTime').val()==''){
			timeLevel = 120;
			$('#cmbTime').val(timeLevel);
		 }
		 else
			 timeLevel = parseInt($('#cmbTime').val());	
		 StarGame();
		 Timer();
	});
}
function titleLvl(){
	$('#lvltxt').addClass('animated slideInRight');
	$('#lvltxt').removeAttr('style');
	setTimeout(function() { 
		$('#lvltxt').removeClass('animated slideInRight');
		$('#lvltxt').addClass('animated slideOutLeft');
	}, 1500);
}
//Finaliza el juego al completar los puntos por nivel
function EndGame(seconds_left){
  	// Timer();
  	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 10
	if (nivel== 2){
		if(punto== 10){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>13) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level1', '3'); 
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=13) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level1 <= 1){
			  console.log('entro')
			  window.localStorage.setItem('level1', '2'); 
		  }
		} else if ($('#contratiempo').text()>5 && $('#contratiempo').text()<=10) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level1 == 0){
			  window.localStorage.setItem('level1', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=5) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
  	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 15
	if (nivel== 3){
		if(punto== 15){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>20) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level2', '3'); 
		} else if ($('#contratiempo').text()>15 && $('#contratiempo').text()<=20) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level2 <= 1){
			  window.localStorage.setItem('level2', '2'); 
		  }
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=15) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level2 == 0){
			  window.localStorage.setItem('level2', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 20
	if (nivel== 4){
	  if(punto== 20){
	  $('#WinModal').modal('show');
      // clearInterval(interval);
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>30) {
          punto = punto + 100;
        	$('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level3', '3'); 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 60;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level3 <= 1){
			window.localStorage.setItem('level3', '2'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
          punto = punto + 40;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level3 == 0){
			window.localStorage.setItem('level3', '1'); 
		}
      } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
			$('.punto').text(punto);
			contCoin(5);
			count(5);
			$('#btnNext').css({'display':'none'})
      }

			fin=true;

			ShareScore();
			contStar();
	    }
	}
	//Si el nivel seleccionado es dos la cantidad de puntos para ganar sera de 25
	if(nivel== 5){
		if(punto== 25){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>30) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level4', '3'); 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 100;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level4 <= 1){
			window.localStorage.setItem('level4', '2'); 
		}
      } else if ($('#contratiempo').text()>15 && $('#contratiempo').text()<=20) {
          punto = punto + 80;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level4 == 0){
			window.localStorage.setItem('level4', '1'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=15) {
          punto = punto + 60;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level4 == 0){
			window.localStorage.setItem('level4', '1'); 
		}
      } else if ($('#contratiempo').text()>5 && $('#contratiempo').text()<=10) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=5) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;

			ShareScore();
			contStar();
			// Timer();
		}
	}
	//Si el nivel seleccionado es tres la cantidad de puntos para ganar sera de 30
	if(nivel== 6){
		if(punto==30){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>30) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level5', '3'); 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 150;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level5', '3'); 
      } else if ($('#contratiempo').text()>15 && $('#contratiempo').text()<=20) {
          punto = punto + 120;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(level5 <= 1){
				window.localStorage.setItem('level5', '2'); 
			}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=15) {
          punto = punto + 60;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(level5 == 0){
				window.localStorage.setItem('level5', '1'); 
			} 
      } else if ($('#contratiempo').text()>5 && $('#contratiempo').text()<=15) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=5) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;
			// Timer();

			ShareScore();
			contStar();
		}
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 35
	if (nivel== 7){
		if(punto== 35){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>30) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level6', '3'); 
		} else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level6 <= 1){
			  window.localStorage.setItem('level6', '2'); 
		  }
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level6 == 0){
			  window.localStorage.setItem('level6', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
  	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 40
	if (nivel== 8){
		if(punto== 40){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>40) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level7', '3'); 
		} else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level7 <= 1){
			  window.localStorage.setItem('level7', '2'); 
		  }
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=30) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level7 == 0){
			  window.localStorage.setItem('level7', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 45
	if (nivel== 9){
	  if(punto== 45){
	  $('#WinModal').modal('show');
      // clearInterval(interval);
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>40) {
          punto = punto + 100;
        	$('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level8', '3'); 
      } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
          punto = punto + 60;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level8 <= 1){
			window.localStorage.setItem('level8', '2'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=30) {
          punto = punto + 40;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level8 == 0){
			window.localStorage.setItem('level8', '1'); 
		}
      } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
			$('.punto').text(punto);
			contCoin(5);
			count(5);
			$('#btnNext').css({'display':'none'})
      }

			fin=true;

			ShareScore();
			contStar();
	    }
	}
	//Si el nivel seleccionado es dos la cantidad de puntos para ganar sera de 50
	if(nivel== 10){
		if(punto== 50){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>50) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level9', '3'); 
      } else if ($('#contratiempo').text()>40 && $('#contratiempo').text()<=50) {
          punto = punto + 100;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level9 <= 1){
			window.localStorage.setItem('level9', '2'); 
		}
      } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
          punto = punto + 80;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level9 == 0){
			window.localStorage.setItem('level9', '1'); 
		}
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 60;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level9 == 0){
			window.localStorage.setItem('level9', '1'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;

			ShareScore();
			contStar();
			// Timer();
		}
	}
	//Si el nivel seleccionado es tres la cantidad de puntos para ganar sera de 55
	if(nivel== 11){
		if(punto==55){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>80) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level10', '3'); 
      } else if ($('#contratiempo').text()>70 && $('#contratiempo').text()<=80) {
          punto = punto + 150;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level10', '3'); 
      } else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=70) {
          punto = punto + 120;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(level10 <= 1){
				window.localStorage.setItem('level10', '2'); 
			}
      } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
          punto = punto + 100;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(level10 <= 1){
				window.localStorage.setItem('level10', '2'); 
			}
      } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
          punto = punto + 80;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(level10 == 0){
				window.localStorage.setItem('level10', '1'); 
			} 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 60;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(level10 == 0){
				window.localStorage.setItem('level10', '1'); 
			} 
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;
			// Timer();

			ShareScore();
			contStar();
		}
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 60
	if (nivel== 12){
		if(punto== 60){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>13) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level11', '3'); 
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=13) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level11 <= 1){
			  console.log('entro')
			  window.localStorage.setItem('level11', '2'); 
		  }
		} else if ($('#contratiempo').text()>5 && $('#contratiempo').text()<=10) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level11 == 0){
			  window.localStorage.setItem('level11', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=5) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
  	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 65
	if (nivel== 13){
		if(punto== 65){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>20) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level12', '3'); 
		} else if ($('#contratiempo').text()>15 && $('#contratiempo').text()<=20) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level12 <= 1){
			  window.localStorage.setItem('level12', '2'); 
		  }
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=15) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level12 == 0){
			  window.localStorage.setItem('level12', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 70
	if (nivel== 14){
	  if(punto== 70){
	  $('#WinModal').modal('show');
      // clearInterval(interval);
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>30) {
          punto = punto + 100;
        	$('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level13', '3'); 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 60;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level13 <= 1){
			window.localStorage.setItem('level13', '2'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
          punto = punto + 40;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level13 == 0){
			window.localStorage.setItem('level13', '1'); 
		}
      } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
			$('.punto').text(punto);
			contCoin(5);
			count(5);
			$('#btnNext').css({'display':'none'})
      }

			fin=true;

			ShareScore();
			contStar();
	    }
	}
	//Si el nivel seleccionado es dos la cantidad de puntos para ganar sera de 75
	if(nivel== 15){
		if(punto== 75){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>30) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level14', '3'); 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 100;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level14 <= 1){
			window.localStorage.setItem('level14', '2'); 
		}
      } else if ($('#contratiempo').text()>15 && $('#contratiempo').text()<=20) {
          punto = punto + 80;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level14 == 0){
			window.localStorage.setItem('level14', '1'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=15) {
          punto = punto + 60;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level14 == 0){
			window.localStorage.setItem('level14', '1'); 
		}
      } else if ($('#contratiempo').text()>5 && $('#contratiempo').text()<=10) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=5) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;

			ShareScore();
			contStar();
			// Timer();
		}
	}
	//Si el nivel seleccionado es tres la cantidad de puntos para ganar sera de 80
	if(nivel== 16){
		if(punto==80){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>30) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level15', '3'); 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 150;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level15', '3'); 
      } else if ($('#contratiempo').text()>15 && $('#contratiempo').text()<=20) {
          punto = punto + 120;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(level15 <= 1){
				window.localStorage.setItem('level15', '2'); 
			}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=15) {
          punto = punto + 60;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(level15 == 0){
				window.localStorage.setItem('level15', '1'); 
			} 
      } else if ($('#contratiempo').text()>5 && $('#contratiempo').text()<=15) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=5) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;
			// Timer();

			ShareScore();
			contStar();
		}
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 85
	if (nivel== 17){
		if(punto== 85){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>30) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level16', '3'); 
		} else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level16 <= 1){
			  window.localStorage.setItem('level16', '2'); 
		  }
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level16 == 0){
			  window.localStorage.setItem('level16', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
  	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 90
	if (nivel== 18){
		if(punto== 90){
		$('#WinModal').modal('show');
		// clearInterval(interval);
		// se suman puntos segun el tiempo que tarde para ganar
		if ($('#contratiempo').text()>40) {
			punto = punto + 100;
			  $('.punto').text(punto);// Imprimer en la cantidad de puntos
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.winstar3').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(25);
		  count(25);
		  window.localStorage.setItem('level17', '3'); 
		} else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
			punto = punto + 60;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.winstar2').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(20);
		  count(20);
		  if(level17 <= 1){
			  window.localStorage.setItem('level17', '2'); 
		  }
		} else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=30) {
			punto = punto + 40;
			  $('.punto').text(punto);
		  // estrellas// 
		  $('.winstar1').css({'display':'block'});
		  $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		  contCoin(15);
		  count(15);
		  if(level17 == 0){
			  window.localStorage.setItem('level17', '1'); 
		  }
		} else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
			punto = punto + 20;
			  $('.punto').text(punto);
			  contCoin(5);
			  count(5);
			  $('#btnNext').css({'display':'none'})
		}
  
			  fin=true;
  
			  ShareScore();
			  contStar();
		  }
	}
	//Si el nivel seleccionado es uno la cantidad de puntos para ganar sera de 95
	if (nivel== 19){
	  if(punto== 95){
	  $('#WinModal').modal('show');
      // clearInterval(interval);
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>40) {
          punto = punto + 100;
        	$('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level18', '3'); 
      } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
          punto = punto + 60;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level18 <= 1){
			window.localStorage.setItem('level18', '2'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=30) {
          punto = punto + 40;
        	$('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level18 == 0){
			window.localStorage.setItem('level18', '1'); 
		}
      } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
			$('.punto').text(punto);
			contCoin(5);
			count(5);
			$('#btnNext').css({'display':'none'})
      }

			fin=true;

			ShareScore();
			contStar();
	    }
	}
	//Si el nivel seleccionado es dos la cantidad de puntos para ganar sera de 100
	if(nivel== 20){
		if(punto== 100){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>50) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.winstar3').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(25);
		count(25);
		window.localStorage.setItem('level19', '3'); 
      } else if ($('#contratiempo').text()>40 && $('#contratiempo').text()<=50) {
          punto = punto + 100;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.winstar2').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(20);
		count(20);
		if(level19 <= 1){
			window.localStorage.setItem('level19', '2'); 
		}
      } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
          punto = punto + 80;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level19 == 0){
			window.localStorage.setItem('level19', '1'); 
		}
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 60;
          $('.punto').text(punto);
		// estrellas// 
		$('.winstar1').css({'display':'block'});
		$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
		contCoin(15);
		count(15);
		if(level19 == 0){
			window.localStorage.setItem('level19', '1'); 
		}
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})

        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;

			ShareScore();
			contStar();
			// Timer();
		}
	}
	//Si el nivel seleccionado es tres la cantidad de puntos para ganar sera de 105
	if(nivel== 21){
		if(punto==105){
			$('#WinModal').modal('show');
      // se suman puntos segun el tiempo que tarde para ganar
      if ($('#contratiempo').text()>80) {
          punto = punto + 200;
          $('.punto').text(punto);// Imprimer en la cantidad de puntos
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level20', '3'); 
      } else if ($('#contratiempo').text()>70 && $('#contratiempo').text()<=80) {
          punto = punto + 150;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('level20', '3'); 
      } else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=70) {
          punto = punto + 120;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(level20 <= 1){
				window.localStorage.setItem('level20', '2'); 
			}
      } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
          punto = punto + 100;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(level20 <= 1){
				window.localStorage.setItem('level20', '2'); 
			}
      } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
          punto = punto + 80;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(level20 == 0){
				window.localStorage.setItem('level20', '1'); 
			} 
      } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
          punto = punto + 60;
          $('.punto').text(punto);
			// estrellas// 
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(level20 == 0){
				window.localStorage.setItem('level20', '1'); 
			} 
      } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
          punto = punto + 40;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
        } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
          punto = punto + 20;
		  $('.punto').text(punto);
		  contCoin(5);
		  count(5);
		  $('#btnNext').css({'display':'none'})
      }
			fin=true;
			// Timer();

			ShareScore();
			contStar();
		}
	}
	
}
//sumatoria de estrellas de los level
function contStar(){
	var level1 = Number(localStorage.getItem('level1')); 
	var level2 = Number(localStorage.getItem('level2')); 
	var level3 = Number(localStorage.getItem('level3')); 
	var level4 = Number(localStorage.getItem('level4'));
	var level5 = Number(localStorage.getItem('level5')); 
	var level6 = Number(localStorage.getItem('level6')); 
	var level7 = Number(localStorage.getItem('level7')); 
	var level8 = Number(localStorage.getItem('level8')); 
	var level9 = Number(localStorage.getItem('level9'));
	var level10 = Number(localStorage.getItem('level10'));
	var level11 = Number(localStorage.getItem('level11'));
	var level12 = Number(localStorage.getItem('level12'));
	var level13 = Number(localStorage.getItem('level13'));
	var level14 = Number(localStorage.getItem('level14'));
	var level15 = Number(localStorage.getItem('level15'));
	var level16 = Number(localStorage.getItem('level16'));
	var level17 = Number(localStorage.getItem('level17'));
	var level18 = Number(localStorage.getItem('level18'));
	var level19 = Number(localStorage.getItem('level19'));
	var level20 = Number(localStorage.getItem('level20'));
	ContStar = level1 + level2 + level3 + level4 + level5 + level6 + level7 + level8 + level9 + level10 + level11 + level12 + level13 + level14 + level15 + level16 + level17 + level18 + level19 + level20;
	// console.log('starEasy =', starEasy );
	// console.log('starMid =', starMid );
	// console.log('starHard =', starHard );
	// console.log('sumando contador=', ContStar)
	window.localStorage.setItem('contStarMemoria', ContStar); 
	$('#contratiempo').css({
		'color':'transparent'
	})
}
//sumar coin
function contCoin(n){
	var sumaCoin= n + coin
	window.localStorage.setItem('coin', sumaCoin); 
}
//efecto contador
function count(n){
	var counter = { var: 0 };
	var contador = TweenMax.to(counter, 4, {
	  var: n, 
	  onUpdate: function () {
		var number = Math.ceil(counter.var);
		$('#GetCoin').html(number);
		if(number === counter.var){ 
			contador.kill(); 
		}
	  },
	  ease:Circ.easeOut
	});
  }
//Crea los campos donde estaran contenidos las cartas, que se muestran de forma aleatoria en funcion a las preguntas y respuestas
function StarGame(){
	var carts =[];
	var NumCards= nivel;
	$('#ttl').text(ttl);
	$('#coin').text(coin);
	GetRandomNumber(NumCards);
	for (var i = 0; i < arrayOrder.length; i++){
		valor =  randomDeHasta(0, arrayOrder.length - 1);
		$('#imagenes').append('<div id="contQuestion-'+ arrayOrder[i] +'" class="caja " onclick="comparar(this.id)"><span class="front-face"><img src="imgs/logo_carta.png"></span><p id="question-'+arrayOrder[i]+'" class="caja2 animated flipInY"></p></div>')
		$('#imagenes').append('<div id="contAnswer-'+ arrayOrder[i]+'" class="caja " onclick="comparar(this.id)"><span class="front-face"><img src="imgs/logo_carta.png"><audio id="sound-acert" controls preload="auto"><source src="sounds/explodeGood.mp3" controls></source></audio></span><p id="answer-'+arrayOrder[i]+'" class="caja2 animated flipInY"></p></div>')
		$('#answer-' + arrayOrder[i]).text(answerArray[arrayOrder[i]]);
		$('#question-' + arrayOrder[i]).text(questionArray[arrayOrder[i]]);
	mezclar();
	}
	titleLvl();
	if(nivel==3 || nivel==9 || nivel==15){
		var xn=0
		var intervalAjuste = setInterval(function(){
			$('#imagenes').addClass('boxImg')
			$('.caja').addClass('cajax3')
			if(xn==2){
				clearInterval(intervalAjuste);
			}else{
				xn++
			}
		},200)
	}
	if(nivel>=9){
		$('#imagenes').css({'bottom':'unset'})		
	}
}
//Conteo regresivo segun el nivel seleccionado
function Timer(){
  //Conteo Regresivo
  var seconds_left = timeLevel;
  // var minutes_left = 0;

  var interval = setInterval(function() {

  document.getElementById('contratiempo').innerHTML =  --seconds_left;
  
//   if (document.getElementById('contratiempo').innerHTML == 1 ) {
// 	$('#lvltxt').css({'display':'none'}); 
// 	$('#ofw').removeClass('overFw');  
// 	$('.front-face').removeClass('animated flipInY ');  
// 	$('.caja2').removeClass('animated flipInY flipp');
// 	$('.caja').removeClass('animated flipInY ');  
// 	$('.retry').css({'display':'none'});
// 	const
// 	$objetivo =  document.body, // A qué le tomamos la foto
// 	$contenedorCanvas = document.querySelector("#contenedorCanvas"); // En dónde ponemos el elemento canvas

// 	html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
// 		.then(function(canvas) {
// 		// Cuando se resuelva la promesa traerá el canvas
// 		$contenedorCanvas.appendChild(canvas); // Lo agregamos como hijo del div
// 		});
	
// 	// console.log("tomada la foto");
//   }
  if (fin==false) {
  	  EndGame(seconds_left);
	  if (seconds_left <= 0){
	    //document.getElementById('contratiempo').innerHTML = "Se acabo el tiempo";
	    clearInterval(interval);
	   //Envia mensaje y reinicia la partida
	   //alert("Game Over!");
	   $('#LooseModal').modal('show')
	   //location.reload();
		ShareScore();		
		ttl--;
		window.localStorage.setItem('ttl', ttl); 
		// console.log(ttl)
	  }
	}
  // return interval;
  }, 1000);

}
// compartir puntaje del juego en facebook o twitter
function ShareScore(){

	var level=0;

	switch(nivel) {
	    case 6:

			level=1;
			break;
		case 8:

			level=2;

			break;

		case 12:
			        
			level=3;

			break;
	}


	if (localStorage.getItem("UserId")!=null){
		var infogame = { "UserId":localStorage.getItem("UserId"), "GameId":4, "TopicId" :3, "levelId" :level,"Score":punto};

		RegisterGame(infogame);
	}

	public_FB();


}
//Funcion para generar un rango aleatorio desde un punto a otro
function randomDeHasta(de, hasta){
    return Math.floor(Math.random() * (hasta - de + 1) + de);
}
//Una vez Creados las cajas con sus cartas de preguntas y respuestas, se mezclan para dar una posicion aleatoria
function mezclar() {
    var imagenes = $("#imagenes").children();
    var boxContent = $("#imagenes div:first-child");
    var newOrder = new Array();

    for (i=0; i<imagenes.length; i++) {
        newOrder[i] = $("#"+boxContent.attr("id") +" p").text();
        boxContent = boxContent.next();
    }
    var boxContent = $("#imagenes div:first-child");
    for (z=0; z<imagenes.length; z++) {
        randIndex = randomDeHasta(0, newOrder.length - 1);
        $("#"+boxContent.attr("id") +" p").text(newOrder[randIndex]);
        newOrder.splice(randIndex, 1);
        boxContent = boxContent.next();
    }
}
//Agregar un numero aleatorio al arreglo arrayOrder
function GetRandomNumber(cardsNum){
	//Si el arreglo tiene tantasposiciones como cartas se mostraran no se crea un nuevo numero
	if (arrayOrder.length < cardsNum){
		var value = Math.floor(Math.random()*31);

		//Si el arreglo esta vacio se agrega el numero
		if (arrayOrder.length == 0){
			arrayOrder.push(value);
		}
		else {
			var exist = false;

			//Recorrer el arreglo arrayOrder para validar si el numero aleatorio ya existe en el arreglo
			for (var i = 0; i < arrayOrder.length; i++){
				//Validar si el numero aleatorio existe en el arreglo
				if (arrayOrder[i] == value){
					exist = true;
				}
			}
			//Si el numero no existe se agrega al arreglo
			if (exist == false){
				arrayOrder.push(value);
			}
		}
		//Se llama a la funcion recursiva para obtener un nuevo numero aleatorio
		GetRandomNumber(cardsNum);
	}
}
//ver todas las cartar por 5 segundos
function verCartas(){
	 var coinModal = Number(window.localStorage.getItem('coin'));
	console.log('ver cartas')
	 if(coinModal >= 50){
	 	var sumaCoin = coinModal - 50
	 	window.localStorage.setItem('coin', sumaCoin); 
	 	$('#coin').text(sumaCoin);
		 $(".front-face").addClass('front-flip');
		 $('.caja2').addClass("flipp");
		 $('.pantBlock').css({'display':'block'});
	 	setTimeout(function() {
			$(".front-face").removeClass('front-flip');
			$('.caja2').removeClass("flipp");
			$('.pantBlock').css({'display':'none'});
	 	}, 5000);
	 }else{		
		$('.titAviso').html(noCoin)
	 	ModalAviso();
	 }
}
//abrir Modal
function ModalAviso(n){    
    if (Onanimated == 'true'){
        document.getElementById("ModalAviso").className ="mostrar animated fadeIn"; 
		document.getElementById("BoxAviso").className ="relative animated bounceIn"; 
		document.getElementById("BoxTuto").className ="relative animated bounceIn"; 
    }else{
        document.getElementById("ModalAviso").className ="ModalAviso mostrar "; 
		document.getElementById("BoxAviso").className ="BoxAviso relative "; 
		document.getElementById("BoxTuto").className ="BoxAviso relative "; 
	}
	if(n !=''){
		$('.nv').text(n);
	}
}
//cerrar modal
function cerrarModal(){
    if (Onanimated == 'true'){
		document.getElementById("BoxAviso").className ="relative animated zoomOut" ;
		document.getElementById("BoxTuto").className ="relative animated zoomOut" ;
      setTimeout(function() { 
        document.getElementById("ModalAviso").className ="";
     }, 500);
    }else{
		document.getElementById("ModalAviso").className ="";
    }
}
//desbloquea los botones memoria
function blockMemoria(){
    if(level1 >= 1){
        $("#2").css({'display':'none'});
        $('#level2').removeClass('btnDisable');        
    }
    if(level2 >= 1){
        $("#3").css({'display':'none'});
        $('#level3').removeClass('btnDisable');
    }
    if(level3 >= 1){
        $("#4").css({'display':'none'});
        $('#level4').removeClass('btnDisable');
    }
    if(level4 >= 1){
        $("#5").css({'display':'none'});
        $('#level5').removeClass('btnDisable');
    }
    if(level5 >= 1){
        $("#6").css({'display':'none'});
        $('#level6').removeClass('btnDisable');
    }
    if(level6 >= 1){
        $("#7").css({'display':'none'});
        $('#level7').removeClass('btnDisable');
    }
    if(level7 >= 1){
        $("#8").css({'display':'none'});
        $('#level8').removeClass('btnDisable');
    }
    if(level8 >= 1){
        $("#9").css({'display':'none'});
        $('#level9').removeClass('btnDisable');
    }
    if(level9 >= 1){
        $("#10").css({'display':'none'});
        $('#level10').removeClass('btnDisable');
    }    
}
var firstClick = "";
var firstCard ="";
var pos =0;
var punto= 0;
var contCarts= 0;
//Validacion de las preguntas y respuestas acertadas
function comparar(id){
	contCarts++;
	console.log(contCarts);
	//Indica la accion al hacer el primer click
	if(contCarts<3){
		if (firstClick==""){
			firstClick= $("#" + id).children()[1].innerText;//Ubicacion de
			firstCard= $("#" + id).children()[1];
			//console.log($("#" + id).find('.front-face'));
			$("#" + id).find('.front-face').addClass('front-flip');
			$(firstCard).addClass("flipp");
			isQuestion= true;
			for(i=0; i<answerArray.length; i++) {
				if(answerArray[i]==firstClick){
					isQuestion= false;
					pos= i;
				}
				if (questionArray[i]==firstClick) {
					isQuestion= true;
					pos= i;
				}
			}

		}
		else{//Determina si la primera carta es una Pregunta
			//Abre la segunda carta, la compara con la primera, suma 5 puntos al score y da un efecto de desvanecer las cartas
			secondClick= $("#" + id).children()[1].innerText;
			secondCard = $("#" + id).children()[1];
			if(isQuestion== false){
				//Cartas acertadas
				if(questionArray[pos] == secondClick){
					punto= punto + 5;
					console.log('id=',id)
					$("#" + id).find('.front-face').addClass('front-flip');
					$(firstCard).addClass("flipp");
					$(secondCard).addClass('flipp');
					// $('#sound-acert').each(function(){
					// 	this.play();
					// });
					//EffectMusic();
					EffectMusic(window.localStorage.getItem('#sound-acert'));
					setTimeout(function() {

							contCarts=0;
						$("#" + id).addClass("ocultar",{duration:1000});
						$("#" + id).find('.front-face').addClass('front-flip');
						$(firstCard.parentNode).addClass("ocultar",{duration:1000});
								firstClick = "";
								firstCard="";
						$("#" + id).find('.front-face').removeClass('animated flipInY'); 
						$("#" + id).find('.caja2').removeClass('animated flipInY flipp');  
						$("#" + id).find('.caja').removeClass('animated flipInY');		
						EndGame();
					}, 1000);
				}
				else{//Si la pareja de cartas abiertas no es un acierto, se cierran ambas
					if($('#imagenes div').children().hasClass('flipp')){
							$(secondCard).addClass('flipp');
							$("#" + id).find('.front-face').addClass('front-flip animated flipInY');
					    setTimeout(function() {

							contCarts=0;
					    	$('.front-face').removeClass('front-flip');
					     $('#imagenes div').children().removeClass("flipp");
					     	firstClick = "";
							firstCard="";
					    }, 1000);

					}
				}
			}
			else{//Determina si la primera carta es una Respuesta
				//Abre la segunda carta, la compara con la primera, suma 5 puntos al score y da un efecto de desvanecer las cartas
				if(answerArray[pos] == secondClick){
					punto= punto + 5;
					$("#" + id).find('.front-face').addClass('front-flip animated flipInY');
					$(firstCard).addClass("flipp");
					$(secondCard).addClass('flipp');
					// $('#sound-acert').each(function(){
					// 	this.play();
					// });
					//EffectMusic();
					EffectMusic(window.localStorage.getItem('#sound-acert'));
					setTimeout(function() {

							contCarts=0;
						$("#" + id).addClass("ocultar",{duration:1000});
						$(firstCard.parentNode).addClass("ocultar",{duration:1000});
						firstClick = "";
						firstCard="";
						$("#" + id).find('.front-face').removeClass('animated flipInY'); 
						$("#" + id).find('.caja2').removeClass('animated flipInY flipp');  
						$("#" + id).find('.caja').removeClass('animated flipInY');
						EndGame();
					}, 1000);
				}
				else{//Si la pareja de cartas abiertas no es un acierto, se cierran ambas
					if($('#imagenes div').children().hasClass('flipp')){
							$("#" + id).find('.front-face').addClass('front-flip animated flipInY');
							$(secondCard).addClass("flipp");
					    setTimeout(function() {

							contCarts=0;
					    	$('.front-face').removeClass('front-flip');
					    	$('#imagenes div').children().removeClass("flipp");
					    	firstClick = "";
							firstCard="";
					    }, 1000);

					}
				}
			}
			$('#punto').text(punto);// Imprimer en la cantidad de puntos
			$('.punto').text(punto);// Imprimer en la cantidad de puntos
		 	pos =0;
		}
	}
}


function public_TW(){
	console.log("entro en twitter");
	var level='';

	switch(nivel) {
	    case 6:

			level='EASY';
			break;
		case 8:

			level='MEDIUM';

			break;

		case 12:
			        
			level='HARD';

			break;
	}

	var msj="GAME: MEMORIA  TOPIC:IPv4  NEVEL: "+level+" POINTS: "+punto;

	var src='https://angelgame.acostasite.com/Game/img/memory_img.png';

	window.open("https://twitter.com/share?text="+msj+"&url=https://angelgame.acostasite.com/Game/juegos/Memoria/index-es.html");


}


function public_FB(){
	var level='';

	switch(nivel) {
	    case 6:

			level='EASY';
			break;
		case 8:

			level='MEDIUM';

			break;

		case 12:
			        
			level='HARD';

			break;
	}
	
	var msj="GAME: MEMORIA  TOPIC:IPv4  NEVEL: "+level+" POINTS: "+punto;

	$(".fb-xfbml-parse-ignore").attr("href","https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Fjuegos%2FMemoria%2Findex.html?description="+msj+"&method=1&amp;src=sdkpreparse");
}

function reload(){
	localStorage.setItem("Next", 'no'); 
	location.reload();
}
var dataInstag;
function public_IG(){
	console.log("entro en instagram");
	var level='';
	switch(nivel) {
	case 6:

		level='EASY';
		break;
	case 8:

		level='MEDIUM';

		break;

	case 12:
				
		level='HARD';

		break;
}
	var msj="GAME: MEMORIA  TOPIC:IPv4  NEVEL: "+level+" POINTS: "+punto;
	/*window.plugins.socialsharing.shareViaInstagram(
		'Message via Instagram', 
		'https://angelgame.acostasite.com/images/icon/icon.png','https://angelgame.acostasite.com'
	  );	*/
	 // var assetLocalIdentifier = "../../img/congratulations.png";
	 Instagram.isInstalled(function (err, installed) {
		if (installed) {
			console.log("Instagram is"+ installed); // installed app version on Android
			navigator.screenshot.save(function(error,response){
				if(error){
					console.error(error);
					return;
				}
				
				// Something like: /storage/emulated/0/Pictures/screenshot_1477924039236.jpg
				console.log(response.filePath);
		
				/*Instagram.shareAsset(function(result) {
					alert('Instagram.shareAsset success: ' + result);
				}, function(e) {
					alert('Instagram.shareAsset error: ' + e);
				}, response.filePath);*/
				getBase64FromImageUrl(response.filePath, msj);
				
			});
		} else {
			alert("Instagram no esta instalado");
		}
	});

	
	 

/*	module.controller('ThisCtrl', function($scope, $cordovaInstagram) {
		// Get image from camera, base64 is good. See the
		// $cordovaCamera docs for more info
	  
		$cordovaInstagram.share($scope.image.data, $scope.image.caption).then(function() {
		  // Worked
		}, function(err) {
		  // Didn't work
		});
	  })*/
  }

  function getBase64FromImageUrl(url, msj) {
	var img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.width =this.width;
		canvas.height =this.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(this, 0, 0);

		var dataURL = canvas.toDataURL("image/png");
		dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		
		Instagram.share(dataInstag, msj, function (err) {
			if (err) {
				console.log("Not shared");
			} else {
				console.log("shared");
			}
		});
	};

	img.src = url;
}

function openResult(evt, tabla) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
	  tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
	  tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(tabla).style.display = "block";
	evt.currentTarget.className += " active";
  }

