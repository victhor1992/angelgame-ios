var contAllStarDrag;
var contAllStarCruci = Number(localStorage.getItem('contAllStarCruci'));
var contStarTrivia = Number(localStorage.getItem('contStarTrivia'));
var contStarMemoria = Number(localStorage.getItem('contStarMemoria'));
var ContStarConcepto = Number(localStorage.getItem('contStarconcepto'));
var Onanimated = localStorage.getItem('Animated');
var level1 = Number(localStorage.getItem('level1')); 
var level2 = Number(localStorage.getItem('level2')); 
var level3 = Number(localStorage.getItem('level3')); 
var level4 = Number(localStorage.getItem('level4'));
var level5 = Number(localStorage.getItem('level5')); 
var level6 = Number(localStorage.getItem('level6')); 
var level7 = Number(localStorage.getItem('level7')); 
var level8 = Number(localStorage.getItem('level8')); 
var level9 = Number(localStorage.getItem('level9'));
var level10 = Number(localStorage.getItem('level10'));
var text1
var text2
var Aceptar
var language = localStorage.getItem('selectedLang');

var contStarDragEternet = Number(localStorage.getItem('contStarDragEternet')); 
var contStarDragIcmpv6 = Number(localStorage.getItem('contStarDragIcmpv6')); 
var contStarDragIpv4 = Number(localStorage.getItem('contStarDragIpv4')); 
var contStarDragIpv4Build = Number(localStorage.getItem('contStarDragIpv4Build'));
var contStarDragIpv6 = Number(localStorage.getItem('contStarDragIpv6')); 
var contStarDragIpv6Build = Number(localStorage.getItem('contStarDragIpv6Build')); 
var contStarDragTcp = Number(localStorage.getItem('contStarDragTcp')); 
var contStarDragUdp = Number(localStorage.getItem('contStarDragUdp')); 
var contStarDragUdpHeader = Number(localStorage.getItem('contStarDragUdpHeader'));

//abrir Modal
function ModalAviso(n){    
    if (Onanimated == 'true'){
        document.getElementById("ModalAviso").className ="mostrar animated fadeIn"; 
        document.getElementById("BoxAviso").className ="relative animated bounceIn"; 
    }else{
        document.getElementById("ModalAviso").className ="ModalAviso mostrar "; 
        document.getElementById("BoxAviso").className ="BoxAviso relative "; 
    }
    cantStarDrag(n);
    cantStarConcepts(n);
}
//cerrar modal
function cerrarModal(){
    if (Onanimated == 'true'){
        document.getElementById("BoxAviso").className ="relative animated zoomOut" ;
      setTimeout(function() { 
        document.getElementById("ModalAviso").className ="";
     }, 500);
    }else{
        document.getElementById("ModalAviso").className ="";
    }
}
//colocar la cantidad de estrellas para cada nivel block

function avisoModal(n){
    blockDrag()    
    ModalAviso(n)
}
//funciones para juego drag
//escribe la cantidad de estrellas en el modal
function cantStarDrag(n){
    var contAllStarDrag = contStarDragEternet + contStarDragIcmpv6 + contStarDragIpv4 + contStarDragIpv4Build + contStarDragIpv6 + contStarDragIpv6Build + contStarDragTcp + contStarDragUdp + contStarDragUdpHeader
    if(n == 5){
        $("#cant").text('20');
        $("#cant2").text('20');
        $("#cantTotal").text(contAllStarDrag);
    }
    if(n == 6){
        $("#cant").text('25');
        $("#cant2").text('25');
        $("#cantTotal").text(contAllStarDrag);
    }
    if(n == 7){
        $("#cant").text('45');
        $("#cant2").text('45');
        $("#cantTotal").text(contAllStarDrag);
    }
    if(n == 8){
        $("#cant").text('55');
        $("#cant2").text('55');
        $("#cantTotal").text(contAllStarDrag);
    }
    if(n == 9){
        $("#cant").text('65');
        $("#cant2").text('65');
        $("#cantTotal").text(contAllStarDrag);
    }    
}
//desbloquea los botones 
function blockDrag(){       
    var contStarDrag =Number( window.localStorage.getItem('contAllStarDrag')); 
    console.log(contStarDrag) 
    if(contStarDrag >= 20){
        $("#5").css({'display':'none'});
        $('#udpHeader').removeClass('btnDisable');
        $('.bnt5').css({'display':'none'})
    }
    if(contStarDrag >= 25){
        $("#6").css({'display':'none'});
        $('#ICMPv6').removeClass('btnDisable');
        $('.bnt6').css({'display':'none'})
    }
    if(contStarDrag >= 45){
        $("#7").css({'display':'none'});
        $('#ipv4Build').removeClass('btnDisable');
        $('.bnt7').css({'display':'none'})
    }
    if(contStarDrag >= 55){
        $("#8").css({'display':'none'});
        $('#ipv6Build').removeClass('btnDisable');
        $('.bnt8').css({'display':'none'})
    }
    if(contStarDrag >= 65){
        $("#9").css({'display':'none'});
        $('#Ethernet').removeClass('btnDisable');
        $('.bnt9').css({'display':'none'})
    }    
}
//funciones para trivia
//desblock botones trivia
function blockTrivia(){    
    cantStarTrivia();
    if(contStarTrivia >= 6){
        $("#extreme").css({'display':'none'});
        $('#level4').removeClass('btnDisable');
    }
}
//desblock botones conceptos
function blockConceptos(){  
    var contStarConcep =Number(localStorage.getItem('contStarconcepto'));
    if(contStarConcep>= 5){
        $("#4").css({'display':'none'});
        $('#level4').removeClass('btnDisable');
        $('.bnt4').css({'display':'none'})
    }
    if(contStarConcep>= 7){
        $("#5").css({'display':'none'});
        $('#level5').removeClass('btnDisable');
        $('.bnt5').css({'display':'none'})
    }
}
//escribe la cantidad de estrellas en el modal
function cantStarTrivia(){
        $("#cant").text('6');
        $("#cant2").text('6');
        $("#cantTotal").text(contStarTrivia)     
}
//escribe la cantidad de estrellas en el modal
function cantStarConcepts(n){
    if(n == 4){
        $("#cant").text('5');
        $("#cant2").text('5');
        $("#cantTotal").text(ContStarConcepto)    
    }
    if(n == 5){
        $("#cant").text('7');
        $("#cant2").text('7');
        $("#cantTotal").text(ContStarConcepto)    
    }
       
}

