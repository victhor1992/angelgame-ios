var language = localStorage.getItem('selectedLang');
var tituloTTL
var TiempoTTL
var ComprarTTL
var hasPerdido
var puntos
var intentalodenuevo
var publicarRedes
var felicidades
var nivelCompleto
var text1
var text2
var Aceptar

if(language=='Eng'){
    tituloTTL='YOU DO NOT HAVE MORE TTL'
    TiempoTTL='Time until next TTL'
    ComprarTTL='Buy TTL'
    hasPerdido='Game over'
    intentalodenuevo='Try Again!'
    publicarRedes='Post your score on social networks'
    puntos="Score"
    felicidades='Congratulations!'
    nivelCompleto='Level Completed'
    text1= 'You need'
    text2= 'Stars to unlock this level'
    Aceptar='Accept'
}else if(language=='Por'){
    tituloTTL='VOCÊ NÃO TEM MAIS TTL'
    TiempoTTL='Tempo até o próximo TTL'
    ComprarTTL='Compre TTL'
    hasPerdido='Fim de Jogo'
    intentalodenuevo='Tenta Novo!'
    publicarRedes='Postar sua pontos nas redes sociais'
    puntos="Ponto"
    felicidades='Parabéns!'
    nivelCompleto='Nivel Máximo'
    text1= 'Você precisa de'
    text2= 'estrelas para desbloquear este nível'
    Aceptar='Aceitar'
}else {
    tituloTTL='NO TIENES MÁS TTL'
    TiempoTTL='Tiempo hasta el próximo TTL'
    ComprarTTL='Comprar TTL'
    hasPerdido='Has perdido'
    intentalodenuevo='¡Inténtalo de nuevo!'
    publicarRedes='Publique su puntaje en las redes sociales'
    puntos="Puntos"
    felicidades='¡Felicidades!'
    nivelCompleto='Nivel Completado'
    text1= 'Necesita'
    text2= 'Estrellas para desbloquear este nivel'
    Aceptar='Aceptar'
}

//html ttl
var htmlTTL='<div class="modal-dialog" role="document">'
htmlTTL+='             <div  class="modal-content">'
htmlTTL+='                  <div class="modal-body text-center">'
htmlTTL+='                       <div class="box">'
htmlTTL+='                           <h2 class="modal-title animated zoomIn txtTTL">'+tituloTTL+'</h2>'
htmlTTL+='                           <div class="row">'
htmlTTL+='                               <div class="col-md-4 col-xs-4 ">'
htmlTTL+='                                   <img id="imgTTL" src="imgs/ttl0.png" alt="" class="animated jackInTheBox" >'
htmlTTL+='                               </div>'
htmlTTL+='                               <div class="col-md-8 col-xs-8">'
htmlTTL+='                                   <p class="post">'+TiempoTTL+'</p>'
htmlTTL+='                                   <div id="TiempoTTL"><span id="Min">00</span>:<span id="Seg">00</span></div>'
htmlTTL+='                                   <button class="btnCompra" onclick="comprar()">'+ComprarTTL+'</button>'
htmlTTL+='                               </div>'
htmlTTL+='                           </div>'
htmlTTL+='                       </div>'
htmlTTL+='                  </div>' 
htmlTTL+='             <div class="modal-footer">' 
htmlTTL+='                 <a id="reload" class="animated heartBeat delay-1s" style="display: none" href="" onclick="location.reload()">'                
htmlTTL+='                    <button class="btnPlayIcon"></button>'         
htmlTTL+='                 </a>'     
htmlTTL+='                 <a href="/" >'     
htmlTTL+='                    <button class="btnHomeIcon" onclick="goHomeF()"></button>'             
htmlTTL+='                 </a>'             
htmlTTL+='            </div>'               
htmlTTL+='      </div>'     
htmlTTL+='</div>' 

//html modal Lose
var htmlLose='<div class="modal-dialog" role="document">'                         
htmlLose+='       <div  class="modal-content">'
htmlLose+='            <div class="modal-body text-center">'
htmlLose+='                <div class="box">  '
htmlLose+='                    <h2>'+hasPerdido+'</h2>'
htmlLose+='                    <a href="javascript:reload();"><h4>'+intentalodenuevo+'</h4></a>'
htmlLose+='                    <div class=trivia></div>'
htmlLose+='                    <p class="post">'+publicarRedes+'</p>'
htmlLose+='                    <div class="score">'
htmlLose+='                         '+puntos+': <div class="punto"> 0</div>'
htmlLose+='                    </div>'
htmlLose+='                    <a class="fb-xfbml-parse-ignore" target="_blank"><button class="FB"></button></a>'
htmlLose+='                    <a class="tw-xfbml-parse-ignore" target="_blank"><button class="TW" onclick="public_TW()"></button></a>'
htmlLose+='                    <button class="IG" onclick="public_IG()"></button>  '
htmlLose+='               </div>'
htmlLose+='           </div>'
htmlLose+='           <div class="modal-footer">'
htmlLose+='               <a id="btnbacklose" href="" >'
htmlLose+='                  <button class="bntAtrasIcon"></button>'
htmlLose+='               </a>'
htmlLose+='               <a href="/" >'
htmlLose+='                  <button class="btnHomeIcon" onclick="goHomeF()"></button>'
htmlLose+='               </a>'
htmlLose+='           </div>'
htmlLose+='      </div>'
htmlLose+='</div>'

//html modal Win
var htmlWin='<canvas></canvas>'
htmlWin+='<div class="modal-dialog" role="document">'
htmlWin+='  <div class="modal-content">'
htmlWin+='    <div class="modal-body text-center">'              
htmlWin+='        <div class="box">' 
htmlWin+='            <div class="relative">' 
htmlWin+='                <div class="boxStar">' 
htmlWin+='                     <img src="imgs/starB.png" alt="winstar" class="winstarShadow1 ">' 
htmlWin+='                     <img src="imgs/starB.png" alt="winstar" class="winstarShadow">' 
htmlWin+='                     <img src="imgs/starB.png" alt="winstar" class="winstarShadow2 ">' 
htmlWin+='                </div>' 
htmlWin+='                <div class="boxStarW">' 
htmlWin+='                     <img src="imgs/star.png" alt="winstarW" class="winstar1 animated zoomIn delay-1s faster">' 
htmlWin+='                     <img src="imgs/star.png" alt="winstarW" class="winstar2 animated zoomIn delay-2s faster">' 
htmlWin+='                     <img src="imgs/star.png" alt="winstarW" class="winstar3 animated zoomIn delay-3s faster">' 
htmlWin+='                </div>' 
htmlWin+='            </div>' 
htmlWin+='            <div class="ShowCoin">+ ' 
htmlWin+='               <span id="GetCoin">0</span>' 
htmlWin+='               <img src="imgs/coin50.png" alt="" class="wincoin">' 
htmlWin+='            </div>' 
htmlWin+='            <h2 class="animated tada">'+felicidades+'</h2>' 
htmlWin+='            <h4 id="finalizado" >'+nivelCompleto+'</h4>' 
htmlWin+='           <div class=trivia></div>'
htmlWin+='            <p class="post">'+publicarRedes+'</p>' 
htmlWin+='            <div class="score">' 
htmlWin+='                '+puntos+': <div class="punto"> 0</div>' 
htmlWin+='            </div>' 
htmlWin+='            <a class="fb-xfbml-parse-ignore" target="_blank"><button class="FB"></button></a>' 
htmlWin+='            <button class="TW" onclick="public_TW()"></button>' 
htmlWin+='            <button class="IG" onclick="public_IG()"></button>' 
htmlWin+='         </div>'
htmlWin+='    </div>'  
htmlWin+='    <div class="modal-footer relative">'  
htmlWin+='          <a id="btnbackwin" href="" >'  
htmlWin+='            <button class="bntAtrasIcon"></button>'  
htmlWin+='          </a>'  
htmlWin+='          <a href="/" >'                
htmlWin+='              <button class="btnHomeIcon" onclick="goHomeF()"></button>'  
htmlWin+='          </a> '
htmlWin+='          <div class="disabl1" id="nextLVL" ></div>'
htmlWin+='          <a id="urlNext" href="">'
htmlWin+='              <button id="btnNext" class="bntNextIcon"></button>'  
htmlWin+='          </a> '
htmlWin+='      </div>'  
htmlWin+='   </div>'  
htmlWin+='</div>'  


// html modal aviso
var htmlAviso ='<div id="BoxAviso" class="">'
    htmlAviso+='    <h1 class="titAviso">'+text1+' <span id="cant">0</span> '+text2+'</h1> '
    htmlAviso+='    <div class="estrellas"><span id="cantTotal">0</span>/<span id="cant2">0</span><img  src="imgs/star.png" alt="star" class="avisoStar"></div>'
    htmlAviso+='    <button class="btnComprar" onclick="cerrarModal()">'+Aceptar+'</button>  ' 
    htmlAviso+='</div>'
                           

// html coin y ttl
var htmlCoinTTL=' <div class="boxTtl">'
    htmlCoinTTL+='  <span id="ttl"></span>'
    htmlCoinTTL+='</div>'
    htmlCoinTTL+='<div class="boxCoin">'
    htmlCoinTTL+='  <span id="coin"></span> '
    htmlCoinTTL+='</div>'

$(document).ready(function(){
    $('#WinModal').html(htmlWin)
    $('#LooseModal').html(htmlLose)
    $('#TTLModal').html(htmlTTL) 
    $('#ModalAviso').html(htmlAviso);
    $('.head, #menu').append(htmlCoinTTL);        
})

// confeti
$(document).ready(function(){
    window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame;  
    var canvas = document.querySelector("canvas");
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight;
    var ctx = canvas.getContext("2d");
    ctx.globalCompositeOperation = "source-over";
    var particles = [];
    var pIndex = 0;
    var x, y, frameId;
  
    function Dot(x,y,vx,vy,color){
      this.x = x;
      this.y = y;
      this.vx = vx;
      this.vy = vy;
      this.color = color;
      particles[pIndex] = this;
      this.id = pIndex;
      pIndex++;
      this.life = 0;
      this.maxlife = 600;
      this.degree = getRandom(0,360);
      this.size = Math.floor(getRandom(8,10));
    };
  
    Dot.prototype.draw = function(x, y){
  
      this.degree += 1;
      this.vx *= 0.99;
      this.vy *= 0.999;
      this.x += this.vx+Math.cos(this.degree*Math.PI/180);
      this.y += this.vy;
      this.width = this.size;
      this.height = Math.cos(this.degree*Math.PI/45)*this.size;
      ctx.fillStyle = this.color;
      ctx.beginPath();
      ctx.moveTo(this.x+this.x/2, this.y+this.y/2);
      ctx.lineTo(this.x+this.x/2+this.width/2, this.y+this.y/2+this.height);
      ctx.lineTo(this.x+this.x/2+this.width+this.width/2, this.y+this.y/2+this.height);
      ctx.lineTo(this.x+this.x/2+this.width, this.y+this.y/2);
      ctx.closePath();
      ctx.fill();
      this.life++;
      if(this.life >= this.maxlife){
        delete particles[this.id];
      }
    }
    window.addEventListener("resize", function(){
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;
      x = canvas.width / 2;
      y = canvas.height / 2;
    });
  
    function loop(){
      ctx.clearRect(0,0, canvas.width, canvas.height);
      if(frameId % 3 == 0) {
          new Dot(canvas.width*Math.random()-canvas.width+canvas.width/2*Math.random(), -canvas.height/2, getRandom(1, 3),  getRandom(2, 4),"#feb500");
          new Dot(canvas.width*Math.random()+canvas.width-canvas.width*Math.random(), -canvas.height/2,  -1 * getRandom(1, 3),  getRandom(2, 4),"#FFF");
          new Dot(canvas.width*Math.random()-canvas.width+canvas.width/2*Math.random(), -canvas.height/2, getRandom(1, 3),  getRandom(2, 4),"#e10707");
          new Dot(canvas.width*Math.random()+canvas.width-canvas.width*Math.random(), -canvas.height/2,  -1 * getRandom(1, 3),  getRandom(2, 4),"#008cca");
      }
      for(var i in particles){
        particles[i].draw();
      }
      frameId = requestAnimationFrame(loop);
    }
  
    loop();
  
    function getRandom(min, max) {
      return Math.random() * (max - min) + min;
    }
  
});
  

    
        
            
        
        
            
        
    
   

