var PlayMusica = localStorage.getItem('Audio');
window.localStorage.setItem('resp1', ''); 
window.localStorage.setItem('resp2', '');
window.localStorage.setItem('resp3', '');
window.localStorage.setItem('resp4', '');
window.localStorage.setItem('resp5', '');
window.localStorage.setItem('resp6', '');
window.localStorage.setItem('audio', PlayMusica); 
var AudioEffect = localStorage.getItem('sound-acert');
window.localStorage.setItem('#sound-acert', AudioEffect);
var ttl = Number(window.localStorage.getItem('ttl'));
var arregloFinal = [];
fin= false;
var newArreglo = [];
var resanswers = [];
var level=0;
var timeLevel= 0;
var cont=0;
var punto = 0;
var nivel=0;
var pas1=0;
var pas2=0;
var pas3=0;
var pas4=0;
var pas5=0;
var pas6=0;
var starlvl1 = window.localStorage.getItem('ConceptoStarlvl1');
var starlvl2 = window.localStorage.getItem('ConceptoStarlvl2');
var starlvl3 = window.localStorage.getItem('ConceptoStarlvl3');
var starlvl4 = window.localStorage.getItem('ConceptoStarlvl4');
var starlvl5 = window.localStorage.getItem('ConceptoStarlvl5');
var ContStar = Number(localStorage.getItem('contStarconcepto'));
var ttl = Number(window.localStorage.getItem('ttl'));
var coin = Number(window.localStorage.getItem('coin'));
var audioElement = document.createElement('audio');
var language = localStorage.getItem('selectedLang');
var totalVal ="";
var w1 = "falso"
var w2 = "falso"
var w3 = "falso"
var w4 = "falso"
var w5 = "falso"
var w6 = "falso"
var numInput = '1'
var block
var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
// respuestas nivel 1
var palabra0;
var palabra1;
var palabra2;
var palabra3;
var palabra4;
var palabra5;
var palabra6;
var palabra7;
var palabra8;
var palabra9;
var palabra10;
var palabra11;
var palabra12;
var palabra13;
var palabra14;
var palabra15;
var palabra16;
var palabra17;
var palabra18;
var palabra19;
var palabra18;
var palabra20;
var palabra21;
var palabra22;
var numPreg;
var boxPosition = [];
var answers = [];

if(starlvl1 == null){
    starlvl1 = '0'; 
}
if(starlvl2 == null){
    starlvl2 = '0'; 
}
if(starlvl3 == null){
    starlvl3 = '0'; 
}
if(starlvl4 == null){
  starlvl4 = '0'; 
}
if(starlvl5 == null){
  starlvl5 = '0'; 
}
$(document).ready(function(){ 
  blockConceptos()
  $("#levelModal .modal-content").append(infoLink)
  $('#ttl').text(ttl);
	$('#coin').text(coin);
	if(ttl == 0){
		contadorMinutos();
		blockTTL();			
	}else if(ttl <= 4){
		contadorMinutos();
	}
	if(ttl > 0){
		$('#levelModal').modal('show');
		$('#levelModal button').click(function(){
			$('#levelModal').modal('hide');
		})
	}
	if(language == 'Eng'){
    $('#levelModalLabel').text('Basic concepts');
    $('.btnAtras').text('Back');
    $('.lvlTitle').text('Choose a level of difficulty!');
    $('#level1').text('Level 1');
    $('#level2').text('Level 2');
    $('#level3').text('Level 3');
    $('#level4').text('Level 4');
    $('#level5').text('Level 5');
    $('.tmp').text('Time:');
    $('.felic').text('Congratulations!');
    $('.nivelComple').text('Level Complete');
    $('.rede').text('Post your score on social networks');
    $('.pts').text('Points:');
    $('.volver').text('Play again');
    $('.perd').text('Game over');
    $('.idn').text('Try Again!');
    $('#nottl').text('YOU DO NOT HAVE MORE TTL');
    $('.thp').text('Time until next TTL');
    $('.btnCompra').text('Buy TTL');
    $('.acb').text('Drag the box below');

  }else if(language == 'Por'){
    $('#levelModalLabel').text('Conceitos básicos');
    $('.btnAtras').text('Voltar');
    $('.lvlTitle').text('Escolha um nível de dificuldade!');
    $('#level1').text('Nível 1');
    $('#level2').text('Nível 2');
    $('#level3').text('Nível 3');
    $('#level4').text('Nível 4');
    $('#level5').text('Nível 5');
    $('.felic').text('Parabéns!');
    $('.tmp').text('Tempo:');
    $('.nivelComple').text('Nível Completo');
    $('.rede').text('Postar sua pontos nas redes sociais');
    $('.pts').text('Ponto:');
    $('.volver').text('Jogar novamente');
    $('.perd').text('Fim de Jogo');
    $('.idn').text('Tenta Novo!');
    $('#nottl').text('VOCÊ NÃO TEM MAIS TTL ');
    $('.thp').text('Tempo até o próximo TTL');
    $('.btnCompra').text('Compre TTL');
    $('.acb').text('Arraste a caixa abaixo');

  }else{
    $('#levelModalLabel').text('Conceptos Básicos');
    $('.btnAtras').text('Atrás');
    $('.lvlTitle').text('¡Elige un nivel de dificultad!');
    $('#level1').text('Nivel 1');
    $('#level2').text('Nivel 2');
    $('#level3').text('Nivel 3');
    $('#level4').text('Nivel 4');
    $('#level5').text('Nivel 5');
    $('.tmp').text('Tiempo:');
    $('.felic').text('¡Felicidades!');
    $('.nivelComple').text('Nivel Completo');
    $('.rede').text('Publique su puntaje en las redes sociales');
    $('.pts').text('Puntos:');
    $('.volver').text('Volver a jugar');
    $('.perd').text('Has perdido');
    $('.idn').text('Inténtalo de nuevo!');
    $('#nottl').text('NO TIENES MÁS TTL ');
    $('.thp').text('Tiempo hasta el próximo TTL');
    $('.btnCompra').text('Comprar TTL');
    $('.acb').text('Arrastre el cuadro de abajo ');

  }
	var WindowW = $(window).innerWidth();
  var WindowH = $(window).innerHeight();
  var boxW = $('.conOverFw').innerWidth();
	var boxH = $('.conOverFw').innerHeight();
  $('.conceptos').removeClass('modal-open')
  $('.conceptos').css({
		'height':WindowH + 'px',
		'width':WindowW + 'px'
  });
  $('.head').css({
		'width':WindowW + 'px'
  });
  $('.conOverFw').css({
		'height':boxH + 'px',
		'width':boxW + 'px'
	});
	//Control de sonido
    PlayMusic(window.localStorage.getItem('audio'));
});
//funcion para mostrar el las imagenes por idioma
function tutorial(language, juego){
	if (language == "Eng") {
		$('.titAvisoTuto').text('Hello, welcome to the Basic concepts game, we will explain how to play.');
		$('.tutoBtn').text('Close');
		$('.txtCheck').text('Do not show again');		
	}else if (language == "Por"){		
		$('.titAvisoTuto').text('Olá, bem-vindo ao jogo da Conceitos básicos, vamos explicar como jogar.');
		$('.tutoBtn').text('Fechar');
		$('.txtCheck').text('Não mostrar novamente');
	}else{
		$('.titAvisoTuto').text('Hola, bienvenidos al juego de Conceptos Básicos, te explicaremos cómo jugar.');
		$('.tutoBtn').text('Cerrar');
		$('.txtCheck').text('No volver a mostrar');
	}	
	$('.img1').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'-'+language+'.jpg');
	$('.img2').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'2-'+language+'.jpg');
	$('.img3').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'3-'+language+'.jpg');
}	
//Activar el sonido
function PlayMusic(active){
	if (active == "1"){
		audioElement.setAttribute('id', 'sonido');
		audioElement.setAttribute('src', 'sounds/conceptos-world.mp3');
    	audioElement.setAttribute('autoplay', 'autoplay');
    	audioElement.addEventListener("ended", function() {
	        this.currentTime = 0;
	        this.play();
	    }, false);
	}
	else {
		audioElement.pause();
		active=0;
	}
}
//activa el efecto
function EffectMusic(active){
	window.localStorage.setItem('#sound-acert', active);
	if (active=="1") {
		$('#sound-acert').each(function(){
			this.play();
		});
	}else{
		active=0;
	}

}
// evalua si tiene vidas
$(document).ready(function(){    
  if(ttl == 0){
			contadorMinutos();
			blockTTL();			
		}else if(ttl <= 4){
			contadorMinutos();
    }    
    Level();
    star();
    showTuto('conceptos');
    loadLVL()
});//Fin de ready
// coloca las respuestas aleatoriamente
function restAnswers(j){
      var numRandom = Math.floor((Math.random() * boxPosition.length) + 1) - 1;
        // console.log(boxPosition[numRandom])
        // console.log(answers[boxPosition[numRandom]])
      if (boxPosition.length != 1){
        for (var i = 0; i <= boxPosition.length - 1; i++){
          if (i == numRandom){
            $('#respuestas').append('<div id="chld'+ boxPosition[numRandom] +'" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 relative"></div>')
            $('#chld'+boxPosition[numRandom]).append('<button id="btn'+ boxPosition[numRandom] +'" class="btnShw rp"><span id="'+boxPosition[numRandom]+'"> '+ answers[boxPosition[numRandom]] + '</span></button>')
            $('#chld'+boxPosition[numRandom]).append('<div id="block'+ boxPosition[numRandom] +'" class="blck"></div>')
            $( "#btn"+boxPosition[numRandom] ).attr("onclick","bwrite('"+answers[boxPosition[numRandom]]+" ', '"+boxPosition[numRandom]+"');");	
            //*****Elimina un objeto de un array
            boxPosition = jQuery.grep(boxPosition, function(b) {
              return b != boxPosition[numRandom];
            });  
          }  
        }
      }
      else {  
        $('#respuestas').append('<div id="chld'+ boxPosition[numRandom] +'" class="col-xs-3 col-sm-3 col-md-3 col-lg-3 relative"></div>')
        $('#chld'+boxPosition[numRandom]).append('<button id="btn'+ boxPosition[numRandom] +'" class="btnShw rp"><span id="'+boxPosition[numRandom]+'"> '+ answers[boxPosition[numRandom]] + '</span></button>')
        $('#chld'+boxPosition[numRandom]).append('<div id="block'+ boxPosition[numRandom] +'" class="blck"></div>')
        $( "#btn"+boxPosition[numRandom] ).attr("onclick","bwrite('"+answers[boxPosition[numRandom]]+" ', '"+boxPosition[numRandom]+"');");	
        //******* Elimina un objeto de un array
        boxPosition = jQuery.grep(boxPosition, function(b) {
          return b != boxPosition[0];
        });  
      }
      if (boxPosition.length != 0){
        j++;
        restAnswers(j);
      }      
}
//funcion para colocar estrellas level
function star(){
    if (starlvl1 == '3'){      
      document.getElementById("lvl1-1").src = 'imgs/star.png';
      document.getElementById("lvl1-2").src = 'imgs/star.png';
      document.getElementById("lvl1-3").src = 'imgs/star.png';
    }else if(starlvl1 == '2'){
      document.getElementById("lvl1-1").src = 'imgs/star.png';
      document.getElementById("lvl1-2").src = 'imgs/star.png';
    }else if(starlvl1 == '1'){
      document.getElementById("lvl1-1").src = 'imgs/star.png';
    }
     if (starlvl2 == '3'){      
       document.getElementById("lvl2-1").src = 'imgs/star.png';
       document.getElementById("lvl2-2").src = 'imgs/star.png';
       document.getElementById("lvl2-3").src = 'imgs/star.png';
     }else if(starlvl2 == '2'){      
       document.getElementById("lvl2-1").src = 'imgs/star.png';
       document.getElementById("lvl2-2").src = 'imgs/star.png';
     }else if(starlvl2 == '1'){      
       document.getElementById("lvl2-1").src = 'imgs/star.png';
     }
     if (starlvl3 == '3'){      
       document.getElementById("lvl3-1").src = 'imgs/star.png';
       document.getElementById("lvl3-2").src = 'imgs/star.png';
       document.getElementById("lvl3-3").src = 'imgs/star.png';
     }else if(starlvl3 == '2'){      
       document.getElementById("lvl3-1").src = 'imgs/star.png';
       document.getElementById("lvl3-2").src = 'imgs/star.png';
     }else if(starlvl3 == '1'){      
       document.getElementById("lvl3-1").src = 'imgs/star.png';
     }
    if (starlvl4 == '3'){      
      document.getElementById("lvl4-1").src = 'imgs/star.png';
      document.getElementById("lvl4-2").src = 'imgs/star.png';
      document.getElementById("lvl4-3").src = 'imgs/star.png';
    }else if(starlvl4 == '2'){      
      document.getElementById("lvl4-1").src = 'imgs/star.png';
      document.getElementById("lvl4-2").src = 'imgs/star.png';
    }else if(starlvl4 == '1'){      
      document.getElementById("lvl4-1").src = 'imgs/star.png';
    }
    if (starlvl5 == '3'){      
      document.getElementById("lvl5-1").src = 'imgs/star.png';
      document.getElementById("lvl5-2").src = 'imgs/star.png';
      document.getElementById("lvl5-3").src = 'imgs/star.png';
    }else if(starlvl5 == '2'){      
      document.getElementById("lvl5-1").src = 'imgs/star.png';
      document.getElementById("lvl5-2").src = 'imgs/star.png';
    }else if(starlvl5 == '1'){      
      document.getElementById("lvl5-1").src = 'imgs/star.png';
    }
}
//mostrar preguntas
function showQ(q , v, n , w){
  for (var i = 1; i <= 10; i++){
  $('#q'+i).css({'display':'none'});
    if(i <= 6 ){
      $('#v'+i).removeClass('btnActive');      
      $('#respuesta'+i).css({'display':'none'});
      $('#btnr'+i).css({'display':'none'});
    }
  }
  $('#btnr'+n).css({'display':'block'});
  $('#respuesta'+n).css({'display':'block'});
  $('#'+q).css({'display':'block'});
  $('#'+v).addClass('btnActive');
  numInput = n;
  showBtn();
  blkBtn(w);
}
//imprime los input
function input(){
  for (var i = 1; i <= 6; i++){
    $('.boxRespuesta').append('<div id="box'+i+'"></div>');
    $('#box'+i).append('<input id="respuesta'+i+'" type="text" class="input col-xs-10 col-sm-10 col-md-10 col-lg-10 ">');
    $('#box'+i).append('<button id="btnr'+i+'" class="input col-xs-1 col-sm-1 col-md-1 col-lg-1 btnBorrar " onclick="borrarPalabra()"><i class="fa fa-close"></i></button>');    
  } 
  for (var a = 1; a < word1.split(' ').length ; a++){
    $('#b1').append('<div class="Cuadro"></div>');
  }
  for (var b = 1; b < word2.split(' ').length ; b++){
    $('#b2').append('<div class="Cuadro"></div>');
  }
  for (var c = 1; c < word3.split(' ').length ; c++){
    $('#b3').append('<div class="Cuadro"></div>');
  }
  for (var d = 1; d < word4.split(' ').length ; d++){
    $('#b4').append('<div class="Cuadro"></div>');
  }
  for (var e = 1; e < word5.split(' ').length ; e++){
    $('#b5').append('<div class="Cuadro"></div>');
  }
  for (var f = 1; f < word6.split(' ').length ; f++){
    $('#b6').append('<div class="Cuadro"></div>');
  }
  $('#v1').click()
}
function showBtn(){
  for (var i = 0; i< answers.length; i++){
    $('#block'+i).removeAttr('style');
    $('#btn'+i).removeAttr('style');
  }
  palabra = $('#respuesta'+numInput).val()
  arrPalabras = palabra.split(' ')
    for (var i = 0; i <= numPreg; i++){
      for (var j = 0; j <= 3; j++){
        if(answers[i] == arrPalabras[j]){
          $('#btn'+i).css({'background':'#0b2339'});
          $('#block'+i).css({'display':'block'});
        }
      }      
    }     
}
//seleciona las preguntas y las respuesta de cada nivel
function selecNivel(){
  if(nivel == 1){
    console.log('entre leve 1')
  // respuestas nivel 1
  palabra0 ="Static"//pregunta 1
  palabra1 ="Servers"
  palabra2 ="Fragment"//pregunta 2
  palabra3 ="Offset"
  palabra4 ="One-way"//pregunta 3
  palabra5 ="Gateways"
  palabra6 ="Header"//pregunta 4
  palabra7 ="Checksum"
  palabra8 ="Network"//pregunta 5
  palabra9 ="Address"
  palabra10 ="Translation"
  palabra11 ="Dynamic" //pregunta 6
  palabra12 ="Host"
  palabra13 ="Configuration"
  palabra14 ="Protocol"
  numPreg = 14;
  word1= palabra0+' '+palabra1+' '
  word2= palabra2+' '+palabra3+' '
  word3= palabra4+' '+palabra5+' '
  word4= palabra6+' '+palabra7+' '
  word5= palabra8+' '+palabra9+' '+palabra10+' '
  word6= palabra11+' '+palabra12+' '+palabra13+' '+palabra14+' '
  boxPosition = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
  answers = [
    palabra0,
    palabra1,
    palabra2,
    palabra3,
    palabra4,
    palabra5,
    palabra6,
    palabra7,
    palabra8,
    palabra9,
    palabra10,
    palabra11,
    palabra12,
    palabra13,
    palabra14,
  ];
  if(language == 'Eng'){
    $('#q1, #p1').text('1 - Consist of a computer where the information is stored and an HTTP server that responds to the request protocols.');
    $('#q2, #p2').text('2 - It is a mechanism that allows separating (or fragmenting) an IP packet between several data blocks.');
    $('#q3, #p3').text('3 - Allow alerts to flow in one direction only.');
    $('#q4, #p4').text('4 - It is a checksum used in version 4 of the Internet Protocol (IPv4) to detect corruption in the header of IPv4 packets.');
    $('#q5, #p5').text('5 - Network Address Translation, is a mechanism for transforming private to public IP addressing and vice versa.');
    $('#q6, #p6').text('6 - It is a protocol that allows a computer connected to a network to obtain its configuration.');   

  }else if(language == 'Por'){
    $('#q1, #p1').text('1 - Consistem em um computador em que as informações são armazenadas e em um servidor HTTP que responde aos protocolos de solicitação.');
    $('#q2, #p2').text('2 - É um mecanismo que permite separar (ou fragmentar) um pacote IP entre vários blocos de dados.');
    $('#q3, #p3').text('3 - Permitir que os alertas fluam apenas em uma direção.');
    $('#q4, #p4').text('4 - É uma soma de verificação usada na versão 4 do Protocolo da Internet (IPv4) para detectar corrupção no cabeçalho dos pacotes IPv4.');
    $('#q5, #p5').text('5 - Network Address Translation, é um mecanismo para transformar endereços IP privados em públicos e vice-versa.');
    $('#q6, #p6').text('6 - É um protocolo que permite que um computador conectado a uma rede obtenha sua configuração.');

  }else{
    $('#q1, #p1').text('1 - Consiste en un computador en donde está almacenada la información y un servidor HTTP que responde a los protocolos de pedido.');
    $('#q2, #p2').text('2 - Es un mecanismo que permite separar (o fragmentar) un paquete IP entre varios bloques de datos.');
    $('#q3, #p3').text('3 - Permiten que las alertas fluyan en una única dirección.');
    $('#q4, #p4').text('4 - Es una suma de verificación utilizada en la versión 4 del Protocolo de Internet (IPv4) para detectar daños en el encabezado de los paquetes IPv4.');
    $('#q5, #p5').text('5 - Traducción de Dirección de Red, es un mecanismo de transformación de direccionamiento IP privado a público y viceversa.');
    $('#q6, #p6').text('6 - Es un protocolo que permite que un equipo conectado a una red pueda obtener su configuración.');   

  }
  //fin respuestas nivel 1
  }
  if(nivel == 2){
    console.log('entre leve 2')
    // respuestas nivel 2
    palabra0 ="OSI"//pregunta 1
    palabra1 ="Model"
    palabra2 ="IP"//pregunta 2
    palabra3 ="Adress"
    palabra4 ="Internet"//pregunta 3
    palabra5 ="Protocol"
    palabra6 ="Version-4"
    palabra7 ="Request"//pregunta 4
    palabra8 ="for"
    palabra9 ="Comment"
    palabra10 ="Domain"//pregunta 5
    palabra11 ="Name" 
    palabra12 ="System"
    palabra13 ="Local"//pregunta 6
    palabra14 ="Area"
    palabra15 ="Network"
    word1= palabra0+' '+palabra1+' '
    word2= palabra2+' '+palabra3+' '
    word3= palabra4+' '+palabra5+' '+palabra6+' '
    word4= palabra7+' '+palabra8+' '+palabra9+' '
    word5= palabra10+' '+palabra11+' '+palabra12+' '
    word6= palabra13+' '+palabra14+' '+palabra15+' '
    numPreg = 15;
    boxPosition = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
    answers = [
      palabra0,
      palabra1,
      palabra2,
      palabra3,
      palabra4,
      palabra5,
      palabra6,
      palabra7,
      palabra8,
      palabra9,
      palabra10,
      palabra11,
      palabra12,
      palabra13,
      palabra14,
      palabra15,
    ];
    if(language == 'Eng'){
      $('#q1, #p1').text('1 - It is a reference model for network protocols (not a network architecture), created in 1980 by the International Organization for Standardization (ISO).');
      $('#q2, #p2').text('2 - It is a set of numbers that identifies, logically and hierarchically, a network interface (communication / connection element) of a device that uses the protocol.');
      $('#q3, #p3').text('3 - It is the fourth version of the Internet Protocol (IP), a network interconnection protocol.');
      $('#q4, #p4').text("4 - They are a series of publications from the Internet Engineering Task Force that describe various aspects of the Internet's operation better known by its acronym RFC.");
      $('#q5, #p5').text('5 - It is a decentralized hierarchical naming system for devices connected to IP networks such as the Internet or a private network.');
      $('#q6, #p6').text('6 - These are the smallest networks, such as those that exist in a call center or cyber café, or a department.');   
  
    }else if(language == 'Por'){
      $('#q1, #p1').text('1 - É um modelo de referência para protocolos de rede (não uma arquitetura de rede), criado em 1980 pela International Organization for Standardization (ISO).');
      $('#q2, #p2').text('2 - É um conjunto de números que identifica, lógica e hierarquicamente, uma interface de rede (elemento de comunicação / conexão) de um dispositivo que usa o protocolo.');
      $('#q3, #p3').text('3 - É a quarta versão do Internet Protocol (IP), um protocolo de interconexão de rede.');
      $('#q4, #p4').text('4 - São uma série de publicações da Internet Engineering Task Force que descrevem vários aspectos da operação da Internet, mais conhecidos por sua sigla RFC.');
      $('#q5, #p5').text('5 - É um sistema de nomeação hierárquica descentralizado para dispositivos conectados a redes IP, como a Internet ou uma rede privada.');
      $('#q6, #p6').text('6 - Essas são as redes menores, como as que existem em um call center, cyber café ou departamento.');
  
    }else{
      $('#q1, #p1').text('1 - Es un modelo de referencia para los protocolos de la red (no es una arquitectura de red), creado en el año 1980 por la Organización Internacional de Normalización (ISO).');
      $('#q2, #p2').text('2 - Es un conjunto de números que identifica, de manera lógica y jerárquica, a una Interfaz en red (elemento de comunicación/conexión) de un dispositivo que utilice el protocolo.');
      $('#q3, #p3').text('3 - Es la cuarta versión del Internet Protocol (IP), un protocolo de interconexión de redes.');
      $('#q4, #p4').text('4 - Son una serie de publicaciones del grupo de trabajo de ingeniería de internet que describen diversos aspectos del funcionamiento de Internet más conocidos por sus siglas RFC.');
      $('#q5, #p5').text('5 - Es un sistema de nomenclatura jerárquico descentralizado para dispositivos conectados a redes IP como Internet o una red privada.');
      $('#q6, #p6').text('6 - Se trata de las redes de menor tamaño, como las que existen en un locutorio, cyber café o un departamento.');   
  
    }
    //fin respuestas nivel 2
  }
  if(nivel == 3){
    console.log('entre leve 3')
    // respuestas nivel 3
    palabra0 ="Transmission"//pregunta 1
    palabra1 ="Control"
    palabra2 ="Protocol"
    palabra3 ="World"//pregunta 2
    palabra4 ="Wide"
    palabra5 ="Web"
    palabra6 ="Internet"//pregunta 3
    palabra7 ="Engineering"
    palabra8 ="Task"
    palabra9 ="Force"
    palabra10 ="International"//pregunta 4
    palabra11 ="Organization" 
    palabra12 ="for"
    palabra13 ="Standardization"
    palabra14 ="Wireless" //pregunta 5
    palabra15 ="Local"
    palabra16 ="Area"
    palabra17 ="Network"
    palabra18 ="Asymmetric"//pregunta 6
    palabra19 ="Digital"
    palabra20 ="Subscriber"
    palabra21 ="Line"
    word1= palabra0+' '+palabra1+' '+palabra2+' '
    word2= palabra3+' '+palabra4+' '+palabra5+' '
    word3= palabra6+' '+palabra7+' '+palabra8+' '+palabra9+' '
    word4= palabra10+' '+palabra11+' '+palabra12+' '+palabra13+' '
    word5= palabra14+' '+palabra15+' '+palabra16+' '+palabra17+' '
    word6= palabra18+' '+palabra19+' '+palabra20+' '+palabra21+' '
    numPreg = 21;
    boxPosition = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];
    answers = [
      palabra0,
      palabra1,
      palabra2,
      palabra3,
      palabra4,
      palabra5,
      palabra6,
      palabra7,
      palabra8,
      palabra9,
      palabra10,
      palabra11,
      palabra12,
      palabra13,
      palabra14,
      palabra15,
      palabra16,
      palabra17,
      palabra18,
      palabra19,
      palabra20,
      palabra21,
    ];
    if(language == 'Eng'){
      $('#q1, #p1').text('1 - TCP stands for:');
      $('#q2, #p2').text('2 - Logical system for accessing and searching the information available on the Internet, whose information units are web pages.');
      $('#q3, #p3').text('3 - It is an open international standardization organization, which aims to contribute to Internet engineering, known as the IETF.');
      $('#q4, #p4').text("4 - It is an organization for the creation of international standards made up of various national standardization organizations.");
      $('#q5, #p5').text('5 - It is a local type network whose computers do not need to be linked through cables to connect.');
      $('#q6, #p6').text('6 - It is a kind of technology that allows connection to the Internet through the use of the traditional telephone line, transmitting the digital information in an analog way through the symmetrical copper pairs cable.');   
  
    }else if(language == 'Por'){
      $('#q1, #p1').text('1 - TCP significa:');
      $('#q2, #p2').text('2 - Sistema lógico para acessar e pesquisar as informações disponíveis na Internet, cujas unidades de informação são páginas da web.');
      $('#q3, #p3').text('3 - É uma organização internacional de padronização aberta, que visa contribuir com a engenharia da Internet, conhecida como IETF.');
      $('#q4, #p4').text('4 - É uma organização para a criação de padrões internacionais compostos por várias organizações nacionais de padronização.');
      $('#q5, #p5').text('5 - É uma rede local, cujos computadores não precisam ser conectados através de cabos para se conectar.');
      $('#q6, #p6').text('6 - É um tipo de tecnologia que permite a conexão à Internet através do uso da linha telefônica tradicional, transmitindo as informações digitais de forma analógica através do cabo simétrico de pares de cobre.');
  
    }else{
      $('#q1, #p1').text('1 - Las siglas TCP corresponden a:');
      $('#q2, #p2').text('2 - Sistema lógico de acceso y búsqueda de la información disponible en Internet, cuyas unidades informativas son las páginas web.');
      $('#q3, #p3').text('3 - Es una organización internacional abierta de normalización, que tiene como objetivos el contribuir a la ingeniería de Internet, conocida como IETF.');
      $('#q4, #p4').text('4 - Es una organización para la creación de estándares internacionales compuesta por diversas organizaciones nacionales de normalización.');
      $('#q5, #p5').text('5 - Es una red de tipo local cuyos equipos no necesitan estar vinculados a través de cables para conectarse.');
      $('#q6, #p6').text('6 - Es una clase de tecnología que permite la conexión a Internet mediante el uso de la línea telefónica tradicional, transmitiendo la información digital de modo analógico a través del cable de pares simétricos de cobre.');   
      
    }
    //fin respuestas nivel 3
  }
  if(nivel == 4){
    console.log('entre leve 4')
    // respuestas nivel 4    
    palabra0 ="LAC"//pregunta 1
    palabra1 ="NIC"
    palabra2 ="Va"//pregunta 2
    palabra3 ="ria"
    palabra4 ="ble"
    palabra5 ="Pro"//pregunta 3
    palabra6 ="to"
    palabra7 ="col"
    palabra8 ="E"//pregunta 4
    palabra9 ="ther"
    palabra10 ="net"
    palabra11 ="Pri"//pregunta 5
    palabra12 ="va"
    palabra13 ="te"
    palabra14 ="Hy"//pregunta 6
    palabra15 ="per"
    palabra16 ="text"
    word1= palabra0+' '+palabra1+' '
    word2= palabra2+' '+palabra3+' '+palabra4+' '
    word3= palabra5+' '+palabra6+' '+palabra7+' '
    word4= palabra8+' '+palabra9+' '+palabra10+' '
    word5= palabra11+' '+palabra12+' '+palabra13+' '
    word6= palabra14+' '+palabra15+' '+palabra16+' '
    numPreg = 16;
    boxPosition = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
    answers = [
      palabra0,
      palabra1,
      palabra2,
      palabra3,
      palabra4,
      palabra5,
      palabra6,
      palabra7,
      palabra8,
      palabra9,
      palabra10,
      palabra11,
      palabra12,
      palabra13,
      palabra14,
      palabra15,
      palabra16,
    ];
    if(language == 'Eng'){
      $('#q1, #p1').text('1 - Latin America & Caribbean Network Information Centre correspond to acronyms:');
      $('#q2, #p2').text('2 - VLSM stands for: ??? Length Subnet Mask');
      $('#q3, #p3').text('3 - VPN stands for: File Transfer ???');
      $('#q4, #p4').text('4 - Is a local area network standard for computers, for its Spanish acronym Multiple Access with Carrier Listening and Collision Detection (CSMA / CD). Its name comes from the physical concept of ether');
      $('#q5, #p5').text('5 - VPN stands for: Virtual ??? Network');
      $('#q6, #p6').text('6 - HTTP stands for: ??? Transfer protocol');   

    }else if(language == 'Por'){
      $('#q1, #p1').text('1 - Latin America & Caribbean Network Information Centre corresponde a siglas:');
      $('#q2, #p2').text('2 - VLSM significa: ??? Length Subnet Mask');
      $('#q3, #p3').text('3 - FTP significa: File Transfer ???');
      $('#q4, #p4').text('4 - É um padrão de rede local para computadores, por seu acrônimo em espanhol Acesso Múltiplo com Detecção de Escuta e Colisão por Operadora (CSMA / CD). Seu nome vem do conceito físico de éter');
      $('#q5, #p5').text('5 - VPN significa: Virtual ??? Network');
      $('#q6, #p6').text('6 - HTTP significa: ??? Transfer protocol');
  
    }else{
      $('#q1, #p1').text('1 - Latin America & Caribbean Network Information Centre corresponden a las siglas:');
      $('#q2, #p2').text('2 - Las siglas VLSM corresponde a: ??? Length Subnet Mask');
      $('#q3, #p3').text('3 - Las siglas FTP corresponde a: File Transfer ???');
      $('#q4, #p4').text('4 - Es un estándar de redes de área local para computadores, por sus siglas en español Acceso Múltiple con Escucha de Portadora y Detección de Colisiones (CSMA/CD). Su nombre procede del concepto físico de éter');
      $('#q5, #p5').text('5 - Las siglas VPN corresponde a: Virtual ??? Network');
      $('#q6, #p6').text('6 - Las siglas HTTP corresponde a: ??? Transfer Protocol');   
      
    }
    //fin respuestas nivel 4
  }
  if(nivel == 5){
    console.log('entre leve 5')
    // respuestas nivel 5    
    palabra0 ="World"//pregunta 1
    palabra1 ="Wide"
    palabra2 ="Web"    
    palabra3 ="International"//pregunta 2
    palabra4 ="Organization" 
    palabra5 ="for"
    palabra6 ="Standardization"    
    palabra7 ="Asymmetric"//pregunta 3
    palabra8 ="Digital"
    palabra9 ="Subscriber"
    palabra10 ="Line"
    palabra11 ="Dynamic" //pregunta 4
    palabra12 ="Host"
    palabra13 ="Configuration"
    palabra14 ="Protocol"
    palabra15 ="Wireless" //pregunta 5
    palabra16 ="Local"
    palabra17 ="Area"
    palabra18 ="Network"    
    palabra19 ="Internet"//pregunta 6
    palabra20 ="Engineering"
    palabra21 ="Task"
    palabra22 ="Force"
    word1= palabra0+' '+palabra1+' '+palabra2+' '
    word2= palabra3+' '+palabra4+' '+palabra5+' '+palabra6+' '
    word3= palabra7+' '+palabra8+' '+palabra9+' '+palabra10+' '
    word4= palabra11+' '+palabra12+' '+palabra13+' '+palabra14+' '
    word5= palabra15+' '+palabra16+' '+palabra17+' '+palabra18+' '
    word6= palabra19+' '+palabra20+' '+palabra21+' '+palabra22+' '
    numPreg = 22;
    boxPosition = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];
    answers = [
      palabra0,
      palabra1,
      palabra2,
      palabra3,
      palabra4,
      palabra5,
      palabra6,
      palabra7,
      palabra8,
      palabra9,
      palabra10,
      palabra11,
      palabra12,
      palabra13,
      palabra14,
      palabra15,
      palabra16,
      palabra17,
      palabra18,
      palabra19,
      palabra20,
      palabra21,
      palabra22,
    ];
    if(language == 'Eng'){
      $('#q1, #p1').text('1 - Logical system for accessing and searching the information available on the Internet, whose information units are web pages.');
      $('#q2, #p2').text('2 - It is an organization for the creation of international standards made up of various national standardization organizations.');
      $('#q3, #p3').text('3 - It is a kind of technology that allows connection to the Internet through the use of the traditional telephone line, transmitting the digital information in an analog way through the symmetrical copper pairs cable.');
      $('#q4, #p4').text("4 - It is a protocol that allows a computer connected to a network to obtain its configuration.");
      $('#q5, #p5').text('5 - It is a local type network whose computers do not need to be linked through cables to connect.');
      $('#q6, #p6').text('6 - It is an open international standardization organization, which aims to contribute to Internet engineering, known as the IETF.');   
  
    }else if(language == 'Por'){
      $('#q1, #p1').text('1 - Sistema lógico para acessar e pesquisar as informações disponíveis na Internet, cujas unidades de informação são páginas da web.');
      $('#q2, #p2').text('2 - É uma organização para a criação de padrões internacionais compostos por várias organizações nacionais de padronização.');
      $('#q3, #p3').text('3 - É um tipo de tecnologia que permite a conexão à Internet através do uso da linha telefônica tradicional, transmitindo as informações digitais de forma analógica através do cabo simétrico de pares de cobre.');
      $('#q4, #p4').text('4 - É um protocolo que permite que um computador conectado a uma rede obtenha sua configuração.');
      $('#q5, #p5').text('5 - É uma rede local, cujos computadores não precisam ser conectados através de cabos para se conectar.');
      $('#q6, #p6').text('6 - É uma organização internacional de padronização aberta, que visa contribuir com a engenharia da Internet, conhecida como IETF.');
  
    }else{
      $('#q1, #p1').text('1 - Sistema lógico de acceso y búsqueda de la información disponible en Internet, cuyas unidades informativas son las páginas web.');
      $('#q2, #p2').text('2 - Es una organización para la creación de estándares internacionales compuesta por diversas organizaciones nacionales de normalización.');
      $('#q3, #p3').text('3 - Es una clase de tecnología que permite la conexión a Internet mediante el uso de la línea telefónica tradicional, transmitiendo la información digital de modo analógico a través del cable de pares simétricos de cobre.');
      $('#q4, #p4').text('4 - Es un protocolo que permite que un equipo conectado a una red pueda obtener su configuración.');
      $('#q5, #p5').text('5 - Es una red de tipo local cuyos equipos no necesitan estar vinculados a través de cables para conectarse.');
      $('#q6, #p6').text('6 - Es una organización internacional abierta de normalización, que tiene como objetivos el contribuir a la ingeniería de Internet, conocida como IETF.');   
      
    }
    //fin respuestas nivel 5
  }

}
function loadLVL(){
  if(localStorage.getItem('nextLvl')=='1'){  
    $('#ModalTuto').removeClass('mostrar');      
    $('#levelModal, .modal-backdrop, #ModalTuto').addClass('oculto'); 
    Level2() 
  }
  if(localStorage.getItem('nextLvl')=='2'){  
    $('#ModalTuto').removeClass('mostrar');    
    $('#levelModal, .modal-backdrop, #ModalTuto').addClass('oculto'); 
    Level3()
  } 
  if(localStorage.getItem('nextLvl')=='3'){  
    $('#ModalTuto').removeClass('mostrar');    
    $('#levelModal, .modal-backdrop, #ModalTuto').addClass('oculto'); 
    Level4()
  } 
  if(localStorage.getItem('nextLvl')=='4'){  
    $('#ModalTuto').removeClass('mostrar');    
    $('#levelModal, .modal-backdrop, #ModalTuto').addClass('oculto'); 
    Level5()
  } 
  localStorage.setItem('nextLvl', '0')
}
function nextLvl(n){
  if(n==1){
    localStorage.setItem('nextLvl', '1');
    reloadJuego()
  }
  if(n==2){
    localStorage.setItem('nextLvl', '2');
    reloadJuego()
  } 
  if(n==3){
    localStorage.setItem('nextLvl', '3');
    reloadJuego()
  }
  if(n==4){
    localStorage.setItem('nextLvl', '4');
    reloadJuego()
  } 
}
function reloadJuego(){
  location.reload()
}
function titleLvl(){
	$('#lvltxt').addClass('animated slideInRight');
  $('#lvltxt').removeAttr('style');
  $('#lvlN').text(nivel);
	setTimeout(function() { 
		$('#lvltxt').removeClass('animated slideInRight');
		$('#lvltxt').addClass('animated slideOutLeft');
  }, 1500);  
}
function Level1(){
  nivel= 1;
  titleLvl()
    var elemClic= document.getElementById('nextLVL')
	  elemClic.setAttribute("onclick","nextLvl(1);");
     if ($('#cmbTime').val()==''){
			timeLevel = 120;
			$('#cmbTime').val(timeLevel);
		}
		 else
      timeLevel = parseInt($('#cmbTime').val());
    selecNivel()
    Timer();     
    restAnswers();
    input();
}
function Level2(){
  nivel= 2;
  titleLvl()
  var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl(2);");
  if ($('#cmbTime').val()==''){
   timeLevel = 130;
   $('#cmbTime').val(timeLevel);
 }
  else
   timeLevel = parseInt($('#cmbTime').val());
  selecNivel()
  Timer();     
  restAnswers();
  input();
}
function Level3(){
  nivel= 3;
  titleLvl()
  var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl(3);");
  if ($('#cmbTime').val()==''){
  timeLevel = 140;
  $('#cmbTime').val(timeLevel);
}
  else
  timeLevel = parseInt($('#cmbTime').val());
  selecNivel()
  Timer();
  restAnswers();
  input();
}
function Level4(){
  nivel= 4;
  titleLvl()
  var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl(4);");
  if ($('#cmbTime').val()==''){
  timeLevel = 140;
  $('#cmbTime').val(timeLevel);
}
  else
  timeLevel = parseInt($('#cmbTime').val());
  selecNivel()
  Timer();
  restAnswers();
  input();
}
function Level5(){
  nivel= 5;
  titleLvl()
  if ($('#cmbTime').val()==''){
  timeLevel = 180;
  $('#cmbTime').val(timeLevel);
}
  else
  timeLevel = parseInt($('#cmbTime').val());
  selecNivel()
  Timer();
  $('#btnNext').css({'display':'none'})    
  restAnswers();
  input();
}
//Determina el Nivel en que se va a iniciar
function Level(){
  $('#level1').click(function(){
    Level1()
  });
  $('#level2').click(function(){
    Level2()
  });
  $('#level3').click(function(){
    Level3()
  });
  $('#level4').click(function(){
    Level4()
  });
  $('#level5').click(function(){
    Level5()
  });
}
function configuration(){

  var ruta = "https://angelgame.acostasite.com/ApiAngel/Api";

  return ruta;
}
//para dar los puntos y estrellas
function EndGame(){
   if(nivel==1){
     if ($('#contratiempo').text()>55) {
         punto = punto + 100;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('ConceptoStarlvl1', '3'); 
     } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
         punto = punto + 60;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starlvl1 <= 1){
           window.localStorage.setItem('ConceptoStarlvl1', '2'); 
         }        
     } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=30) {
         punto = punto + 40;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starlvl1 <= 1){
           window.localStorage.setItem('ConceptoStarlvl1', '2'); 
         }
       } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
         punto = punto + 20;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starlvl1 == 0){
           window.localStorage.setItem('ConceptoStarlvl1', '1'); 
         }
     }
     contStar()
   } 
   if(nivel==2){
     if ($('#contratiempo').text()>60) {
         punto = punto + 200;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('ConceptoStarlvl2', '3'); 
     } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
         punto = punto + 100;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starlvl2 <= 1){
           window.localStorage.setItem('ConceptoStarlvl2', '2'); 
         }
     } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
         punto = punto + 80;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starlvl2 <= 1){
           window.localStorage.setItem('ConceptoStarlvl2', '2'); 
         }
     } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
         punto = punto + 60;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starlvl2 == 0){
           window.localStorage.setItem('ConceptoStarlvl2', '1'); 
         }
     } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
         punto = punto + 40;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starlvl2 == 0){
           window.localStorage.setItem('ConceptoStarlvl2', '1'); 
         }
       } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
         punto = punto + 20;
         $('.punto').text(punto);
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(5);
         count(5);
     }   
     contStar()
   }
   if(nivel==3){  
     if ($('#contratiempo').text()>80) {
         punto = punto + 200;
         $('.punto').text(punto);
         // estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('ConceptoStarlvl3', '3'); 

     } else if ($('#contratiempo').text()>70 && $('#contratiempo').text()<=80) {
         punto = punto + 150;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('ConceptoStarlvl3', '3'); 
     } else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=70) {
         punto = punto + 120;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starlvl3 <= 1){
           window.localStorage.setItem('ConceptoStarlvl3', '2'); 
         }
     } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
         punto = punto + 100;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starlvl3 <= 1){
           window.localStorage.setItem('ConceptoStarlvl3', '2'); 
         }
     } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
         punto = punto + 80;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starlvl3 == 0){
           window.localStorage.setItem('ConceptoStarlvl3', '1'); 
         }
     } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
         punto = punto + 60;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starlvl3 == 0){
           window.localStorage.setItem('ConceptoStarlvl3', '1'); 
         }
     } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
         punto = punto + 40;
         $('.punto').text(punto);
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(5);
         count(5);

       } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
         punto = punto + 20;
         $('.punto').text(punto);
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(5);
         count(5);
     }
     contStar()
   }
   if(nivel==4){  
    if ($('#contratiempo').text()>80) {
        punto = punto + 200;
        $('.punto').text(punto);
        // estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.winstar3').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(25);
        count(25);
        window.localStorage.setItem('ConceptoStarlvl4', '3'); 

    } else if ($('#contratiempo').text()>70 && $('#contratiempo').text()<=80) {
        punto = punto + 150;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.winstar3').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(25);
        count(25);
        window.localStorage.setItem('ConceptoStarlvl4', '3'); 
    } else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=70) {
        punto = punto + 120;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(20);
        count(20);
        if(starlvl4 <= 1){
          window.localStorage.setItem('ConceptoStarlvl4', '2'); 
        }
    } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
        punto = punto + 100;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(20);
        count(20);
        if(starlvl4 <= 1){
          window.localStorage.setItem('ConceptoStarlvl4', '2'); 
        }
    } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
        punto = punto + 80;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(15);
        count(15);
        if(starlvl4 == 0){
          window.localStorage.setItem('ConceptoStarlvl4', '1'); 
        }
    } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
        punto = punto + 60;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(15);
        count(15);
        if(starlvl4 == 0){
          window.localStorage.setItem('ConceptoStarlvl4', '1'); 
        }
    } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
        punto = punto + 40;
        $('.punto').text(punto);
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(5);
        count(5);

    } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
        punto = punto + 20;
        $('.punto').text(punto);
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(5);
        count(5);
    }
    contStar()
   }
   if(nivel==5){  
    if ($('#contratiempo').text()>80) {
        punto = punto + 200;
        $('.punto').text(punto);
        // estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.winstar3').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(25);
        count(25);
        window.localStorage.setItem('ConceptoStarlvl5', '3'); 

    } else if ($('#contratiempo').text()>70 && $('#contratiempo').text()<=80) {
        punto = punto + 150;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.winstar3').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(25);
        count(25);
        window.localStorage.setItem('ConceptoStarlvl5', '3'); 
    } else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=70) {
        punto = punto + 120;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(20);
        count(20);
        if(starlvl5 <= 1){
          window.localStorage.setItem('ConceptoStarlvl5', '2'); 
        }
    } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
        punto = punto + 100;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.winstar2').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(20);
        count(20);
        if(starlvl5 <= 1){
          window.localStorage.setItem('ConceptoStarlvl5', '2'); 
        }
    } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
        punto = punto + 80;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(15);
        count(15);
        if(starlvl5 == 0){
          window.localStorage.setItem('ConceptoStarlvl5', '1'); 
        }
    } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
        punto = punto + 60;
        $('.punto').text(punto);
// estrellas// 
        $('.winstar1').css({'display':'block'});
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(15);
        count(15);
        if(starlvl5 == 0){
          window.localStorage.setItem('ConceptoStarlvl5', '1'); 
        }
    } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
        punto = punto + 40;
        $('.punto').text(punto);
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(5);
        count(5);

    } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
        punto = punto + 20;
        $('.punto').text(punto);
        $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
        contCoin(5);
        count(5);
    }
    contStar()
   }
   //para controlar textFont
   var computedFontSize = window.getComputedStyle(document.getElementById("levelModalLabel")).fontSize;
   var fontSz= parseFloat(computedFontSize)
   console.log('fontSz=',fontSz)
   if(fontSz>=32 && fontSz<=34 ){
    console.log('entrooooo=',fontSz)
     $('.tmp , #ttl , #coin, .score').css({//15px
       'font-size': '11px',
     })    
     $('.lvlTitle, #LooseModal h4').css({//18px
       'font-size':'14px',
     })  
     $('#levelModalLabel').css({//26px
       'font-size':'20px',
     }) 
     $('.titAviso, .head, .post').css({//16px
       'font-size':'12px',
     })
     $('#lvltxt').css({
       'font-size':'43px',
     }) 
     $('#LooseModal h2').css({
       'font-size':'30px',
     }) 
     
   }else if(fontSz>=46 && fontSz<=47 ){
     $('.lvlTitle').css({
       'font-size':'9px',
     })  
   }
}
//block cuando la vida esta en 0
function blockTTL(){
  $('#levelModal').modal('hide');
  $('#TTLModal').modal('show');
}
//sumar coin
function contCoin(n){
	var sumaCoin= n + coin
	window.localStorage.setItem('coin', sumaCoin); 
}
//efecto contador
function count(n){
	var counter = { var: 0 };
	var contador = TweenMax.to(counter, 4, {
	  var: n, 
	  onUpdate: function () {
		var number = Math.ceil(counter.var);
		$('#GetCoin').html(number);
		if(number === counter.var){ 
			contador.kill(); 
		}
	  },
	  ease:Circ.easeOut
	});
}
function contStar(){
	var starlvl1c = Number(localStorage.getItem('ConceptoStarlvl1'));
	var starlvl2c = Number(localStorage.getItem('ConceptoStarlvl2'));
  var starlvl3c = Number(localStorage.getItem('ConceptoStarlvl3'));
  var starlvl4c = Number(localStorage.getItem('ConceptoStarlvl4'));
  var starlvl5c = Number(localStorage.getItem('ConceptoStarlvl5'));
	ContStar = starlvl1c + starlvl2c + starlvl3c + starlvl4c + starlvl5c;
	//   console.log('starlvl1c =', starlvl1c );
	//  console.log('starlvl2c =', starlvl2c );
	//   console.log('starlvl3c =', starlvl3c );
	//  console.log('sumando contador=', ContStar)
  window.localStorage.setItem('contStarconcepto', ContStar); 
  $('#contratiempo').css({
		'color':'transparent'
	})
}
// block botones
function blkBtn(wbtn){
  if(wbtn == 'w1'){
    block = w1
  }else if(wbtn == 'w2'){
    block = w2
  }else if(wbtn == 'w3'){
    block = w3
  }else if(wbtn == 'w4'){
    block = w4
  }else if(wbtn == 'w5'){
    block = w5
  }else if(wbtn == 'w6'){
    block = w6
  }
  if(block == 'correcto'){
    for (var i = 0; i <= numPreg; i++){    
      $('#block'+i).css({'display':'block'});
    }
  }else{
    for (var i = 0; i <= numPreg; i++){    
      $('#block'+i).removeAttr('style');
    }
  } 
}
//validar respuesta
function validar(word){
  if(nivel == 1){
    if(word == word1){    
      $('#respuesta1').css({'background':'#4ee84e'});
      $('#btn0 , #btn1').prop('disabled', true);
      $('#btn0 , #btn1').addClass('disableG animated fadeOut');
      $('#c1').css({'display':'block'});
      $('#respuesta1').prop("disabled", true);
      $('#btnr1').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr1').addClass('dispNone');
      }, 500);    
      window.localStorage.setItem('resp1', word);
      $('#respuesta1').val(word);
      w1='correcto'
      blkBtn('w1')
      $('#v1').click()
        punto = punto + 30;
        $('#score').text(punto);
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word2){
      $('#respuesta2').css({'background':'#4ee84e'});
      $('#btn2 , #btn3').prop('disabled', true);
      $('#btn2 , #btn3').addClass('disableG animated fadeOut');
      $('#c2').css({'display':'block'});
      $('#respuesta2').prop("disabled", true);    
      $('#btnr2').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr2').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp2', word); 
      $('#respuesta2').val(word);
      w2='correcto'   
      blkBtn('w2')
      $('#v2').click()
      punto = punto + 30;
      $('#score').text(punto);
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word3){
      $('#respuesta3').css({'background':'#4ee84e'});
      $('#btn4 , #btn5').prop('disabled', true);
      $('#btn4 , #btn5').addClass('disableG animated fadeOut');
      $('#c3').css({'display':'block'});
      $('#respuesta3').prop("disabled", true);
      $('#btnr3').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr3').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp3', word); 
      $('#respuesta3').val(word);
      w3='correcto' 
      blkBtn('w3')
      $('#v3').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word4){
      $('#respuesta4').css({'background':'#4ee84e'});
      $('#btn6 , #btn7').prop('disabled', true);
      $('#btn6 , #btn7').addClass('disableG animated fadeOut');
      $('#c4').css({'display':'block'});
      $('#respuesta4').prop("disabled", true);
      $('#btnr4').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr4').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp4', word);  
      $('#respuesta4').val(word);  
      w4='correcto'
      blkBtn('w4')
      $('#v4').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word5){
      $('#respuesta5').css({'background':'#4ee84e'});
      $('#btn8 , #btn9, #btn10').prop('disabled', true);
      $('#btn8 , #btn9, #btn10').addClass('disableG animated fadeOut');
      $('#c5').css({'display':'block'});
      $('#respuesta5').prop("disabled", true); 
      $('#btnr5').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr5').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp5', word);
      $('#respuesta5').val(word);
      w5='correcto' 
      blkBtn('w5')
      $('#v5').click() 
      punto = punto + 40;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word6){
      $('#respuesta6').css({'background':'#4ee84e'});
      $('#btn11 , #btn12 , #btn13, #btn14').prop('disabled', true);
      $('#btn11 , #btn12 , #btn13, #btn14').addClass('disableG animated fadeOut');
      $('#c6').css({'display':'block'});
      $('#respuesta6').prop("disabled", true);
      $('#btnr6').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr6').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp6', word); 
      $('#respuesta6').val(word);
      w6='correcto'
      blkBtn('w6')
      $('#v6').click() 
      punto = punto + 50;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
  }
  if(nivel == 2){
    if(word == word1){    
      $('#respuesta1').css({'background':'#4ee84e'});
      $('#btn0 , #btn1').prop('disabled', true);
      $('#btn0 , #btn1').addClass('disableG animated fadeOut');
      $('#c1').css({'display':'block'});
      $('#respuesta1').prop("disabled", true);
      $('#btnr1').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr1').addClass('dispNone');
      }, 500);    
      window.localStorage.setItem('resp1', word);
      $('#respuesta1').val(word);
      w1='correcto'
      blkBtn('w1')
      $('#v1').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word2){
      $('#respuesta2').css({'background':'#4ee84e'});
      $('#btn2 , #btn3').prop('disabled', true);
      $('#btn2 , #btn3').addClass('disableG animated fadeOut');
      $('#c2').css({'display':'block'});
      $('#respuesta2').prop("disabled", true);    
      $('#btnr2').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr2').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp2', word); 
      $('#respuesta2').val(word);
      w2='correcto'   
      blkBtn('w2')
      $('#v2').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word3){
      $('#respuesta3').css({'background':'#4ee84e'});
      $('#btn4 , #btn5 , #btn6').prop('disabled', true);
      $('#btn4 , #btn5 , #btn6').addClass('disableG animated fadeOut');
      $('#c3').css({'display':'block'});
      $('#respuesta3').prop("disabled", true);
      $('#btnr3').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr3').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp3', word); 
      $('#respuesta3').val(word);
      w3='correcto' 
      blkBtn('w3')
      $('#v3').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word4){
      $('#respuesta4').css({'background':'#4ee84e'});
      $('#btn7 , #btn8 , #btn9').prop('disabled', true);
      $('#btn7 , #btn8 , #btn9').addClass('disableG animated fadeOut');
      $('#c4').css({'display':'block'});
      $('#respuesta4').prop("disabled", true);
      $('#btnr4').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr4').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp4', word);  
      $('#respuesta4').val(word);  
      w4='correcto'
      blkBtn('w4')
      $('#v4').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word5){
      $('#respuesta5').css({'background':'#4ee84e'});
      $('#btn10 , #btn11 , #btn12').prop('disabled', true);
      $('#btn10 , #btn11 , #btn12').addClass('disableG animated fadeOut');
      $('#c5').css({'display':'block'});
      $('#respuesta5').prop("disabled", true); 
      $('#btnr5').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr5').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp5', word);
      $('#respuesta5').val(word);
      w5='correcto' 
      blkBtn('w5')
      $('#v5').click() 
      punto = punto + 40;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word6){
      $('#respuesta6').css({'background':'#4ee84e'});
      $('#btn13 , #btn14 , #btn15').prop('disabled', true);
      $('#btn13 , #btn14 , #btn15').addClass('disableG animated fadeOut');
      $('#c6').css({'display':'block'});
      $('#respuesta6').prop("disabled", true);
      $('#btnr6').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr6').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp6', word); 
      $('#respuesta6').val(word);
      w6='correcto'
      blkBtn('w6')
      $('#v6').click() 
      punto = punto + 50;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
  }
  if(nivel == 3){
    if(word == word1){    
      $('#respuesta1').css({'background':'#4ee84e'});
      $('#btn0 , #btn1 , #btn2').prop('disabled', true);
      $('#btn0 , #btn1 , #btn2').addClass('disableG animated fadeOut');
      $('#c1').css({'display':'block'});
      $('#respuesta1').prop("disabled", true);
      $('#btnr1').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr1').addClass('dispNone');
      }, 500);    
      window.localStorage.setItem('resp1', word);
      $('#respuesta1').val(word);
      w1='correcto'
      blkBtn('w1')
      $('#v1').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word2){
      $('#respuesta2').css({'background':'#4ee84e'});
      $('#btn3 , #btn4 , #btn5').prop('disabled', true);
      $('#btn3 , #btn4 , #btn5').addClass('disableG animated fadeOut');
      $('#c2').css({'display':'block'});
      $('#respuesta2').prop("disabled", true);    
      $('#btnr2').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr2').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp2', word); 
      $('#respuesta2').val(word);
      w2='correcto'   
      blkBtn('w2')
      $('#v2').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word3){
      $('#respuesta3').css({'background':'#4ee84e'});
      $('#btn6 , #btn7 , #btn8 , #btn9').prop('disabled', true);
      $('#btn6 , #btn7 , #btn8 , #btn9').addClass('disableG animated fadeOut');
      $('#c3').css({'display':'block'});
      $('#respuesta3').prop("disabled", true);
      $('#btnr3').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr3').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp3', word); 
      $('#respuesta3').val(word);
      w3='correcto' 
      blkBtn('w3')
      $('#v3').click()
      punto = punto + 30;
      $('#score').text(punto);    
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word4){
      $('#respuesta4').css({'background':'#4ee84e'});
      $('#btn10 , #btn11 , #btn12 , #btn13').prop('disabled', true);
      $('#btn10 , #btn11 , #btn12 , #btn13').addClass('disableG animated fadeOut');
      $('#c4').css({'display':'block'});
      $('#respuesta4').prop("disabled", true);
      $('#btnr4').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr4').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp4', word);  
      $('#respuesta4').val(word);  
      w4='correcto'
      blkBtn('w4')
      $('#v4').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word5){
      $('#respuesta5').css({'background':'#4ee84e'});
      $('#btn14 , #btn15 , #btn16 , #btn17').prop('disabled', true);
      $('#btn14 , #btn15 , #btn16 , #btn17').addClass('disableG animated fadeOut');
      $('#c5').css({'display':'block'});
      $('#respuesta5').prop("disabled", true); 
      $('#btnr5').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr5').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp5', word);
      $('#respuesta5').val(word);
      w5='correcto' 
      blkBtn('w5')
      $('#v5').click()
      punto = punto + 40;
      $('#score').text(punto);    
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word6){
      $('#respuesta6').css({'background':'#4ee84e'});
      $('#btn18 , #btn19 , #btn20 , #btn21').prop('disabled', true);
      $('#btn18 , #btn19 , #btn20 , #btn21').addClass('disableG animated fadeOut');
      $('#c6').css({'display':'block'});
      $('#respuesta6').prop("disabled", true);
      $('#btnr6').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr6').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp6', word); 
      $('#respuesta6').val(word);
      w6='correcto'
      blkBtn('w6')
      $('#v6').click()
      punto = punto + 50;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
  }
  if(nivel == 4){
    if(word == word1){    
      $('#respuesta1').css({'background':'#4ee84e'});
      $('#btn0 , #btn1').prop('disabled', true);
      $('#btn0 , #btn1').addClass('disableG animated fadeOut');
      $('#c1').css({'display':'block'});
      $('#respuesta1').prop("disabled", true);
      $('#btnr1').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr1').addClass('dispNone');
      }, 500);    
      window.localStorage.setItem('resp1', word);
      $('#respuesta1').val(word);
      w1='correcto'
      blkBtn('w1')
      $('#v1').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word2){
      $('#respuesta2').css({'background':'#4ee84e'});
      $('#btn2 , #btn3 , #btn4').prop('disabled', true);
      $('#btn2 , #btn3 , #btn4').addClass('disableG animated fadeOut');
      $('#c2').css({'display':'block'});
      $('#respuesta2').prop("disabled", true);    
      $('#btnr2').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr2').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp2', word); 
      $('#respuesta2').val(word);
      w2='correcto'   
      blkBtn('w2')
      $('#v2').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word3){
      $('#respuesta3').css({'background':'#4ee84e'});
      $('#btn5 , #btn6 , #btn7').prop('disabled', true);
      $('#btn5 , #btn6 , #btn7').addClass('disableG animated fadeOut');
      $('#c3').css({'display':'block'});
      $('#respuesta3').prop("disabled", true);
      $('#btnr3').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr3').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp3', word); 
      $('#respuesta3').val(word);
      w3='correcto' 
      blkBtn('w3')
      $('#v3').click()
      punto = punto + 30;
      $('#score').text(punto);    
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word4){
      $('#respuesta4').css({'background':'#4ee84e'});
      $('#btn8 , #btn9 , #btn10').prop('disabled', true);
      $('#btn8 , #btn9 , #btn10').addClass('disableG animated fadeOut');
      $('#c4').css({'display':'block'});
      $('#respuesta4').prop("disabled", true);
      $('#btnr4').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr4').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp4', word);  
      $('#respuesta4').val(word);  
      w4='correcto'
      blkBtn('w4')
      $('#v4').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word5){
      $('#respuesta5').css({'background':'#4ee84e'});
      $('#btn11 , #btn12 , #btn13').prop('disabled', true);
      $('#btn11 , #btn12 , #btn13').addClass('disableG animated fadeOut');
      $('#c5').css({'display':'block'});
      $('#respuesta5').prop("disabled", true); 
      $('#btnr5').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr5').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp5', word);
      $('#respuesta5').val(word);
      w5='correcto' 
      blkBtn('w5')
      $('#v5').click()
      punto = punto + 40;
      $('#score').text(punto);    
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word6){
      $('#respuesta6').css({'background':'#4ee84e'});
      $('#btn14 , #btn15 , #btn16').prop('disabled', true);
      $('#btn14 , #btn15 , #btn16').addClass('disableG animated fadeOut');
      $('#c6').css({'display':'block'});
      $('#respuesta6').prop("disabled", true);
      $('#btnr6').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr6').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp6', word); 
      $('#respuesta6').val(word);
      w6='correcto'
      blkBtn('w6')
      $('#v6').click()
      punto = punto + 50;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
  }
  if(nivel == 5){
    if(word == word1){    
      $('#respuesta1').css({'background':'#4ee84e'});
      $('#btn0 , #btn1 , #btn2').prop('disabled', true);
      $('#btn0 , #btn1 , #btn2').addClass('disableG animated fadeOut');
      $('#c1').css({'display':'block'});
      $('#respuesta1').prop("disabled", true);
      $('#btnr1').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr1').addClass('dispNone');
      }, 500);    
      window.localStorage.setItem('resp1', word);
      $('#respuesta1').val(word);
      w1='correcto'
      blkBtn('w1')
      $('#v1').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word2){
      $('#respuesta2').css({'background':'#4ee84e'});
      $('#btn3 , #btn4 , #btn5 , #btn6').prop('disabled', true);
      $('#btn3 , #btn4 , #btn5 , #btn6').addClass('disableG animated fadeOut');
      $('#c2').css({'display':'block'});
      $('#respuesta2').prop("disabled", true);    
      $('#btnr2').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr2').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp2', word); 
      $('#respuesta2').val(word);
      w2='correcto'   
      blkBtn('w2')
      $('#v2').click()
      punto = punto + 30;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word3){
      $('#respuesta3').css({'background':'#4ee84e'});
      $('#btn7 , #btn8 , #btn9 , #btn10').prop('disabled', true);
      $('#btn7 , #btn8 , #btn9 , #btn10').addClass('disableG animated fadeOut');
      $('#c3').css({'display':'block'});
      $('#respuesta3').prop("disabled", true);
      $('#btnr3').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr3').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp3', word); 
      $('#respuesta3').val(word);
      w3='correcto' 
      blkBtn('w3')
      $('#v3').click()
      punto = punto + 30;
      $('#score').text(punto);    
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word4){
      $('#respuesta4').css({'background':'#4ee84e'});
      $('#btn11 , #btn12 , #btn13 , #btn14').prop('disabled', true);
      $('#btn11 , #btn12 , #btn13 , #btn14').addClass('disableG animated fadeOut');
      $('#c4').css({'display':'block'});
      $('#respuesta4').prop("disabled", true);
      $('#btnr4').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr4').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp4', word);  
      $('#respuesta4').val(word);  
      w4='correcto'
      blkBtn('w4')
      $('#v4').click()
      punto = punto + 30;
      $('#score').text(punto);  
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word5){
      $('#respuesta5').css({'background':'#4ee84e'});
      $('#btn15 , #btn16 , #btn17 , #btn18').prop('disabled', true);
      $('#btn15 , #btn16 , #btn17 , #btn18').addClass('disableG animated fadeOut');
      $('#c5').css({'display':'block'});
      $('#respuesta5').prop("disabled", true); 
      $('#btnr5').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr5').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp5', word);
      $('#respuesta5').val(word);
      w5='correcto' 
      blkBtn('w5')
      $('#v5').click()
      punto = punto + 40;
      $('#score').text(punto);    
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
    if(word == word6){
      $('#respuesta6').css({'background':'#4ee84e'});
      $('#btn19 , #btn20 , #btn21 , #btn22').prop('disabled', true);
      $('#btn19 , #btn20 , #btn21 , #btn22').addClass('disableG animated fadeOut');
      $('#c6').css({'display':'block'});
      $('#respuesta6').prop("disabled", true);
      $('#btnr6').addClass('animated fadeOut');
      setTimeout(function() { 
        $('#btnr6').addClass('dispNone');
      }, 500);  
      window.localStorage.setItem('resp6', word); 
      $('#respuesta6').val(word);
      w6='correcto'
      blkBtn('w6')
      $('#v6').click()
      punto = punto + 50;
      $('#score').text(punto);   
      EffectMusic(window.localStorage.getItem('#sound-acert'));
    }
  }
  if(w1 =="correcto" && w2 == "correcto" && w3 == "correcto" && w4 == "correcto" && w5 == "correcto" && w6 == "correcto" ){
    $('#WinModal').modal('show');
    EndGame();// llamado a la funcion del puntaje
    fin=true;
    Timer();
  }
}
// escribe en el input
function bwrite(word ,n){
  totalVal = window.localStorage.getItem('resp'+numInput); 
  if(totalVal.split(' ').length <= 4){
    totalVal = totalVal + word;
    $('#btn'+n).css({'background':'#0b2339'});
    $('#block'+n).css({'display':'block'});
    $('#respuesta'+numInput).val(totalVal);
    window.localStorage.setItem('resp'+numInput, totalVal); 
    validar(totalVal, n); 
  }
}
// borra palabras del input
function borrarPalabra(){
  totalVal = '';
  $('#respuesta'+numInput).val(totalVal)
  window.localStorage.setItem('resp'+numInput, totalVal);
  for (var i = 0; i< answers.length; i++){
    $('#block'+i).removeAttr('style');
    $('#btn'+i).removeAttr('style');
  }
}
//Conteo regresivo
function Timer(){
    //Conteo Regresivo
  var seconds_left = timeLevel;
  // var minutes_left = 0;

  var interval = setInterval(function() {

  document.getElementById('contratiempo').innerHTML =  --seconds_left;

  if (document.getElementById('contratiempo').innerHTML == 1 ) {
    // const
    // $objetivo =  document.body, // A qué le tomamos la foto
    // $contenedorCanvas = document.querySelector("#contenedorCanvas"); // En dónde ponemos el elemento canvas

    // html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
    //   .then(function(canvas) {
    //   // Cuando se resuelva la promesa traerá el canvas
    //   $contenedorCanvas.appendChild(canvas);// Lo agregamos como hijo del div
    //   });
    }

  if (fin==false) {
    if (seconds_left <= 0){
      // document.getElementById('contratiempo').innerHTML = "Se acabo el tiempo";
     $('#LooseModal').modal('show')
     $('.punto').text(punto);
      clearInterval(interval);
      ////location.reload();
     // $( "#pregunta0,#pregunta1, #pregunta2, #pregunta3, #pregunta4, #pregunta5, #pregunta6, #pregunta7, #pregunta8, #pregunta9, #pregunta10, #pregunta11, #pregunta12, #pregunta13" ).draggable({ disabled: true });
   //  //****** Elimina las cajas de respuesta al culminar el tiempo
      $( "#pregunta0, #pregunta1, #pregunta2, #pregunta3, #pregunta4, #pregunta5, #pregunta6, #pregunta7, #pregunta8, #pregunta9, #pregunta10, #pregunta11, #pregunta12, #pregunta13, #pregunta14, #pregunta15, #pregunta16, #pregunta17, #pregunta19, #pregunta20, #pregunta21, #pregunta22" ).remove();

      ShareScore();
      ttl--;
	    window.localStorage.setItem('ttl', ttl); 
  }
  }
  }, 1000);
}
// Funcion que inicia el juego en el nivel avanzado
function ShareScore(){
    if (localStorage.getItem("UserId")!=null){
    var infogame = { "UserId":localStorage.getItem("UserId"), "GameId":6, "Basic Concepts" :1, "levelId" :nivel,"Score":punto};

    RegisterGame(infogame);

    public_FB();
  }
}
function public_TW(){

  var level='';

  switch(nivel) {
      case 1:

      level='EASY';
      break;
    case 2:

      level='MEDIUM';

      break;

    case 3:
              
      level='HARD';

      break;
  }

  var msj="GAME: BASIC CONCEPTS   TOPIC:1  NEVEL: "+level+" POINTS: "+punto;

  var src='https://angelgame.acostasite.com/Game/img/drag_img.png';

  window.open("https://twitter.com/share?text="+msj+"&url=https://angelgame.acostasite.com/Game/juegos/conceptos/index.html");
}
function public_FB(){

  var level='';

  switch(nivel) {
      case 1:

      level='EASY';
      break;
    case 2:

      level='MEDIUM';

      break;

    case 3:
              
      level='HARD';

      break;
  }

  var msj="GAME: BASIC CONCEPTS   TOPIC:1  NEVEL: "+level+" POINTS: "+punto;

  $(".fb-xfbml-parse-ignore").attr("href","https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Fjuegos%2Fconceptos%2Findex-es.html?description="+msj+"&method=1&amp;src=sdkpreparse");
}
function reload(){

  localStorage.setItem('intentar', nivel);

  location.reload();
}
var dataInstag;
function public_IG(){
	console.log("entro en instagram");
	var level='';
	switch(nivel) {
	case 6:

		level='EASY';
		break;
	case 8:

		level='MEDIUM';

		break;

	case 12:
				
		level='HARD';

		break;
}

  var msj="GAME: BASIC CONCEPTS   TOPIC:1  NEVEL: "+level+" POINTS: "+punto;
	/*window.plugins.socialsharing.shareViaInstagram(
		'Message via Instagram', 
		'https://angelgame.acostasite.com/images/icon/icon.png','https://angelgame.acostasite.com'
	  );	*/
	 // var assetLocalIdentifier = "../../img/congratulations.png";
	 Instagram.isInstalled(function (err, installed) {
		if (installed) {
			console.log("Instagram is"+ installed); // installed app version on Android
			navigator.screenshot.save(function(error,response){
				if(error){
					console.error(error);
					return;
				}
				
				// Something like: /storage/emulated/0/Pictures/screenshot_1477924039236.jpg
				console.log(response.filePath);
		
				/*Instagram.shareAsset(function(result) {
					alert('Instagram.shareAsset success: ' + result);
				}, function(e) {
					alert('Instagram.shareAsset error: ' + e);
				}, response.filePath);*/
				getBase64FromImageUrl(response.filePath, msj);
				
			});
		} else {
			alert("Instagram no esta instalado");
		}
	});

	
	 

/*	module.controller('ThisCtrl', function($scope, $cordovaInstagram) {
		// Get image from camera, base64 is good. See the
		// $cordovaCamera docs for more info
	  
		$cordovaInstagram.share($scope.image.data, $scope.image.caption).then(function() {
		  // Worked
		}, function(err) {
		  // Didn't work
		});
	  })*/
}
function getBase64FromImageUrl(url, msj) {
	var img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.width =this.width;
		canvas.height =this.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(this, 0, 0);

		var dataURL = canvas.toDataURL("image/png");
		dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		
		Instagram.share(dataInstag, msj, function (err) {
			if (err) {
				console.log("Not shared");
			} else {
				console.log("shared");
			}
		});
	};

	img.src = url;
}
function Zoom(src){
	console.log("zoom", src)
	$("#imgZoom").attr('src', src);
	$("#ModalZoom").css({"display": "block"});
	document.getElementById("imgZoom").className ="imgZm animated fadeInUp slow"; 
}
function cerrarZoom(){
	console.log("Cerrar Zoom")
	$("#ModalZoom").css({"display": "none"});
}