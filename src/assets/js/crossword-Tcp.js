
// window.localStorage.setItem('leveltcp', '1');

var AllCruciStarIpv4 = Number(window.localStorage.getItem('AllCruciStarIpv4'));
var AllCruciStarIp2v6 = Number(window.localStorage.getItem('AllCruciStarIp2v6'));
var AllCruciStarGover = Number(window.localStorage.getItem('AllCruciStarGover'));
var AllCruciStarTcp = Number(window.localStorage.getItem('AllCruciStarTcp'));
var CruciStarTcpLvl1 = Number(window.localStorage.getItem('CruciStarTcpLvl1'));
var CruciStarTcpLvl2 = Number(window.localStorage.getItem('CruciStarTcpLvl2'));
var CruciStarTcpLvl3 = Number(window.localStorage.getItem('CruciStarTcpLvl3'));
var CruciStarTcpLvl4 = Number(window.localStorage.getItem('CruciStarTcpLvl4'));
var CruciStarTcpLvl5 = Number(window.localStorage.getItem('CruciStarTcpLvl5'));
var audioElement = document.createElement('audio');
var Onanimated = localStorage.getItem('Animated');
var ttl =  Number(window.localStorage.getItem('ttl'));
var coin = Number(window.localStorage.getItem('coin'));
var language = localStorage.getItem('selectedLang');
var leveltcp = localStorage.getItem('leveltcp');
var levelActual="";
var respuesta1 ='';
var respuesta2 ='';
var respuesta3 ='';
var respuesta4 ='';
var respuesta5 ='';
var respuesta6 ='';
var respuesta7 ='';
var respuesta8 ='';
var respuesta9 ='';
var respuesta10 ='';
var respuesta11 ='';
var respuesta12 ='';
var respuesta13 ='';
var respuesta14 ='';
var respuesta15 ='';
var respuesta16 ='';
var conjuntoPalabra;
var aviso = "Necesita 35 <img src='imgs/coin50.png' alt='' class='coin'> Para utilzar este comodín"
var finish ="Has completado todos los niveles del crucigramas TCP"
var lang =''
if(this.language == 'Eng'){
	aviso= "You need 35 <img src='imgs/coin50.png' alt='' class='coin'> to use this wildcard";
	finish ="You have completed all levels of the TCP crossword puzzle"
	lang='-en'
}else if(this.language == 'Por'){
	aviso= "Você precisa de 35 <img src='imgs/coin50.png' alt='' class='coin'> para usar este curinga";
	finish ="Você concluiu todos os níveis das palavras cruzadas do TCP"
	lang='-por'
}
if(CruciStarTcpLvl1 == null){
	window.localStorage.setItem('CruciStarTcpLvl1', '0');
}
if(CruciStarTcpLvl2 == null){
	window.localStorage.setItem('CruciStarTcpLvl2', '0');
}
if(CruciStarTcpLvl3 == null){
	window.localStorage.setItem('CruciStarTcpLvl3', '0');
}
if(CruciStarTcpLvl4== null){
	window.localStorage.setItem('CruciStarTcpLvl4', '0');
}
if(CruciStarTcpLvl5 == null){
	window.localStorage.setItem('CruciStarTcpLvl5', '0');
}

$(document).ready(function(){
	$('#ttl').text(ttl);
	$('#coin').text(coin);
	$('#nextLVL').css({'display':'none'});
	var elembacklose= document.getElementById('btnbacklose')
	elembacklose.setAttribute("href","crucigramaMenu"+lang+".html");
	var elembackwin= document.getElementById('btnbackwin')
	elembackwin.setAttribute("href","crucigramaMenu"+lang+".html");
	var elembacklose= document.getElementById('urlNext')
	elembacklose.setAttribute("onclick","siguiente();");
	if(window.localStorage.getItem('leveltcp')=='5'){
		$('#urlNext').css({'display':'none'})
	}
	PlayMusic(window.localStorage.getItem('audio'));
	if(ttl == 0){
		contadorMinutos();
		blockTTL();			
	}else if(ttl <= 4){
		contadorMinutos();
	}
	if(ttl > 0){
		$('#lvl'+leveltcp).removeAttr('style');
		levelActual = leveltcp;
		$('#lvlN').text(leveltcp);
		$('#lvltxt').addClass('animated slideInRight');
		$('#lvltxt').removeAttr('style');
		setTimeout(function() { 
		$('#lvltxt').removeClass('animated slideInRight');
		$('#lvltxt').addClass('animated slideOutLeft');
		}, 1500);
		 Timer();
	}
	validar();

	$('input').click(function(){
		$(this).addClass('td_input');
		})
		$('input').focusout(function(){
    		$(this).removeClass('td_input');
    	});

    $('input').focus(function(){

    		if($(this).val()!=''){

    			$(this).val('');
    		}
	});

	$("#input-0-15").focus(function(){

    	ShowQuestion(1);
    });

    $("#input-2-3").focus(function(){

    	ShowQuestion(2);
    });

    $("#input-3-14").focus(function(){

    	ShowQuestion(3);
    });

    $("#input-4-5").focus(function(){

    	ShowQuestion(4);
    });

    $("#input-4-10").focus(function(){

    	ShowQuestion(5);
    });

    $("#input-5-12").focus(function(){

    	ShowQuestion(6);
    });

    $("#input-5-2").focus(function(){

    	ShowQuestion(7);
    });

    $("#input-5-7").focus(function(){

    	ShowQuestion(8);
    });

    $("#input-6-6").focus(function(){

    	ShowQuestion(9);
    });

    $("#input-7-0").focus(function(){

    	ShowQuestion(10);
    });

     $("#input-9-3").focus(function(){

    	ShowQuestion(11);
    });

     $("#input-9-9").focus(function(){

    	ShowQuestion(12);
    });

     $("#input-10-10").focus(function(){

    	ShowQuestion(13);
    });

     $("#input-10-13").focus(function(){

    	ShowQuestion(14);
    });

     $("#input-13-0").focus(function(){

    	ShowQuestion(15);
    });

     $("#input-13-7").focus(function(){

    	ShowQuestion(16);
    });

     $("#input-15-8").focus(function(){

    	ShowQuestion(17);
    });    
})

var actual='';
//usada nivel 1 (p1)
var word1h = 'tcp';
//usada nivel 1 (p2)
var word1v = 'udp';
//usada nivel 2 (p1)
var word3h = 'osi';
//usada nivel 2 (p2)
var word3v = 'internet';
//usada nivel 2 (p3)
var word5h = 'intranet';
//usada nivel 3 (p1)
var word11h = 'icmp';
//usada nivel 3 (p2)
var word11v = 'arp';
//usada nivel 3 (p3)
var word15h = 'datagrama';
//usada nivel 4 (p1)
var word2v = 'management';
//usada nivel 4 (p2)
var word7h = 'transfer';
//usada nivel 4 (p3)
var word7v = 'mail';
//usada nivel 4 (p4)
var word10h = 'address';
//usada nivel 5
var word4v = 'fragmentation';
//usada nivel 5
var word6v = 'checksum';
//usada nivel 5
var word8v = 'push';
//usada nivel 5
var word9h = 'socket';

var punto=0;
var fin= false;
var timeLevel = 150;
function PlayMusic(active){
	if (active == "1"){
		audioElement.setAttribute('id', 'sonido');
		audioElement.setAttribute('src', 'sounds/world3.mp3');
    	audioElement.setAttribute('autoplay', 'autoplay');
    	audioElement.addEventListener("ended", function() {
	        this.currentTime = 0;
	        this.play();
	    }, false);
	}
	else {
		audioElement.pause();
		active=0;
	}
}
function blockTTL(){
	$('#levelModal').modal('hide');
	$('#TTLModal').modal('show');
}
function screenshot(){
	$('.efecv').css({'display':'none'});	
	$('.efecv2').css({'display':'none'});	
	$('.efech').css({'display':'none'});	
	$('.efech2').css({'display':'none'});
	$('#lvltxt').css({'display':'none'});
	//para tomar la foto
	const
    $objetivo =  document.body, // A qué le tomamos la foto
    $contenedorCanvas = document.querySelector("#contenedorCanvas2"); // En dónde ponemos el elemento canvas
    html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
      .then(function(canvas) {
      // Cuando se resuelva la promesa traerá el canvas
      $contenedorCanvas.appendChild(canvas);// Lo agregamos como hijo del div
      });    
    console.log("tomada la foto");
}
function EndGame(nivel){
	setTimeout(function() {      
	$('#lvl').text(nivel);
	$('#WinModal').modal('show');
	if(nivel==1){
		window.localStorage.setItem('leveltcp', '2');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarTcpLvl1', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarTcpLvl1 <= 1){
				window.localStorage.setItem('CruciStarTcpLvl1', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarTcpLvl1 == 0){
				window.localStorage.setItem('CruciStarTcpLvl1', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==2){
		window.localStorage.setItem('leveltcp', '3');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarTcpLvl2', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarTcpLvl2 <= 1){
				window.localStorage.setItem('CruciStarTcpLvl2', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarTcpLvl2 == 0){
				window.localStorage.setItem('CruciStarTcpLvl2', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==3){
		window.localStorage.setItem('leveltcp', '4');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarTcpLvl3', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarTcpLvl3 <= 1){
				window.localStorage.setItem('CruciStarTcpLvl3', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarTcpLvl3 == 0){
				window.localStorage.setItem('CruciStarTcpLvl3', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==4){
		window.localStorage.setItem('leveltcp', '5');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarTcpLvl4', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarTcpLvl4 <= 1){
				window.localStorage.setItem('CruciStarTcpLvl4', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarTcpLvl4 == 0){
				window.localStorage.setItem('CruciStarTcpLvl4', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==5){
		window.localStorage.setItem('leveltcp', '6');
		$('#sig').css({'display':'none'});
		$('#finalizado').text(finish);
		window.localStorage.setItem('leveltcp', '1');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarTcpLvl5', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarTcpLvl5 <= 1){
				window.localStorage.setItem('CruciStarTcpLvl5', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarTcpLvl5 == 0){
				window.localStorage.setItem('CruciStarTcpLvl5', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
        fin=true;
		SumaCont();
		ShareScore();
		contStar();
        // Timer();
	}, 250);
}
//sumar coin
function contCoin(n){
	var sumaCoin= n + coin
	window.localStorage.setItem('coin', sumaCoin); 
}
//efecto contador
function count(n){
	var counter = { var: 0 };
	var contador = TweenMax.to(counter, 4, {
	  var: n, 
	  onUpdate: function () {
		var number = Math.ceil(counter.var);
		$('#GetCoin').html(number);
		if(number === counter.var){ 
			contador.kill(); 
		}
	  },
	  ease:Circ.easeOut
	});
}
function SumaCont(){
	var contAllStarCruci = AllCruciStarIpv4 + AllCruciStarIp2v6 + AllCruciStarGover  + AllCruciStarTcp 
	window.localStorage.setItem('contAllStarCruci', contAllStarCruci); 
	console.log(contAllStarCruci);
}
function contStar(){
	var Lvl1 = Number(window.localStorage.getItem('CruciStarTcpLvl1'));
	var Lvl2 = Number(window.localStorage.getItem('CruciStarTcpLvl2'));
	var Lvl3 = Number(window.localStorage.getItem('CruciStarTcpLvl3'));
	var Lvl4 = Number(window.localStorage.getItem('CruciStarTcpLvl4'));
	var Lvl5 = Number(window.localStorage.getItem('CruciStarTcpLvl5'));
	ContStar = Lvl1 + Lvl2 + Lvl3 + Lvl4 + Lvl5;
	//   console.log('starEasyc =', starEasyc );
	//  console.log('starMidc =', starMidc );
	//   console.log('starHardc =', starHardc );
	//  console.log('sumando contador=', ContStar)
	window.localStorage.setItem('AllCruciStarTcp', ContStar); 
	$('#contratiempo').css({
		'color':'transparent'
	})
}
//cerrar modal
function cerrarModal(){
    if (Onanimated == 'true'){
        document.getElementById("BoxAudi").className ="relative animated zoomOut" ;
      setTimeout(function() { 
        document.getElementById("ModalAudi").className ="";
     }, 500);
    }else{
		document.getElementById("ModalAudi").className ="";
    }
}
function Otravez(){
	window.localStorage.setItem('leveltcp', levelActual);
	reload()
}
function siguiente(){
	reload()
}
//mesaje de alerta 
function msj(){
	$("#palabras").css({'display':'none'});
	$(".btnComprar").css({'margin-top':'111px'});
	$(".titAudi").html(aviso)
}
// funcion de la letra 
function letra(lvl){
	var vCoin = Number(window.localStorage.getItem('coin'));
	conjuntoPalabra = '<span>Vertical<input type="checkbox" id="v1"  ></span><br><span>Horizontal<input type="checkbox" id="h1"  ></span>'
	$("#palabras").html(conjuntoPalabra)
	if(lvl == 1){
		if(vCoin >= 35){
			$( "#v1" ).attr("onclick","showL('v1','palabra1');");
			$( "#h1" ).attr("onclick","showL('h1','palabra2');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}
	if(lvl == 2){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical<input type="checkbox" id="v1"  ></span><br><span>Horizontal 1<input type="checkbox" id="h1"  ></span><br><span>Horizontal 2<input type="checkbox" id="h2"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#v1" ).attr("onclick","showL('v1','palabra3');");
			$( "#h1" ).attr("onclick","showL('h1','palabra4');");
			$( "#h2" ).attr("onclick","showL('h2','palabra5');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}
	if(lvl == 3){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical<input type="checkbox" id="v1"  ></span><br><span>Vertical 2<input type="checkbox" id="v2"  ></span><br><span>Horizontal <input type="checkbox" id="h1"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#v1" ).attr("onclick","showL('v1','palabra6');");
			$( "#v2" ).attr("onclick","showL('v2','palabra7');");
			$( "#h1" ).attr("onclick","showL('h1','palabra8');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}	
	if(lvl == 4){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical 1<input type="checkbox" id="v1"  ></span><br><span>Vertical 2<input type="checkbox" id="v2"  ></span><br><span>Vertical 3<input type="checkbox" id="v3"  ></span><br><span>Horizontal <input type="checkbox" id="h1"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#v1" ).attr("onclick","showL('v1','palabra9');");
			$( "#v2" ).attr("onclick","showL('v2','palabra11');");
			$( "#v3" ).attr("onclick","showL('v3','palabra10');");
			$( "#h1" ).attr("onclick","showL('h1','palabra12');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}
	if(lvl == 5){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical 1<input type="checkbox" id="v1"  ></span><br><span>Vertical 2<input type="checkbox" id="v2"  ></span><br><span>Vertical 3<input type="checkbox" id="v3"  ></span><br><span>Horizontal<input type="checkbox" id="h1"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#v1" ).attr("onclick","showL('v1','palabra13');");
			$( "#v2" ).attr("onclick","showL('v2','palabra14');");
			$( "#v3" ).attr("onclick","showL('v3','palabra15');");
			$( "#h1" ).attr("onclick","showL('h1','palabra16');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}			
	if (Onanimated == 'true'){
		document.getElementById("ModalAudi").className ="mostrar animated fadeIn"; 
		document.getElementById("BoxAudi").className ="relative animated bounceIn"; 
		$('.efecv').addClass('animated slideInRight');	
		$('.efecv2').addClass('animated slideInLeft');	
		$('.efech').addClass('animated slideInUp');	
		$('.efech2').addClass('animated slideInDown');	
	}else{
		document.getElementById("ModalAudi").className ="ModalAudi mostrar "; 
		document.getElementById("BoxAudi").className ="BoxAudi relative "; 
	}
	
}
function showL(id,idpalabra) {	
	// Get the checkbox
	var checkBox = document.getElementById(id);  
	// If the checkbox is checked, display the output text
	if (checkBox.checked == true){
		// var vCoin = Number(window.localStorage.getItem('coin')); para cuando se utilizan las monedas
		var vCoin = 36;
		if(vCoin >= 35){
			//  var sumaCoin = vCoin - 35 restar las monedas
			//  window.localStorage.setItem('coin', sumaCoin); 
			//  $('#coin').text(sumaCoin);	
			 $("#"+idpalabra).removeAttr('style');
			 checkBox.disabled = true;
		}else{
			msj();		
		}
		
	} else {
		$("#"+idpalabra).css({'display':'none'});
	}
}
function Timer(){
  //Conteo Regresivo
  var seconds_left = timeLevel;
  var interval = setInterval(function() {
  document.getElementById('contratiempo').innerHTML =  --seconds_left;
  if (fin==false) {
	  if (seconds_left <= 0){
	    clearInterval(interval);
	   cerrarModal()
	   $('#LooseModal').modal('show')
	   ShareScore();
	   ttl--;
		window.localStorage.setItem('ttl', ttl); 
	  }
	}
  }, 1000);
}
//usad nivel 1
function validWord1h(){
	//Validar palabra 1 horizontal
	var answer1h = $("#input-0-15").val().toLowerCase() + $("#input-0-16").val().toLowerCase() + $("#input-0-17").val().toLowerCase();
	if (answer1h == word1h){
		$("#cell-0-15").css("background-color","#17DB00");
		$("#cell-0-16").css("background-color","#17DB00");
		$("#cell-0-17").css("background-color","#17DB00");
		$("#input-0-15").attr("disabled","disabled");
		$("#input-0-16").attr("disabled","disabled");
		$("#input-0-17").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta1 = 'correcto';
		if(respuesta2 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta1 == 'correcto' && respuesta2 == 'correcto'){
			EndGame(1);
		}
	}
	else if (answer1h == ""){

	}
	else if (answer1h != word1h){
		$("#cell-0-15").css("background-color","#f00");
		$("#cell-0-16").css("background-color","#f00");
		$("#cell-0-17").css("background-color","#f00");
	}
}
//usad nivel 1
function validWord1v(){
	//Validar palabra 1 vertical ['#input-1-15','#input-2-15','#input-0-17'];
	var answer1v = $("#input-1-15").val().toLowerCase() + $("#input-2-15").val().toLowerCase() + $("#input-0-17").val().toLowerCase();
	if (answer1v == word1v){
		$("#cell-1-15").css("background-color","#17DB00");
		$("#cell-2-15").css("background-color","#17DB00");
		$("#cell-0-17").css("background-color","#17DB00");
		$("#input-1-15").attr("disabled","disabled");
		$("#input-2-15").attr("disabled","disabled");
		$("#input-0-17").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta2 = 'correcto';
		if(respuesta1 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta1 == 'correcto' && respuesta2 == 'correcto'){
			EndGame(1);
		}
	}
	else if (answer1v == ""){

	}
	else if (answer1v != word1v){
        $("#cell-1-15").css("background-color","#f00");
		$("#cell-2-15").css("background-color","#f00");
        $("#cell-0-17").css("background-color","#f00");
	}
}
//usad nivel 2
function validWordlvl2P1(){
	var answer3h = $("#lvl2input-1-1").val().toLowerCase() + $("#lvl2input-1-2").val().toLowerCase() + $("#lvl2input-1-3").val().toLowerCase();
	if (answer3h == word3h){
		$("#lvl2cell-1-1").css("background-color","#17DB00");
		$("#lvl2cell-1-2").css("background-color","#17DB00");
		$("#lvl2cell-1-3").css("background-color","#17DB00");
		$("#lvl2input-1-1").attr("disabled","disabled");
		$("#lvl2input-1-2").attr("disabled","disabled");
		$("#lvl2input-1-3").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta3 = 'correcto'
		if(respuesta3 == 'correcto' && respuesta4 == 'correcto' && respuesta5 == 'correcto'){
			$('#fin').focus();
			EndGame(2);
		}
	}
	else if (answer3h == ""){

	}
	else if (answer3h != word3h){
		$("#lvl2cell-1-1").css("background-color","#f00");
		$("#lvl2cell-1-2").css("background-color","#f00");
		$("#lvl2cell-1-3").css("background-color","#f00");
	}
}
//usad nivel 2
function validWordlvl2P2(){
	//Validar palabra 3 vertical
	var answer3v = $("#lvl2input-1-3").val().toLowerCase() + $("#lvl2input-2-2").val().toLowerCase() + $("#lvl2input-2-3").val().toLowerCase() + $("#lvl2input-2-4").val().toLowerCase() + $("#lvl2input-2-5").val().toLowerCase() + $("#lvl2input-2-6").val().toLowerCase() + $("#lvl2input-2-7").val().toLowerCase() + $("#lvl2input-2-8").val().toLowerCase()
	console.log('')
	if (answer3v == word3v){
		$("#lvl2cell-1-3").css("background-color","#17DB00");
		$("#lvl2cell-2-2").css("background-color","#17DB00");
		$("#lvl2cell-2-3").css("background-color","#17DB00");
		$("#lvl2cell-2-4").css("background-color","#17DB00");
		$("#lvl2cell-2-5").css("background-color","#17DB00");
		$("#lvl2cell-2-6").css("background-color","#17DB00");
		$("#lvl2cell-2-7").css("background-color","#17DB00");
		$("#lvl2cell-2-8").css("background-color","#17DB00");
		$("#lvl2input-2-2").attr("disabled","disabled");
		$("#lvl2input-2-3").attr("disabled","disabled");
		$("#lvl2input-2-4").attr("disabled","disabled");
		$("#lvl2input-2-5").attr("disabled","disabled");
		$("#lvl2input-2-6").attr("disabled","disabled");
		$("#lvl2input-2-7").attr("disabled","disabled");
		$("#lvl2input-2-8").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta4 = 'correcto';
		if(respuesta3 == 'correcto' && respuesta4 == 'correcto' && respuesta5 == 'correcto'){
			$('#fin').focus();
			EndGame(2);
		}
	}
	else if (answer3v == ""){

	}
	else if (answer3v != word3v){
		$("#lvl2cell-1-3").css("background-color","#f00");
		$("#lvl2cell-2-2").css("background-color","#f00");
		$("#lvl2cell-2-3").css("background-color","#f00");
		$("#lvl2cell-2-4").css("background-color","#f00");
		$("#lvl2cell-2-5").css("background-color","#f00");
		$("#lvl2cell-2-6").css("background-color","#f00");
		$("#lvl2cell-2-7").css("background-color","#f00");
		$("#lvl2cell-2-8").css("background-color","#f00");
	}
}
//usad nivel 2
function validWordlvl2P3(){
	//Validar palabra 5 horizontal
	var answer5h = $("#lvl2input-3-1").val().toLowerCase() + $("#lvl2input-3-2").val().toLowerCase() + $("#lvl2input-3-3").val().toLowerCase() + $("#lvl2input-2-5").val().toLowerCase() + $("#lvl2input-3-5").val().toLowerCase() + $("#lvl2input-3-6").val().toLowerCase() + $("#lvl2input-3-7").val().toLowerCase() + $("#lvl2input-3-8").val().toLowerCase()
	if (answer5h == word5h){
		$("#lvl2cell-3-1").css("background-color","#17DB00");
		$("#lvl2cell-3-2").css("background-color","#17DB00");
		$("#lvl2cell-3-3").css("background-color","#17DB00");
		$("#lvl2cell-2-5").css("background-color","#17DB00");
		$("#lvl2cell-3-5").css("background-color","#17DB00");
		$("#lvl2cell-3-6").css("background-color","#17DB00");
		$("#lvl2cell-3-7").css("background-color","#17DB00");
		$("#lvl2cell-3-8").css("background-color","#17DB00");
		$("#lvl2input-3-1").attr("disabled","disabled");
		$("#lvl2input-3-2").attr("disabled","disabled")
		$("#lvl2input-3-3").attr("disabled","disabled");
		$("#lvl2input-2-5").attr("disabled","disabled");
		$("#lvl2input-3-5").attr("disabled","disabled");
		$("#lvl2input-3-6").attr("disabled","disabled");
		$("#lvl2input-3-7").attr("disabled","disabled");
		$("#lvl2input-3-8").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta5 = 'correcto';
		if(respuesta3 == 'correcto' && respuesta4 == 'correcto' && respuesta5 == 'correcto'){
			$('#fin').focus();
			EndGame(2);
		}
	}
	else if (answer5h == ""){

	}
	else if (answer5h != word5h){
		$("#lvl2cell-3-1").css("background-color","#f00");
		$("#lvl2cell-3-2").css("background-color","#f00");
		$("#lvl2cell-3-3").css("background-color","#f00");
		$("#lvl2cell-2-5").css("background-color","#f00");
		$("#lvl2cell-3-5").css("background-color","#f00");
		$("#lvl2cell-3-6").css("background-color","#f00");
		$("#lvl2cell-3-7").css("background-color","#f00");
		$("#lvl2cell-3-8").css("background-color","#f00");
	}
}
//usad nivel 3
function validWordlvl3P1(){
	//Validar palabra 11 horizontal
	var answer11h = $("#lvl3input-1-1").val().toLowerCase() + $("#lvl3input-1-2").val().toLowerCase() + $("#lvl3input-1-3").val().toLowerCase() + $("#lvl3input-1-4").val().toLowerCase();
	
	if (answer11h == word11h){
		$("#lvl3cell-1-1").css("background-color","#17DB00");
		$("#lvl3cell-1-2").css("background-color","#17DB00");
		$("#lvl3cell-1-3").css("background-color","#17DB00");
		$("#lvl3cell-1-4").css("background-color","#17DB00");
		$("#lvl3input-1-1").attr("disabled","disabled");
		$("#lvl3input-1-2").attr("disabled","disabled");
		$("#lvl3input-1-3").attr("disabled","disabled");
		$("#lvl3input-1-4").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta6 = 'correcto';
		if(respuesta6 == 'correcto' && respuesta7 == 'correcto' && respuesta8 == 'correcto' ){
			$('#fin').focus();
			EndGame(3);
		}
	}
	else if (answer11h == ""){}
	else if (answer11h != word11h){
		$("#lvl3cell-1-1").css("background-color","#f00");
		$("#lvl3cell-1-2").css("background-color","#f00");
		$("#lvl3cell-1-3").css("background-color","#f00");
		$("#lvl3cell-1-4").css("background-color","#f00");
	}
}
//usad nivel 3
function validWordlvl3P2(){
	//Validar palabra 11 vertical
	var answer11v = $("#lvl3input-2-1").val().toLowerCase() + $("#lvl3input-2-2").val().toLowerCase() + $("#lvl3input-2-3").val().toLowerCase()
	if (answer11v == word11v){
		$("#lvl3cell-2-1").css("background-color","#17DB00");
		$("#lvl3cell-2-2").css("background-color","#17DB00");
		$("#lvl3cell-2-3").css("background-color","#17DB00");
		$("#lvl3input-2-1").attr("disabled","disabled");
		$("#lvl3input-2-2").attr("disabled","disabled");
		$("#lvl3input-2-3").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta7 = 'correcto';
		if(respuesta6 == 'correcto' && respuesta7 == 'correcto' && respuesta8 == 'correcto'){
			$('#fin').focus();
			EndGame(3);
		}
	}
	else if (answer11v == ""){

	}
	else if (answer11v != word11v){
		$("#lvl3cell-2-1").css("background-color","#f00");
		$("#lvl3cell-2-2").css("background-color","#f00");
		$("#lvl3cell-2-3").css("background-color","#f00");
	}
}
//usad nivel 3
function validWordlvl3P3(){
	var answer15h = $("#lvl3input-3-1").val().toLowerCase() + $("#lvl3input-3-2").val().toLowerCase() + $("#lvl3input-3-3").val().toLowerCase() + $("#lvl3input-3-4").val().toLowerCase() + $("#lvl3input-3-5").val().toLowerCase() + $("#lvl3input-2-2").val().toLowerCase() + $("#lvl3input-3-7").val().toLowerCase() + $("#lvl3input-1-3").val().toLowerCase() + $("#lvl3input-3-9").val().toLowerCase()
	console.log(answer15h,' = ',word15h)
	if (answer15h == word15h){
		$("#lvl3cell-3-1").css("background-color","#17DB00");
		$("#lvl3cell-3-2").css("background-color","#17DB00");
		$("#lvl3cell-3-3").css("background-color","#17DB00");
		$("#lvl3cell-3-4").css("background-color","#17DB00");
		$("#lvl3cell-3-5").css("background-color","#17DB00");
		$("#lvl3cell-2-2").css("background-color","#17DB00");
		$("#lvl3cell-3-7").css("background-color","#17DB00");
		$("#lvl3cell-1-3").css("background-color","#17DB00");
		$("#lvl3cell-3-9").css("background-color","#17DB00");
		$("#lvl3input-3-1").attr("disabled","disabled");
		$("#lvl3input-3-2").attr("disabled","disabled");
		$("#lvl3input-3-3").attr("disabled","disabled");
		$("#lvl3input-3-4").attr("disabled","disabled");
		$("#lvl3input-3-5").attr("disabled","disabled");
		$("#lvl3input-2-2").attr("disabled","disabled");
		$("#lvl3input-3-7").attr("disabled","disabled");
		$("#lvl3input-1-3").attr("disabled","disabled");
		$("#lvl3input-3-9").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta8 = 'correcto';
		if(respuesta6 == 'correcto' && respuesta7 == 'correcto' && respuesta8 == 'correcto'){
			$('#fin').focus();
			EndGame(3);
		}
	}
	else if (answer15h == ""){}
	else if (answer15h != word15h){
		$("#lvl3cell-3-1").css("background-color","#f00");
		$("#lvl3cell-3-2").css("background-color","#f00");
		$("#lvl3cell-3-3").css("background-color","#f00");
		$("#lvl3cell-3-4").css("background-color","#f00");
		$("#lvl3cell-3-5").css("background-color","#f00");
		$("#lvl3cell-2-2").css("background-color","#f00");
		$("#lvl3cell-3-7").css("background-color","#f00");
		$("#lvl3cell-1-3").css("background-color","#f00");
		$("#lvl3cell-3-9").css("background-color","#f00");
	}
}
//usad nivel 4
function validWordlvl4P1(){
	var answer2v = $("#lvl4input-1-1").val().toLowerCase() + $("#lvl4input-1-2").val().toLowerCase() + $("#lvl4input-1-3").val().toLowerCase() + $("#lvl4input-1-4").val().toLowerCase() + $("#lvl4input-1-5").val().toLowerCase() + $("#lvl4input-1-6").val().toLowerCase() + $("#lvl4input-1-7").val().toLowerCase() + $("#lvl4input-1-8").val().toLowerCase() + $("#lvl4input-1-9").val().toLowerCase() + $("#lvl4input-1-10").val().toLowerCase();
	
	if (answer2v == word2v){
		$("#lvl4cell-1-1").css("background-color","#17DB00");
		$("#lvl4cell-1-2").css("background-color","#17DB00");
		$("#lvl4cell-1-3").css("background-color","#17DB00");
		$("#lvl4cell-1-4").css("background-color","#17DB00");
		$("#lvl4cell-1-5").css("background-color","#17DB00");
		$("#lvl4cell-1-6").css("background-color","#17DB00");
		$("#lvl4cell-1-7").css("background-color","#17DB00");
		$("#lvl4cell-1-8").css("background-color","#17DB00");
		$("#lvl4cell-1-9").css("background-color","#17DB00");
		$("#lvl4cell-1-10").css("background-color","#17DB00");
		$("#lvl4input-1-1").attr("disabled","disabled");
		$("#lvl4input-1-2").attr("disabled","disabled");
		$("#lvl4input-1-3").attr("disabled","disabled");
		$("#lvl4input-1-4").attr("disabled","disabled");
		$("#lvl4input-1-5").attr("disabled","disabled");
		$("#lvl4input-1-6").attr("disabled","disabled");
		$("#lvl4input-1-7").attr("disabled","disabled");
		$("#lvl4input-1-8").attr("disabled","disabled");
		$("#lvl4input-1-9").attr("disabled","disabled");
		$("#lvl4input-1-10").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta9 = 'correcto';
		if(respuesta9 == 'correcto' && respuesta10 == 'correcto' && respuesta11 == 'correcto' && respuesta12 == 'correcto'){
			$('#fin').focus();
			EndGame(4);
		}
	}
	else if (answer2v == ""){

	}
	else if (answer2v != word2v){
		$("#lvl4cell-1-1").css("background-color","#f00");
		$("#lvl4cell-1-2").css("background-color","#f00");
		$("#lvl4cell-1-3").css("background-color","#f00");
		$("#lvl4cell-1-4").css("background-color","#f00");
		$("#lvl4cell-1-5").css("background-color","#f00");
		$("#lvl4cell-1-6").css("background-color","#f00");
		$("#lvl4cell-1-7").css("background-color","#f00");
		$("#lvl4cell-1-8").css("background-color","#f00");
		$("#lvl4cell-1-9").css("background-color","#f00");
		$("#lvl4cell-1-10").css("background-color","#f00");
	}
}
//usad nivel 4
function validWordlvl4P2(){
	var answer7h = $("#lvl4input-2-1").val().toLowerCase() + $("#lvl4input-2-2").val().toLowerCase() + $("#lvl4input-2-3").val().toLowerCase() + $("#lvl4input-2-4").val().toLowerCase() + $("#lvl4input-2-5").val().toLowerCase() + $("#lvl4input-2-6").val().toLowerCase() + $("#lvl4input-2-7").val().toLowerCase() + $("#lvl4input-2-8").val().toLowerCase();
	if (answer7h == word7h){
		$("#lvl4cell-2-1").css("background-color","#17DB00");
		$("#lvl4cell-2-2").css("background-color","#17DB00");
		$("#lvl4cell-2-3").css("background-color","#17DB00");
		$("#lvl4cell-2-4").css("background-color","#17DB00");
		$("#lvl4cell-2-5").css("background-color","#17DB00");
		$("#lvl4cell-2-6").css("background-color","#17DB00");
		$("#lvl4cell-2-7").css("background-color","#17DB00");
		$("#lvl4cell-2-8").css("background-color","#17DB00");
		$("#lvl4input-2-1").attr("disabled","disabled");
		$("#lvl4input-2-2").attr("disabled","disabled");
		$("#lvl4input-2-3").attr("disabled","disabled");
		$("#lvl4input-2-4").attr("disabled","disabled");
		$("#lvl4input-2-5").attr("disabled","disabled");
		$("#lvl4input-2-6").attr("disabled","disabled");
		$("#lvl4input-2-7").attr("disabled","disabled");
		$("#lvl4input-2-8").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta10 = 'correcto';
		if(respuesta9 == 'correcto' && respuesta10 == 'correcto' && respuesta11 == 'correcto' && respuesta12 == 'correcto'){
			$('#fin').focus();
			EndGame(4);
		}
	}
	else if (answer7h == ""){

	}
	else if (answer7h != word7h){
		$("#lvl4cell-2-1").css("background-color","#f00");
		$("#lvl4cell-2-2").css("background-color","#f00");
		$("#lvl4cell-2-3").css("background-color","#f00");
		$("#lvl4cell-2-4").css("background-color","#f00");
		$("#lvl4cell-2-5").css("background-color","#f00");
		$("#lvl4cell-2-6").css("background-color","#f00");
		$("#lvl4cell-2-7").css("background-color","#f00");
		$("#lvl4cell-2-8").css("background-color","#f00");
	}
}
//usad nivel 4
function validWordlvl4P3(){
	//Validar palabra 7 vertical
	var answer7v = $("#lvl4input-3-1").val().toLowerCase() + $("#lvl4input-4-1").val().toLowerCase() + $("#lvl4input-3-3").val().toLowerCase() + $("#lvl4input-3-4").val().toLowerCase();
	if (answer7v == word7v){
		$("#lvl4cell-3-1").css("background-color","#17DB00");
		$("#lvl4cell-4-1").css("background-color","#17DB00");
		$("#lvl4cell-3-3").css("background-color","#17DB00");
		$("#lvl4cell-3-4").css("background-color","#17DB00");
		$("#lvl4input-3-1").attr("disabled","disabled");
		$("#lvl4input-4-1").attr("disabled","disabled");
		$("#lvl4input-3-3").attr("disabled","disabled");
		$("#lvl4input-3-4").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta11 = 'correcto';
		if(respuesta9 == 'correcto' && respuesta10 == 'correcto' && respuesta11 == 'correcto' && respuesta12 == 'correcto'){
			$('#fin').focus();
			EndGame(4);
		}
	}
	else if (answer7v == ""){

	}
	else if (answer7v != word7v){
		$("#lvl4cell-3-1").css("background-color","#f00");
		$("#lvl4cell-4-1").css("background-color","#f00");
		$("#lvl4cell-3-3").css("background-color","#f00");
		$("#lvl4cell-3-4").css("background-color","#f00");
	}
}
//usad nivel 4
function validWordlvl4P4(){
	//Validar palabra 10 horizontal
	var answer10h = $("#lvl4input-4-1").val().toLowerCase() + $("#lvl4input-4-2").val().toLowerCase() + $("#lvl4input-4-3").val().toLowerCase() + $("#lvl4input-4-4").val().toLowerCase() + $("#lvl4input-1-6").val().toLowerCase() + $("#lvl4input-4-6").val().toLowerCase() + $("#lvl4input-2-5").val().toLowerCase();
	console.log(answer10h ,' = ', word10h)
	if (answer10h == word10h){
		$("#lvl4cell-4-1").css("background-color","#17DB00");
		$("#lvl4cell-4-2").css("background-color","#17DB00");
		$("#lvl4cell-4-3").css("background-color","#17DB00");
		$("#lvl4cell-4-4").css("background-color","#17DB00");
		$("#lvl4cell-1-6").css("background-color","#17DB00");
		$("#lvl4cell-4-6").css("background-color","#17DB00");
		$("#lvl4cell-2-5").css("background-color","#17DB00");
		$("#lvl4input-4-1").attr("disabled","disabled");
		$("#lvl4input-4-2").attr("disabled","disabled");
		$("#lvl4input-4-3").attr("disabled","disabled");
		$("#lvl4input-4-4").attr("disabled","disabled");
		$("#lvl4input-1-6").attr("disabled","disabled");
		$("#lvl4input-4-6").attr("disabled","disabled");
		$("#lvl4input-2-5").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta12 = 'correcto';
		if(respuesta9 == 'correcto' && respuesta10 == 'correcto' && respuesta11 == 'correcto' && respuesta12 == 'correcto'){
			$('#fin').focus();
			EndGame(4);
		}
	}
	else if (answer10h == ""){}
	else if (answer10h != word10h){
		$("#cell-7-0").css("background-color","#f00");
		$("#lvl4cell-4-1").css("background-color","#f00");
		$("#lvl4cell-4-2").css("background-color","#f00");
		$("#lvl4cell-4-3").css("background-color","#f00");
		$("#lvl4cell-4-4").css("background-color","#f00");
		$("#lvl4cell-1-6").css("background-color","#f00");
		$("#lvl4cell-4-6").css("background-color","#f00");
		$("#lvl4cell-2-5").css("background-color","#f00");
	}
}
//usad nivel 5
function validWordlvl5P1(){
	//Validar palabra 4 vertical
	var answer4v = $("#lvl5input-1-1").val().toLowerCase() + $("#lvl5input-1-2").val().toLowerCase() + $("#lvl5input-1-3").val().toLowerCase() + $("#lvl5input-1-4").val().toLowerCase() + $("#lvl5input-1-5").val().toLowerCase() + $("#lvl5input-1-6").val().toLowerCase() + $("#lvl5input-1-7").val().toLowerCase() + $("#lvl5input-1-8").val().toLowerCase() + $("#lvl5input-1-9").val().toLowerCase() + $("#lvl5input-1-10").val().toLowerCase() + $("#lvl5input-1-11").val().toLowerCase() + $("#lvl5input-1-12").val().toLowerCase() + $("#lvl5input-1-13").val().toLowerCase();
	if (answer4v == word4v){
		$("#lvl5cell-1-1").css("background-color","#17DB00");
		$("#lvl5cell-1-2").css("background-color","#17DB00");
		$("#lvl5cell-1-3").css("background-color","#17DB00");
		$("#lvl5cell-1-4").css("background-color","#17DB00");
		$("#lvl5cell-1-5").css("background-color","#17DB00");
		$("#lvl5cell-1-6").css("background-color","#17DB00");
		$("#lvl5cell-1-7").css("background-color","#17DB00");
		$("#lvl5cell-1-8").css("background-color","#17DB00");
		$("#lvl5cell-1-9").css("background-color","#17DB00");
		$("#lvl5cell-1-10").css("background-color","#17DB00");
		$("#lvl5cell-1-11").css("background-color","#17DB00");
		$("#lvl5cell-1-12").css("background-color","#17DB00");
		$("#lvl5cell-1-13").css("background-color","#17DB00");
		$("#lvl5input-1-1").attr("disabled","disabled");
		$("#lvl5input-1-2").attr("disabled","disabled");
		$("#lvl5input-1-3").attr("disabled","disabled");
		$("#lvl5input-1-4").attr("disabled","disabled");
		$("#lvl5input-1-5").attr("disabled","disabled");
		$("#lvl5input-1-6").attr("disabled","disabled");
		$("#lvl5input-1-7").attr("disabled","disabled");
		$("#lvl5input-1-8").attr("disabled","disabled");
		$("#lvl5input-1-9").attr("disabled","disabled");
		$("#lvl5input-1-10").attr("disabled","disabled");
		$("#lvl5input-1-11").attr("disabled","disabled");
		$("#lvl5input-1-12").attr("disabled","disabled");
		$("#lvl5input-1-13").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta14 = 'correcto';
		if(respuesta13 == 'correcto' && respuesta14 == 'correcto' && respuesta15 == 'correcto' && respuesta16 == 'correcto'){
			$('#fin').focus();
			EndGame(5);
		}
	}
	else if (answer4v == ""){

	}
	else if (answer4v != word4v){
		$("#lvl5cell-1-1").css("background-color","#f00");
		$("#lvl5cell-1-2").css("background-color","#f00");
		$("#lvl5cell-1-3").css("background-color","#f00");
		$("#lvl5cell-1-4").css("background-color","#f00");
		$("#lvl5cell-1-5").css("background-color","#f00");
		$("#lvl5cell-1-6").css("background-color","#f00");
		$("#lvl5cell-1-7").css("background-color","#f00");
		$("#lvl5cell-1-8").css("background-color","#f00");
		$("#lvl5cell-1-9").css("background-color","#f00");
		$("#lvl5cell-1-10").css("background-color","#f00");
		$("#lvl5cell-1-11").css("background-color","#f00");
		$("#lvl5cell-1-12").css("background-color","#f00");
		$("#lvl5cell-1-13").css("background-color","#f00");
	}
}
//usad nivel 5
function validWordlvl5P2(){
	//Validar palabra 6 vertical
	var answer6v = $("#lvl5input-2-1").val().toLowerCase() + $("#lvl5input-2-2").val().toLowerCase() + $("#lvl5input-2-3").val().toLowerCase() + $("#lvl5input-2-4").val().toLowerCase() + $("#lvl5input-2-5").val().toLowerCase() + $("#lvl5input-2-6").val().toLowerCase() + $("#lvl5input-2-7").val().toLowerCase() + $("#lvl5input-2-8").val().toLowerCase();
	if (answer6v == word6v){
		$("#lvl5cell-2-1").css("background-color","#17DB00");
		$("#lvl5cell-2-2").css("background-color","#17DB00");
		$("#lvl5cell-2-3").css("background-color","#17DB00");
		$("#lvl5cell-2-4").css("background-color","#17DB00");
		$("#lvl5cell-2-5").css("background-color","#17DB00");
		$("#lvl5cell-2-6").css("background-color","#17DB00");
		$("#lvl5cell-2-7").css("background-color","#17DB00");
		$("#lvl5cell-2-8").css("background-color","#17DB00");
		$("#lvl5input-2-1").attr("disabled","disabled");
		$("#lvl5input-2-2").attr("disabled","disabled");
		$("#lvl5input-2-3").attr("disabled","disabled");
		$("#lvl5input-2-4").attr("disabled","disabled");
		$("#lvl5input-2-5").attr("disabled","disabled");
		$("#lvl5input-2-6").attr("disabled","disabled");
		$("#lvl5input-2-7").attr("disabled","disabled");
		$("#lvl5input-2-8").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);		
		respuesta13 = 'correcto';
		if(respuesta13 == 'correcto' && respuesta14 == 'correcto' && respuesta15 == 'correcto' && respuesta16 == 'correcto'){
			$('#fin').focus();
			EndGame(5);
		}
	}
	else if (answer6v == ""){

	}
	else if (answer6v != word6v){
		$("#lvl5cell-2-1").css("background-color","#f00");
		$("#lvl5cell-2-2").css("background-color","#f00");
		$("#lvl5cell-2-3").css("background-color","#f00");
		$("#lvl5cell-2-4").css("background-color","#f00");
		$("#lvl5cell-2-5").css("background-color","#f00");
		$("#lvl5cell-2-6").css("background-color","#f00");
		$("#lvl5cell-2-7").css("background-color","#f00");
		$("#lvl5cell-2-8").css("background-color","#f00");
	}
}
//usad nivel 5
function validWordlvl5P3(){
	//Validar palabra 8 vertical
	var answer8v = $("#lvl5input-3-1").val().toLowerCase() + $("#lvl5input-3-2").val().toLowerCase() + $("#lvl5input-3-3").val().toLowerCase() + $("#lvl5input-2-2").val().toLowerCase();
	if (answer8v == word8v){
		$("#lvl5cell-3-1").css("background-color","#17DB00");
		$("#lvl5cell-3-2").css("background-color","#17DB00");
		$("#lvl5cell-3-3").css("background-color","#17DB00");
		$("#lvl5cell-2-2").css("background-color","#17DB00");
		$("#lvl5input-3-1").attr("disabled","disabled");
		$("#lvl5input-3-2").attr("disabled","disabled");
		$("#lvl5input-3-3").attr("disabled","disabled");
		$("#lvl5input-2-2").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta15 = 'correcto';
		if(respuesta13 == 'correcto' && respuesta14 == 'correcto' && respuesta15 == 'correcto' && respuesta16 == 'correcto'){
			$('#fin').focus();
			EndGame(5);
		}
	}
	else if (answer8v == ""){}
	else if (answer8v != word8v){
		$("#lvl5cell-3-1").css("background-color","#f00");
		$("#lvl5cell-3-2").css("background-color","#f00");
		$("#lvl5cell-3-3").css("background-color","#f00");
		$("#lvl5cell-2-2").css("background-color","#f00");
	}
}
//usad nivel 5
function validWordlvl5P4(){
	//Validar palabra 9 horizontal
	var answer9h = $("#lvl5input-4-1").val().toLowerCase() + $("#lvl5input-4-2").val().toLowerCase() + $("#lvl5input-4-3").val().toLowerCase() + $("#lvl5input-2-5").val().toLowerCase() + $("#lvl5input-4-4").val().toLowerCase() + $("#lvl5input-1-8").val().toLowerCase();
	console.log(answer9h, '=',word9h)
	if (answer9h == word9h){
		$("#lvl5cell-4-1").css("background-color","#17DB00");
		$("#lvl5cell-4-2").css("background-color","#17DB00");
		$("#lvl5cell-4-3").css("background-color","#17DB00");
		$("#lvl5cell-2-5").css("background-color","#17DB00");
		$("#lvl5cell-4-4").css("background-color","#17DB00");
		$("#lvl5cell-1-8").css("background-color","#17DB00");
		$("#lvl5input-4-1").attr("disabled","disabled");
		$("#lvl5input-4-2").attr("disabled","disabled");
		$("#lvl5input-4-3").attr("disabled","disabled");
		$("#lvl5input-2-5").attr("disabled","disabled");
		$("#lvl5input-4-4").attr("disabled","disabled");
		$("#lvl5input-1-8").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta16 = 'correcto';
		if(respuesta13 == 'correcto' && respuesta14 == 'correcto' && respuesta15 == 'correcto' && respuesta16 == 'correcto'){
			$('#fin').focus();
			EndGame(5);
		}
	}
	else if (answer9h == ""){}
	else if (answer9h != word9h){
		$("#lvl5cell-4-1").css("background-color","#f00");
		$("#lvl5cell-4-2").css("background-color","#f00");
		$("#lvl5cell-4-3").css("background-color","#f00");
		$("#lvl5cell-2-5").css("background-color","#f00");
		$("#lvl5cell-4-4").css("background-color","#f00");
		$("#lvl5cell-1-8").css("background-color","#f00");
	}
}
function next(i){
	for(var j=i;j<=22;j++)
	{
        var vacio=check_boxes(j);
		if(vacio!='lleno')
		{
			$(vacio).focus();
			break;
		}
	}
}
function ShareScore(){
if (localStorage.getItem("UserId")!=null){
  var infogame = { "UserId":localStorage.getItem("UserId"), "GameId":1, "TopicId" :2, "levelId" :4,"Score":punto};

  RegisterGame(infogame);
}
}
function public_FB(){


	var msj="GAME: CROSSWORD  LEVEL:"+leveltcp+" TOPIC:TCP  POINTS: "+punto;

	$(".fb-xfbml-parse-ignore").attr("href","https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Fjuegos%2Fcrucigrama%2Fcrosswordip6.html?description="+msj+"&method=1&amp;src=sdkpreparse");
}
function validar(){
	//nivel 1 pregunta 1_1
	$('#input-0-15').on('keyup',function(e) {
		// Backspace
		if (e.keyCode==40){
			$('#input-1-15').focus();
		}
		if (e.keyCode==39){
			$('#input-0-16').focus();
		}
		if($('#input-0-15').val()!='')
		{
			$('#input-0-16').focus();

			actual='1.1';
		}

	});
	$('#input-0-16').on('keyup',function(e) {
		// Backspace
		if ($('#input-0-16').val()==''){
			$('#input-0-15').focus();
		}
		if (e.keyCode==39){
			$('#input-0-17').focus();
		}
		if($('#input-0-16').val()!='')
		{
			if(respuesta2 == 'correcto'){
				$('#fin').focus();
			}else{
			$('#input-0-17').focus();	
			}			
		}
	});
	$('#input-0-17').on('keyup',function(e) {
		// Backspace
		if ($('#input-0-17').val()==''){
			if($('#input-2-15').val()!=''){
				$('#input-0-16').focus();
			}else{
				$('#input-2-15').focus();
			}			
		}
		if (e.keyCode==39){
			$('#input-1-15').focus();
		}
		if($('#input-0-17').val()!='')
		{	
			if(respuesta2 == 'correcto' && respuesta1 == 'correcto'){
				$('#fin').focus();
			}else{
				if($('#input-1-15').val()!=''){
					$('#input-0-15').focus();
				}else{
					$('#input-1-15').focus();
				}			
			}			
		}
	});
	//nivel 1 pregunta 1_2
	$('#input-1-15').on('keyup',function(e) {
		// Backspace
		
		if ($('#input-1-15').val()==''){
			$('#input-0-17').focus();
		}
		if (e.keyCode==38){
			$('#input-2-15').focus();
		}
		if($('#input-1-15').val()!='')
		{
			$('#input-2-15').focus();

			actual='1.2';
		}

	});
	$('#input-2-15').on('keyup',function(e) {
		// Backspace
		
		if ($('#input-2-15').val()==''){
			$('#input-1-15').focus();
		}
		actual='1.2';

		if($('#input-2-15').val()!='')
		{	
			if(respuesta1 == 'correcto'){
				$('#fin').focus();
			}else{
				$('#input-0-17').focus();
			}	
						
		}

	});
	$('#input-3-15').on('keyup',function(e) {
		// Backspace
		
		if ($('#input-3-15').val()==''){
			$('#input-2-15').focus();
		}
		if (e.keyCode==39){
			$('#input-3-14').focus();
		}
		if($('#input-3-15').val()!='')
		{
			if(respuesta1 == 'correcto'){
				$('#fin').focus();
			}
			if(actual=='1.2')
			{
				next(1);

			}else if(actual=='3.1'){

				next(4);
			}
		}

	});
	
	//nivel 2 pregunta 1
    $('#lvl2input-1-1').on('keyup',function(e) {
		// Backspace
        if($('#lvl2input-1-1').val()!='')
        {
            $('#lvl2input-1-2').focus();
        }

    });
    $('#lvl2input-1-2').on('keyup',function(e) {
		// Backspace
		
		if ($('#lvl2input-1-2').val()==''){
			$('#lvl2input-1-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl2input-1-3').focus();
		}
        if($('#lvl2input-1-2').val()!='')
        {
           	$('#lvl2input-1-3').focus();

			actual='2';
        }

    });
    $('#lvl2input-1-3').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-1-3').val()==''){
			$('#lvl2input-1-2').focus();
		}
        if($('#lvl2input-1-3').val()!='')
        {
			if($('#lvl2input-2-2').val()==''){
				$('#lvl2input-2-2').focus();
			}else if($('#lvl2input-3-1').val()==''){
				$('#lvl2input-3-1').focus();
			}else{
				$('#fin').focus();
			}	
		} 
	});	

	//nivel 2 pregunta 2
    $('#lvl2input-2-2').on('keyup',function(e) {
		// Backspace
        if ($('#lvl2input-2-2').val()==''){
			$('#lvl2input-1-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl2input-1-3').focus();
		}
        if($('#lvl2input-2-2').val()!='')
        {
           	$('#lvl2input-2-3').focus();
        }
    });
    $('#lvl2input-2-3').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-2-3').val()==''){
			$('#lvl2input-2-2').focus();
		}
		if (e.keyCode==39){
			$('#lvl2input-2-4').focus();
		}
        if($('#lvl2input-2-3').val()!='')
        {
           	$('#lvl2input-2-4').focus();
        }
    });
    $('#lvl2input-2-4').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-2-4').val()==''){
			$('#lvl2input-2-3').focus();
		}
        if($('#lvl2input-2-4').val()!='')
        {	
			if($('#lvl2input-2-5').val()!=''){
				$('#lvl2input-2-6').focus();
			}else{
				$('#lvl2input-2-5').focus();
			}			
		} 
	});		
	$('#lvl2input-2-5').on('keyup',function(e) {//intercepta la 3
		// Backspace
		if ($('#lvl2input-2-5').val()==''){
			if(actual==2){
				$('#lvl2input-2-4').focus();
			}else{
				$('#lvl2input-3-3').focus();
			}			
		}
        if($('#lvl2input-2-5').val()!='')
        {	
			if(actual==2){
				$('#lvl2input-2-6').focus();
			}else{
				$('#lvl2input-3-5').focus();
			}				
		} 

    });
    $('#lvl2input-2-6').on('keyup',function(e) {
		// Backspace
		if ($('#lvl2input-2-6').val()==''){
			if($('#lvl2input-2-5').val()!=''){
				$('#lvl2input-2-4').focus();
			}else{
				$('#lvl2input-2-5').focus();
			}			
		}
		if (e.keyCode==39){
			$('#lvl2input-2-7').focus();
		}
        if($('#lvl2input-2-6').val()!='')
        {
           	$('#lvl2input-2-7').focus();
        }

    });
    $('#lvl2input-2-7').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-2-7').val()==''){
			$('#lvl2input-2-6').focus();
		}
        if($('#lvl2input-2-7').val()!='')
        {
			$('#lvl2input-2-8').focus();
		} 
	});	
	$('#lvl2input-2-8').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-2-8').val()==''){
			$('#lvl2input-2-7').focus();
		}
        if($('#lvl2input-2-8').val()!='')
        {
			if($('#lvl2input-3-1').val()==''){
				$('#lvl2input-3-1').focus();
			}else if($('#lvl2input-2-2').val()==''){
				$('#lvl2input-2-2').focus();
			}else{
				$('#fin').focus();
			}	
		} 
	});	

	//nivel 2 pregunta 3
    $('#lvl2input-3-1').on('keyup',function(e) {
		// Backspace
        if ($('#lvl2input-3-1').val()==''){
			$('#lvl2input-2-8').focus();
		}
		if (e.keyCode==39){
			$('#lvl2input-3-2').focus();
		}
        if($('#lvl2input-3-1').val()!='')
        {
           	$('#lvl2input-3-2').focus();
        }
	});
	$('#lvl2input-3-2').on('keyup',function(e) {
		// Backspace
        if ($('#lvl2input-3-2').val()==''){
			$('#lvl2input-3-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl2input-3-3').focus();
		}
        if($('#lvl2input-3-2').val()!='')
        {
           	$('#lvl2input-3-3').focus();
        }
    });
    $('#lvl2input-3-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl2input-3-3').val()==''){
			$('#lvl2input-3-2').focus();
		}
        if($('#lvl2input-3-3').val()!='')
        {	
			if($('#lvl2input-2-5').val()!=''){
				$('#lvl2input-3-5').focus();
			}else{
				$('#lvl2input-2-5').focus();
			}			
		} 
    });
    $('#lvl2input-3-5').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-3-5').val()==''){
			if($('#lvl2input-2-5').val()!=''){
				$('#lvl2input-3-3').focus();
			}else{
				$('#lvl2input-2-5').focus();
			}			
		}
        if($('#lvl2input-3-5').val()!='')
        {	
			$('#lvl2input-3-6').focus();		
		} 
	});	
    $('#lvl2input-3-6').on('keyup',function(e) {
		// Backspace
		if ($('#lvl2input-3-6').val()==''){
			$('#lvl2input-3-5').focus();			
		}
		if (e.keyCode==39){
			$('#lvl2input-3-7').focus();
		}
        if($('#lvl2input-3-6').val()!='')
        {
           	$('#lvl2input-3-7').focus();
        }

    });
    $('#lvl2input-3-7').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-3-7').val()==''){
			$('#lvl2input-3-6').focus();
		}
        if($('#lvl2input-3-7').val()!='')
        {
			$('#lvl2input-3-8').focus();
		} 
	});	
	$('#lvl2input-3-8').on('keyup',function(e) {
		// Backspace		
		if ($('#lvl2input-3-8').val()==''){
			$('#lvl2input-3-7').focus();
		}
        if($('#lvl2input-3-8').val()!='')
        {
			if($('#lvl2input-1-1').val()==''){
				$('#lvl2input-1-1').focus();
			}else if($('#lvl2input-2-2').val()==''){
				$('#lvl2input-2-2').focus();
			}else{
				$('#fin').focus();
			}			
		} 
	});	
	
	//nivel 3 pregunta 1
    $('#lvl3input-1-1').on('keyup',function(e) {
		// Backspace
		if (e.keyCode==39){
			$('#lvl3input-1-2').focus();
		}
        if($('#lvl3input-1-1').val()!='')
        {
           	$('#lvl3input-1-2').focus();
        }
	});
	$('#lvl3input-1-2').on('keyup',function(e) {
		// Backspace
        if ($('#lvl3input-1-2').val()==''){
			$('#lvl3input-1-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl3input-1-3').focus();
		}
        if($('#lvl3input-1-2').val()!='')
        {
			if($('#lvl3input-1-3').val()!=''){
				$('#lvl3input-1-4').focus();
			}else{
				$('#lvl3input-1-3').focus();
			}
           
		}
		actual=5
    });
    $('#lvl3input-1-3').on('keyup',function(e) {//intercepta la 3
		// Backspace			
		if ($('#lvl3input-1-3').val()==''){
			$('#lvl3input-1-2').focus();
			if(actual==5){
				$('#lvl3input-1-2').focus();
			}else{
				$('#lvl3input-3-7').focus();
			}
		}
        if($('#lvl3input-1-3').val()!='')
        {	
			if(actual==5){
				$('#lvl3input-1-4').focus();
			}else{
				$('#lvl3input-3-9').focus();
			}	
		} 
    });
	$('#lvl3input-1-4').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl3input-1-4').val()==''){
			$('#lvl3input-1-3').focus();
		}
        if($('#lvl3input-1-3').val()!='')
        {	
			if($('#lvl3input-2-1').val()==''){
				$('#lvl3input-2-1').focus();
			}else if($('#lvl3input-3-1').val()==''){
				$('#lvl3input-3-1').focus();
			}else{
				$('#fin').focus();
			}				
		} 
		actual=5
	});
	
	//nivel 3 pregunta 2
    $('#lvl3input-2-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl3input-2-1').val()==''){
			$('#lvl3input-1-4').focus();
		}
		if (e.keyCode==39){
			$('#lvl3input-2-2').focus();
		}
        if($('#lvl3input-2-1').val()!='')
        {
			if($('#lvl3input-2-2').val()!=''){
				$('#lvl3input-2-3').focus();
			}else {
				$('#lvl3input-2-2').focus();
			}
           
		}
		actual=6
	});
	$('#lvl3input-2-2').on('keyup',function(e) {//intercepta la 3
		// Backspace
        if ($('#lvl3input-2-2').val()==''){
			if(actual==6){
				$('#lvl3input-2-1').focus();
			}else{
				$('#lvl3input-3-5').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl3input-2-3').focus();
		}
        if($('#lvl3input-2-2').val()!='')
        {
			if(actual==6){
				$('#lvl3input-2-3').focus();
			}else{
				$('#lvl3input-3-7').focus();
			}	
        }
    });
	$('#lvl3input-2-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl3input-2-3').val()==''){
			$('#lvl3input-2-2').focus();
		}
        if($('#lvl3input-2-3').val()!='')
        {	
			if($('#lvl3input-3-1').val()==''){
				$('#lvl3input-3-1').focus();
			}else if($('#lvl3input-1-1').val()==''){
				$('#lvl3input-1-1').focus();
			}else{
				$('#fin').focus();
			}				
		} 
		actual=6
	});

	//nivel 3 pregunta 3
	$('#lvl3input-3-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl3input-3-1').val()==''){
			$('#lvl3input-2-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl3input-3-2').focus();
		}
		if($('#lvl3input-3-1').val()!='')
		{
			$('#lvl3input-3-2').focus();
		}
	});
	$('#lvl3input-3-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl3input-3-2').val()==''){
			$('#lvl3input-3-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl3input-3-3').focus();
		}
		if($('#lvl3input-3-2').val()!='')
		{
			$('#lvl3input-3-3').focus();
		}
	});
	$('#lvl3input-3-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl3input-3-3').val()==''){
			$('#lvl3input-3-2').focus();
		}
		if($('#lvl3input-3-3').val()!='')
		{	
			$('#lvl3input-3-4').focus();	
		} 
	});
	$('#lvl3input-3-4').on('keyup',function(e) {
		// Backspace
		if ($('#lvl3input-3-4').val()==''){
			$('#lvl3input-3-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl3input-1-2').focus();
		}
		if($('#lvl3input-3-4').val()!='')
		{
			$('#lvl3input-3-5').focus();
		}
	});
	$('#lvl3input-3-5').on('keyup',function(e) {
		// Backspace
		if ($('#lvl3input-3-5').val()==''){
			$('#lvl3input-3-4').focus();
		}
		if (e.keyCode==39){
			$('#lvl3input-1-3').focus();
		}
		if($('#lvl3input-3-5').val()!='')
		{	
			if($('#lvl3input-2-2').val()!=''){
				$('#lvl3input-3-7').focus();
			}else{
				$('#lvl3input-2-2').focus();
			}
			
		}
		actual=7
	});
	$('#lvl3input-3-7').on('keyup',function(e) {
		// Backspace
		if ($('#lvl3input-3-7').val()==''){

			if($('#lvl3input-2-2').val()!=''){
				$('#lvl3input-3-5').focus();
			}else{
				$('#lvl3input-2-2').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl3input-1-3').focus();
		}
		if($('#lvl3input-3-7').val()!='')
		{
			if($('#lvl3input-1-3').val()!=''){
				$('#lvl3input-3-9').focus();
			}else{
				$('#lvl3input-1-3').focus();
			}
		}
		actual=7
	});
	$('#lvl3input-3-9').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl3input-3-9').val()==''){

			if($('#lvl3input-1-3').val()!=''){
				$('#lvl3input-3-7').focus();
			}else{
				$('#lvl3input-1-3').focus();
			}
		}
		if($('#lvl3input-3-9').val()!='')
		{	
			if($('#lvl3input-1-1').val()==''){
				$('#lvl3input-1-1').focus();
			}else if($('#lvl3input-2-1').val()==''){
				$('#lvl3input-2-1').focus();
			}else{
				$('#fin').focus();
			}				
		} 
		actual=7
	});

	//nivel 4 pregunta 1
	$('#lvl4input-1-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-1').val()==''){
			$('#lvl4input-1-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-1-2').focus();
		}
		if($('#lvl4input-1-1').val()!='')
		{
			$('#lvl4input-1-2').focus();
		}
	});
	$('#lvl4input-1-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-2').val()==''){
			$('#lvl4input-1-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-1-3').focus();
		}
		if($('#lvl4input-1-2').val()!='')
		{
			$('#lvl4input-1-3').focus();
		}
	});
	$('#lvl4input-1-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-1-3').val()==''){
			$('#lvl4input-1-2').focus();
		}
		if($('#lvl4input-1-3').val()!='')
		{	
			$('#lvl4input-1-4').focus();	
		} 
	});
	$('#lvl4input-1-4').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-4').val()==''){
			$('#lvl4input-1-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-1-2').focus();
		}
		if($('#lvl4input-1-4').val()!='')
		{
			$('#lvl4input-1-5').focus();
		}
	});
	$('#lvl4input-1-5').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-5').val()==''){
			$('#lvl4input-1-4').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-1-6').focus();
		}
		if($('#lvl4input-1-5').val()!='')
		{	
			if($('#lvl4input-1-6').val()!=''){
				$('#lvl4input-1-7').focus();
			}else{
				$('#lvl4input-1-6').focus();
			}			
		}
		actual=1
	});
	$('#lvl4input-1-6').on('keyup',function(e) {//intercepta la 4
		// Backspace
		if ($('#lvl4input-1-6').val()==''){
			if(actual==1){
				$('#lvl4input-1-5').focus();
			}else{
				$('#lvl4input-4-4').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl4input-1-3').focus();
		}
		if($('#lvl4input-1-6').val()!='')
		{	
			if(actual==1){
				$('#lvl4input-1-7').focus();
			}else{
				$('#lvl4input-4-6').focus();
			}			
		}
	});
	$('#lvl4input-1-7').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-7').val()==''){
			if($('#lvl4input-1-6').val()!=''){
				$('#lvl4input-1-5').focus();
			}else{
				$('#lvl4input-1-6').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl4input-1-3').focus();
		}
		if($('#lvl4input-1-7').val()!='')
		{
			$('#lvl4input-1-8').focus();
		}
		actual=1
	});
	$('#lvl4input-1-8').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-8').val()==''){

			$('#lvl4input-1-7').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-1-9').focus();
		}
		if($('#lvl4input-1-8').val()!='')
		{
			$('#lvl4input-1-9').focus();
		}
		actual=1
	});
	$('#lvl4input-1-9').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-1-9').val()==''){
			$('#lvl4input-1-8').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-1-10').focus();
		}
		if($('#lvl4input-1-9').val()!='')
		{
			$('#lvl4input-1-10').focus();
		}
		actual=1
	});
	$('#lvl4input-1-10').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-1-10').val()==''){
			$('#lvl4input-1-9').focus();
		}
		if($('#lvl4input-1-10').val()!='')
		{	
			if($('#lvl4input-2-1').val()==''){
				$('#lvl4input-2-1').focus();
			}else if($('#lvl4input-3-1').val()==''){
				$('#lvl4input-3-1').focus();
			}else if($('#lvl4input-4-2').val()==''){
				$('#lvl4input-4-2').focus();
			}else{
				$('#fin').focus();
			}				
		} 
	});

	//nivel 4 pregunta 2
	$('#lvl4input-2-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-2-1').val()==''){
			$('#lvl4input-1-10').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-2-2').focus();
		}
		if($('#lvl4input-2-1').val()!='')
		{
			$('#lvl4input-2-2').focus();
		}
	});
	$('#lvl4input-2-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-2-2').val()==''){
			$('#lvl4input-2-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-2-3').focus();
		}
		if($('#lvl4input-2-2').val()!='')
		{
			$('#lvl4input-2-3').focus();
		}
	});
	$('#lvl4input-2-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-2-3').val()==''){
			$('#lvl4input-2-2').focus();
		}
		if($('#lvl4input-2-3').val()!='')
		{	
			$('#lvl4input-2-4').focus();	
		} 
	});
	$('#lvl4input-2-4').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-2-4').val()==''){
			$('#lvl4input-2-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-2-5').focus();
		}
		if($('#lvl4input-2-4').val()!='')
		{
			if($('#lvl4input-2-5').val()!=''){
				$('#lvl4input-2-6').focus();
			}else{
				$('#lvl4input-2-5').focus();
			}	
		}
		actual=2
	});
	$('#lvl4input-2-5').on('keyup',function(e) {//intercepta la 4
		// Backspace
		if ($('#lvl4input-2-5').val()==''){
			if(actual==2){
				$('#lvl4input-2-4').focus();
			}else{
				$('#lvl4input-4-6').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl4input-1-6').focus();
		}
		if($('#lvl4input-2-5').val()!='')
		{	
			$('#lvl4input-2-6').focus();		
		}
	});
	$('#lvl4input-2-6').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-2-6').val()==''){
			if($('#lvl4input-2-5').val()!=''){
				$('#lvl4input-2-4').focus();
			}else{
				$('#lvl4input-2-5').focus();
			}	
		}
		if (e.keyCode==39){
			$('#lvl4input-2-7').focus();
		}
		if($('#lvl4input-2-6').val()!='')
		{	
			$('#lvl4input-2-7').focus();		
		}
		actual=2
	});
	$('#lvl4input-2-7').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-2-7').val()==''){
			$('#lvl4input-2-6').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-2-3').focus();
		}
		if($('#lvl4input-2-7').val()!='')
		{
			$('#lvl4input-2-8').focus();
		}
	});
	$('#lvl4input-2-8').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-2-8').val()==''){
			$('#lvl4input-2-7').focus();
		}
		if($('#lvl4input-2-8').val()!='')
		{	
			if($('#lvl4input-1-1').val()==''){
				$('#lvl4input-1-1').focus();
			}else if($('#lvl4input-3-1').val()==''){
				$('#lvl4input-3-1').focus();
			}else if($('#lvl4input-4-2').val()==''){
				$('#lvl4input-4-2').focus();
			}else{
				$('#fin').focus();
			}				
		} 
	});

	//nivel 4 pregunta 3
	$('#lvl4input-3-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-3-1').val()==''){
			$('#lvl4input-2-8').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-3-3').focus();
		}
		if($('#lvl4input-3-1').val()!='')
		{
			if($('#lvl4input-4-1').val()!=''){
				$('#lvl4input-3-3').focus();
			}else{
				$('#lvl4input-4-1').focus();
			}	
		}
		actual=3
	});
	$('#lvl4input-3-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-3-3').val()==''){
			if($('#lvl4input-4-1').val()!=''){
				$('#lvl4input-3-1').focus();
			}else{
				$('#lvl4input-4-1').focus();
			}	
		}
		if($('#lvl4input-3-3').val()!='')
		{	
			$('#lvl4input-3-4').focus();	
		} 
		actual=3
	});
	$('#lvl4input-3-4').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-3-4').val()==''){
			$('#lvl4input-3-3').focus();
		}
		if($('#lvl4input-3-4').val()!='')
		{	
			if($('#lvl4input-1-1').val()==''){
				$('#lvl4input-1-1').focus();
			}else if($('#lvl4input-2-1').val()==''){
				$('#lvl4input-2-1').focus();
			}else if($('#lvl4input-4-2').val()==''){
				$('#lvl4input-4-2').focus();
			}else{
				$('#fin').focus();
			}				
		} 
	});

	//nivel 4 pregunta 4
	$('#lvl4input-4-1').on('keyup',function(e) {//intercepta la 3
		// Backspace
		if ($('#lvl4input-4-1').val()==''){
			$('#lvl4input-3-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-4-2').focus();
		}
		if($('#lvl4input-4-1').val()!=''){
			if(actual==3){
				$('#lvl4input-3-3').focus();
			}else{
				$('#lvl4input-4-2').focus();
			}
			
		}
	});
	$('#lvl4input-4-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-4-2').val()==''){
			$('#lvl4input-3-4').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-2-3').focus();
		}
		if($('#lvl4input-4-2').val()!='')
		{
			$('#lvl4input-4-3').focus();
		}
	});
	$('#lvl4input-4-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-4-3').val()==''){
			$('#lvl4input-4-2').focus();
		}
		if($('#lvl4input-4-3').val()!='')
		{	
			$('#lvl4input-4-4').focus();	
		} 
	});
	$('#lvl4input-4-4').on('keyup',function(e) {
		// Backspace
		if ($('#lvl4input-4-4').val()==''){
			$('#lvl4input-4-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl4input-4-5').focus();
		}
		if($('#lvl4input-4-4').val()!='')
		{
			if($('#lvl4input-1-6').val()!=''){
				$('#lvl4input-4-6').focus();
			}else{
				$('#lvl4input-1-6').focus();
			}	
		}
		actual=4
	});
	$('#lvl4input-4-6').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl4input-4-6').val()==''){
			if($('#lvl4input-1-6').val()!=''){
				$('#lvl4input-4-4').focus();
			}else{
				$('#lvl4input-1-6').focus();
			}	
		}
		if($('#lvl4input-4-6').val()!='')
		{	
			if($('#lvl4input-2-5').val()==''){
				$('#lvl4input-2-5').focus();
			}else if($('#lvl4input-1-1').val()==''){
				$('#lvl4input-1-1').focus();
			}else if($('#lvl4input-3-1').val()==''){
				$('#lvl4input-3-1').focus();
			}else if($('#lvl4input-2-1').val()==''){
				$('#lvl4input-2-1').focus();
			}else{
				$('#fin').focus();
			}				
		} 
		actual=4
	});

	//nivel 5 pregunta 1
	$('#lvl5input-1-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-1').val()==''){
			$('#lvl5input-1-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-2').focus();
		}
		if($('#lvl5input-1-1').val()!='')
		{
			$('#lvl5input-1-2').focus();
		}
	});
	$('#lvl5input-1-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-2').val()==''){
			$('#lvl5input-1-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-3').focus();
		}
		if($('#lvl5input-1-2').val()!='')
		{
			$('#lvl5input-1-3').focus();
		}
	});
	$('#lvl5input-1-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-1-3').val()==''){
			$('#lvl5input-1-2').focus();
		}
		if($('#lvl5input-1-3').val()!='')
		{	
			$('#lvl5input-1-4').focus();	
		} 
	});
	$('#lvl5input-1-4').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-4').val()==''){
			$('#lvl5input-1-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-5').focus();
		}
		if($('#lvl5input-1-4').val()!='')
		{
			$('#lvl5input-1-5').focus();
		}
	});
	$('#lvl5input-1-5').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-5').val()==''){
			$('#lvl5input-1-4').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-6').focus();
		}
		if($('#lvl5input-1-5').val()!='')
		{	
			$('#lvl5input-1-6').focus();			
		}
		actual=1
	});
	$('#lvl5input-1-6').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-6').val()==''){
			$('#lvl5input-1-5').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-7').focus();
		}
		if($('#lvl5input-1-6').val()!='')
		{	
			$('#lvl5input-1-7').focus();		
		}
	});
	$('#lvl5input-1-7').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-7').val()==''){
			$('#lvl5input-1-6').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-3').focus();
		}
		if($('#lvl5input-1-7').val()!='')
		{
			if($('#lvl5input-1-8').val()!=''){
				$('#lvl5input-1-9').focus();
			}else{
				$('#lvl5input-1-8').focus();
			}			
		}
		actual=1
	});
	$('#lvl5input-1-8').on('keyup',function(e) {//intercepta la 3
		// Backspace
		if ($('#lvl5input-1-8').val()==''){
			if(actual==1){
				$('#lvl5input-1-7').focus();
			}else{
				$('#lvl5input-4-4').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl5input-1-9').focus();
		}
		if($('#lvl5input-1-8').val()!='')
		{	
			$('#lvl5input-1-9').focus();			
		}
	});
	$('#lvl5input-1-9').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-9').val()==''){
			$('#lvl5input-1-8').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-10').focus();
		}
		if($('#lvl5input-1-9').val()!='')
		{
			$('#lvl5input-1-10').focus();
		}
		actual=1
	});
	$('#lvl5input-1-10').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-10').val()==''){
			$('#lvl5input-1-9').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-11').focus();
		}
		if($('#lvl5input-1-10').val()!='')
		{
			$('#lvl5input-1-11').focus();
		}
		actual=1
	});
	$('#lvl5input-1-11').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-11').val()==''){

			$('#lvl5input-1-10').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-9').focus();
		}
		if($('#lvl5input-1-11').val()!='')
		{
			$('#lvl5input-1-12').focus();
		}
		actual=1
	});
	$('#lvl5input-1-12').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-1-12').val()==''){
			$('#lvl5input-1-11').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-13').focus();
		}
		if($('#lvl5input-1-12').val()!='')
		{
			$('#lvl5input-1-13').focus();
		}
		actual=1
	});
	$('#lvl5input-1-13').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-1-13').val()==''){
			$('#lvl5input-1-12').focus();
		}
		if($('#lvl5input-1-13').val()!='')
		{	
			if($('#lvl5input-2-1').val()==''){
				$('#lvl5input-2-1').focus();
			}else if($('#lvl5input-3-1').val()==''){
				$('#lvl5input-3-1').focus();
			}else if($('#lvl5input-4-1').val()==''){
				$('#lvl5input-4-1').focus();
			}else{
				$('#fin').focus();
			}				
		} 
	});

	//nivel 5 pregunta 2
	$('#lvl5input-2-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-2-1').val()==''){
			$('#lvl5input-1-13').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-1-2').focus();
		}
		if($('#lvl5input-2-1').val()!='')
		{
			if($('#lvl5input-2-2').val()!=''){
				$('#lvl5input-2-3').focus();
			}else{
				$('#lvl5input-2-2').focus();
			}				
		}
		actual=2
	});
	$('#lvl5input-2-2').on('keyup',function(e) {//intercepta la 3
		// Backspace
		if ($('#lvl5input-2-2').val()==''){
			if(actual==2){
				$('#lvl5input-2-1').focus();
			}else{
				$('#lvl5input-3-3').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl5input-1-9').focus();
		}
		if($('#lvl5input-2-2').val()!='')
		{	
			if(actual==2){
				$('#lvl5input-2-3').focus();
			}else{
				$('#lvl5input-4-1').focus();
			}			
		}
	});
	$('#lvl5input-2-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-2-3').val()==''){
			$('#lvl5input-2-2').focus();
		}
		if($('#lvl5input-2-3').val()!='')
		{	
			$('#lvl5input-2-4').focus();	
		} 
		actual=2
	});
	$('#lvl5input-2-4').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-2-4').val()==''){
			$('#lvl5input-2-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-2-5').focus();
		}
		if($('#lvl5input-2-4').val()!='')
		{
			if($('#lvl5input-2-5').val()!=''){
				$('#lvl5input-2-6').focus();
			}else{
				$('#lvl5input-2-5').focus();
			}	
		}
		actual=2
	});
	$('#lvl5input-2-5').on('keyup',function(e) {//intercepta la 4
		// Backspace
		if ($('#lvl5input-2-5').val()==''){
			if(actual==2){
				$('#lvl5input-2-4').focus();
			}else{
				$('#lvl5input-4-3').focus();
			}
		}
		if (e.keyCode==39){
			$('#lvl5input-2-6').focus();
		}
		if($('#lvl5input-2-5').val()!='')
		{	
			if(actual==2){
				$('#lvl5input-2-6').focus();
			}else{
				$('#lvl5input-4-4').focus();
			}		
		}
	});
	$('#lvl5input-2-6').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-2-6').val()==''){
			$('#lvl5input-2-5').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-2-7').focus();
		}
		if($('#lvl5input-2-6').val()!='')
		{	
			$('#lvl5input-2-7').focus();		
		}
		actual=2
	});
	$('#lvl5input-2-7').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-2-7').val()==''){
			$('#lvl5input-2-6').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-2-8').focus();
		}
		if($('#lvl5input-2-7').val()!='')
		{
			$('#lvl5input-2-8').focus();			
		}
		actual=2
	});
	$('#lvl5input-2-8').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-2-8').val()==''){
			$('#lvl5input-2-7').focus();
		}
		if($('#lvl5input-2-8').val()!='')
		{	
			if($('#lvl5input-1-1').val()==''){
				$('#lvl5input-1-1').focus();
			}else if($('#lvl5input-3-1').val()==''){
				$('#lvl5input-3-1').focus();
			}else if($('#lvl5input-4-1').val()==''){
				$('#lvl5input-4-1').focus();
			}else{
				$('#fin').focus();
			}				
		} 
	});
	
	//nivel 5 pregunta 3
	$('#lvl5input-3-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-3-1').val()==''){
			$('#lvl5input-2-8').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-3-2').focus();
		}
		if($('#lvl5input-3-1').val()!='')
		{
			$('#lvl5input-3-2').focus();
		}
	});
	$('#lvl5input-3-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-3-2').val()==''){
			$('#lvl5input-3-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-3-3').focus();
		}
		if($('#lvl5input-3-2').val()!='')
		{
			$('#lvl5input-3-3').focus();
		}
	});
	$('#lvl5input-3-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-3-3').val()==''){
			$('#lvl5input-3-2').focus();
		}
		if($('#lvl5input-3-3').val()!='')
		{	
			if($('#lvl5input-2-2').val()==''){
				$('#lvl5input-2-2').focus();
			}else if($('#lvl5input-1-1').val()==''){
				$('#lvl5input-1-1').focus();
			}else if($('#lvl5input-2-1').val()==''){
				$('#lvl5input-2-1').focus();
			}else if($('#lvl5input-4-1').val()==''){
				$('#lvl5input-4-1').focus();
			}else{
				$('#fin').focus();
			}
		} 
		actual=3
	});
		
	//nivel 5 pregunta 4
	$('#lvl5input-4-1').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-4-1').val()==''){
			$('#lvl5input-3-3').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-4-2').focus();
		}
		if($('#lvl5input-4-1').val()!='')
		{
			$('#lvl5input-4-2').focus();
		}
	});
	$('#lvl5input-4-2').on('keyup',function(e) {
		// Backspace
		if ($('#lvl5input-4-2').val()==''){
			$('#lvl5input-4-1').focus();
		}
		if (e.keyCode==39){
			$('#lvl5input-4-3').focus();
		}
		if($('#lvl5input-4-2').val()!='')
		{
			$('#lvl5input-4-3').focus();
		}
	});
	$('#lvl5input-4-3').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-4-3').val()==''){
			$('#lvl5input-4-2').focus();
		}
		if($('#lvl5input-4-3').val()!='')
		{	
			if($('#lvl5input-2-5').val()!=''){
				$('#lvl5input-4-4').focus();
			}else{
				$('#lvl5input-2-5').focus();
			}
		} 
		actual=4
	});
	$('#lvl5input-4-4').on('keyup',function(e) {
		// Backspace			
		if ($('#lvl5input-4-4').val()==''){
			if($('#lvl5input-2-5').val()!=''){
				$('#lvl5input-4-3').focus();
			}else{
				$('#lvl5input-2-5').focus();
			}
		}
		if($('#lvl5input-4-3').val()!='')
		{	
			if($('#lvl5input-1-8').val()==''){
				$('#lvl5input-1-8').focus();
			}else if($('#lvl5input-1-1').val()==''){
				$('#lvl5input-1-1').focus();
			}else if($('#lvl5input-2-1').val()==''){
				$('#lvl5input-2-1').focus();
			}else if($('#lvl5input-4-1').val()==''){
				$('#lvl5input-4-1').focus();
			}else{
				$('#fin').focus();
			}
		} 
		actual=4
	});
}
function ShowQuestion(id){
	if (id == 1){
		$("#q1").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 2){
		$("#q2").show(500);
		$("#q1").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 3){
		$("#q3").show(500);
		$("#q2").hide(500);
		$("#q1").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 4){
		$("#q4").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q1").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 5){
		$("#q5").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q1").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 6){
		$("#q6").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q1").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 7){
		$("#q7").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q1").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 8){
		$("#q8").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q1").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 9){
		$("#q9").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q1").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 10){
		$("#q10").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q1").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 11){
		$("#q11").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q1").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 12){
		$("#q12").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q1").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 13){
		$("#q13").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q1").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 14){
		$("#q14").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q1").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 15){
		$("#q15").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q1").hide(500);
		$("#q16").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 16){
		$("#q16").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q1").hide(500);
		$("#q17").hide(500);
	}
	else if (id == 17){
		$("#q17").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		$("#q16").hide(500);
		$("#q1").hide(500);
	}
}
function public_TW(){


	var msj="GAME: CROSSWORD  LEVEL:"+leveltcp+" TOPIC:TCP  POINTS: "+punto;
	var src='https://angelgame.acostasite.com/images/background/crucigramas.jpg';
	window.open("https://twitter.com/share?text="+msj+"&url=https://angelgame.acostasite.com/Game/juegos/crucigrama/crosswordip6-es.html");
}
function reload(){

  location.reload();
}
var dataInstag;
function public_IG(){
	console.log("entro en instagram");
	var msj="GAME: CROSSWORD  LEVEL:"+leveltcp+" TOPIC:TCP  POINTS: "+punto;
	 Instagram.isInstalled(function (err, installed) {
		if (installed) {
			console.log("Instagram is"+ installed); 
			navigator.screenshot.save(function(error,response){
				if(error){
					console.error(error);
					return;
				}
				console.log(response.filePath);
				getBase64FromImageUrl(response.filePath, msj);
				
			});
		} else {
			alert("Instagram no esta instalado");
		}
	});
}
function getBase64FromImageUrl(url, msj) {
	var img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.width =this.width;
		canvas.height =this.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(this, 0, 0);

		var dataURL = canvas.toDataURL("image/png");
		dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		
		Instagram.share(dataInstag, msj, function (err) {
			if (err) {
				console.log("Not shared");
			} else {
				console.log("shared");
			}
		});
	};

	img.src = url;
}
function Zoom(src){
	console.log("zoom", src)
	$("#imgZoom").attr('src', src);
	$("#ModalZoom").css({"display": "block"});
	document.getElementById("imgZoom").className ="imgZm animated fadeInUp slow"; 
}
function cerrarZoom(){
	console.log("Cerrar Zoom")
	$("#ModalZoom").css({"display": "none"});
}