var PlayMusica = localStorage.getItem('Audio');
window.localStorage.setItem('audio', PlayMusica); 
var AudioEffect = localStorage.getItem('sound-acert');
window.localStorage.setItem('#sound-acert', AudioEffect);
var AllCruciStarIpv4 = Number(window.localStorage.getItem('AllCruciStarIpv4'));
var AllCruciStarIpv6 = Number(window.localStorage.getItem('AllCruciStarIpv6'));
var AllCruciStarGover = Number(window.localStorage.getItem('AllCruciStarGover'));
var AllCruciStarTcp = Number(window.localStorage.getItem('AllCruciStarTcp'));
var ttl = Number(window.localStorage.getItem('ttl'));
var coin = Number(window.localStorage.getItem('coin'));
var audioElement = document.createElement('audio');
var levelIpv4 = localStorage.getItem('levelIpv4');
var levelIpv6 = localStorage.getItem('levelIpv6');
var levelGover = localStorage.getItem('levelGover');
var leveltcp = localStorage.getItem('leveltcp');
var language = localStorage.getItem('selectedLang');
var tituloTTL
var TiempoTTL
var ComprarTTL
var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
if(language=='Eng'){
    tituloTTL='YOU DO NOT HAVE MORE TTL'
    TiempoTTL='Time until next TTL'
    ComprarTTL='Buy TTL'
}else if(language=='Por'){
    tituloTTL='VOCÊ NÃO TEM MAIS TTL'
    TiempoTTL='Tempo até o próximo TTL'
    ComprarTTL='Compre TTL'
}else {
    tituloTTL='NO TIENES MÁS TTL'
    TiempoTTL='Tiempo hasta el próximo TTL'
    ComprarTTL='Comprar TTL'
}
if(levelIpv4 == null || levelIpv4 == ''|| levelIpv4 == '0'){
	window.localStorage.setItem('levelIpv4', '1');
}
if(levelIpv6 == null || levelIpv6 == ''|| levelIpv6 == '0'){
	window.localStorage.setItem('levelIpv6', '1');
}
if(levelGover == null || levelGover == ''|| levelGover == '0'){
	window.localStorage.setItem('levelGover', '1');
}
if(leveltcp == null || leveltcp == ''|| leveltcp == '0'){
	window.localStorage.setItem('leveltcp', '1');
}
if(AllCruciStarIpv4 == null){
	window.localStorage.setItem('AllCruciStarIpv4', '0'); 
}
if(AllCruciStarIpv6 == null){
	window.localStorage.setItem('AllCruciStarIpv6', '0'); 
}
if(AllCruciStarGover == null){
	window.localStorage.setItem('AllCruciStarGover', '0'); 
}
if(AllCruciStarTcp == null){
	window.localStorage.setItem('AllCruciStarTcp', '0'); 
}

//html ttl
var htmlTTL='<div class="modal-dialog" role="document">'
htmlTTL+='             <div  class="modal-content">'
htmlTTL+='                  <div class="modal-body text-center">'
htmlTTL+='                       <div class="box">'
htmlTTL+='                           <h2 class="modal-title animated zoomIn TTL">'+tituloTTL+'</h2>'
htmlTTL+='                           <div class="row">'
htmlTTL+='                               <div class="col-md-4 col-xs-4 ">'
htmlTTL+='                                   <img id="imgTTL" src="imgs/ttl0.png" alt="" class="animated jackInTheBox" >'
htmlTTL+='                               </div>'
htmlTTL+='                               <div class="col-md-8 col-xs-8">'
htmlTTL+='                                   <p class="post">'+TiempoTTL+'</p>'
htmlTTL+='                                   <div id="TiempoTTL"><span id="Min">00</span>:<span id="Seg">00</span></div>'
htmlTTL+='                                   <button class="btnCompra" onclick="comprar()">'+ComprarTTL+'</button>'
htmlTTL+='                               </div>'
htmlTTL+='                           </div>'
htmlTTL+='                       </div>'
htmlTTL+='                  </div>' 
htmlTTL+='             <div class="modal-footer">' 
htmlTTL+='                 <a id="reload" class="animated heartBeat delay-1s" style="display: none" href="" onclick="location.reload()">'                
htmlTTL+='                    <button class="btnPlayIcon"></button>'         
htmlTTL+='                 </a>'     
htmlTTL+='                 <a href="/">'     
htmlTTL+='                    <button class="btnHomeIcon"></button>'             
htmlTTL+='                 </a>'             
htmlTTL+='            </div>'               
htmlTTL+='      </div>'     
htmlTTL+='</div>' 

$(document).ready(function(){
	$("#levelModal .modal-content").append(infoLink)
	showTuto('crucigrama');
	starsCruci();
	SumaCont();
	$('#TTLModal').html(htmlTTL) 
	$('#levelModal').modal('show');	
	$('#levelModal button').click(function(){
		// Set time
		if ($('#cmbTime').val()==''){
			timeLevel = 200;
			$('#cmbTime').val(timeLevel);
		}
		 else
			timeLevel = parseInt($('#cmbTime').val());
		
		// Guardar
		localStorage.setItem("timeCrossWord",timeLevel);
		
		// Hide
		$('#levelModal').modal('hide');
	})
	var WindowW = $(window).innerWidth();
	var WindowH = $(window).innerHeight();

	$('.crucigrama').css({
		'height':WindowH + 'px',
		'width':WindowW + 'px'
	});
	if(ttl == 0){
		contadorMinutos();
		blockTTL();			
	}else if(ttl <= 4){
		contadorMinutos();
	}
	//Control de sonido
    PlayMusic(window.localStorage.getItem('audio'));

 //    //Pausa el sonido cuando se ejecuta en segundo plano
	// $.winFocus(function(event, play) {

	// 	if (play) 
	// 		$(".play").stop().delay('fast', function(e) {
	// 			audioElement.play();
	// 		});
	// 	else {
	// 		audioElement.pause();
	// 	}
	// }, false);

});

//funcion para mostrar el las imagenes por idioma
function tutorial(language, juego){
	if (language == "Eng") {
		$('.titAvisoTuto').text('Hello, welcome to the Crossword game, we will explain how to play.');
		$('.tutoBtn').text('Close');
		$('.txtCheck').text('Do not show again');		
	}else if (language == "Por"){		
		$('.titAvisoTuto').text('Olá, bem-vindo ao jogo da Palavras cruzadas, vamos explicar como jogar.');
		$('.tutoBtn').text('Fechar');
		$('.txtCheck').text('Não mostrar novamente');
	}else{
		$('.titAvisoTuto').text('Hola, bienvenidos al juego de Crucigrama, te explicaremos cómo jugar.');
		$('.tutoBtn').text('Cerrar');
		$('.txtCheck').text('No volver a mostrar');
	}	
	$('.img1').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'-'+language+'.jpg');
	$('.img2').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'2-'+language+'.jpg');
}
$(window).resize(function(){
	var WindowW = $(window).innerWidth();
	var WindowH = $(window).innerHeight();

	$('.crucigrama').css({
		'height':WindowH + 'px',
		'width':WindowW + 'px'
	});
	
})
function blockTTL(){
	$('#levelModal').modal('hide');
	$('#TTLModal').modal('show');
}
function starsCruci(){
	$('#StarIPv4').text(AllCruciStarIpv4);
	$('#StarIPv6').text(AllCruciStarIpv6);
	$('#StarGovernance').text(AllCruciStarGover);
	$('#StarTcp').text(AllCruciStarTcp);
}
function SumaCont(){
	var contAllStarCruci = AllCruciStarIpv4 + AllCruciStarIpv6 + AllCruciStarGover + AllCruciStarTcp 
	window.localStorage.setItem('contAllStarCruci', contAllStarCruci); 
	console.log(contAllStarCruci);
}
//Activar el sonido
function PlayMusic(active){
	if (active == "1"){
		audioElement.setAttribute('id', 'sonido');
		audioElement.setAttribute('src', 'sounds/world3.mp3');
    	audioElement.setAttribute('autoplay', 'autoplay');
    	audioElement.addEventListener("ended", function() {
	        this.currentTime = 0;
	        this.play();
	    }, false);
	}
	else {
		audioElement.pause();
		active=0;
	}
}
var dataInstag;
function public_IG(){


	console.log("entro en instagram");
	var msj="GAME: OUTPUT INTERPRETER TOPIC:IPv4  POINTS: ";
	/*window.plugins.socialsharing.shareViaInstagram(
		'Message via Instagram', 
		'https://angelgame.acostasite.com/images/icon/icon.png','https://angelgame.acostasite.com'
	  );	*/
	 // var assetLocalIdentifier = "../../img/congratulations.png";
	 Instagram.isInstalled(function (err, installed) {
		if (installed) {
			console.log("Instagram is"+ installed); // installed app version on Android
			navigator.screenshot.save(function(error,response){
				if(error){
					console.error(error);
					return;
				}
				
				// Something like: /storage/emulated/0/Pictures/screenshot_1477924039236.jpg
				console.log(response.filePath);
		
				/*Instagram.shareAsset(function(result) {
					alert('Instagram.shareAsset success: ' + result);
				}, function(e) {
					alert('Instagram.shareAsset error: ' + e);
				}, response.filePath);*/
				getBase64FromImageUrl(response.filePath, msj);
				
			});
		} else {
			alert("Instagram no esta instalado");
		}
	});

	
	 

/*	module.controller('ThisCtrl', function($scope, $cordovaInstagram) {
		// Get image from camera, base64 is good. See the
		// $cordovaCamera docs for more info
	  
		$cordovaInstagram.share($scope.image.data, $scope.image.caption).then(function() {
		  // Worked
		}, function(err) {
		  // Didn't work
		});
	  })*/
  }

  function getBase64FromImageUrl(url, msj) {
	var img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.width =this.width;
		canvas.height =this.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(this, 0, 0);

		var dataURL = canvas.toDataURL("image/png");
		dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		
		Instagram.share(dataInstag, msj, function (err) {
			if (err) {
				console.log("Not shared");
			} else {
				console.log("shared");
			}
		});
	};

	img.src = url;
}