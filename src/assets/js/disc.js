var PlayMusica = localStorage.getItem('Audio');
window.localStorage.setItem('audio', PlayMusica); 
var AudioEffect = localStorage.getItem('sound-acert');
window.localStorage.setItem('#sound-acert', AudioEffect);
var language = localStorage.getItem('selectedLang');
var DiscStarIpv4 = Number(window.localStorage.getItem('DiscStarIpv4'));
var DiscStarIpv6 = Number(window.localStorage.getItem('DiscStarIpv6'));
var starEasy = window.localStorage.getItem('DiscStarIpv4Easy');
var starHard = window.localStorage.getItem('DiscStarIpv4Hard');
var ttl = Number(window.localStorage.getItem('ttl'));
var coin = Number(window.localStorage.getItem('coin'));
var segPlus=5;
var DiscoSpeed=0;
var PreguntaSpeed=0;
var timeLevel=0;
var nivel=0;
var levelTxt
var seconds_left;
var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
if(DiscStarIpv4 == null){
	window.localStorage.setItem('DiscStarIpv4', '0'); 
}
if(DiscStarIpv6 == null){
	window.localStorage.setItem('DiscStarIpv6', '0'); 
}
if(starEasy == null){
  starEasy = '0'; 
}
if(starHard == null){
    starHard = '0'; 
}
function starMenu(){
  $('#StarIPv6').text(DiscStarIpv6);
  $('#StarIPv4').text(DiscStarIpv4);  
}
if(this.language == 'Eng'){
  localStorage.setItem('language', '2');
}else if(this.language == 'Por'){
  localStorage.setItem('language', '3');
}else{
  localStorage.setItem('language', '1');
}
var audioElement = document.createElement('audio');
if(localStorage.getItem("language")==1)
{

    var questions = [{
    descrip: "Bucle de retorno",
    choices: ["127.0.0.1","127.1.0.0","0.0.0.1/1","127.127.0/1","255.0.0.1"],
    correctAnswer: "127.0.0.1",
    color: "#F7FF06"
     }, {
    descrip: "Ruta por defecto",
    choices: ["0.0.0.11","0.0.0.0","124.0.1.0","0.0.0","0.0.1.1"],
    correctAnswer: "0.0.0.0",
    color: "#60FF00"
    }, {
    descrip: "Multidifusión",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.255.255.255","255.0.0.0"],
    correctAnswer: ["224.0.0.0", "255.255.255.255"],
    color: "#FFFFFF"
    },{
    descrip: "Clase A",
    choices: ["0.0.0.0","Full-Network","0.0.0.0/255","Broadcast","127.255.255.255"],
    correctAnswer: ["0.0.0.0", "127.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Clase B",
    choices: ["128.0.0.0","239.0.0.255","191.255.255.255","255.0.0.0","Broadcast"],
    correctAnswer: ["128.0.0.0", "191.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Clase C",
    choices: ["224.0.0.0","192.0.0.0","224.0.0.0/1","223.255.255.255","Broadcast"],
    correctAnswer: ["192.0.0.0", "223.255.255.255"],
    color: "#F7FF06"
     //},{
    // descrip: "Clase D",
    // choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.255.255.255","239.255.255.255"],
    // correctDnswer: ["224.0.0.0", "239.255.255.255"],
    // color: "#60FF00"
     }, {
    descrip: "Clase E",
    choices: ["240.0.0.0","239.0.0.255","255.255.255.255","Broadcast","224.0.0.255"],
    correctAnswer: ["240.0.0.0", "255.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Nat a nivel de operador",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","100.64.0.0/10","255.0.0.0"],
    correctAnswer: ["100.64.0.0/10"],
    color: "#F7FF06"
    },{
    descrip: "Link local",
    choices: ["224.0.0.0","169.254.0.0/16","224.0.0.0/1","255.0.0.0","224.0.0.255"],
    correctAnswer: ["169.254.0.0/16"],
    color: "#60FF00"
    },{
    descrip: "Comunicaciones dentro de una red privada",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.0.0.0","172.16.0.0/12"],
    correctAnswer: ["172.16.0.0/12"],
    color: "#60FF00"
    },{
    descrip: "Solo Valida como dirección de origen",
    choices: ["224.0.0.0","239.0.0.255","0.0.0.0/8","255.0.0.0","224.0.0.255"],
    correctAnswer: ["0.0.0.0/8"],
    color: "#F7FF06"
    }, {
    descrip: "Dirección Privada",
    choices: ["172.0.0.0","168.10.0.1","192.168.0.0/16","10.0.0.0/8","172.16.0.0/12"],
    correctAnswer: ["10.0.0.0/8","172.16.0.0/12","192.168.0.0/16"],
    color: "#60FF00"
    }]; 



}else if(localStorage.getItem("language")==2){

    var questions = [{
    descrip: "Loopback",
    choices: ["127.0.0.1","127.1.0.0","0.0.0.1/1","127.127.0/1","255.0.0.1"],
    correctAnswer: "127.0.0.1",
    color: "#F7FF06"
     }, {
    descrip: "Default route",
    choices: ["0.0.0.11","0.0.0.0","124.0.1.0","0.0.0","0.0.1.1"],
    correctAnswer: "0.0.0.0",
    color: "#60FF00"
    }, {
    descrip: "Multicast",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.255.255.255","255.0.0.0"],
    correctAnswer: ["224.0.0.0", "255.255.255.255"],
    color: "#FFFFFF"
    },{
    descrip: "Class A",
    choices: ["0.0.0.0","Full-Network","0.0.0.0/255","Broadcast","127.255.255.255"],
    correctAnswer: ["0.0.0.0", "127.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Class B",
    choices: ["128.0.0.0","239.0.0.255","191.255.255.255","255.0.0.0","Broadcast"],
    correctAnswer: ["128.0.0.0", "191.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Class C",
    choices: ["224.0.0.0","192.0.0.0","224.0.0.0/1","223.255.255.255","Broadcast"],
    correctAnswer: ["192.0.0.0", "223.255.255.255"],
    color: "#F7FF06"
    // },{
    // descrip: "Class D",
    // choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.255.255.255","239.255.255.255","Broadcast"],
    // correctDnswer: ["224.0.0.0", "239.255.255.255"],
    // color: "#60FF00"
    }, {
    descrip: "Class E",
    choices: ["240.0.0.0","239.0.0.255","255.255.255.255","Broadcast","224.0.0.255"],
    correctAnswer: ["240.0.0.0", "255.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Nat at operator level",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","100.64.0.0/10","255.0.0.0"],
    correctAnswer: ["100.64.0.0/10"],
    color: "#F7FF06"
    },{
    descrip: "Local link",
    choices: ["224.0.0.0","169.254.0.0/16","224.0.0.0/1","255.0.0.0","224.0.0.255"],
    correctAnswer: ["169.254.0.0/16"],
    color: "#60FF00"
    },{
    descrip: "Communications within a private network",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.0.0.0","172.16.0.0/12"],
    correctAnswer: ["172.16.0.0/12"],
    color: "#60FF00"
    },{
    descrip: "Only valid as origin address",
    choices: ["224.0.0.0","239.0.0.255","0.0.0.0/8","255.0.0.0","224.0.0.255"],
    correctAnswer: ["0.0.0.0/8"],
    color: "#F7FF06"
    }, {
    descrip: "Private address",
    choices: ["172.0.0.0","168.10.0.1","192.168.0.0/16","10.0.0.0/8","172.16.0.0/12"],
    correctAnswer: ["10.0.0.0/8","172.16.0.0/12","192.168.0.0/16"],
    color: "#60FF00"
    }]; 

}else if(localStorage.getItem("language")==3){

    var questions = [{
    descrip: "Loopback",
    choices: ["127.0.0.1","127.1.0.0","0.0.0.1/1","127.127.0/1","255.0.0.1"],
    correctAnswer: "127.0.0.1",
    color: "#F7FF06"
     }, {
    descrip: "Rota padrão",
    choices: ["0.0.0.11","0.0.0.0","124.0.1.0","0.0.0","0.0.1.1"],
    correctAnswer: "0.0.0.0",
    color: "#60FF00"
    }, {
    descrip: "Multicast",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.255.255.255","255.0.0.0"],
    correctAnswer: ["224.0.0.0", "255.255.255.255"],
    color: "#FFFFFF"
    },{
    descrip: "Class A",
    choices: ["0.0.0.0","Full-Network","0.0.0.0/255","Broadcast","127.255.255.255"],
    correctAnswer: ["0.0.0.0", "127.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Class B",
    choices: ["128.0.0.0","239.0.0.255","191.255.255.255","255.0.0.0","Broadcast"],
    correctAnswer: ["128.0.0.0", "191.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Class C",
    choices: ["224.0.0.0","192.0.0.0","224.0.0.0/1","223.255.255.255","Broadcast"],
    correctAnswer: ["192.0.0.0", "223.255.255.255"],
    color: "#F7FF06"
    // },{
    // descrip: "Class D",
    // choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.255.255.255","239.255.255.255","Broadcast"],
    // correctDnswer: ["224.0.0.0", "239.255.255.255"],
    // color: "#60FF00"
    }, {
    descrip: "Class E",
    choices: ["240.0.0.0","239.0.0.255","255.255.255.255","Broadcast","224.0.0.255"],
    correctAnswer: ["240.0.0.0", "255.255.255.255"],
    color: "#60FF00"
    },{
    descrip: "Nat ao nível do operador",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","100.64.0.0/10","255.0.0.0"],
    correctAnswer: ["100.64.0.0/10"],
    color: "#F7FF06"
    },{
    descrip: "Link local",
    choices: ["224.0.0.0","169.254.0.0/16","224.0.0.0/1","255.0.0.0","224.0.0.255"],
    correctAnswer: ["169.254.0.0/16"],
    color: "#60FF00"
    },{
    descrip: "Comunicações em uma rede privada",
    choices: ["224.0.0.0","239.0.0.255","224.0.0.0/1","255.0.0.0","172.16.0.0/12"],
    correctAnswer: ["172.16.0.0/12"],
    color: "#60FF00"
    },{
    descrip: "Válido apenas como endereço de origem",
    choices: ["224.0.0.0","239.0.0.255","0.0.0.0/8","255.0.0.0","224.0.0.255"],
    correctAnswer: ["0.0.0.0/8"],
    color: "#F7FF06"
    }, {
    descrip: "Endereço privado",
    choices: ["172.0.0.0","168.10.0.1","192.168.0.0/16","10.0.0.0/8","172.16.0.0/12"],
    correctAnswer: ["10.0.0.0/8","172.16.0.0/12","192.168.0.0/16"],
    color: "#60FF00"
    }]; 
}
$(document).ready(function(){
  $("#levelModal .modal-content").append(infoLink)
  if(localStorage.getItem("intentar")==1 || localStorage.getItem("intentar")==3 ) 
  {

    $('#levelModal').modal('hide');

    if(localStorage.getItem("intentar")==1){

		DiscoSpeed= 14000;
		PreguntaSpeed= 5000;
		if ($('#cmbTime').val()==''){
			timeLevel = 55;
			$('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());
		nivel=1;
		Timer();
		Questions();

    }else if(localStorage.getItem("intentar")==3){

      DiscoSpeed= 10000;
      PreguntaSpeed= 5000;
      if ($('#cmbTime').val()==''){
			timeLevel = 55;
			$('#cmbTime').val(timeLevel);
		}
		else
			timeLevel = parseInt($('#cmbTime').val());
		nivel=3;
      Timer();
      Questions();
    }

    localStorage.removeItem('intentar');

  }else{

    
    if(ttl == 0){
			contadorMinutos();
			blockTTL();			
		}else if(ttl <= 4){
			contadorMinutos();
    }
    if(ttl > 0){
      $('#levelModal').modal('show');
      $('#levelModal .modal-body button').click(function(){
        $('#levelModal').modal('hide');
      });
      Level();
    }
    
  }
 
  function blockTTL(){
    $('#levelModal').modal('hide');
    $('#TTLModal').modal('show');
 }
  /*-------------------------------------*/
  var w_width = $(window).innerWidth();
  var w_height = $(window).innerHeight();

  /*-------------------------------------*/
  /*    Tamaño de la caja principal      */
  /*-------------------------------------*/
  // $('#box').css({
  //   'width': w_width + 'px',
  //   'height' : w_height + 'px',
  //   'padding' : '5px'
  // });

  /*-------------------------------------*/
  /*Funciones que se ejecutan en el ready*/
  /*-------------------------------------*/
  PlayMusic(window.localStorage.getItem('audio'));
  loadLVL()

});
function displayLVL(){
  $('#lvlN').text(levelTxt);
  $('#lvltxtD').addClass('animated slideInRight');
  $('#lvltxtD').removeAttr('style');
  setTimeout(function() { 
    $('#lvltxtD').removeClass('animated slideInRight');
    $('#lvltxtD').addClass('animated slideOutLeft');
  }, 1500);
}
function Level1(){
  levelD= 'easy'
  if(language=='Eng'){
    levelTxt= 'Easy'
  }else if(language=='Por'){
    levelTxt= 'Fácil'
  }else {
    levelTxt= 'Fácil'
  }
  DiscoSpeed= 14000;
  PreguntaSpeed= 11000;
  displayLVL()
  var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl(1);");
  if ($('#cmbTime').val()==''){
    timeLevel = 65;
    $('#cmbTime').val(timeLevel);
  }
else
  timeLevel = parseInt($('#cmbTime').val());
  nivel=1;
  Timer();
  Questions();
}
function Level2(){
  levelD= 'hard'
  if(language=='Eng'){
    levelTxt= 'hard'
  }else if(language=='Por'){
    levelTxt= 'Díficil'
  }else {
    levelTxt= 'Díficil'
  }
  DiscoSpeed= 10000;
  PreguntaSpeed= 8000;
  displayLVL()
  $('#nextLVL').css({'display':'none'})
  if ($('#cmbTime').val()==''){
  timeLevel = 80;
  $('#cmbTime').val(timeLevel);
}
else
  timeLevel = parseInt($('#cmbTime').val());
  nivel=3;
  Timer();
  Questions();
 
}
function Level(){
  $('#level1').click(function(){
    Level1()
  })
  $('#level2').click(function(){
    Level2()
  })  
}

function loadLVL(){
  if(localStorage.getItem('nextLvl')=='1'){    
    $('#levelModal, .modal-backdrop').addClass('oculto'); 
    Level2() 
  }
  localStorage.setItem('nextLvl', '0')
}
function nextLvl(n){
  if(n==1){
    localStorage.setItem('nextLvl', '1');
    reloadJuego()
  }
  if(n==2){
    localStorage.setItem('nextLvl', '2');
    reloadJuego()
  }  
}
function reloadJuego(){
  location.reload()
}
//funcion para colocar estrellas level
function star(){
  if (starEasy == '3'){      
      document.getElementById("easy1").src = 'imgs/star.png';
      document.getElementById("easy2").src = 'imgs/star.png';
      document.getElementById("easy3").src = 'imgs/star.png';
    }else if(starEasy == '2'){
      document.getElementById("easy1").src = 'imgs/star.png';
      document.getElementById("easy2").src = 'imgs/star.png';
    }else if(starEasy == '1'){
      document.getElementById("easy1").src = 'imgs/star.png';
    }    
    
    if (starHard == '3'){      
      document.getElementById("hard1").src = 'imgs/star.png';
      document.getElementById("hard2").src = 'imgs/star.png';
      document.getElementById("hard3").src = 'imgs/star.png';
    }else if(starHard == '2'){      
      document.getElementById("hard1").src = 'imgs/star.png';
      document.getElementById("hard2").src = 'imgs/star.png';
    }else if(starHard == '1'){      
      document.getElementById("hard1").src = 'imgs/star.png';
    }
}
  /*-------------------------------------*/
  /*Funcion de la activacion del audio*/
  /*-------------------------------------*/
function PlayMusic(active){
  //console.log(window.localStorage.setItem('audio', active))
  if (active == "1"){
    audioElement.setAttribute('id', 'sonido');
    audioElement.setAttribute('src', 'sounds/world1.mp3');
      audioElement.setAttribute('autoplay', 'autoplay');
      audioElement.addEventListener("ended", function() {
          this.currentTime = 0;
          this.play();
      }, false);
  }
  else {
    audioElement.pause();
    active=0;
  }

  /*$.winFocus(function(event, play, audio) {
    if (active=="1"){
      //console.log(active);
      if (play){ 
        $(".play").stop().delay('fast', function(e) {
          audioElement.play();
        });
      
    }else{
        audioElement.pause();
        active=0;
      }
  }

  }, false);*/
}
function segundosPlus(){
  $('#plus').text('+'+segPlus+'s')   
  $('#plus').addClass('animated fadeIn') 
  $('#plus').css({'display':'block'})          
  setTimeout(function() { 
    $('#plus').removeClass('fadeIn')  
    $('#plus').addClass('fadeOut')    
  }, 2000);
  setTimeout(function() {     
    $('#plus').removeClass('fadeOut') 
    $('#plus').css({'display':'none'})    
  }, 3000);
  seconds_left= seconds_left + segPlus
}
var isFinish = false;
var contador = 0;
// tiempo del juego
function Timer(){
  //Conteo Regresivo
  seconds_left = timeLevel;
  // var minutes_left = 0;
  var interval = setInterval(function() {
  //document.getElementById('contratiempo').innerHTML =  '0'+minutes_left +':'+ --seconds_left;
  document.getElementById('contratiempo').innerHTML =   --seconds_left;

  if (seconds_left <= 5){
    $('.ballonsBox').animate({
      'opacity':'0'
    },{ duration: 2500, queue: false });
  }
  if (seconds_left <= 0){
      //Alerta de partida finalizada
  if (contador>=20) {
    if (contador>=35) {
          $('.winstar1').css({'display':'block'});
          $('.winstar2').css({'display':'block'});
          $('.winstar3').css({'display':'block'});
          $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
          contCoin(25);
          count(25);
          if(levelD == 'easy'){
            window.localStorage.setItem('DiscStarIpv4Easy', '3'); 
          }else{
            window.localStorage.setItem('DiscStarIpv4Hard', '3'); 
          }
    }else if (contador>=30 && contador <=34 ) {
      $('.winstar1').css({'display':'block'});
          $('.winstar2').css({'display':'block'});
          $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
          contCoin(20);
          count(20);
          if(starEasy <= 1){
            if(levelD == 'easy'){
              window.localStorage.setItem('DiscStarIpv4Easy', '2'); 
            }else{
              window.localStorage.setItem('DiscStarIpv4Hard', '2'); 
            }
          }  
    }else if (contador>=25 && contador <=29) {
      $('.winstar1').css({'display':'block'});
      $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
      contCoin(15);
      count(15);
          if(starEasy == 0){
            if(levelD == 'easy'){
              window.localStorage.setItem('DiscStarIpv4Easy', '1'); 
            }else{
              window.localStorage.setItem('DiscStarIpv4Hard', '1'); 
            }
          }
    }
 
    $('.ballonsBox').stop().css('opacity','0');    
    $('#WinModal').modal('show');
    $('#contratiempo').css({'color':'transparent'});
    $('.punto').text(contador);
    contStar();
    ShareScore();
    }else{
     $('.ballonsBox').remove();
    $('#LooseModal').modal('show')
    //alert('Game Over');

    //document.getElementById('contratiempo').innerHTML = "Se acabo el tiempo";
    clearInterval(interval);

    $('.punto').text(contador);

    ShareScore();
    ttl--;
		window.localStorage.setItem('ttl', ttl); 
    }  

  }

  }, 1000);
}
//sumar coin
function contCoin(n){
	var sumaCoin= n + coin
	window.localStorage.setItem('coin', sumaCoin); 
}
//efecto contador
function count(n){
	var counter = { var: 0 };
	var contador = TweenMax.to(counter, 4, {
	  var: n, 
	  onUpdate: function () {
		var number = Math.ceil(counter.var);
		$('#GetCoin').html(number);
      if(number === counter.var){ 
        contador.kill(); 
      }
	  },
	  ease:Circ.easeOut
	});
}
function sumaTemas(){  
    // suma de todos los temas
    var AllStarIpv4 = Number(window.localStorage.getItem('DiscStarIpv4'));
    var AllStarIpv6 = Number(window.localStorage.getItem('DiscStarIpv6'));
    var AllContStarIpv4 = AllStarIpv4 + AllStarIpv6;
    console.log('sumando contador all=', AllContStarIpv4)
    window.localStorage.setItem('contStarDisc', AllContStarIpv4); 
}
function contStar(){
  var starEasyc = Number(localStorage.getItem('DiscStarIpv4Easy'));
  var starHardc = Number(localStorage.getItem('DiscStarIpv4Hard'));
  var ContStarIpv4 = starEasyc + starHardc;
	  // console.log('starEasyc =', starEasyc );
	  // console.log('starHardc =', starHardc );
    // console.log('sumando contador=', ContStarIpv4)
  window.localStorage.setItem('DiscStarIpv4', ContStarIpv4); 
  
  sumaTemas();
}
function EffectMusic(active, x){

	window.localStorage.setItem('#sound-acert', active);
	if (active=="1") {
		$('#blopsoun'+x).each(function(){
			this.play();
		});
	}else{
		active=0;
	}

}
function Questions(){
  $('#ttl').text(ttl);
  $('#coin').text(coin);
  blockbtn();
  function IntervalPreguntas(){

    /*Genera la pregunta aleatoria*/
    var cantidad_q = questions.length;
    var random_q = Math.floor((Math.random()* cantidad_q));     
    var descriptions = questions[random_q].descrip;
    
    /* Se consigue el color por la pregunta */
    var color = questions[random_q].color;

    $('#ActualQuestion').text(descriptions);
    $('#ActualQuestion').css('color', color);

    return random_q;

  }
  var random_q = IntervalPreguntas();
  var count = 0;
  var y = 0;
  IntervalOpciones();
  

  /*Funcion que genera opciones dentro de un tiempo*/

  function IntervalOpciones() {
    /*Se obtiene lo que returna la funcion de preguntas*/ 

    var opciones = questions[random_q].choices;
    var cantidad_opciones = questions[random_q].choices.length;
        //console.log(cantidad_opciones+ ' cantidad_opciones');
    if(count < cantidad_opciones){

        /*Se consigue el color de la pregunta actual*/
        var color = questions[random_q].color;

        var random_globo = Math.floor((Math.random()* 5) + 1);
        var width = $('.ballonsBox').innerWidth();
        var x = 1; 
        for (var i = 1; i <= cantidad_opciones; i++) {
            /*Se crea el 'globo'*/
            if(y < 70){
              $('.ballonsBox').append('<div id="disco'+y+'" class="globo'+i+' click" onclick="comparacion('+random_q+',this,'+y+')"><span>' + questions[random_q].choices[count] +'</span><img id="burb'+y+'" class="bomba" src="imgs/bomba.png"><audio id="blopsoun'+y+'" controls preload="auto"><source src="sounds/blop.mp3" controls></source></audio></div>');
             
            }else{

            }
            /*-------------------------------------*/
            /*      Animacion para los globos      */
            /*-------------------------------------*/
             var random_vel = Math.floor((Math.random() * 7000) + 1000);
            $('.globo'+i).animate({bottom: '180%'}, random_vel + DiscoSpeed);            
              /*Se le asigna el color a las opciones*/
              $('#disco'+ y + ' span').css('color', color);
           
            if (x < 4) {
              x++;
            }
            else{
              x = 1;
            }
            /*Obtener las opciones con sus respectivas opciones dentro de un intervalo de tiempo*/
            // console.log(questions[random_q].choices[count]);
            count++;
            y++;
            //console.log(y);
          }  
        /*Condición que reinicia el contador*/
        if(count == cantidad_opciones){
          count = 0;
          isFinish = true;

        }
    }

  } //Fin de IntervalOpciones
  /*Llamado de la función 'increment' donde se especifica el tiempo*/
  
    $('#shotDisc').click(function() {
          var inter=setInterval(function() {
            random_q = IntervalPreguntas();
            IntervalOpciones();
            isFinish = false;
            blockbtn();
            clearInterval(inter);
        }, 500);
    });
    function blockbtn(){
      $('#shotDisc').css({'opacity':'0.7'});
      $('#shotDisc').prop('disabled', true);
      setTimeout(function() {
        $('#shotDisc').removeAttr('style');
        $('#shotDisc').prop('disabled', false);
    }, 4000);
    
  }
  
//    if (isFinish) {
//      setInterval(function() {
//        random_q = IntervalPreguntas();
//        IntervalOpciones();
//        isFinish = false;
//    }, PreguntaSpeed);
//  } 

} //Fin de Questions
/*----------------------------------------------------------*/
/*funcion que determina si el globo es correcto o incorrecto*/
/*----------------------------------------------------------*/
function comparacion(random_q, globo, x){
  EffectMusic(window.localStorage.getItem('#sound-acert'), x);
  var globo_selec = $(globo).text();
  console.log('El globo seleccionado es ' + globo_selec);
  var cantidad_correcta = questions[random_q].correctAnswer.length
  var pregunta_actual = questions[random_q].descrip;
  //Compara las respuestas correctas e incorrectas 
  if(pregunta_actual == "Multicast" || pregunta_actual == "Multidifusión"){

    if(globo_selec == "224.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus()      
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
      }, 500);
      contador = contador + 5;
      $('#punto').text(contador);
      
      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "239.255.255.255"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);


      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }

  }  
  if(pregunta_actual == "Loopback" || pregunta_actual == "Bucle de retorno"){
    if(globo_selec == "127.0.0.1"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);

    }
  }
  if(pregunta_actual == "Ruta por defecto" || pregunta_actual == "Default route" || pregunta_actual == "Rota padrão"){
    if(globo_selec == "0.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Dirección Privada" || pregunta_actual == "Endereço privado" || pregunta_actual == "Private address"){
    if(globo_selec == "10.0.0.0/8"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "172.16.0.0/12"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "192.168.0.0/16"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Clase A" || pregunta_actual == "Class A"){
    if(globo_selec == "0.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "127.255.255.255"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Clase B" || pregunta_actual == "Class B"){
    if(globo_selec == "128.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "191.255.255.255"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Clase C" || pregunta_actual == "Class C"){
    if(globo_selec == "192.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "223.255.255.255"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Clase D" || pregunta_actual == "Class D"){
    if(globo_selec == "224.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "239.255.255.255"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Clase E" || pregunta_actual == "Class E"){
    if(globo_selec == "240.0.0.0"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else if(globo_selec == "255.255.255.255"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Nat a nivel de operador" || pregunta_actual == "Nat ao nível do operador" || pregunta_actual == "Nat at operator level"){
    if(globo_selec == "100.64.0.0/10"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Link local" || pregunta_actual == "Local link"){
    if(globo_selec == "169.254.0.0/16"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Comunicaciones dentro de una red privada" || pregunta_actual == "Comunicações em uma rede privada" || pregunta_actual == "Communications within a private network"){
    if(globo_selec == "172.16.0.0/12"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
  if(pregunta_actual == "Solo Valida como dirección de origen" || pregunta_actual == "Válido apenas como endereço de origem" || pregunta_actual == "Only valid as origin address"){
    if(globo_selec == "0.0.0.0/8"){
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      segundosPlus() 
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      contador = contador + 5;
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-check')
      $('.answer i').css({
        'opacity':'1',
        'color': '#58F975'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
    else{
      document.getElementById("burb"+x).src = "imgs/bom.gif"
      setTimeout(function() { 
        $(globo).stop().css('opacity','0');
     }, 500);
      globo_selec = null;

      if(contador <= 0){
        contador = 0;
      } 
      else{
         contador = contador - 2;
         if(contador <= 0){
          contador = 0;
        }
      }
      $('#punto').text(contador);

      $('.answer i').removeClass();
      $('.answer i').addClass('fa fa-times')
      $('.answer i').css({
        'opacity':'1',
        'color': '#FF3232'
      });
      $('.answer i').animate({
        'opacity':'0'
      },500);
    }
  }
};
function ShareScore(){
  if (localStorage.getItem("UserId")!=null){
  var infogame = { "UserId":localStorage.getItem("UserId"), "GameId":2, "TopicId" :1, "levelId" :nivel,"Score":contador};

  RegisterGame(infogame);

  public_FB();
}
}
function public_TW(){

  var level='';

  switch(nivel) {
      case 1:

      level='EASY';
      break;
    case 2:

      level='MEDIUM';

      break;

    case 3:
              
      level='HARD';

      break;
  }

  var src='https://angelgame.acostasite.com/Game/img/disco.png';

  var msj="GAME: FlYLING DISCS  TOPIC:IPv4  NEVEL: "+level+" POINTS: "+contador;

  window.open("https://twitter.com/share?text="+msj+"&url=https://angelgame.acostasite.com/Game/juegos/discos/index-es.html");
}
function public_FB(){

  var level='';

  switch(nivel) {
      case 1:

      level='EASY';
      break;
    case 2:

      level='MEDIUM';

      break;

    case 3:
              
      level='HARD';

      break;
  }

  var msj="GAME: FlYLING DISCS  TOPIC:IPv4  NEVEL: "+level+" POINTS: "+contador;

  $(".fb-xfbml-parse-ignore").attr("href","https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Fjuegos%2Fdiscos%2Findex.html?description="+msj+"&method=1&amp;src=sdkpreparse");
}
function reload(){
  localStorage.setItem('intentar', nivel);
  location.reload();
}
var dataInstag;
function public_IG(){
  console.log("entro en instagram");
  var level='';
  switch(nivel) {
    case 6:
  
      level='EASY';
      break;
    case 8:
  
      level='MEDIUM';
  
      break;
  
    case 12:
          
      level='HARD';
  
      break;
  }

	var msj="GAME: FlYLING DISCS  TOPIC:IPv4  NEVEL: "+level+" POINTS: "+contador;
	/*window.plugins.socialsharing.shareViaInstagram(
		'Message via Instagram', 
		'https://angelgame.acostasite.com/images/icon/icon.png','https://angelgame.acostasite.com'
	  );	*/
	 // var assetLocalIdentifier = "../../img/congratulations.png";
	 Instagram.isInstalled(function (err, installed) {
		if (installed) {
			console.log("Instagram is"+ installed); // installed app version on Android
			navigator.screenshot.save(function(error,response){
				if(error){
					console.error(error);
					return;
				}
				
				// Something like: /storage/emulated/0/Pictures/screenshot_1477924039236.jpg
				console.log(response.filePath);
		
				/*Instagram.shareAsset(function(result) {
					alert('Instagram.shareAsset success: ' + result);
				}, function(e) {
					alert('Instagram.shareAsset error: ' + e);
				}, response.filePath);*/
				getBase64FromImageUrl(response.filePath, msj);
				
			});
		} else {
			alert("Instagram no esta instalado");
		}
	});

	
	 

/*	module.controller('ThisCtrl', function($scope, $cordovaInstagram) {
		// Get image from camera, base64 is good. See the
		// $cordovaCamera docs for more info
	  
		$cordovaInstagram.share($scope.image.data, $scope.image.caption).then(function() {
		  // Worked
		}, function(err) {
		  // Didn't work
		});
	  })*/
}

  function getBase64FromImageUrl(url, msj) {
	var img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.width =this.width;
		canvas.height =this.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(this, 0, 0);

		var dataURL = canvas.toDataURL("image/png");
		dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		
		Instagram.share(dataInstag, msj, function (err) {
			if (err) {
				console.log("Not shared");
			} else {
				console.log("shared");
			}
		});
	};

	img.src = url;
}

function openResult(evt, tabla) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
	  tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
	  tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(tabla).style.display = "block";
	evt.currentTarget.className += " active";
  }
