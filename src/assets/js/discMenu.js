var language = localStorage.getItem('selectedLang');
var tituloTTL
var TiempoTTL
var ComprarTTL
var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
if(language=='Eng'){
    tituloTTL='YOU DO NOT HAVE MORE TTL'
    TiempoTTL='Time until next TTL'
    ComprarTTL='Buy TTL'
}else if(language=='Por'){
    tituloTTL='VOCÊ NÃO TEM MAIS TTL'
    TiempoTTL='Tempo até o próximo TTL'
    ComprarTTL='Compre TTL'
}else {
    tituloTTL='NO TIENES MÁS TTL'
    TiempoTTL='Tiempo hasta el próximo TTL'
    ComprarTTL='Comprar TTL'
}
var htmlTTL='<div class="modal-dialog" role="document">'
htmlTTL+='             <div  class="modal-content">'
htmlTTL+='                  <div class="modal-body text-center">'
htmlTTL+='                       <div class="box">'
htmlTTL+='                           <h2 class="modal-title animated zoomIn TTL">'+tituloTTL+'</h2>'
htmlTTL+='                           <div class="row">'
htmlTTL+='                               <div class="col-md-4 col-xs-4 ">'
htmlTTL+='                                   <img id="imgTTL" src="imgs/ttl0.png" alt="" class="animated jackInTheBox" >'
htmlTTL+='                               </div>'
htmlTTL+='                               <div class="col-md-8 col-xs-8">'
htmlTTL+='                                   <p class="post">'+TiempoTTL+'</p>'
htmlTTL+='                                   <div id="TiempoTTL"><span id="Min">00</span>:<span id="Seg">00</span></div>'
htmlTTL+='                                   <button class="btnCompra" onclick="comprar()">'+ComprarTTL+'</button>'
htmlTTL+='                               </div>'
htmlTTL+='                           </div>'
htmlTTL+='                       </div>'
htmlTTL+='                  </div>' 
htmlTTL+='             <div class="modal-footer">' 
htmlTTL+='                 <a id="reload" class="animated heartBeat delay-1s" style="display: none" href="" onclick="location.reload()">'                
htmlTTL+='                    <button class="btnPlayIcon"></button>'         
htmlTTL+='                 </a>'     
htmlTTL+='                 <a href="/">'     
htmlTTL+='                    <button class="btnHomeIcon"></button>'             
htmlTTL+='                 </a>'             
htmlTTL+='            </div>'               
htmlTTL+='      </div>'     
htmlTTL+='</div>' 

$(document).ready(function(){
	$("#levelModal .modal-content").append(infoLink)
  showTuto('disco');
  $('#TTLModal').html(htmlTTL) 
}); 
//funcion para mostrar el las imagenes por idioma
function tutorial(language, juego){
	if (language == "Eng") {
		$('.titAvisoTuto').text('Hello, welcome to the Disc game, we will explain how to play.');
		$('.tutoBtn').text('Close');
		$('.txtCheck').text('Do not show again');		
	}else if (language == "Por"){		
		$('.titAvisoTuto').text('Olá, bem-vindo ao jogo da Disco, vamos explicar como jogar.');
		$('.tutoBtn').text('Fechar');
		$('.txtCheck').text('Não mostrar novamente');
	}else{
		$('.titAvisoTuto').text('Hola, bienvenidos al juego de Disco, te explicaremos cómo jugar.');
		$('.tutoBtn').text('Cerrar');
		$('.txtCheck').text('No volver a mostrar');
	}	
	$('.img1').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'-'+language+'.jpg');
	$('.img2').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'2-'+language+'.jpg');
	$('.img3').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'3-'+language+'.jpg');
}
