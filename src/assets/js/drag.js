var PlayMusica = localStorage.getItem('Audio');
window.localStorage.setItem('audio', PlayMusica); 
var AudioEffect = localStorage.getItem('sound-acert');
window.localStorage.setItem('#sound-acert', AudioEffect);
var ttl = Number(window.localStorage.getItem('ttl'));
var contStarDragEternet = Number(localStorage.getItem('contStarDragEternet')); 
var contStarDragIcmpv6 = Number(localStorage.getItem('contStarDragIcmpv6')); 
var contStarDragIpv4 = Number(localStorage.getItem('contStarDragIpv4')); 
var contStarDragIpv4Build = Number(localStorage.getItem('contStarDragIpv4Build'));
var contStarDragIpv6 = Number(localStorage.getItem('contStarDragIpv6')); 
var contStarDragIpv6Build = Number(localStorage.getItem('contStarDragIpv6Build')); 
var contStarDragTcp = Number(localStorage.getItem('contStarDragTcp')); 
var contStarDragUdp = Number(localStorage.getItem('contStarDragUdp')); 
var contStarDragUdpHeader = Number(localStorage.getItem('contStarDragUdpHeader'));
var text1
var text2
var Aceptar
var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
if(language=='Eng'){
    text1= 'You need'
    text2= 'Stars to unlock this level'
    Aceptar='Accept'
}else if(language=='Por'){
    text1= 'Você precisa de'
    text2= 'estrelas para desbloquear este nível'
    Aceptar='Aceitar'
}else {
    text1= 'Necesita'
    text2= 'Estrellas para desbloquear este nivel'
    Aceptar='Aceptar'
}

if(contStarDragEternet == null){
	window.localStorage.setItem('contStarDragEternet', '0'); 
}
if(contStarDragIcmpv6 == null){
	window.localStorage.setItem('contStarDragIcmpv6', '0'); 
}
if(contStarDragIpv4 == null){
	window.localStorage.setItem('contStarDragIpv4', '0'); 
}
if(contStarDragIpv4Build == null){
	window.localStorage.setItem('contStarDragIpv4Build', '0'); 
}
if(contStarDragIpv6 == null){
	window.localStorage.setItem('contStarDragIpv6', '0'); 
}
if(contStarDragIpv6Build == null){
	window.localStorage.setItem('contStarDragIpv6Build', '0'); 
}
if(contStarDragTcp == null){
	window.localStorage.setItem('contStarDragTcp', '0'); 
}
if(contStarDragUdp == null){
	window.localStorage.setItem('contStarDragUdp', '0'); 
}
if(contStarDragUdpHeader == null){
	window.localStorage.setItem('contStarDragUdpHeader', '0'); 
}
            
                        
var htmlAviso ='<div id="BoxAviso" class="">'
    htmlAviso+='    <h1 class="titAviso">'+text1+' <span id="cant">0</span> '+text2+'</h1> '
    htmlAviso+='    <div class="estrellas"><span id="cantTotal">0</span>/<span id="cant2">0</span><img  src="imgs/star.png" alt="star" class="avisoStar"></div>'
    htmlAviso+='    <button class="btnComprar" onclick="cerrarModal()">'+Aceptar+'</button>  ' 
	htmlAviso+='</div>'
	
var audioElement = document.createElement('audio');
$(document).ready(function(){	
	$("#levelModal .modal-content").append(infoLink)
	$('#ModalAviso').html(htmlAviso); 
	showTuto('drag');
	if(ttl == 0){
		contadorMinutos();
		blockTTL();			
	}else if(ttl <= 4){
		contadorMinutos();
	}
	if(ttl > 0){
		$('#levelModal').modal('show');
		$('#levelModal button').click(function(){
			$('#levelModal').modal('hide');
		})
	}
	
	var WindowW = $(window).innerWidth();
	var WindowH = $(window).innerHeight();

	$('.drag').css({
		'height':WindowH + 'px',
		'width':WindowW + 'px'
	});
	//Control de sonido
    PlayMusic(window.localStorage.getItem('audio'));
	starsDrag();
	SumaCont();
 //    //Pausa el sonido cuando se ejecuta en segundo plano
	// $.winFocus(function(event, play) {

	// 	if (play) 
	// 		$(".play").stop().delay('fast', function(e) {
	// 			audioElement.play();
	// 		});
	// 	else {
	// 		audioElement.pause();
	// 	}
	// }, false);

});

//funcion para mostrar el las imagenes por idioma
function tutorial(language, juego){
	if (language == "Eng") {
		$('.titAvisoTuto').text('Hello, welcome to the Drag game, we will explain how to play.');
		$('.tutoBtn').text('Close');
		$('.txtCheck').text('Do not show again');		
	}else if (language == "Por"){		
		$('.titAvisoTuto').text('Olá, bem-vindo ao jogo da Drag, vamos explicar como jogar.');
		$('.tutoBtn').text('Fechar');
		$('.txtCheck').text('Não mostrar novamente');
	}else{
		$('.titAvisoTuto').text('Hola, bienvenidos al juego de Drag, te explicaremos cómo jugar.');
		$('.tutoBtn').text('Cerrar');
		$('.txtCheck').text('No volver a mostrar');
	}	
	$('.img1').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'-'+language+'.jpg');
	$('.img2').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'2-'+language+'.jpg');
	$('.img3').attr('src','imgs/imgTuto/'+juego+'/imgtutorial-'+juego+'3-'+language+'.jpg');
}
	
$(window).resize(function(){
	var WindowW = $(window).innerWidth();
	var WindowH = $(window).innerHeight();

	$('.drag').css({
		'height':WindowH + 'px',
		'width':WindowW + 'px'
	});
	
})

function blockTTL(){
    $('#levelModal').modal('hide');
    $('#TTLModal').modal('show');
 }
//Activar el sonido
function PlayMusic(active){
	if (active == "1"){
		audioElement.setAttribute('id', 'sonido');
		audioElement.setAttribute('src', 'sounds/world3.mp3');
    	audioElement.setAttribute('autoplay', 'autoplay');
    	audioElement.addEventListener("ended", function() {
	        this.currentTime = 0;
	        this.play();
	    }, false);
	}
	else {
		audioElement.pause();
		active=0;
	}
}
function starsDrag(){
	$('#StarEthernet').text(contStarDragEternet);
	$('#StarICMPv6').text(contStarDragIcmpv6);
	$('#StarIPv4').text(contStarDragIpv4);
	$('#StarIPv4Build').text(contStarDragIpv4Build);
	$('#StarIPv6').text(contStarDragIpv6);
	$('#StarIPv6Build').text(contStarDragIpv6Build);
	$('#StarTCP').text(contStarDragTcp);
	$('#StarUDP').text(contStarDragUdp);
	$('#StarUDPBuild').text(contStarDragUdpHeader);
} 
function SumaCont(){
	var contAllStarDrag = Number(localStorage.getItem('contStarDragEternet')) + Number(localStorage.getItem('contStarDragIcmpv6')) + Number(localStorage.getItem('contStarDragIpv4')) + Number(localStorage.getItem('contStarDragIpv4Build')) + Number(localStorage.getItem('contStarDragIpv6')) + Number(localStorage.getItem('contStarDragIpv6Build')) + Number(localStorage.getItem('contStarDragTcp')) + Number(localStorage.getItem('contStarDragUdp')) + Number(localStorage.getItem('contStarDragUdpHeader'))
	window.localStorage.setItem('contAllStarDrag', contAllStarDrag); 
	
}