
  var boxPositionudp = [0,1,2,3,4];
  var arregloFinal = [];
  var cont= 0;
  fin= false;
  var answersudp = [
    "Source Port",
    "Destination Port",
    "Length",
    "Checksum",
    "data",
  ];

  var newArreglo = [];
  var resAnswersudp = [];
  var level=0;
  var timeLevel= 0;
  var punto = 0;
  var nivel=0;
  var starEasy = window.localStorage.getItem('DragUdpStarEasy');
  var starMid = window.localStorage.getItem('DragUdpStarMid');
  var starHard = window.localStorage.getItem('DragUdpStarHard');
  var ContStar = Number(localStorage.getItem('contStarDragUdp'));
  var ttl = Number(window.localStorage.getItem('ttl'));
  var coin = Number(window.localStorage.getItem('coin'));
  var language = localStorage.getItem('selectedLang');
  var infoLink='<a id="info" class="animated pulse infinite delay-2s" href="https://angelgame.acostasite.com/" target="_blank"></a>'
  var lang 
  if(language=='Eng'){
    lang= '-en'
  }else if(language=='Por'){
    lang= '-por'
  }else {
    lang= ''
  }
// console.log('starEasy =', starEasy );
    // console.log('starMid =', starMid );
    // console.log('starHard =', starHard );
     //console.log('contStar= ', ContStar);
  if(starEasy == null){
    starEasy = '0'; 
    localStorage.setItem('DragUdpStarEasy', '0');
    }
    if(starMid == null){
      starMid = '0'; 
      localStorage.setItem('DragUdpStarMid', '0');
    }
    if(starHard == null){
      starHard = '0'; 
      localStorage.setItem('DragUdpStarHard', '0');
    }
    if(ContStar == null){
      localStorage.setItem('contStarDragUdp', '0');
    }
function loadLVL(){
  if(localStorage.getItem('nextLvl')=='1'){    
    $('#levelModal, .modal-backdrop').addClass('oculto'); 
    Level2() 
  }
  if(localStorage.getItem('nextLvl')=='2'){     
   $('#levelModal, .modal-backdrop').addClass('oculto'); 
    Level3()
  } 
  localStorage.setItem('nextLvl', '0')
}
function nextLvl(n){
  if(n==1){
    localStorage.setItem('nextLvl', '1');
    reloadJuego()
  }
  if(n==2){
    localStorage.setItem('nextLvl', '2');
    reloadJuego()
  }  
}
function reloadJuego(){
  location.reload()
}
//Determina el Nivel en que se va a iniciar
function Level1(){
  nivel= 1;
  if(language=='Eng'){
    levelTxt= 'Easy'
  }else if(language=='Por'){
    levelTxt= 'Fácil'
  }else {
    levelTxt= 'Fácil'
  }
  var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl("+nivel+");");
  if ($('#cmbTime').val()==''){
		timeLevel = 40;
    $('#cmbTime').val(timeLevel);
    console.log('lvl1')
	}else{
    timeLevel = parseInt($('#cmbTime').val());
     $('#level1').remove();
     $('#level2').remove();
     $('#level3').remove();
   }
     PlayGame();
     Timer();  
     displayLVL()
}
function Level2(){ 
   nivel= 2;
   if(language=='Eng'){
    levelTxt= 'Medium'
  }else if(language=='Por'){
    levelTxt= 'Meio'
  }else {
    levelTxt= 'Medio'
  }
   var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl("+nivel+");");
    if ($('#cmbTime').val()==''){
  timeLevel = 70;
  $('#cmbTime').val(timeLevel);
  console.log('lvl2')
}
 else{
   timeLevel = parseInt($('#cmbTime').val());
   $('#level1').remove();
   $('#level2').remove();
   $('#level3').remove();
 }
 PlayGame();
 Timer();
 displayLVL()
}
function Level3(){  
   nivel= 3;
   if(language=='Eng'){
    levelTxt= 'Hard'
  }else if(language=='Por'){
    levelTxt= 'Difícil'
  }else {
    levelTxt= 'Difícil'
  }
   var elemClic= document.getElementById('nextLVL')
  elemClic.setAttribute("onclick","nextLvl("+nivel+");");
    if ($('#cmbTime').val()==''){
  timeLevel = 100;
  $('#cmbTime').val(timeLevel);
  console.log('lvl3')
}else{
   timeLevel = parseInt($('#cmbTime').val());
   $('#level1').remove();
   $('#level2').remove();
   $('#level3').remove();
 }
 LevelHard();
  Timer(); 
  var elemModalAviso= document.getElementById('nextLVL')
	    elemModalAviso.setAttribute("onclick","ModalAviso(5);");
  displayLVL()
}
function Level(){
  $('#level1').click(function(){
    Level1()
  })
  $('#level2').click(function(){
    Level2()
  })
  $('#level3').click(function(){
    Level3()
  })
}
function displayLVL(){
		$('#lvlN').text(levelTxt);
		$('#lvltxtD').addClass('animated slideInRight');
		$('#lvltxtD').removeAttr('style');
		setTimeout(function() { 
      $('#lvltxtD').removeClass('animated slideInRight');
      $('#lvltxtD').addClass('animated slideOutLeft');
		}, 1500);
}
$(document).ready(function(){
  $("#levelModal .modal-content").append(infoLink)
  blockDrag();
  loadLVL()
  $('#nextLVL').addClass('bnt5')
  var elemClic= document.getElementById('urlNext')
  elemClic.setAttribute("href","drag-udpHeader"+lang+".html");	
	var elembackwin= document.getElementById('btnbackwin')
	elembackwin.setAttribute("onclick","reloadJuego();");
	var elembacklose= document.getElementById('btnbacklose')
	elembacklose.setAttribute("onclick","reloadJuego();");
  if(localStorage.getItem("intentar")==1 || localStorage.getItem("intentar")==2 || localStorage.getItem("intentar")==3 ) 
  {

    $('#levelModal').modal('hide');

    if(localStorage.getItem("intentar")==1){

       nivel= 1;
        if ($('#cmbTime').val()==''){
		timeLevel = 40;
		$('#cmbTime').val(timeLevel);
	}
	 else
		timeLevel = parseInt($('#cmbTime').val());
       PlayGame();
       Timer();

    }else if(localStorage.getItem("intentar")==2){

       nivel= 2;
       if ($('#cmbTime').val()==''){
		timeLevel = 70;
		$('#cmbTime').val(timeLevel);
	}
	 else
		timeLevel = parseInt($('#cmbTime').val());
       PlayGame();
       Timer();

    }else if(localStorage.getItem("intentar")==3){

      nivel=3;
       if ($('#cmbTime').val()==''){
		timeLevel = 100;
		$('#cmbTime').val(timeLevel);
	}
	 else
		timeLevel = parseInt($('#cmbTime').val());
      LevelHard();
      Timer();
    }

    localStorage.removeItem('intentar');

  }else{
    if(ttl == 0){
			contadorMinutos();
			blockTTL();			
		}else if(ttl <= 4){
			contadorMinutos();
		}
    Level();
    star();
  }
});//Fin de ready
//funcion para colocar estrellas level
function star(){
  if (starEasy == '3'){
      
      document.getElementById("easy1").src = 'imgs/star.png';
      document.getElementById("easy2").src = 'imgs/star.png';
      document.getElementById("easy3").src = 'imgs/star.png';
    }else if(starEasy == '2'){
      document.getElementById("easy1").src = 'imgs/star.png';
      document.getElementById("easy2").src = 'imgs/star.png';
    }else if(starEasy == '1'){
      document.getElementById("easy1").src = 'imgs/star.png';
    }
    
    if (starMid == '3'){
      
      document.getElementById("mid1").src = 'imgs/star.png';
      document.getElementById("mid2").src = 'imgs/star.png';
      document.getElementById("mid3").src = 'imgs/star.png';
    }else if(starMid == '2'){
      
      document.getElementById("mid1").src = 'imgs/star.png';
      document.getElementById("mid2").src = 'imgs/star.png';
    }else if(starMid == '1'){
      
      document.getElementById("mid1").src = 'imgs/star.png';
    }
    if (starHard == '3'){
      
      document.getElementById("hard1").src = 'imgs/star.png';
      document.getElementById("hard2").src = 'imgs/star.png';
      document.getElementById("hard3").src = 'imgs/star.png';
    }else if(starHard == '2'){
      
      document.getElementById("hard1").src = 'imgs/star.png';
      document.getElementById("hard2").src = 'imgs/star.png';
    }else if(starHard == '1'){
      
      document.getElementById("hard1").src = 'imgs/star.png';
    }
}
function EndGame(){

  if(nivel==1){

     if ($('#contratiempo').text()>30) {
         punto = punto + 100;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('DragUdpStarEasy', '3'); 
     } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
         punto = punto + 60;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starEasy <= 1){
           window.localStorage.setItem('DragUdpStarEasy', '2'); 
         }        
     } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
         punto = punto + 40;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starEasy <= 1){
           window.localStorage.setItem('DragUdpStarEasy', '2'); 
         }
       } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
         punto = punto + 20;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starEasy == 0){
           window.localStorage.setItem('DragUdpStarEasy', '1'); 
         }
     }
     contStar()
   }

 
   if(nivel==2){
     

     if ($('#contratiempo').text()>60) {
         punto = punto + 200;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('DragUdpStarMid', '3'); 
     } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
         punto = punto + 100;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starMid <= 1){
           window.localStorage.setItem('DragUdpStarMid', '2'); 
         }
     } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
         punto = punto + 80;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starMid <= 1){
           window.localStorage.setItem('DragUdpStarMid', '2'); 
         }
     } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
         punto = punto + 60;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starMid == 0){
           window.localStorage.setItem('DragUdpStarMid', '1'); 
         }
     } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
         punto = punto + 40;
         $('.punto').text(punto);
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starMid == 0){
           window.localStorage.setItem('DragUdpStarMid', '1'); 
         }
       } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
         punto = punto + 20;
         $('.punto').text(punto);
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(5);
         count(5);
     }   
     contStar()
   }

   if(nivel==3){
   
     
     if ($('#contratiempo').text()>80) {
         punto = punto + 200;
         $('.punto').text(punto);
         // estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('DragUdpStarHard', '3'); 

     } else if ($('#contratiempo').text()>70 && $('#contratiempo').text()<=80) {
         punto = punto + 150;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.winstar3').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(25);
         count(25);
         window.localStorage.setItem('DragUdpStarHard', '3'); 
     } else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=70) {
         punto = punto + 120;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starHard <= 1){
           window.localStorage.setItem('DragUdpStarHard', '2'); 
         }
     } else if ($('#contratiempo').text()>50 && $('#contratiempo').text()<=60) {
         punto = punto + 100;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.winstar2').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(20);
         count(20);
         if(starHard <= 1){
           window.localStorage.setItem('DragUdpStarHard', '2'); 
         }
     } else if ($('#contratiempo').text()>30 && $('#contratiempo').text()<=40) {
         punto = punto + 80;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starHard == 0){
           window.localStorage.setItem('DragUdpStarHard', '1'); 
         }
     } else if ($('#contratiempo').text()>20 && $('#contratiempo').text()<=30) {
         punto = punto + 60;
         $('.punto').text(punto);
// estrellas// 
         $('.winstar1').css({'display':'block'});
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(15);
         count(15);
         if(starHard == 0){
           window.localStorage.setItem('DragUdpStarHard', '1'); 
         }
     } else if ($('#contratiempo').text()>10 && $('#contratiempo').text()<=20) {
         punto = punto + 40;
         $('.punto').text(punto);
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(5);
         count(5);

       } else if ($('#contratiempo').text()>=0 && $('#contratiempo').text()<=10) {
         punto = punto + 20;
         $('.punto').text(punto);
         $('.wincoin').addClass('wincoin animated heartBeat delay-1s');
         contCoin(5);
         count(5);
     }
     contStar()
 }
}
function blockTTL(){
  $('#levelModal').modal('hide');
  $('#TTLModal').modal('show');
}
//sumar coin
function contCoin(n){
	var sumaCoin= n + coin
	window.localStorage.setItem('coin', sumaCoin); 
}
//efecto contador
function count(n){
	var counter = { var: 0 };
	var contador = TweenMax.to(counter, 4, {
	  var: n, 
	  onUpdate: function () {
		var number = Math.ceil(counter.var);
		$('#GetCoin').html(number);
		if(number === counter.var){ 
			contador.kill(); 
		}
	  },
	  ease:Circ.easeOut
	});
  }
function contStar(){
	var starEasyc = Number(localStorage.getItem('DragUdpStarEasy'));
	var starMidc = Number(localStorage.getItem('DragUdpStarMid'));
	var starHardc = Number(localStorage.getItem('DragUdpStarHard'));
	ContStar = starEasyc + starMidc + starHardc;
	//  console.log('starEasyc =', starEasyc );
	//   console.log('starMidc =', starMidc );
	//  console.log('starHardc =', starHardc );
  window.localStorage.setItem('contStarDragUdp', ContStar); 
  SumaCont()
  blockDrag()
  $('#contratiempo').css({
		'color':'transparent'
	})
}
//Conteo regresivo
function Timer(){
    //Conteo Regresivo
  var seconds_left = timeLevel;
  // var minutes_left = 0;

  var interval = setInterval(function() {

   document.getElementById('contratiempo').innerHTML =  --seconds_left;
  // if (document.getElementById('contratiempo').innerHTML == 1 ) {
  //   const
  //   $objetivo =  document.body, // A qué le tomamos la foto
  //   $contenedorCanvas = document.querySelector("#contenedorCanvas"); // En dónde ponemos el elemento canvas

  //   html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
  //     .then(function(canvas) {
  //     // Cuando se resuelva la promesa traerá el canvas
  //     $contenedorCanvas.appendChild(canvas);// Lo agregamos como hijo del div
  //     });
    
  //   console.log("tomada la foto");
  //   }
  if (fin==false) {
    if (seconds_left <= 0)
    {
      //document.getElementById('contratiempo').innerHTML = "Se acabo el tiempo";
      $('#LooseModal').modal('show')
      
      clearInterval(interval);
      //location.reload();
     // $( "#pregunta0,#pregunta1, #pregunta2, #pregunta3, #pregunta4, #pregunta5, #pregunta6, #pregunta7, #pregunta8, #pregunta9, #pregunta10, #pregunta11, #pregunta12, #pregunta13" ).draggable({ disabled: true });
   //  //****** Elimina las cajas de respuesta al culminar el tiempo
      $( "#pregunta0,#pregunta1, #pregunta2, #pregunta3, #pregunta4, #pregunta5, #pregunta6, #pregunta7, #pregunta8, #pregunta9, #pregunta10, #pregunta11, #pregunta12, #pregunta13" ).remove();

      ShareScore();
      ttl--;
	    window.localStorage.setItem('ttl', ttl); 
    }
  }
  }, 1000);
}


// Funcion que inicia el juego en el nivel avanzado
function LevelHard(){
  $('#ttl').text(ttl);
	$('#coin').text(coin);
   arregloFinal = boxPositionudp;

  //****** Crea las cajas de las respuestas
  function restAnswersudp(j){
    exist= false;
    var numRandom = Math.floor((Math.random() * boxPositionudp.length) + 1) - 1;

    if (boxPositionudp.length != 1){
      for (var i = 0; i <= boxPositionudp.length - 1; i++){
        if (i == numRandom){

          $('#pregunta').append('<div id="pregunta'+ boxPositionudp[numRandom] +'" class="caja"> '+ answersudp[boxPositionudp[numRandom]] + '</div>')

          //*****Elimina un objeto de un array
          boxPositionudp = jQuery.grep(boxPositionudp, function(b) {
            return b != boxPositionudp[numRandom];
          });
        }

      }
    }
    else {

      $('#pregunta').append('<div id="pregunta'+ boxPositionudp[numRandom] +'" class="caja"> '+ answersudp[boxPositionudp[numRandom]] + '</div>')

      //******* Elimina un objeto de un array
      boxPositionudp = jQuery.grep(boxPositionudp, function(b) {
        return b != boxPositionudp[0];
      });
    }


    if (boxPositionudp.length != 0){
      j++;
      restAnswersudp(j);
    }

}

  restAnswersudp(0);

//**********Valida Cajas Respuesta
  for (var j = 0; j < arregloFinal.length; j++){
    $( "#pregunta" + arregloFinal[j]).draggable({ revert: true,revertDuration: 0 });
    $( "#udp-" + arregloFinal[j] ).droppable({
      accept: "#pregunta" + arregloFinal[j],
      activeClass: "",
      hoverClass: "",

      drop: function( event, ui ) {
        $( this )
          .addClass( "ui-state-highlight" )//*** Color que se le asigna al Input donde se introduce la caja correcta
          //***** Operador ternario
          .attr( "placeholder", ui['draggable'][0].outerText == "Internet Header LLength" ? "IHT" : ui['draggable'][0].outerText)
              punto+=5;
              $('#score').text(punto);
              $('.punto').text(punto);
              $('#' + ui['draggable'][0].id).remove();
              cont= cont + 1;
              if(cont==5){                
                $('#WinModal').modal('show');
                window.localStorage.setItem('Nperdidas', '0'); 
                EndGame();// llamado a la funcion del puntaje
                fin=true;
                Timer();
                ShareScore();
              }
      }

    });
  }
  //**********  /Valida Cajas Respuesta
}



//inicia el juego segun el nivel
function PlayGame(){
  $('#ttl').text(ttl);
	$('#coin').text(coin);
      ///***** Crea de forma aleatoria las palabras en la caja contenedora

  for(var i=0; i<Math.ceil(answersudp.length / nivel); i++){
    var value = Math.round(Math.random()* 4);
    newArreglo[value]= value;
    $('#udp-' + value).attr( "placeholder", answersudp[value]);
    $('#udp-' + value).addClass('help-input');
    //*** crea el resto del arreglo (las que no salen en la caja contentenedora)
    boxPositionudp = jQuery.grep(boxPositionudp, function(a) {
     return a != value;
     answersudp[a];
    });


  }//Fin de For i

  arregloFinal = boxPositionudp;

      //****** Crea las cajas de las respuestas
  function restAnswersudp(j){
    exist= false;
    var numRandom = Math.floor((Math.random() * boxPositionudp.length) + 1) - 1;

    if (boxPositionudp.length != 1){
      for (var i = 0; i <= boxPositionudp.length - 1; i++){
        if (i == numRandom){

          $('#pregunta').append('<div id="pregunta'+ boxPositionudp[numRandom] +'" class="caja"> '+ answersudp[boxPositionudp[numRandom]] + '</div>')

          //*****Elimina un objeto de un array
          boxPositionudp = jQuery.grep(boxPositionudp, function(b) {
            return b != boxPositionudp[numRandom];
          });
          cont= cont + 1;
        }

      }
    }
    else {

      $('#pregunta').append('<div id="pregunta'+ boxPositionudp[numRandom] +'" class="caja"> '+ answersudp[boxPositionudp[numRandom]] + '</div>')

      //******* Elimina un objeto de un array
      boxPositionudp = jQuery.grep(boxPositionudp, function(b) {
        return b != boxPositionudp[0];
      });
     cont= cont + 1;
    }


    if (boxPositionudp.length != 0){
      j++;
      restAnswersudp(j);
    }

}
  restAnswersudp(0);

  //**********Valida Cajas Respuesta
    for (var j = 0; j < arregloFinal.length; j++){
      $( "#pregunta" + arregloFinal[j]).draggable({ revert: true,revertDuration: 0 });
      $( "#udp-" + arregloFinal[j] ).droppable({
        accept: "#pregunta" + arregloFinal[j],
        activeClass: "",
        hoverClass: "",

        drop: function( event, ui ) {
          $( this )
            .addClass( "ui-state-highlight" )//*** Color que se le asigna al Input donde se introduce la caja correcta
            //***** Operador ternario
            .attr( "placeholder", ui['draggable'][0].outerText == "Internet Header LLength" ? "IHT" : ui['draggable'][0].outerText)
                punto+=5;
                $('#score').text(punto);
                $('.punto').text(punto);
                cont= cont - 1;
                $('#' + ui['draggable'][0].id).remove();
                if(cont==0){                
                  $('#WinModal').modal('show');
                  window.localStorage.setItem('Nperdidas', '0'); 
                  EndGame();// llamado a la funcion del puntaje
                  fin=true;
                  Timer();
                  ShareScore();
                }
        }

      });
    }
  //**********   /Valida Cajas Respuesta
}


function ShareScore(){
  if (localStorage.getItem("UserId")!=null){
  var infogame = { "UserId":localStorage.getItem("UserId"), "GameId":3, "TopicId" :4, "levelId" :nivel,"Score":punto};

  RegisterGame(infogame);

  public_FB();
}
}


function public_TW(){

  var level='';

  switch(nivel) {
      case 1:

      level='EASY';
      break;
    case 2:

      level='MEDIUM';

      break;

    case 3:
              
      level='HARD';

      break;
  }

  var msj="GAME: DRAG  TOPIC:UDP  NEVEL: "+level+" POINTS: "+punto;

  var src='https://angelgame.acostasite.com/Game/img/drag_img.png';
  window.open("https://twitter.com/share?text="+msj+"&url=https://angelgame.acostasite.com/Game/juegos/Drag/udp-es.html");
  
}

function public_FB(){

  var level='';

  switch(nivel) {
      case 1:

      level='EASY';
      break;
    case 2:

      level='MEDIUM';

      break;

    case 3:
              
      level='HARD';

      break;
  }

  var msj="GAME: DRAG  TOPIC:UDP  NEVEL: "+level+" POINTS: "+punto;

  $(".fb-xfbml-parse-ignore").attr("href","https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Fjuegos%2FDrag%2Fudp.html?description="+msj+"&method=1&amp;src=sdkpreparse");
}

function reload(){

  localStorage.setItem('intentar', nivel);

  location.reload();
}
var dataInstag;
function public_IG(){
	console.log("entro en instagram");
	var level='';
	switch(nivel) {
	case 6:

		level='EASY';
		break;
	case 8:

		level='MEDIUM';

		break;

	case 12:
				
		level='HARD';

		break;
}

	var msj="GAME: DRAG  TOPIC:UDP  NEVEL: "+level+" POINTS: "+punto;
	/*window.plugins.socialsharing.shareViaInstagram(
		'Message via Instagram', 
		'https://angelgame.acostasite.com/images/icon/icon.png','https://angelgame.acostasite.com'
	  );	*/
	 // var assetLocalIdentifier = "../../img/congratulations.png";
	 Instagram.isInstalled(function (err, installed) {
		if (installed) {
			console.log("Instagram is"+ installed); // installed app version on Android
			navigator.screenshot.save(function(error,response){
				if(error){
					console.error(error);
					return;
				}
				
				// Something like: /storage/emulated/0/Pictures/screenshot_1477924039236.jpg
				console.log(response.filePath);
		
				/*Instagram.shareAsset(function(result) {
					alert('Instagram.shareAsset success: ' + result);
				}, function(e) {
					alert('Instagram.shareAsset error: ' + e);
				}, response.filePath);*/
				getBase64FromImageUrl(response.filePath, msj);
				
			});
		} else {
			alert("Instagram no esta instalado");
		}
	});

	
	 

/*	module.controller('ThisCtrl', function($scope, $cordovaInstagram) {
		// Get image from camera, base64 is good. See the
		// $cordovaCamera docs for more info
	  
		$cordovaInstagram.share($scope.image.data, $scope.image.caption).then(function() {
		  // Worked
		}, function(err) {
		  // Didn't work
		});
	  })*/
  }

  function getBase64FromImageUrl(url, msj) {
	var img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.width =this.width;
		canvas.height =this.height;

		var ctx = canvas.getContext("2d");
		ctx.drawImage(this, 0, 0);

		var dataURL = canvas.toDataURL("image/png");
		dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		
		Instagram.share(dataInstag, msj, function (err) {
			if (err) {
				console.log("Not shared");
			} else {
				console.log("shared");
			}
		});
	};

	img.src = url;
}

function Zoom(src){
	console.log("zoom", src)
	$("#imgZoom").attr('src', src);
	$("#ModalZoom").css({"display": "block"});
	document.getElementById("imgZoom").className ="imgZm animated fadeInUp slow"; 
}
function cerrarZoom(){
	console.log("Cerrar Zoom")
	$("#ModalZoom").css({"display": "none"});
}