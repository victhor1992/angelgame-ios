

var AllCruciStarIpv4 = Number(window.localStorage.getItem('AllCruciStarIpv4'));
var AllCruciStarIpv6 = Number(window.localStorage.getItem('AllCruciStarIpv6'));
var AllCruciStarGover = Number(window.localStorage.getItem('AllCruciStarGover'));
var CruciStarGoverLvl1 = Number(window.localStorage.getItem('CruciStarGoverLvl1'));
var CruciStarGoverLvl2 = Number(window.localStorage.getItem('CruciStarGoverLvl2'));
var CruciStarGoverLvl3 = Number(window.localStorage.getItem('CruciStarGoverLvl3'));
var CruciStarGoverLvl4 = Number(window.localStorage.getItem('CruciStarGoverLvl4'));
var audioElement = document.createElement('audio');
var ttl = Number(window.localStorage.getItem('ttl'));
var coin = Number(window.localStorage.getItem('coin'));
var Onanimated = localStorage.getItem('Animated');
var language = localStorage.getItem('selectedLang');
var levelGover = localStorage.getItem('levelGover');
var levelActual="";
var respuesta1 ='';
var respuesta2 ='';
var respuesta3 ='';
var respuesta4 ='';
var respuesta5 ='';
var respuesta6 ='';
var respuesta7 ='';
var respuesta8 ='';
var respuesta9 ='';
var respuesta10 ='';
var respuesta11 ='';
var respuesta12 ='';
var respuesta13 ='';
var respuesta14 ='';
var respuesta15 ='';
var respuesta16 ='';
var conjuntoPalabra;
var preg ='';
var lang =''
var aviso = "Necesita 35 <img src='imgs/coin50.png' alt='' class='coin'> Para utilzar este comodín"
var finish ="Has completado todos los niveles del crucigramas Governance"
if(this.language == 'Eng'){
	aviso= "You need 35 <img src='imgs/coin50.png' alt='' class='coin'> to use this wildcard";
	finish ="You have completed all levels of the Governance crossword puzzle"
	lang ='-en'
}else if(this.language == 'Por'){
	aviso= "Você precisa de 35 <img src='imgs/coin50.png' alt='' class='coin'> para usar este curinga";
	finish ="Você concluiu todos os níveis das palavras cruzadas do Governance"
	lang ='-por'
}
if(CruciStarGoverLvl1 == null){
	window.localStorage.setItem('CruciStarGoverLvl1', '0');
}
if(CruciStarGoverLvl2 == null){
	window.localStorage.setItem('CruciStarGoverLvl2', '0');
}
if(CruciStarGoverLvl3 == null){
	window.localStorage.setItem('CruciStarGoverLvl3', '0');
}
if(CruciStarGoverLvl4 == null){
	window.localStorage.setItem('CruciStarGoverLvl4', '0');
}
$(document).ready(function(){
	$('#ttl').text(ttl);
	$('#coin').text(coin);
	$('#nextLVL').css({'display':'none'});
	var elembacklose= document.getElementById('btnbacklose')
	elembacklose.setAttribute("href","crucigramaMenu"+lang+".html");
	var elembackwin= document.getElementById('btnbackwin')
	elembackwin.setAttribute("href","crucigramaMenu"+lang+".html");
	var elembacklose= document.getElementById('urlNext')
	elembacklose.setAttribute("onclick","siguiente();");
	if(window.localStorage.getItem('levelGover')=='4'){
		$('#urlNext').css({'display':'none'})
	}
	PlayMusic(window.localStorage.getItem('audio'));
	if(ttl == 0){
		contadorMinutos();
		blockTTL();			
	}else if(ttl <= 4){
		contadorMinutos();
	}
	if(ttl > 0){
		$('#lvl'+levelGover).removeAttr('style');
		levelActual = levelGover;
		$('#lvlN').text(levelGover);
		$('#lvltxt').addClass('animated slideInRight');
		$('#lvltxt').removeAttr('style');
		setTimeout(function() { 
		$('#lvltxt').removeClass('animated slideInRight');
		$('#lvltxt').addClass('animated slideOutLeft');
		}, 1500);
		 Timer();
	}
	
	validar();
	$('input').click(function(){
		$(this).addClass('td_input');
		})
		$('input').focusout(function(){
    		$(this).removeClass('td_input');
		});
		
	   	// if (StatusBar.isVisible) {
    		//StatusBar.hide();
    	// 	new kendo.mobile.Application($(document.body), { statusBarStyle: "hidden" });
    	// }
})


function ShareScore(){
	if (localStorage.getItem("UserId")!=null){
	  var infogame = { "UserId":localStorage.getItem("UserId"), "GameId":1, "TopicId" :3, "levelId" :4,"Score":punto};
	
	  RegisterGame(infogame);
	}
}

// nivel 1
var word1v = 'rfc';
// nivel 1
var word2h = 'lacnic';

var word3v = 'netmundial';
// nivel 2
var word4h = 'ietf';
// nivel 2
var word4v = 'ipv6';
//usada nivel 3
var word5h = 'igf';
//usada nivel 3
var word5v ='icann';
//usada nivel 4
var word6v = 'iana';

var word7v = 'opendata';
//usada nivel 4
var word8v = 'iab';

var word9v = 'wsis';
//usada nivel 4
var word10h = 'caribbean';

var word10v = 'cybersecurity';
//usada nivel 4
var word11v = 'ig';

var word12h = 'arin';

var word13h = 'multistakeholder';

var word14v = 'isoc';

var word15v = 'lacigf';

var punto=0;


var fin= false;

var timeLevel = 150;
//Activar el sonido
function PlayMusic(active){

	if (active == "1"){
		audioElement.setAttribute('id', 'sonido');
		audioElement.setAttribute('src', 'sounds/world3.mp3');
    	audioElement.setAttribute('autoplay', 'autoplay');
    	audioElement.addEventListener("ended", function() {
	        this.currentTime = 0;
	        this.play();
	    }, false);
	}
	else {
		audioElement.pause();
		active=0;
	}
}

function blockTTL(){
	$('#levelModal').modal('hide');
	$('#TTLModal').modal('show');
}
function screenshot(){
	$('.efecv').css({'display':'none'});	
	$('.efecv2').css({'display':'none'});	
	$('.efech').css({'display':'none'});	
	$('.efech2').css({'display':'none'});
	$('#lvltxt').css({'display':'none'});
	//para tomar la foto
	const
    $objetivo =  document.body, // A qué le tomamos la foto
    $contenedorCanvas = document.querySelector("#contenedorCanvas2"); // En dónde ponemos el elemento canvas
    html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
      .then(function(canvas) {
      // Cuando se resuelva la promesa traerá el canvas
      $contenedorCanvas.appendChild(canvas);// Lo agregamos como hijo del div
      });    
    console.log("tomada la foto");
}
function EndGame(nivel){
	setTimeout(function() { 
     
	$('#lvl').text(nivel);
	$('#WinModal').modal('show');

	if(nivel==1){
		window.localStorage.setItem('levelGover', '2');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarGoverLvl1', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarGover <= 1){
				window.localStorage.setItem('CruciStarGoverLvl1', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarGover == 0){
				window.localStorage.setItem('CruciStarGoverLvl1', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==2){
		window.localStorage.setItem('levelGover', '3');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarGoverLvl2', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarGover <= 1){
				window.localStorage.setItem('CruciStarGoverLvl2', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarGover == 0){
				window.localStorage.setItem('CruciStarGoverLvl2', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==3){
		window.localStorage.setItem('levelGover', '4');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarGoverLvl3', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarGover <= 1){
				window.localStorage.setItem('CruciStarGoverLvl3', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarGover == 0){
				window.localStorage.setItem('CruciStarGoverLvl3', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
	if(nivel==4){
		$('#sig').css({'display':'none'});
		$('#finish').text(finish);
		window.localStorage.setItem('levelGover', '1');
		if ($('#contratiempo').text()>100) {
			punto = punto + 200;
			$('.punto').text(punto);// Imprimer en la cantidad de punto
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.winstar3').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(25);
			count(25);
			window.localStorage.setItem('CruciStarGoverLvl4', '3');
		} else if ($('#contratiempo').text()>80 && $('#contratiempo').text()<=100) {
			punto = punto + 150;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.winstar2').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(20);
			count(20);
			if(CruciStarGover <= 1){
				window.localStorage.setItem('CruciStarGoverLvl4', '2'); 
			}  

		} else if ($('#contratiempo').text()>60 && $('#contratiempo').text()<=80) {
			punto = punto + 100;
			$('.punto').text(punto);
			$('.winstar1').css({'display':'block'});
			$('.wincoin').addClass('wincoin animated heartBeat delay-1s');
			contCoin(15);
			count(15);
			if(CruciStarGover == 0){
				window.localStorage.setItem('CruciStarGoverLvl4', '1'); 
			}

		}else{
			contCoin(5);
			count(5);
		}
	}
        fin=true;
		SumaCont();
		ShareScore();
		contStar();
        // Timer();
	}, 250);
}
//sumar coin
function contCoin(n){
	var sumaCoin= n + coin
	window.localStorage.setItem('coin', sumaCoin); 
}
//efecto contador
function count(n){
	var counter = { var: 0 };
	var contador = TweenMax.to(counter, 4, {
	  var: n, 
	  onUpdate: function () {
		var number = Math.ceil(counter.var);
		$('#GetCoin').html(number);
		if(number === counter.var){ 
			contador.kill(); 
		}
	  },
	  ease:Circ.easeOut
	});
  }
  function SumaCont(){
	var contAllStarCruci = AllCruciStarIpv4 + AllCruciStarIpv6 + AllCruciStarGover 
	window.localStorage.setItem('contAllStarCruci', contAllStarCruci); 
	console.log(contAllStarCruci);
}
function contStar(){
	var Lvl1 = Number(window.localStorage.getItem('CruciStarGoverLvl1'));
	var Lvl2 = Number(window.localStorage.getItem('CruciStarGoverLvl1'));
	var Lvl3 = Number(window.localStorage.getItem('CruciStarGoverLvl3'));
	var Lvl4 = Number(window.localStorage.getItem('CruciStarGoverLvl4'));
	ContStar = Lvl1 + Lvl2 + Lvl3 + Lvl4
	//   console.log('starEasyc =', starEasyc );
	//  console.log('starMidc =', starMidc );
	//   console.log('starHardc =', starHardc );
	//  console.log('sumando contador=', ContStar)
	window.localStorage.setItem('AllCruciStarGover', ContStar); 
	$('#contratiempo').css({
		'color':'transparent'
	})
}
//cerrar modal
function cerrarModal(){
    if (Onanimated == 'true'){
        document.getElementById("BoxAudi").className ="relative animated zoomOut" ;
      setTimeout(function() { 
        document.getElementById("ModalAudi").className ="";
     }, 500);
    }else{
		document.getElementById("ModalAudi").className ="";
    }
}
function Otravez(){
	window.localStorage.setItem('levelGover', levelActual);
	reload()
}
function siguiente(){
	reload()
}
//mesaje de alerta 
function msj(){
	$("#palabras").css({'display':'none'});
	$(".btnComprar").css({'margin-top':'111px'});
	$(".titAudi").html(aviso)
}
// funcion de la letra 
function letra(lvl){
	var vCoin = Number(window.localStorage.getItem('coin'));
	conjuntoPalabra = '<span>Vertical<input type="checkbox" id="v1"  ></span><br><span>Horizontal<input type="checkbox" id="h1"  ></span>'
	$("#palabras").html(conjuntoPalabra)
	if(lvl == 1){
		if(vCoin >= 35){
			$( "#v1" ).attr("onclick","showL('v1','palabra1');");
			$( "#h1" ).attr("onclick","showL('h1','palabra2');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}
	if(lvl == 2){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical<input type="checkbox" id="v1" ></span><br><span>Horizontal <input type="checkbox" id="h1"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#v1" ).attr("onclick","showL('v1','palabra3');");
			$( "#h1" ).attr("onclick","showL('h1','palabra4');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}
	if(lvl == 3){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical<input type="checkbox" id="v1"  ></span><br><span>Horizontal <input type="checkbox" id="h1"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#BoxAudi" ).css({'height':'238px'});
			$( "#v1" ).attr("onclick","showL('v1','palabra5');");
			$( "#h1" ).attr("onclick","showL('h1','palabra6');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}	
	if(lvl == 4){
		if(vCoin >= 35){
			conjuntoPalabra = '<span>Vertical 1<input type="checkbox" id="v1"  ></span><br><span>Vertical 2<input type="checkbox" id="v2"  ></span><br><span>Vertical 3<input type="checkbox" id="v3"  ></span><br><span>Horizontal<input type="checkbox" id="h1"  ></span>'
			$("#palabras").html(conjuntoPalabra)
			$( "#BoxAudi" ).css({'height':'282px'});
			$( "#v1" ).attr("onclick","showL('v1','palabra7');");
			$( "#v2" ).attr("onclick","showL('v2','palabra8');");
			$( "#v3" ).attr("onclick","showL('v3','palabra10');");
			$( "#h1" ).attr("onclick","showL('h1','palabra9');");
			vCoin= vCoin-35
			window.localStorage.setItem('coin', vCoin)
			$( "#coin" ).text(vCoin)
		}else{
			msj()
		}
	}			
	if (Onanimated == 'true'){
		document.getElementById("ModalAudi").className ="mostrar animated fadeIn"; 
		document.getElementById("BoxAudi").className ="relative animated bounceIn"; 
		$('.efecv').addClass('animated slideInRight');	
		$('.efecv2').addClass('animated slideInLeft');	
		$('.efech').addClass('animated slideInUp');	
		$('.efech2').addClass('animated slideInDown');	
	}else{
		document.getElementById("ModalAudi").className ="ModalAudi mostrar "; 
		document.getElementById("BoxAudi").className ="BoxAudi relative "; 
	}
	
}
function showL(id,idpalabra) {	
	// Get the checkbox
	var checkBox = document.getElementById(id);  
	// If the checkbox is checked, display the output text
	if (checkBox.checked == true){
		// var vCoin = Number(window.localStorage.getItem('coin')); para cuando se utilizan las monedas
		var vCoin = 36;
		if(vCoin >= 35){
			//  var sumaCoin = vCoin - 35 restar las monedas
			//  window.localStorage.setItem('coin', sumaCoin); 
			//  $('#coin').text(sumaCoin);	
			 $("#"+idpalabra).removeAttr('style');
			 checkBox.disabled = true;
		}else{
			msj();		
		}
		
	} else {
		$("#"+idpalabra).css({'display':'none'});
	}
}
function Timer(){
  //Conteo Regresivo
  var seconds_left = timeLevel;
  // var minutes_left = 0;

  var interval = setInterval(function() {

  document.getElementById('contratiempo').innerHTML =  --seconds_left;
  
//   if (document.getElementById('contratiempo').innerHTML == 1 ) {
// 	$('#lvltxt').css({'display':'none'});  
// 	$('.efecv').css({'display':'none'});  
// 	$('.efecv2').css({'display':'none'});  
// 	$('.efech').css({'display':'none'});  
// 	$('.efech2').css({'display':'none'});  
//     const
//     $objetivo =  document.body, // A qué le tomamos la foto
//     $contenedorCanvas = document.querySelector("#contenedorCanvas"); // En dónde ponemos el elemento canvas

//     html2canvas($objetivo) // Llamar a html2canvas y pasarle el elemento
//       .then(function(canvas) {
//       // Cuando se resuelva la promesa traerá el canvas
//       $contenedorCanvas.appendChild(canvas);// Lo agregamos como hijo del div
//       });
    
//     console.log("tomada la foto");
//     }

  if (fin==false) {
  	//EndGame();
	  if (seconds_left <= 0){
	    //document.getElementById('contratiempo').innerHTML = "Se acabo el tiempo";

	    clearInterval(interval);
	   //Envia mensaje y reinicia la partida
	   //alert("Game Over!");
	   cerrarModal()
	   $('#LooseModal').modal('show')
	   //location.reload();
	   ShareScore();
	   ttl--;
		window.localStorage.setItem('ttl', ttl); 
	  }
	}

  }, 1000);

}


//usada nivel 1
function validWord1(){
	var answer1v = $("#input-0-5").val().toLowerCase() + $("#input-1-5").val().toLowerCase() + $("#input-2-5").val().toLowerCase();
	//console.log(answer1v);
	if (answer1v == word1v){

		$("#cell-0-5").css("background-color","#17DB00");
		$("#cell-1-5").css("background-color","#17DB00");
		$("#cell-2-5").css("background-color","#17DB00");
		$("#input-0-5").attr("disabled","disabled");
		$("#input-1-5").attr("disabled","disabled");
		$("#input-2-5").attr("disabled","disabled");
		punto= punto + 50;
		$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta1 = 'correcto';
		if(respuesta2 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta1 == 'correcto' && respuesta2 == 'correcto'){
			EndGame(1);
		}
	}
	else if (answer1v == ""){

	}
	else if (answer1v != word1v){
		$("#cell-0-5").css("background-color","#f00");
		$("#cell-1-5").css("background-color","#f00");
		$("#cell-2-5").css("background-color","#f00");
	}

}
//usad nivel 1
function validWord2(){
	//Validar palabra 2 horizontal
	var answer2h = $("#input-2-0").val().toLowerCase() + $("#input-2-1").val().toLowerCase() + $("#input-2-2").val().toLowerCase() + $("#input-2-3").val().toLowerCase() + $("#input-2-4").val().toLowerCase() + $("#input-2-5").val().toLowerCase();
	if (answer2h == word2h){
		$("#cell-2-0").css("background-color","#17DB00");
		$("#cell-2-1").css("background-color","#17DB00");
		$("#cell-2-2").css("background-color","#17DB00");
		$("#cell-2-3").css("background-color","#17DB00");
		$("#cell-2-4").css("background-color","#17DB00");
		$("#cell-2-5").css("background-color","#17DB00");
		$("#input-2-0").attr("disabled","disabled");
		$("#input-2-1").attr("disabled","disabled");
		$("#input-2-2").attr("disabled","disabled");
		$("#input-2-3").attr("disabled","disabled");
		$("#input-2-4").attr("disabled","disabled");
		$("#input-2-5").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta2 = 'correcto';
		if(respuesta1 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta1 == 'correcto' && respuesta2 == 'correcto'){
			EndGame(1);
		}


	}
	else if (answer2h == ""){

	}
	else if (answer2h != word2h){
		$("#cell-2-0").css("background-color","#f00");
		$("#cell-2-1").css("background-color","#f00");
		$("#cell-2-2").css("background-color","#f00");
		$("#cell-2-3").css("background-color","#f00");
		$("#cell-2-4").css("background-color","#f00");
		$("#cell-2-5").css("background-color","#f00");

	}
}
function validWord3(){
	//Validar palabra 3 vertical
	var answer3v = $("#input-2-3").val().toLowerCase() + $("#input-3-3").val().toLowerCase() + $("#input-4-3").val().toLowerCase() + $("#input-5-3").val().toLowerCase() + $("#input-6-3").val().toLowerCase() + $("#input-7-3").val().toLowerCase() + $("#input-8-3").val().toLowerCase() + $("#input-9-3").val().toLowerCase() + $("#input-10-3").val().toLowerCase() + $("#input-11-3").val().toLowerCase();
	if (answer3v == word3v){
		$("#cell-2-3").css("background-color","#17DB00");
		$("#cell-3-3").css("background-color","#17DB00");
		$("#cell-4-3").css("background-color","#17DB00");
		$("#cell-5-3").css("background-color","#17DB00");
		$("#cell-6-3").css("background-color","#17DB00");
		$("#cell-7-3").css("background-color","#17DB00");
		$("#cell-8-3").css("background-color","#17DB00");
		$("#cell-9-3").css("background-color","#17DB00");
		$("#cell-10-3").css("background-color","#17DB00");
		$("#cell-11-3").css("background-color","#17DB00");
		$("#input-2-3").attr("disabled","disabled");
		$("#input-3-3").attr("disabled","disabled");
		$("#input-4-3").attr("disabled","disabled");
		$("#input-5-3").attr("disabled","disabled");
		$("#input-6-3").attr("disabled","disabled");
		$("#input-7-3").attr("disabled","disabled");
		$("#input-8-3").attr("disabled","disabled");
		$("#input-9-3").attr("disabled","disabled");
		$("#input-10-3").attr("disabled","disabled");
		$("#input-11-3").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();
	}
	else if (answer3v == ""){

	}
	else if (answer3v != word3v){
		$("#cell-2-3").css("background-color","#f00");
		$("#cell-3-3").css("background-color","#f00");
		$("#cell-4-3").css("background-color","#f00");
		$("#cell-5-3").css("background-color","#f00");
		$("#cell-6-3").css("background-color","#f00");
		$("#cell-7-3").css("background-color","#f00");
		$("#cell-8-3").css("background-color","#f00");
		$("#cell-9-3").css("background-color","#f00");
		$("#cell-10-3").css("background-color","#f00");
		$("#cell-11-3").css("background-color","#f00");
	}

}
//usad nivel 2
function validWord4h(){
	//Validar palabra horizontal
	var answer4h = $("#input-4-1").val().toLowerCase() + $("#input-4-2").val().toLowerCase() + $("#input-4-3").val().toLowerCase() + $("#input-4-4").val().toLowerCase();
	if (answer4h == word4h){
		$("#cell-4-1").css("background-color","#17DB00");
		$("#cell-4-2").css("background-color","#17DB00");
		$("#cell-4-3").css("background-color","#17DB00");
		$("#cell-4-4").css("background-color","#17DB00");
		$("#input-4-1").attr("disabled","disabled");
		$("#input-4-2").attr("disabled","disabled");
		$("#input-4-3").attr("disabled","disabled");
		$("#input-4-4").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta3 = 'correcto';
		if(respuesta4 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta3 == 'correcto' && respuesta4 == 'correcto'){
			EndGame(2);
		}
	}
	else if (answer4h == ""){

	}
	else if (answer4h != word4h){
		$("#cell-4-1").css("background-color","#f00");
		$("#cell-4-2").css("background-color","#f00");
		$("#cell-4-3").css("background-color","#f00");
		$("#cell-4-4").css("background-color","#f00");

	}

}
//usad nivel 2
function validWord4v(){
	var answer4v = $("#input-4-1").val().toLowerCase() + $("#input-5-1").val().toLowerCase() + $("#input-6-1").val().toLowerCase() + $("#input-7-1").val().toLowerCase();
	if (answer4v == word4v){
		console.log(word4v);
		$("#cell-4-1").css("background-color","#17DB00");
		$("#cell-5-1").css("background-color","#17DB00");
		$("#cell-6-1").css("background-color","#17DB00");
		$("#cell-7-1").css("background-color","#17DB00");
		$("#input-4-1").attr("disabled","disabled");
		$("#input-5-1").attr("disabled","disabled");
		$("#input-6-1").attr("disabled","disabled");
		$("#input-7-1").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta4 = 'correcto';
		if(respuesta3 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta3 == 'correcto' && respuesta4 == 'correcto'){
			EndGame(2);
		}

	}
	else if (answer4v == ""){

	}
	else if (answer4v != word4v){
		$("#cell-4-1").css("background-color","#f00");
		$("#cell-5-1").css("background-color","#f00");
		$("#cell-6-1").css("background-color","#f00");
		$("#cell-7-1").css("background-color","#f00");

	}
}
//usad nivel 3
function validWord5h(){
	//Validar palabra 5 horizontal
	var answer5h = $("#input-4-18").val().toLowerCase() + $("#input-4-19").val().toLowerCase() + $("#input-4-20").val().toLowerCase();
	if (answer5h == word5h){
		$("#cell-4-18").css("background-color","#17DB00");
		$("#cell-4-19").css("background-color","#17DB00");
		$("#cell-4-20").css("background-color","#17DB00");
		$("#input-4-18").attr("disabled","disabled");
		$("#input-4-19").attr("disabled","disabled");
		$("#input-4-20").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta5 = 'correcto';
		if(respuesta6 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta5 == 'correcto' && respuesta6 == 'correcto'){
			EndGame(3);
		}
	}
	else if (answer5h == ""){

	}
	else if (answer5h != word5h){
		$("#cell-4-18").css("background-color","#f00");
		$("#cell-4-19").css("background-color","#f00");
		$("#cell-4-20").css("background-color","#f00");
	}
}
//usad nivel 3
function validWord5v(){
	//Validar palabra 5 Vertical
	var answer5v = $("#input-4-18").val().toLowerCase() + $("#input-5-18").val().toLowerCase() + $("#input-6-18").val().toLowerCase() + $("#input-7-18").val().toLowerCase() + $("#input-8-18").val().toLowerCase();
	if (answer5v == word5v){
		$("#cell-4-18").css("background-color","#17DB00");
		$("#cell-5-18").css("background-color","#17DB00");
		$("#cell-6-18").css("background-color","#17DB00");
		$("#cell-7-18").css("background-color","#17DB00");
		$("#cell-8-18").css("background-color","#17DB00");
		$("#input-4-18").attr("disabled","disabled");
		$("#input-5-18").attr("disabled","disabled");
		$("#input-6-18").attr("disabled","disabled");
		$("#input-7-18").attr("disabled","disabled");
		$("#input-8-18").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta6 = 'correcto';
		if(respuesta5 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta5 == 'correcto' && respuesta6 == 'correcto'){
			EndGame(3);
		}
	}
	else if (answer5v == ""){

	}
	else if (answer5v != word5v){
		$("#cell-4-18").css("background-color","#f00");
		$("#cell-5-18").css("background-color","#f00");
		$("#cell-6-18").css("background-color","#f00");
		$("#cell-7-18").css("background-color","#f00");
		$("#cell-8-18").css("background-color","#f00");
	}
}
//usad nivel 4
function validWord6(){
	//Validar palabra 6 vertical
	var answer6v = $("#input-5-11").val().toLowerCase() + $("#input-6-11").val().toLowerCase() + $("#input-7-11").val().toLowerCase() + $("#input-8-11").val().toLowerCase();
	if (answer6v == word6v){
		$("#cell-5-11").css("background-color","#17DB00");
		$("#cell-6-11").css("background-color","#17DB00");
		$("#cell-7-11").css("background-color","#17DB00");
		$("#cell-8-11").css("background-color","#17DB00");
		$("#input-5-11").attr("disabled","disabled");
		$("#input-6-11").attr("disabled","disabled");
		$("#input-7-11").attr("disabled","disabled");
		$("#input-8-11").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta7 = 'correcto';
		if(respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta7 == 'correcto' && respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
			EndGame(4);
		}
	}
	else if (answer6v == ""){

	}
	else if (answer6v != word6v){
		$("#cell-5-11").css("background-color","#f00");
		$("#cell-6-11").css("background-color","#f00");
		$("#cell-7-11").css("background-color","#f00");
		$("#cell-8-11").css("background-color","#f00");
	}
}
function validWord7(){
	//Validar palabra 7 vertical
	var answer7v = $("#input-6-8").val().toLowerCase() + $("#input-7-8").val().toLowerCase() + $("#input-8-8").val().toLowerCase() + $("#input-9-8").val().toLowerCase() + $("#input-10-8").val().toLowerCase() + $("#input-11-8").val().toLowerCase() + $("#input-12-8").val().toLowerCase() + $("#input-13-8").val().toLowerCase();
	if (answer7v == word7v){
		$("#cell-6-8").css("background-color","#17DB00");
		$("#cell-7-8").css("background-color","#17DB00");
		$("#cell-8-8").css("background-color","#17DB00");
		$("#cell-9-8").css("background-color","#17DB00");
		$("#cell-10-8").css("background-color","#17DB00");
		$("#cell-11-8").css("background-color","#17DB00");
		$("#cell-12-8").css("background-color","#17DB00");
		$("#cell-13-8").css("background-color","#17DB00");

		$("#input-6-8").attr("disabled","disabled");
		$("#input-7-8").attr("disabled","disabled");
		$("#input-8-8").attr("disabled","disabled");
		$("#input-9-8").attr("disabled","disabled");
		$("#input-10-8").attr("disabled","disabled");
		$("#input-11-8").attr("disabled","disabled");
		$("#input-12-8").attr("disabled","disabled");
		$("#input-13-8").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();
	}
	else if (answer7v == ""){

	}
	else if (answer7v != word7v){
		$("#cell-6-8").css("background-color","#f00");
		$("#cell-7-8").css("background-color","#f00");
		$("#cell-8-8").css("background-color","#f00");
		$("#cell-9-8").css("background-color","#f00");
		$("#cell-10-8").css("background-color","#f00");
		$("#cell-11-8").css("background-color","#f00");
		$("#cell-12-8").css("background-color","#f00");
		$("#cell-13-8").css("background-color","#f00");
	}
}
//usad nivel 4
function validWord8(){
	//Validar palabra 8 vertical
	var answer8v = $("#input-6-15").val().toLowerCase() + $("#input-7-15").val().toLowerCase() + $("#input-8-15").val().toLowerCase();
	if (answer8v == word8v){
		$("#cell-6-15").css("background-color","#17DB00");
		$("#cell-7-15").css("background-color","#17DB00");
		$("#cell-8-15").css("background-color","#17DB00");
		$("#input-6-15").attr("disabled","disabled");
		$("#input-7-15").attr("disabled","disabled");
		$("#input-8-15").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta8 = 'correcto';
		if(respuesta7 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta7 == 'correcto' && respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
			EndGame(4);
		}
	}
	else if (answer8v == ""){

	}
	else if (answer8v != word8v){
		$("#cell-6-15").css("background-color","#f00");
		$("#cell-7-15").css("background-color","#f00");
		$("#cell-8-15").css("background-color","#f00");
	}
}
function validWord9(){
	//Validar palabra 9 vertical
	var answer9v = $("#input-8-6").val().toLowerCase() + $("#input-9-6").val().toLowerCase() + $("#input-10-6").val().toLowerCase() + $("#input-11-6").val().toLowerCase();
	if (answer9v == word9v){
		$("#cell-8-6").css("background-color","#17DB00");
		$("#cell-9-6").css("background-color","#17DB00");
		$("#cell-10-6").css("background-color","#17DB00");
		$("#cell-11-6").css("background-color","#17DB00");
		$("#input-8-6").attr("disabled","disabled");
		$("#input-9-6").attr("disabled","disabled");
		$("#input-10-6").attr("disabled","disabled");
		$("#input-11-6").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();

	}
	else if (answer9v == ""){

	}
	else if (answer9v != word9v){
		$("#cell-8-6 ").css("background-color","#f00");
		$("#cell-9-6 ").css("background-color","#f00");
		$("#cell-10-6").css("background-color","#f00");
		$("#cell-11-6").css("background-color","#f00");
	}
}
//usad nivel 4
function validWord10h(){
	//Validar palabra 10 horizontal
	var answer10h = $("#input-8-10").val().toLowerCase() + $("#input-8-11").val().toLowerCase() + $("#input-8-12").val().toLowerCase() + $("#input-8-13").val().toLowerCase() + $("#input-8-14").val().toLowerCase() + $("#input-8-15").val().toLowerCase() + $("#input-8-16").val().toLowerCase() + $("#input-8-17").val().toLowerCase() + $("#input2-8-18").val().toLowerCase();
	if (answer10h == word10h){
		$("#cell-8-10").css("background-color","#17DB00");
		$("#cell-8-11").css("background-color","#17DB00");
		$("#cell-8-12").css("background-color","#17DB00");
		$("#cell-8-13").css("background-color","#17DB00");
		$("#cell-8-14").css("background-color","#17DB00");
		$("#cell-8-15").css("background-color","#17DB00");
		$("#cell-8-16").css("background-color","#17DB00");
		$("#cell-8-17").css("background-color","#17DB00");
		$("#cell2-8-18").css("background-color","#17DB00");
		$("#input-8-10").attr("disabled","disabled");
		$("#input-8-11").attr("disabled","disabled");
		$("#input-8-12").attr("disabled","disabled");
		$("#input-8-13").attr("disabled","disabled");
		$("#input-8-14").attr("disabled","disabled");
		$("#input-8-15").attr("disabled","disabled");
		$("#input-8-16").attr("disabled","disabled");
		$("#input-8-17").attr("disabled","disabled");
		$("#input2-8-18").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta9 = 'correcto';
		if(respuesta8 == 'correcto' && respuesta7 == 'correcto' && respuesta10 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta7 == 'correcto' && respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
			EndGame(4);
		}
	}
	else if (answer10h == ""){

	}
	else if (answer10h != word10h){
		$("#cell-8-10").css("background-color","#f00");
		$("#cell-8-11").css("background-color","#f00");
		$("#cell-8-12").css("background-color","#f00");
		$("#cell-8-13").css("background-color","#f00");
		$("#cell-8-14").css("background-color","#f00");
		$("#cell-8-15").css("background-color","#f00");
		$("#cell-8-16").css("background-color","#f00");
		$("#cell-8-17").css("background-color","#f00");
		$("#cell2-8-18").css("background-color","#f00");

	}
}
function validWord10v(){
	//Validar palabra 10 vertical
	var answer10v = $("#input-8-10").val().toLowerCase() + $("#input-9-10").val().toLowerCase() + $("#input-10-10").val().toLowerCase() + $("#input-11-10").val().toLowerCase() + $("#input-12-10").val().toLowerCase() + $("#input-13-10").val().toLowerCase() + $("#input-14-10").val().toLowerCase() + $("#input-15-10").val().toLowerCase() + $("#input-16-10").val().toLowerCase() + $("#input-17-10").val().toLowerCase() + $("#input-18-10").val().toLowerCase() + $("#input-19-10").val().toLowerCase() + $("#input-20-10").val().toLowerCase();
	if (answer10v == word10v){
		$("#cell-8-10").css("background-color","#17DB00");
		$("#cell-9-10").css("background-color","#17DB00");
		$("#cell-10-10").css("background-color","#17DB00");
		$("#cell-11-10").css("background-color","#17DB00");
		$("#cell-12-10").css("background-color","#17DB00");
		$("#cell-13-10").css("background-color","#17DB00");
		$("#cell-14-10").css("background-color","#17DB00");
		$("#cell-15-10").css("background-color","#17DB00");
		$("#cell-16-10").css("background-color","#17DB00");
		$("#cell-17-10").css("background-color","#17DB00");
		$("#cell-18-10").css("background-color","#17DB00");
		$("#cell-19-10").css("background-color","#17DB00");
		$("#cell-20-10").css("background-color","#17DB00");
		$("#input-8-10").attr("disabled","disabled");
		$("#input-9-10").attr("disabled","disabled");
		$("#input-10-10").attr("disabled","disabled");
		$("#input-11-10").attr("disabled","disabled");
		$("#input-12-10").attr("disabled","disabled");
		$("#input-13-10").attr("disabled","disabled");
		$("#input-14-10").attr("disabled","disabled");
		$("#input-15-10").attr("disabled","disabled");
		$("#input-16-10").attr("disabled","disabled");
		$("#input-17-10").attr("disabled","disabled");
		$("#input-18-10").attr("disabled","disabled");
		$("#input-19-10").attr("disabled","disabled");
		$("#input-20-10").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();
	}
	else if (answer10v == ""){

	}
	else if (answer10v != word10v){
		$("#cell-8-10").css("background-color","#f00");
		$("#cell-9-10").css("background-color","#f00");
		$("#cell-10-10").css("background-color","#f00");
		$("#cell-11-10").css("background-color","#f00");
		$("#cell-12-10").css("background-color","#f00");
		$("#cell-13-10").css("background-color","#f00");
		$("#cell-14-10").css("background-color","#f00");
		$("#cell-15-10").css("background-color","#f00");
		$("#cell-16-10").css("background-color","#f00");
		$("#cell-17-10").css("background-color","#f00");
		$("#cell-18-10").css("background-color","#f00");
		$("#cell-19-10").css("background-color","#f00");
		$("#cell-20-10").css("background-color","#f00");

	}
}
//usad nivel 4
function validWord11(){
	//Validar palabra 11 vertical
	var answer11v = $("#input-8-13").val().toLowerCase() + $("#input-9-13").val().toLowerCase();
	if (answer11v == word11v){
		$("#cell-8-13").css("background-color","#17DB00");
		$("#cell-9-13").css("background-color","#17DB00");
		$("#input-8-13").attr("disabled","disabled");
		$("#input-9-13").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		respuesta10 = 'correcto';
		if(respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta7 == 'correcto'){
			$('#fin').focus();
		}
		if(respuesta7 == 'correcto' && respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
			EndGame(4);
		}
	}
	else if (answer11v == ""){

	}
	else if (answer11v != word11v){
		$("#cell-8-13").css("background-color","#f00");
		$("#cell-9-13").css("background-color","#f00");
	}
}
function validWord12(){
	//Validar palabra 12 horizontal
	var answer12h = $("#input-9-1").val().toLowerCase() + $("#input-9-2").val().toLowerCase() + $("#input-9-3").val().toLowerCase() + $("#input-9-4").val().toLowerCase();
	if (answer12h == word12h){
		$("#cell-9-1").css("background-color","#17DB00");
		$("#cell-9-2").css("background-color","#17DB00");
		$("#cell-9-3").css("background-color","#17DB00");
		$("#cell-9-4").css("background-color","#17DB00");
		$("#input-9-1").attr("disabled","disabled");
		$("#input-9-2").attr("disabled","disabled");
		$("#input-9-3").attr("disabled","disabled");
		$("#input-9-4").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();
	}
	else if (answer12h == ""){

	}
	else if (answer12h != word12h){
		$("#cell-9-1").css("background-color","#f00");
		$("#cell-9-2").css("background-color","#f00");
		$("#cell-9-3").css("background-color","#f00");
		$("#cell-9-4").css("background-color","#f00");
	}
}
function validWord13(){
	//Validar palabra 13 horizontal
	var answer13h = $("#input-11-1").val().toLowerCase() + $("#input-11-2").val().toLowerCase() + $("#input-11-3").val().toLowerCase() + $("#input-11-4").val().toLowerCase() + $("#input-11-5").val().toLowerCase() + $("#input-11-6").val().toLowerCase() + $("#input-11-7").val().toLowerCase() + $("#input-11-8").val().toLowerCase() + $("#input-11-9").val().toLowerCase() + $("#input-11-10").val().toLowerCase() + $("#input-11-11").val().toLowerCase() + $("#input-11-12").val().toLowerCase() + $("#input-11-13").val().toLowerCase() + $("#input-11-14").val().toLowerCase() + $("#input-11-15").val().toLowerCase() + $("#input-11-16").val().toLowerCase();
	if (answer13h == word13h){
		$("#cell-11-1").css("background-color","#17DB00");
		$("#cell-11-2").css("background-color","#17DB00");
		$("#cell-11-3").css("background-color","#17DB00");
		$("#cell-11-4").css("background-color","#17DB00");
		$("#cell-11-5").css("background-color","#17DB00");
		$("#cell-11-6").css("background-color","#17DB00");
		$("#cell-11-7").css("background-color","#17DB00");
		$("#cell-11-8").css("background-color","#17DB00");
		$("#cell-11-9").css("background-color","#17DB00");
		$("#cell-11-10").css("background-color","#17DB00");
		$("#cell-11-11").css("background-color","#17DB00");
		$("#cell-11-12").css("background-color","#17DB00");
		$("#cell-11-13").css("background-color","#17DB00");
		$("#cell-11-14").css("background-color","#17DB00");
		$("#cell-11-15").css("background-color","#17DB00");
		$("#cell-11-16").css("background-color","#17DB00");
		$("#input-11-1").attr("disabled","disabled");
		$("#input-11-2").attr("disabled","disabled");
		$("#input-11-3").attr("disabled","disabled");
		$("#input-11-4").attr("disabled","disabled");
		$("#input-11-5").attr("disabled","disabled");
		$("#input-11-6").attr("disabled","disabled");
		$("#input-11-7").attr("disabled","disabled");
		$("#input-11-8").attr("disabled","disabled");
		$("#input-11-9").attr("disabled","disabled");
		$("#input-13-10").attr("disabled","disabled");
		$("#input-11-11").attr("disabled","disabled");
		$("#input-11-12").attr("disabled","disabled");
		$("#input-11-13").attr("disabled","disabled");
		$("#input-11-14").attr("disabled","disabled");
		$("#input-11-15").attr("disabled","disabled");
		$("#input-11-16").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();

	}
	else if (answer13h == ""){

	}
	else if (answer13h != word13h){
		$("#cell-11-1").css("background-color","#f00");
		$("#cell-11-2").css("background-color","#f00");
		$("#cell-11-3").css("background-color","#f00");
		$("#cell-11-4").css("background-color","#f00");
		$("#cell-11-5").css("background-color","#f00");
		$("#cell-11-6").css("background-color","#f00");
		$("#cell-11-7").css("background-color","#f00");
		$("#cell-11-8").css("background-color","#f00");
		$("#cell-11-9").css("background-color","#f00");
		$("#cell-11-10").css("background-color","#f00");
		$("#cell-11-11").css("background-color","#f00");
		$("#cell-11-12").css("background-color","#f00");
		$("#cell-11-13").css("background-color","#f00");
		$("#cell-11-14").css("background-color","#f00");
		$("#cell-11-15").css("background-color","#f00");
		$("#cell-11-16").css("background-color","#f00");

	}
}
function validWord14(){
	//Validar palabra 14 vertical
	var answer14v = $("#input-11-5").val().toLowerCase() + $("#input-12-5").val().toLowerCase() + $("#input-13-5").val().toLowerCase() + $("#input-14-5").val().toLowerCase();
	if (answer14v == word14v){
		$("#cell-11-5").css("background-color","#17DB00");
		$("#cell-12-5").css("background-color","#17DB00");
		$("#cell-13-5").css("background-color","#17DB00");
		$("#cell-14-5").css("background-color","#17DB00");
		$("#input-11-5").attr("disabled","disabled");
		$("#input-12-5").attr("disabled","disabled");
		$("#input-13-5").attr("disabled","disabled");
		$("#input-14-5").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();
	}
	else if (answer14v == ""){

	}
	else if (answer14v != word14v){
		$("#cell-11-5").css("background-color","#f00");
		$("#cell-12-5").css("background-color","#f00");
		$("#cell-13-5").css("background-color","#f00");
		$("#cell-14-5").css("background-color","#f00");
	}
}
function validWord15(){
	//Validar palabra 15 vertical
	var answer15v = $("#input-11-13").val().toLowerCase() + $("#input-12-13").val().toLowerCase() + $("#input-13-13").val().toLowerCase() + $("#input-14-13").val().toLowerCase() + $("#input-15-13").val().toLowerCase() + $("#input-16-13").val().toLowerCase();
	if (answer15v == word15v){
		$("#cell-11-13").css("background-color","#17DB00");
		$("#cell-12-13").css("background-color","#17DB00");
		$("#cell-13-13").css("background-color","#17DB00");
		$("#cell-14-13").css("background-color","#17DB00");
		$("#cell-15-13").css("background-color","#17DB00");
		$("#cell-16-13").css("background-color","#17DB00");
		$("#input-11-13").attr("disabled","disabled");
		$("#input-12-13").attr("disabled","disabled");
		$("#input-13-13").attr("disabled","disabled");
		$("#input-14-13").attr("disabled","disabled");
		$("#input-15-13").attr("disabled","disabled");
		$("#input-16-13").attr("disabled","disabled");
		punto= punto + 50;
				$('#punto').text(punto);
		$('.punto').text(punto);
		EndGame();
	}
	else if (answer15v == ""){

	}
	else if (answer15v != word15v){
		$("#cell-11-13").css("background-color","#f00");
		$("#cell-12-13").css("background-color","#f00");
		$("#cell-13-13").css("background-color","#f00");
		$("#cell-14-13").css("background-color","#f00");
		$("#cell-15-13").css("background-color","#f00");
		$("#cell-16-13").css("background-color","#f00");
	}
}

/********************************************************/
//	  		Funcion para cambiar la posicion del cursor//
/********************************************************/
function validar(){
	$('#input-0-5').on('keyup',function(e) {
				// Backspace
		if (e.keyCode==40){
			$('#input-15-0').focus();
		}		
		if($('#input-0-5').val()!='')
		{
			$('#input-1-5').focus();
		}
		
	});
	$('#input-1-5').on('keyup',function(e) {
		// Backspace
		if ($('#input-1-5').val()==''){
			$('#input-0-5').focus();
		}
		if (e.keyCode==40){
			$('#input-2-5').focus();
		}
		if($('#input-1-5').val()!='')
		{
			if(respuesta2 == 'correcto'){
				$('#fin').focus();
			}
			 $('#input-2-5').focus();
		}
	   
	});
	$('#input-2-5').on('keyup',function(e) {
				// Backspace
		if ($('#input-2-5').val()==''){
			$('#input-1-5').focus();
		}
		if (e.keyCode==40){
			$('#input-2-0').focus();
		}
		
		if($('#input-2-5').val() != ''){
			if($('#input-0-5').val() == ''){
				$('#input-0-5').focus();
			}else{
				$('#input-2-0').focus();
				ShowQuestion(2);
			}
			
		}
	});
	
	//Palabra 2 Horizontal

	$('#input-2-0').on('keyup',function(e) {
						// Backspace
		if ($('#input-2-0').val()==''){
			$('#input-2-5').focus();
		}
		if (e.keyCode==39){
			$('#input-2-1').focus();
		}
		if($('#input-2-0').val()!='')
		{
			$('#input-2-1').focus();
	    ShowQuestion(2);
		}
		
	});
	$('#input-2-1').on('keyup',function(e) {
	 						// Backspace
		if ($('#input-2-1').val()==''){
			$('#input-2-0').focus();
		}
		if (e.keyCode==39){
			$('#input-2-2').focus();
		}
		if($('#input-2-1').val()!='')
		{
			$('#input-2-2').focus();
	    ShowQuestion(2);
		}
		
	});

	$('#input-2-2').on('keyup',function(e) {
			 						// Backspace
		if ($('#input-2-2').val()==''){
			$('#input-2-1').focus();
		}
		if (e.keyCode==39){
			$('#input-2-3').focus();
		}
		if($('#input-2-2').val() != ''){
		
			$('#input-2-3').focus();
		}
	});

	$('#input-2-3').on('keyup',function(e) {
		// Backspace
		if ($('#input-2-3').val()==''){
			$('#input-2-2').focus();
		}
		if (e.keyCode==39){
			$('#input-2-4').focus();
		}
		if($('#input-2-3').val() != ''){
			$('#input-2-4').focus();
		}
	});

	$('#input-2-4').on('keyup',function(e) {
				// Backspace
		if ($('#input-2-4').val()==''){
			$('#input-2-3').focus();
		}
		if (e.keyCode==39){
			$('#input-2-5').focus();
		}
		if($('#input-2-4').val() != ''){
			if(respuesta1 == 'correcto'){
				$('#fin').focus();
			}
			if($('#input-2-5').val() == ''){
				$('#input-2-5').focus();
			}else{
				$('#input-0-5').focus();
			}
			
			ShowQuestion(3);
		}

	});

	//Pregunta 3

	$('#input-3-3').on('keyup',function(e) {
										// Backspace
		if ($('#input-3-3').val()==''){
			$('#input-2-4').focus();
		}
		if (e.keyCode==40){
			$('#input-4-3').focus();
		}
		if($('#input-3-3').val() != ''){
			$('#input-4-3').focus();
		}

	});
	$('#input-4-3').on('keyup',function(e) {
												// Backspace
		if ($('#input-4-3').val()==''){
			$('#input-4-2').focus();
		}
		if (e.keyCode==40){
			$('#input-5-3').focus();
		}
		if($('#input-4-3').val()!='')
		{
			$('#input-4-4').focus();
		}
			


	 });

	$('#input-5-3').on('keyup',function(e) {
	   												// Backspace
		if ($('#input-5-3').val()==''){
			$('#input-4-3').focus();
		}
		if (e.keyCode==40){
			$('#input-6-3').focus();
		}
		if($('#input-5-3').val()!='')
		{
			$('#input-6-3').focus();
		}
		
	});
	$('#input-6-3').on('keyup',function(e) {
	// Backspace
		if ($('#input-6-3').val()==''){
			$('#input-5-3').focus();
		}
		if (e.keyCode==40){
			$('#input-7-3').focus();
		}
		if($('#input-6-3').val()!='')
		{
			$('#input-7-3').focus();
		}
		
	});
	$('#input-7-3').on('keyup',function(e) {
	   	// Backspace
		if ($('#input-7-3').val()==''){
			$('#input-6-3').focus();
		}
		if (e.keyCode==40){
			$('#input-8-3').focus();
		}
		if($('#input-7-3').val()!='')
		{
			$('#input-8-3').focus();
		}
		
	});

	$('#input-8-3').on('keyup',function(e) {
		   	// Backspace
		if ($('#input-8-3').val()==''){
			$('#input-7-3').focus();
		}
		if (e.keyCode==40){
			$('#input-9-3').focus();
		}
		if($('#input-8-3').val() != ''){
			$('#input-9-3').focus();
		}
	});
	$('#input-9-3').on('keyup',function(e) {
				   	// Backspace
		if ($('#input-9-3').val()==''){
			$('#input-8-3').focus();
		}
		if (e.keyCode==40){
			$('#input-9-3').focus();
		}
		if($('#input-9-3').val() != '' && $('#input-10-3').val() == ''){
			$('#input-10-3').focus();
		}
	});
	$('#input-10-3').on('keyup',function(e) {
						   	// Backspace
		if ($('#input-10-3').val()==''){
			$('#input-9-3').focus();
		}
		if (e.keyCode==40){
			$('#input-11-3').focus();
		}
		if($('#input-10-3').val()!='')
		{
			$('#input-11-3').focus();
		}
		
	});
	$('#input-11-3').on('keyup',function(e) {
								   	// Backspace
		if ($('#input-11-3').val()==''){
			$('#input-10-3').focus();
		}
		if (e.keyCode==40){
			$('#input-4-1').focus();
		}
		if($('#input-11-3').val() != ''){
			$('#input-4-1').focus();
			ShowQuestion(4);
		}

	});




	//Pregunta 4 Horizontal
	$('#input-4-1').on('keyup',function(e) {
	  								   	// Backspace
		if ($('#input-4-1').val()==''){
			$('#input-11-3').focus();
		}
		if (e.keyCode==39){
			$('#input-4-2').focus();
		}
		if($('#input-4-1').val()!='')
		{
			$('#input-4-2').focus();
		}
		
	});

	// if (Question_4_3 !=1 ) {
		// console.log(Question_4_3 +' holaaaaaa');
	$('#input-4-2').on('keyup',function(e) {
			  								   	// Backspace
		if ($('#input-4-2').val()==''){
			$('#input-4-1').focus();
		}
		if (e.keyCode==39){
			$('#input-4-4').focus();
		}
		if($('#input-4-2').val() != ''){
			$('#input-4-3').focus();
		}
	});

	$('#input-4-4').on('keyup',function(e) {
							  								   	// Backspace
		if ($('#input-4-4').val()==''){
			$('#input-4-3').focus();
		}
		if (e.keyCode==39){
			$('#input-5-1').focus();
		}
		if($('#input-4-4').val() != ''){
			if(respuesta4 == 'correcto'){
				$('#fin').focus();
			}
			$('#input-5-1').focus();
		}

	});

	//Pregunta 4 Vertical
	$('#input-5-1').on('keyup',function(e) {
	   							  								   	// Backspace
		if ($('#input-5-1').val()==''){
			$('#input-4-4').focus();
		}
		if (e.keyCode==40){
			$('#input-6-1').focus();
		}
		if($('#input-5-1').val()!='')
		{
			$('#input-6-1').focus();
		}
		
	});
	$('#input-6-1').on('keyup',function(e) {
			  								   	// Backspace
		if ($('#input-6-1').val()==''){
			$('#input-5-1').focus();
		}
		if (e.keyCode==40){
			$('#input-7-1').focus();
		}
		if($('#input-6-1').val()!='')
		{
			$('#input-7-1').focus();
		}
		
	});
	$('#input-7-1').on('keyup',function(e) {
								   	// Backspace
		if ($('#input-7-1').val()==''){
			$('#input-6-1').focus();
		}
		if (e.keyCode==40){
			$('#input-4-18').focus();
		}
		if($('#input-7-1').val() != ''){
			if(respuesta3 == 'correcto'){
				$('#fin').focus();
			}
			if($('#input-4-2').val() == ''){
				$('#input-4-2').focus();				
			}
			ShowQuestion(5);
		}

	});

	//Pregunta 5 Horizontal
	$('#input-4-18').on('keyup',function(e) {
	   						   	// Backspace
		if ($('#input-4-18').val()==''){
			$('#input-7-1').focus();
		}
		if (e.keyCode==39){
			$('#input-4-19').focus();
		}
		if($('#input-4-18').val()!='')
		{
			$('#input-4-19').focus();
		}
		
	});
	$('#input-4-19').on('keyup',function(e) {
	   					   	// Backspace
		if ($('#input-4-19').val()==''){
			$('#input-4-18').focus();
		}
		if (e.keyCode==39){
			$('#input-4-20').focus();
		}
		if($('#input-4-19').val()!='')
		{
			$('#input-4-20').focus();
		}
		
	});
	$('#input-4-20').on('keyup',function(e) {
					   	// Backspace
		if ($('#input-4-20').val()==''){
			$('#input-4-19').focus();
		}
		if (e.keyCode==39){
			$('#input-5-18').focus();
		}
		if($('#input-4-20').val() != ''){
			if(respuesta6 == 'correcto'){
				$('#fin').focus();
			}
			$('#input-5-18').focus();
		}
	});

	//Pregunta 5 Vertical
	$('#input-5-18').on('keyup',function(e) {
	   			   	// Backspace
		if ($('#input-5-18').val()==''){
			$('#input-4-20').focus();
		}
		if (e.keyCode==40){
			$('#input-6-18').focus();
		}
		if($('#input-5-18').val()!='')
		{
			$('#input-6-18').focus();
		}
		
	});
	$('#input-6-18').on('keyup',function(e) {
	   		   	// Backspace
		if ($('#input-6-18').val()==''){
			$('#input-5-18').focus();
		}
		if (e.keyCode==40){
			$('#input-7-18').focus();
		}
		if($('#input-6-18').val()!='')
		{
			$('#input-7-18').focus();
		}
		
	});
	$('#input-7-18').on('keyup',function(e) {
			   		   	// Backspace
		if ($('#input-7-18').val()==''){
			$('#input-6-18').focus();
		}
		if (e.keyCode==40){
			$('#input-8-18').focus();
		}
		if($('#input-7-18').val()!='')
		{
			$('#input-8-18').focus();
		}
		
	});
	$('#input-8-18').on('keyup',function(e) {
			   		   	// Backspace
		if ($('#input-8-18').val()==''){
			$('#input-7-18').focus();
		}
		if (e.keyCode==40){
			$('#input-5-11').focus();
		}
		if($('#input-8-18').val() !=''){
			if(respuesta5 == 'correcto'){
				$('#fin').focus();
			}
			$('#input-4-19').focus();//**********
			ShowQuestion(6);
		}
	
	});
	$('#input2-8-18').on('keyup',function(e) {
		// Backspace
		if ($('#input2-8-18').val()==''){
		$('#input-8-17').focus();
		}
		if (e.keyCode==40){
		$('#input-5-11').focus();
		}
		if($('#input2-8-18').val() !=''){
			if(respuesta8 == 'correcto' && respuesta7 == 'correcto' && respuesta10 == 'correcto'){
				$('#fin').focus();
			}
			$('#input-9-13').focus();
		}

		});


	//Pregunta 6 Vertical
	$('#input-5-11').on('keyup',function(e) {
			   		   	// Backspace
		if ($('#input-5-11').val()==''){
			$('#input-8-18').focus();
		}
		if (e.keyCode==40){
			$('#input-6-11').focus();
		}
		if($('#input-5-11').val()!='')
		{
			$('#input-6-11').focus();
		}
		
	});
	$('#input-6-11').on('keyup',function(e) {
			   		   	// Backspace
		if ($('#input-6-11').val()==''){
			$('#input-5-11').focus();
		}
		if (e.keyCode==40){
			$('#input-7-11').focus();
		}
		if($('#input-6-11').val()!='')
		{
			$('#input-7-11').focus();
		}
		
	});
	$('#input-7-11').on('keyup',function(e) {
			   		   	// Backspace
		if ($('#input-7-11').val()==''){
			$('#input-6-11').focus();
		}
		if (e.keyCode==40){
			$('#input-8-11').focus();
		}
		if($('#input-7-11').val()!='')
		{
			if(respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
				$('#fin').focus();
			}
			if($('#input-8-11').val()!=''){
				$('#input-6-15').focus();
			}
			$('#input-8-11').focus();
		}
		
	});
	$('#input-8-11').on('keyup',function(e) {
					   		   	// Backspace
		if ($('#input-8-11').val()==''){
			$('#input-7-11').focus();
		}
		if (e.keyCode==40){
			$('#input-6-8').focus();
		}
		if($('#input-8-11').val() != ''){
			if(preg ='3'){
				$('#input-8-12').focus();
			}else if($('#input-6-15').val()!=''){
				$('#input-8-12').focus();
			}else{
				$('#input-6-15').focus();
			}			
		}
	});



	//Pregunta 7 Vertical
	$('#input-6-8').on('keyup',function(e) {
				   		   	// Backspace
		if ($('#input-6-8').val()==''){
			$('#input-8-11').focus();
		}
		if (e.keyCode==40){
			$('#input-7-8').focus();
		}
		if($('#input-6-8').val()!='')
		{
			$('#input-7-8').focus();
		}
		
	});
	$('#input-7-8').on('keyup',function(e) {
				   		   	// Backspace
		if ($('#input-7-8').val()==''){
			$('#input-6-8').focus();
		}
		if (e.keyCode==40){
			$('#input-8-8').focus();
		}
		if($('#input-7-8').val()!='')
		{
			$('#input-8-8').focus();
		}
		
	});
	$('#input-8-8').on('keyup',function(e) {
	   				   		   	// Backspace
		if ($('#input-8-8').val()==''){
			$('#input-7-8').focus();
		}
		if (e.keyCode==40){
			$('#input-9-8').focus();
		}
		if($('#input-8-8').val()!='')
		{
			$('#input-9-8').focus();
		}
		
	});
	$('#input-9-8').on('keyup',function(e) {
				   		   	// Backspace
		if ($('#input-9-8').val()==''){
			$('#input-8-8').focus();
		}
		if (e.keyCode==40){
			$('#input-10-8').focus();
		}
		if($('#input-9-8').val()!='')
		{
			$('#input-10-8').focus();
		}
		
	});

	$('#input-10-8').on('keyup',function(e) {
			   		   	// Backspace
		if ($('#input-10-8').val()==''){
			$('#input-9-8').focus();
		}
		if (e.keyCode==40){
			$('#input-11-8').focus();
		}
		if($('#input-11-8').val() == '' && $('#input-12-8').val() == '' && $('#input-10-8').val() != ''){//**********************-----
			$('#input-11-8').focus();


		}
		if ($('#input-11-8').val()	!='') {
			$('#input-12-8').focus();
		}
	});
	$('#input-11-8').on('keyup',function(e) {
			   	// Backspace
		if ($('#input-11-8').val()==''){
			$('#input-10-8').focus();
		}
		if (e.keyCode==40){
			$('#input-12-8').focus();
		}
		if ($('#input-11-8').val()	!='') {
			$('#input-12-8').focus();
		}
	});
	$('#input-12-8').on('keyup',function(e) {
	   	   	// Backspace
		if ($('#input-12-8').val()==''){
			$('#input-11-8').focus();
		}
		if (e.keyCode==40){
			$('#input-13-8').focus();
		}
		if($('#input-12-8').val()!='')
		{
			$('#input-13-8').focus();
		}
		
	});
	$('#input-13-8').on('keyup',function(e) {
	     	// Backspace
		if ($('#input-13-8').val()==''){
			$('#input-12-8').focus();
		}
		if (e.keyCode==40){
			$('#input-6-15').focus();
		}
		if($('#input-13-8').val()!='')
		{
			$('#input-6-15').focus();
	    	ShowQuestion(8);
		}
		
	});


	//Pregunta 8 Vertical
	$('#input-6-15').on('keyup',function(e) {
	    	// Backspace
		if ($('#input-6-15').val()==''){
			$('#input-8-11').focus();
		}
		if (e.keyCode==40){
			$('#input-7-15').focus();
		}
		if($('#input-6-15').val()!='')
		{
			$('#input-7-15').focus();
		}
		
	});
	$('#input-7-15').on('keyup',function(e) {
			// Backspace
		if ($('#input-7-15').val()==''){
			$('#input-6-15').focus();
		}
		if (e.keyCode==40){
			$('#input-7-15').focus();
		}
		if($('#input-7-15').val()!='')
		{
			if(respuesta7 == 'correcto' && respuesta9 == 'correcto' && respuesta10 == 'correcto'){
				$('#fin').focus();
			}
			$('#input-8-15').focus();
		}
		
	});
	$('#input-8-15').on('keyup',function(e) {
			// Backspace
		if ($('#input-8-15').val()==''){
			$('#input-7-15').focus();
		}
		if (e.keyCode==40){
			$('#input-8-6').focus();
		}
		if($('#input-8-15').val() != ''){
			if($('#input-8-10').val() != ''){
				$('#input-8-16').focus();
			}else{
				$('#input-8-10').focus();
			}
			
		}
	});

	//Preguta 9 Vertical
	$('#input-8-6').on('keyup',function(e) {
			// Backspace
		if ($('#input-8-6').val()==''){
			$('#input-8-15').focus();
		}
		if (e.keyCode==40){
			$('#input-9-6').focus();
		}
		if($('#input-8-6').val()!=''){
			$('#input-9-6').focus();
		}
		
	});
	$('#input-9-6').on('keyup',function(e) {
			// Backspace
		if ($('#input-9-6').val()==''){
			$('#input-8-6').focus();
		}
		if (e.keyCode==40){
			$('#input-10-6').focus();
		}
		if($('#input-9-6').val()!='')
		{
			$('#input-10-6').focus();
		}
		
	});
	$('#input-10-6').on('keyup',function(e) {
			// Backspace
		if ($('#input-10-6').val()==''){
			$('#input-9-6').focus();
		}
		if (e.keyCode==40){
			$('#input-11-6').focus();
		}
		
			if($('#input-10-6').val()!='')
			{
				$('#input-11-6').focus();
			}
	});
	$('#input-11-6').on('keyup',function(e) {
			// Backspace
		if ($('#input-11-6').val()==''){
			$('#input-10-6').focus();
		}
		if (e.keyCode==40){
			$('#input-9-10').focus();
		}
		if($('#input-11-6').val() != ''){
			$('#input-9-10').focus();
			ShowQuestion(10);
		}
	});


	//Pregunta 10 Vertical
	// $('#input-8-10').on('keyup',function(e) {
	//     $('#input-9-10').focus();
	// });
	$('#input-9-10').on('keyup',function(e) {
			// Backspace
		if ($('#input-9-10').val()==''){
			$('#input-11-6').focus();
		}
		if (e.keyCode==40){
			$('#input-10-10').focus();
		}
		if($('#input-9-10').val()!='')
		{
			$('#input-10-10').focus();
		}
		
	});
	// $('#input-10-10').on('keyup',function(e) {
	//     $('#input-11-10').focus();
	// });
	// $('#input-11-10').on('keyup',function(e) {
	//     $('#input-12-10').focus();
	// });
	$('#input-10-10').on('keyup',function(e) {
			// Backspace
		if ($('#input-10-10').val()==''){
			$('#input-9-10').focus();
		}
		if (e.keyCode==40){
			$('#input-11-10').focus();
		}
		if($('#input-10-10').val()!='')
		{
			$('#input-11-10').focus();
		}

	});
	$('#input-11-10').on('keyup',function(e) {
			// Backspace
		if ($('#input-11-10').val()==''){
			$('#input-10-10').focus();
		}
		if (e.keyCode==40){
			$('#input-12-10').focus();
		}
		if($('#input-11-10').val() != ''){
			$('#input-12-10').focus();
		}
	});
	$('#input-12-10').on('keyup',function(e) {
			// Backspace
		if ($('#input-12-10').val()==''){
			$('#input-11-10').focus();
		}
		if (e.keyCode==40){
			$('#input-13-10').focus();
		}
		if($('#input-12-10').val()!='')
		{
			$('#input-13-10').focus();
		}
		
	});
	$('#input-13-10').on('keyup',function(e) {
			// Backspace
		if ($('#input-13-10').val()==''){
			$('#input-12-10').focus();
		}
		if (e.keyCode==40){
			$('#input-14-10').focus();
		}
		if($('#input-13-10').val()!='')
		{
			$('#input-14-10').focus();
		}
		
	});
	$('#input-14-10').on('keyup',function(e) {
			// Backspace
		if ($('#input-14-10').val()==''){
			$('#input-13-10').focus();
		}
		if (e.keyCode==40){
			$('#input-15-10').focus();
		}
		if($('#input-14-10').val()!='')
		{
			$('#input-15-10').focus();
		}
		
	});
	$('#input-15-10').on('keyup',function(e) {
	   	// Backspace
		if ($('#input-15-10').val()==''){
			$('#input-14-10').focus();
		}
		if (e.keyCode==40){
			$('#input-16-10').focus();
		}
		if($('#input-15-10').val()!='')
		{
			$('#input-16-10').focus();
		}
		
	});
	$('#input-16-10').on('keyup',function(e) {
	   // Backspace
		if ($('#input-16-10').val()==''){
			$('#input-15-10').focus();
		}
		if (e.keyCode==40){
			$('#input-17-10').focus();
		}
		if($('#input-16-10').val()!='')
		{
			$('#input-17-10').focus();
		}
		
	});
	$('#input-17-10').on('keyup',function(e) {
	      // Backspace
		if ($('#input-17-10').val()==''){
			$('#input-16-10').focus();
		}
		if (e.keyCode==40){
			$('#input-18-10').focus();
		}
		if($('#input-17-10').val()!='')
		{
			$('#input-18-10').focus();
		}
		
	});
	$('#input-18-10').on('keyup',function(e) {
		 // Backspace
		 if ($('#input-18-10').val()==''){
			$('#input-17-10').focus();
		}
		if (e.keyCode==40){
			$('#input-19-10').focus();
		}
		if($('#input-18-10').val()!='')
		{
			$('#input-19-10').focus();
		}
		
	});
	$('#input-19-10').on('keyup',function(e) {
	    // Backspace
		 if ($('#input-19-10').val()==''){
			$('#input-18-10').focus();
		}
		if (e.keyCode==40){
			$('#input-20-10').focus();
		}
		if($('#input-19-10').val()!='')
		{
			$('#input-20-10').focus();
		}
		
	});
	// $('#input-20-10').on('keyup',function(e) {
	//     $('#input-9-13').focus();
	// });
	$('#input-20-10').on('keyup',function(e) {
		// Backspace
		if ($('#input-20-10').val()==''){
			$('#input-19-10').focus();
		}
		if (e.keyCode==40){
			$('#input-8-10').focus();
		}
		if($('#input-20-10').val()!='')
		{
		$('#input-8-10').focus();
		}

	});

	//Pregunta 10 horizontal
	$('#input-8-10').on('keyup',function(e) {
		// Backspace
		preg = 3
		if ($('#input-8-10').val()==''){
			$('#input-8-15').focus();
		}
		if (e.keyCode==39){
			$('#input-8-11').focus();
		}
		if($('#input-8-10').val()!='')
		{
			if($('#input-8-11').val()!=''){
				$('#input-8-12').focus();
			}
			$('#input-8-11').focus();	
		}
	});
	$('#input-8-12').on('keyup',function(e) {
		// Backspace
		if ($('#input-8-12').val()==''){
			$('#input-8-10').focus();
		}
		if (e.keyCode==39){
			$('#input-8-13').focus();
		}
		if($('#input-8-12').val()!='')
		{
			if($('#input-8-13').val()!=''){
				$('#input-8-14').focus();
			}
			$('#input-8-13').focus();	
		}
		
	});
	$('#input-8-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-8-13').val()==''){
			$('#input-8-12').focus();
		}
		if (e.keyCode==39){
			$('#input-8-14').focus();
		}
		if($('#input-8-13').val() != ''){
			$('#input-8-14').focus();
		}
	});
	$('#input-8-14').on('keyup',function(e) {
// Backspace
		if ($('#input-8-14').val()==''){
			$('#input-8-13').focus();
		}
		if (e.keyCode==39){
			$('#input-8-14').focus();
		}
	    if($('#input-8-14').val() != ''){
			if($('#input-8-15').val() != ''){
				$('#input-8-16').focus();
			}
			$('#input-8-15').focus();		
		}
	});

	$('#input-8-16').on('keyup',function(e) {
	   // Backspace
		if ($('#input-8-16').val()==''){
			$('#input-8-14').focus();
		}
		if (e.keyCode==39){
			$('#input-8-17').focus();
		}
		if($('#input-8-16').val()!='')
		{
			$('#input-8-17').focus();
		}
		
	});

	$('#input-8-17').on('keyup',function(e) {
		// Backspace
		if ($('#input-8-17').val()==''){
			$('#input-8-16').focus();
		}
		if (e.keyCode==39){
			$('#input-8-18').focus();
		}
		if($('#input-8-17').val() != ''){
			$('#input2-8-18').focus();
		}
	});

	//Pregunta 11 Vertical

	$('#input-9-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-9-13').val()==''){
			$('#input2-8-18').focus();
		}
		if (e.keyCode==40){
			$('#input-9-1').focus();
		}
		if($('#input-9-13').val()!='')
		{
			if(respuesta8 == 'correcto' && respuesta9 == 'correcto' && respuesta7 == 'correcto'){
				$('#fin').focus();
			}
			if($('#input-5-11').val()==''){
				$('#input-5-11').focus();
			}else if($('#input-6-15').val()==''){
				$('#input-6-15').focus();
			}else if($('#input-8-10').val()==''){
				$('#input-8-10').focus();
			}
			
		}
		
	});
	//Pregunta 12 Horizontal
	$('#input-9-1').on('keyup',function(e) {
		// Backspace
		if ($('#input-9-1').val()==''){
			$('#input-9-13').focus();
		}
		if (e.keyCode==39){
			$('#input-9-2').focus();
		}
		if($('#input-9-1').val()!='')
		{
		$('#input-9-2').focus();	
		}
		
	});
	// $('#input-9-2').on('keyup',function(e) {
	//     $('#input-9-4').focus();
	// });

	$('#input-9-2').on('keyup',function(e) {
		// Backspace
		if ($('#input-9-2').val()==''){
			$('#input-9-1').focus();
		}
		if (e.keyCode==39){
			$('#input-9-3').focus();
		}
		if($('#input-9-2').val() != ''){
			$('#input-9-4').focus();
		}
	});


	$('#input-9-4').on('keyup',function(e) {
		// Backspace
		if ($('#input-9-4').val()==''){
			$('#input-9-2').focus();
		}
		if (e.keyCode==39){
			$('#input-11-1').focus();
		}
		if($('#input-9-4').val()!='')
		{
			$('#input-11-1').focus();
		}
		
	});

	//Pregunta 13 Horizontal 13
	$('#input-11-1').on('keyup',function(e) {
	   // Backspace
		if ($('#input-11-1').val()==''){
			$('#input-9-4').focus();
		}
		if (e.keyCode==39){
			$('#input-11-2').focus();
		}
		if($('#input-11-1').val()!='')
		{
			$('#input-11-2').focus();
		}
		
	});
	$('#input-11-2').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-2').val()==''){
			$('#input-11-1').focus();
		}
		if (e.keyCode==39){
			$('#input-11-3').focus();
		}
		if($('#input-11-2').val() != ''){
			$('#input-11-4').focus();
		}
	});

	$('#input-11-4').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-4').val()==''){
			$('#input-11-2').focus();
		}
		if (e.keyCode==39){
			$('#input-11-5').focus();
		}
		if($('#input-11-4').val() != ''){
			$('#input-11-5').focus();
		}
	});
	$('#input-11-5').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-5').val()==''){
			$('#input-11-4').focus();
		}
		if (e.keyCode==39){
			$('#input-11-6').focus();
		}
		if($('#input-11-5').val() != ''){
			$('#input-11-7').focus();
		}
	});

	$('#input-11-7').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-7').val()==''){
			$('#input-11-5').focus();
		}
		if (e.keyCode==39){
			$('#input-11-8').focus();
		}
		if($('#input-11-7').val() != ''){
		
			$('#input-11-9').focus();
		}
	});

	$('#input-11-9').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-9').val()==''){
			$('#input-11-7').focus();
		}
		if (e.keyCode==39){
			$('#input-11-10').focus();
		}
		if($('#input-11-9').val() != ''){
			$('#input-11-11').focus();
		}
	});
	$('#input-11-11').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-11').val()==''){
			$('#input-11-9').focus();
		}
		if (e.keyCode==39){
			$('#input-11-12').focus();
		}
		if($('#input-11-11').val()!='')
		{
		$('#input-11-12').focus();	
		}
		

	});

	$('#input-11-12').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-12').val()==''){
			$('#input-11-11').focus();
		}
		if (e.keyCode==39){
			$('#input-11-13').focus();
		}
		if($('#input-11-12').val() != '' && $('#input-11-13').val() == '' && $('#input-11-14').val() == ''){
			$('#input-11-13').focus();

		}else if($('#input-11-13').val() != ''){
			$('#input-11-14').focus();
		}
	});
	$('#input-11-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-13').val()==''){
			$('#input-11-12').focus();
		}
		if (e.keyCode==39){
			$('#input-11-14').focus();
		}
		if($('#input-11-13').val() != ''){//***********
			$('#input-11-14').focus();
		}
	});
	$('#input-11-14').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-14').val()==''){
			$('#input-11-13').focus();
		}
		if (e.keyCode==39){
			$('#input-11-15').focus();
		}
		if($('#input-11-14').val()!='')
		{
		$('#input-11-15').focus();	
		}
		
	});
	$('#input-11-15').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-15').val()==''){
			$('#input-11-14').focus();
		}
		if (e.keyCode==39){
			$('#input-11-16').focus();
		}
		if($('#input-11-15').val()!='')
		{
			$('#input-11-16').focus();
		}
		
	});
	$('#input-11-16').on('keyup',function(e) {
		// Backspace
		if ($('#input-11-16').val()==''){
			$('#input-11-15').focus();
		}
		if (e.keyCode==39){
			$('#input-12-5').focus();
		}
		if($('#input-11-16').val()!='')
		{
		$('#input-12-5').focus();
	    ShowQuestion(14);	
		}
		
	});

	//Pregunta 14 Vertical
	$('#input-12-5').on('keyup',function(e) {
	   // Backspace
		if ($('#input-12-5').val()==''){
			$('#input-11-16').focus();
		}
		if (e.keyCode==40){
			$('#input-13-5').focus();
		}
		if($('#input-12-5').val()!='')
		{
			$('#input-13-5').focus();
		}
		
	});
	$('#input-13-5').on('keyup',function(e) {
		// Backspace
		if ($('#input-13-5').val()==''){
			$('#input-12-5').focus();
		}
		if (e.keyCode==40){
			$('#input-14-5').focus();
		}
		if($('#input-13-5').val()!='')
		{
		$('#input-14-5').focus();	
		}
		
	});

	$('#input-14-5').on('keyup',function(e) {
		// Backspace
		if ($('#input-14-5').val()==''){
			$('#input-13-5').focus();
		}
		if (e.keyCode==40){
			$('#input-11-13').focus();
		}
		if($('#input-14-5').val() != ''){
			$('#input-12-13').focus();
			ShowQuestion(15);
		}
	});

	// Pregunta 15 Vertical
	// $('#input-11-13').on('keyup',function(e) {
	//     $('#input-12-13').focus();
	// });
	
	$('#input-12-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-12-13').val()==''){
			$('#input-14-5').focus();
		}
		if (e.keyCode==40){
			$('#input-13-13').focus();
		}
		if($('#input-12-13').val()!='')
		{
			$('#input-13-13').focus();
		}
		
	});
	$('#input-13-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-13-13').val()==''){
			$('#input-12-13').focus();
		}
		if (e.keyCode==40){
			$('#input-14-13').focus();
		}
		if($('#input-13-13').val()!='')
		{
			$('#input-14-13').focus();
		}
		
	});
	$('#input-14-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-14-13').val()==''){
			$('#input-13-13').focus();
		}
		if (e.keyCode==40){
			$('#input-15-13').focus();
		}
		if($('#input-14-13').val()!='')
		{
			$('#input-15-13').focus();
		}
		
	});

	$('#input-15-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-15-13').val()==''){
			$('#input-14-13').focus();
		}
		if($('#input-15-13').val()!='')
		{
			$('#input-16-13').focus();
		}
	});
	$('#input-16-13').on('keyup',function(e) {
		// Backspace
		if ($('#input-16-13').val()==''){
			$('#input-15-13').focus();
		}
		
	});

}

function ShowQuestion(id){
	if (id == 1){
		$("#q1").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 2){
		$("#q2").show(500);
		$("#q1").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 3){
		$("#q3").show(500);
		$("#q2").hide(500);
		$("#q1").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 4){
		$("#q4").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q1").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 5){
		$("#q5").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q1").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 6){
		$("#q6").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q1").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 7){
		$("#q7").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q1").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 8){
		$("#q8").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q1").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 9){
		$("#q9").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q1").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 10){
		$("#q10").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q1").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 11){
		$("#q11").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q1").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 12){
		$("#q12").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q1").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 13){
		$("#q13").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q1").hide(500);
		$("#q14").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 14){
		$("#q14").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q1").hide(500);
		$("#q15").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
	else if (id == 15){
		$("#q15").show(500);
		$("#q2").hide(500);
		$("#q3").hide(500);
		$("#q4").hide(500);
		$("#q5").hide(500);
		$("#q6").hide(500);
		$("#q7").hide(500);
		$("#q8").hide(500);
		$("#q9").hide(500);
		$("#q10").hide(500);
		$("#q11").hide(500);
		$("#q12").hide(500);
		$("#q13").hide(500);
		$("#q14").hide(500);
		$("#q1").hide(500);
		//$("#q16").hide(500);
		//$("#q17").hide(500);
	}
}
function public_FB(){


	var msj="GAME: CROSSWORD  LEVEL:"+levelGover+" TOPIC:governance  POINTS: "+punto;

	$(".fb-xfbml-parse-ignore").attr("href","https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Fjuegos%2Fcrucigrama%2FInternetGovernance.html?description="+msj+"&method=1&amp;src=sdkpreparse");
}
function public_TW(){


	var msj="GAME: CROSSWORD  LEVEL:"+levelGover+" TOPIC:governance  POINTS: "+punto;
	var src='https://angelgame.acostasite.com/images/background/crucigramas.jpg';
	window.open("https://twitter.com/share?text="+msj+"&url=https://angelgame.acostasite.com/Game/juegos/crucigrama/InternetGovernance-es.html");
  }
  function reload(){

	location.reload();
  }
  var dataInstag;
  function public_IG(){
  
  
	  var msj="GAME: CROSSWORD  LEVEL:"+levelGover+" TOPIC:governance  POINTS: "+punto;
	  /*window.plugins.socialsharing.shareViaInstagram(
		  'Message via Instagram', 
		  'https://angelgame.acostasite.com/images/icon/icon.png','https://angelgame.acostasite.com'
		);	*/
	   // var assetLocalIdentifier = "../../img/congratulations.png";
	   Instagram.isInstalled(function (err, installed) {
		  if (installed) {
			  console.log("Instagram is"+ installed); // installed app version on Android
			  navigator.screenshot.save(function(error,response){
				  if(error){
					  console.error(error);
					  return;
				  }
				  
				  // Something like: /storage/emulated/0/Pictures/screenshot_1477924039236.jpg
				  console.log(response.filePath);
		  
				  /*Instagram.shareAsset(function(result) {
					  alert('Instagram.shareAsset success: ' + result);
				  }, function(e) {
					  alert('Instagram.shareAsset error: ' + e);
				  }, response.filePath);*/
				  getBase64FromImageUrl(response.filePath, msj);
				  
			  });
		  } else {
			  alert("Instagram no esta instalado");
		  }
	  });
  
	  
	   
  
  /*	module.controller('ThisCtrl', function($scope, $cordovaInstagram) {
		  // Get image from camera, base64 is good. See the
		  // $cordovaCamera docs for more info
		
		  $cordovaInstagram.share($scope.image.data, $scope.image.caption).then(function() {
			// Worked
		  }, function(err) {
			// Didn't work
		  });
		})*/
	}
  
	function getBase64FromImageUrl(url, msj) {
	  var img = new Image();
  
	  img.setAttribute('crossOrigin', 'anonymous');
  
	  img.onload = function () {
		  var canvas = document.createElement("canvas");
		  canvas.width =this.width;
		  canvas.height =this.height;
  
		  var ctx = canvas.getContext("2d");
		  ctx.drawImage(this, 0, 0);
  
		  var dataURL = canvas.toDataURL("image/png");
		  dataInstag = dataURL/*.replace(/^data:image\/(png|jpg);base64,/, "")*/;
		  
		  Instagram.share(dataInstag, msj, function (err) {
			  if (err) {
				  console.log("Not shared");
			  } else {
				  console.log("shared");
			  }
		  });
	  };
  
	  img.src = url;
  }
  function Zoom(src){
	console.log("zoom", src)
	$("#imgZoom").attr('src', src);
	$("#ModalZoom").css({"display": "block"});
	document.getElementById("imgZoom").className ="imgZm animated fadeInUp slow"; 
}
function cerrarZoom(){
	console.log("Cerrar Zoom")
	$("#ModalZoom").css({"display": "none"});
}