//window.localStorage.setItem('ttl', 5);
//window.localStorage.setItem('coin', 1000);
//localStorage.setItem('goHome', 'si');
var language = localStorage.getItem('selectedLang');
var Onanimated = localStorage.getItem('Animated');
var ttl = Number(window.localStorage.getItem('ttl'));
function goHomeF(){
  localStorage.setItem('goHome', 'si');
}
$(document).ready(function(){
  var root = document.documentElement;
  var platfr= window.localStorage.getItem('platform');
  
  if(platfr=='ios'){    
    root.className += 'ios';
  }   
})
//ir para la tienda
function comprar(){	
  window.localStorage.setItem('comprar', 'si');
  window.location.href = "/";
}
//abrir Modaltuto
function ModalTuto(){    
  if (Onanimated == 'true'){
      document.getElementById("ModalTuto").className ="mostrar animated fadeIn delay-1s"; 
  document.getElementById("BoxTuto").className ="relative animated bounceIn delay-1s"; 
  }else{
      document.getElementById("ModalTuto").className ="ModalAviso mostrar "; 
  document.getElementById("BoxTuto").className ="BoxAviso relative "; 
  }
}
//cerrar Modaltuto
function cerrarTuto(){
  if (Onanimated == 'true'){
  document.getElementById("BoxTuto").className ="relative animated zoomOut" ;
    setTimeout(function() { 
      document.getElementById("ModalTuto").className ="";
   }, 500);
  }else{
  document.getElementById("ModalTuto").className ="";
  }
}
//muestra el tuto 
function showTuto(juego){
  // console.log(juego)
  var noShow = localStorage.getItem('noShow'+juego);
  //window.localStorage.setItem('noShow'+juego, 'false');
  // console.log(noShow)
  if(localStorage.getItem("Next")== 'no'){
    if(noShow=='false'){
      ModalTuto();
      tutorial(language, juego);
    } 
  }
}
//checkbox para dejar de ver el tuto
function noShow(juego){
  var nombrejue = "noShow"+juego;
  
  if (checkBox.checked == true){
    console.log('entro')
    window.localStorage.setItem(nombrejue, 'true');
  }else{
    console.log('salio')
    window.localStorage.setItem(nombrejue, 'false');
  }	
}
//check tiempos cumplidos
function check(){
  
  var timeNewVida1 = Number(localStorage.getItem('fechaRecuperarVida1')); 
  var timeNewVida2 = Number(localStorage.getItem('fechaRecuperarVida2')); 
  var timeNewVida3 = Number(localStorage.getItem('fechaRecuperarVida3')); 
  var timeNewVida4 = Number(localStorage.getItem('fechaRecuperarVida4')); 
  var timeNewVida5 = Number(localStorage.getItem('fechaRecuperarVida5')); 
 
  if(timeNewVida1 == 0){
    window.localStorage.setItem('fechaRecuperarVida1', timeNewVida2);
    window.localStorage.setItem('fechaRecuperarVida2', timeNewVida3);
    window.localStorage.setItem('fechaRecuperarVida3', timeNewVida4);
    window.localStorage.setItem('fechaRecuperarVida4', timeNewVida5);
    window.localStorage.setItem('fechaRecuperarVida5', 0);
  }
}
//conteo regresivo
function conteoRegresivo(NewVida, n){
  var ttl = Number(window.localStorage.getItem('ttl'));
  var interval = setInterval(function() {
    var hoy = new Date();
    var minutoActual = hoy.getTime();
    var remainTime = (new Date(NewVida) - hoy + 1000)/1000;
    var remainSeg = ('0'+ Math.floor(remainTime % 60)).slice(-2);
    var remainMin = ('0'+ Math.floor(remainTime / 60 % 60)).slice(-2);
      document.getElementById('Seg').innerHTML =  remainSeg; 
      document.getElementById('Min').innerHTML =  remainMin; 

      // console.log('Min= '+remainMin+ 'Seg= '+remainSeg)
      //termina el ciclo
      if(minutoActual >= NewVida){     
          $('#reload').removeAttr('style')
        ttl++      
        window.localStorage.setItem('ttl', ttl);
        console.log('tiempo terminado vida= '+ttl)        
        if(ttl <= 4){
          window.localStorage.setItem('fechaRecuperarVida1', 0);
          check();
          contadorMinutos();
        }  
        clearInterval(interval);
        }
    },1000) 
}
// establece cual vida esta regenerandose
function vidas(){
  
  var ttl = Number(window.localStorage.getItem('ttl'));
  var timeNewVida1 = Number(localStorage.getItem('fechaRecuperarVida1')); 
  var timeNewVida2 = Number(localStorage.getItem('fechaRecuperarVida2')); 
  var timeNewVida3 = Number(localStorage.getItem('fechaRecuperarVida3')); 
  var timeNewVida4 = Number(localStorage.getItem('fechaRecuperarVida4')); 
  var timeNewVida5 = Number(localStorage.getItem('fechaRecuperarVida5')); 

    console.log('timeNewVida1= '+timeNewVida1) 
    console.log('timeNewVida2= '+timeNewVida2) 
    console.log('timeNewVida3= '+timeNewVida3) 
    console.log('timeNewVida4= '+timeNewVida4) 
    console.log('timeNewVida5= '+timeNewVida5) 

  var tiempoActual = new Date();
    tiempoActual= tiempoActual.getTime();
if(tiempoActual < timeNewVida1){
  conteoRegresivo(timeNewVida1, 1);
}else if(tiempoActual < timeNewVida2, 2){
  conteoRegresivo(timeNewVida2);
}else if(tiempoActual < timeNewVida3, 3){
  conteoRegresivo(timeNewVida3);
}else if(tiempoActual < timeNewVida4, 4){
  conteoRegresivo(timeNewVida4);
}else if(tiempoActual < timeNewVida5, 5){
  conteoRegresivo(timeNewVida5);
}else{
  
  if(ttl <= 5){
    window.localStorage.setItem('ttl', 5);
  }   
}
}
if(ttl == 5){
  // console.log('entro')
  window.localStorage.setItem('fechaRecuperarVida1', 0);
  window.localStorage.setItem('fechaRecuperarVida2', 0);
  window.localStorage.setItem('fechaRecuperarVida3', 0);
  window.localStorage.setItem('fechaRecuperarVida4', 0);
  window.localStorage.setItem('fechaRecuperarVida5', 0);
} 
// establece los minutos que deben esperar para optener la vida
function contadorMinutos(){ 
  var ttl = Number(window.localStorage.getItem('ttl'));
  var timeNewVida1 = Number(localStorage.getItem('fechaRecuperarVida1')); 
  var timeNewVida2 = Number(localStorage.getItem('fechaRecuperarVida2')); 
  var timeNewVida3 = Number(localStorage.getItem('fechaRecuperarVida3')); 
  var timeNewVida4 = Number(localStorage.getItem('fechaRecuperarVida4')); 
  var timeNewVida5 = Number(localStorage.getItem('fechaRecuperarVida5')); 
  var minutos=  1000 * 60 * 10;
      if(timeNewVida1 == 0 && ttl <= 4){//si ya tiene fecha  no pasa
        //fecha actual
        var fechaSinVida = new Date();
        //fecha a la que debe llegar para optener la vida
        var fechaRecuperarVida = fechaSinVida.getTime() + minutos; 
        // var fechaDentroDeMinutos = new Date(fechaRecuperarVida);
        window.localStorage.setItem('fechaRecuperarVida1', fechaRecuperarVida);
        // console.log('hoy= '+ fechaSinVida)
        // console.log('fechaDentroDeMinutos= '+ fechaDentroDeMinutos) 
      }
      if(timeNewVida2 == 0 && ttl <= 3){ //si ya tiene fecha  no pasa
        //fecha de la vida 4 que ya empezo
        var vidaEmpezada1 = Number(localStorage.getItem('fechaRecuperarVida1')); 
        //fecha a la que debe llegar para optener la vida
        var fechaRecuperarVida2 = vidaEmpezada1 + minutos; 
        //para comparar fechas 
        window.localStorage.setItem('fechaRecuperarVida2', fechaRecuperarVida2);
      }
      if(timeNewVida3 == 0 && ttl <= 2){ //si ya tiene fecha  no pasa
        //fecha de la vida 4 que ya empezo
        var vidaEmpezada2 = Number(localStorage.getItem('fechaRecuperarVida2')); 
        //fecha a la que debe llegar para optener la vida
        var fechaRecuperarVida3 = vidaEmpezada2 + minutos;  
        //para comparar fechas 
        window.localStorage.setItem('fechaRecuperarVida3', fechaRecuperarVida3);
      }
      if(timeNewVida4 == 0 && ttl <= 1){ //si ya tiene fecha  no pasa
        //fecha de la vida 4 que ya empezo
        var vidaEmpezada3 = Number(localStorage.getItem('fechaRecuperarVida3')); 
        //fecha a la que debe llegar para optener la vida
        var fechaRecuperarVida4 = vidaEmpezada3 + minutos; 
        //para comparar fechas 
        window.localStorage.setItem('fechaRecuperarVida4', fechaRecuperarVida4);
      }
      if(timeNewVida5 == 0 && ttl == 0){ //si ya tiene fecha  no pasa
        //fecha de la vida 4 que ya empezo
        var vidaEmpezada4 = Number(localStorage.getItem('fechaRecuperarVida4')); 
        //fecha a la que debe llegar para optener la vida
        var fechaRecuperarVida5 = vidaEmpezada4 + minutos; 
        //para comparar fechas 
        window.localStorage.setItem('fechaRecuperarVida5', fechaRecuperarVida5);
      }
  vidas();
}

//para controlar textFont    //se multiplica con 1.3 y 1.7 (26px)
$(document).ready(function(){
var computedFontSize = window.getComputedStyle(document.getElementById("levelModalLabel")).fontSize;
var fontSz= parseFloat(computedFontSize)
console.log('fontSz=',fontSz)
if(fontSz>=32 && fontSz<=42 ){
  var interval = setInterval(function(){
    $('.textRespuesta, .modal .game-info, .textRespuesta, .click span').css({//11px
      'font-size': '8.5px',
    })  
    $('.btn, .tmp , #ttl , #coin, .score, .txtTiempo, #levelModal .modal-footer p, .drag .input, .drag .caja, .level-difficulty button, .titAvisoTuto, .txtCheck, .memoria #imagenes p, .Contenido p').css({//15px
      'font-size': '11px',
    })        
    $('.titAviso, .head, .post, #score, #times, #titleQuestion, .co, .inc, #palabras span, .txtPrg, #contratiempo, .btnBox .btnShw').css({//16px
      'font-size':'12.5px',
    })
    $('.lvlTitle, #LooseModal h4, #finalizado, .txtTrivia, .level-difficulty h4').css({//18px
      'font-size':'14px',
    })  
    $('.ptos, .titAudi, .drag .title, .btnLetra, #presentation, .disc #ActualQuestion').css({//20px
      'font-size':'15px',
    }) 
    $('#levelModalLabel, #TTLModal .txtTTL, .p5050, .ShowCoin').css({//26px
      'font-size':'20px',
    })      
    $('#LooseModal h2').css({//40px
      'font-size':'30px',
    }) 
    $('.boxTable h1').css({ //36px
      'font-size':'27.5px'
    })  
    $('#WinModal h2, .boxTable .hidden-field').css({//30px
      'font-size':'23px',
    }) 
    $('#lvltxtD, ').css({//40px
      'font-size':'31px',
    }) 
    $('#lvltxt').css({//56px
      'font-size':'43px',
    })
    
  }, 200);   
}else if(fontSz>=43){
  var interval = setInterval(function(){
    $('.textRespuesta, .modal .game-info, .textRespuesta, .click span').css({//11px
      'font-size': '6.1px',
    })  
    $('.btn, .tmp , #ttl , #coin, .score, .txtTiempo, #levelModal .modal-footer p, .drag .input, .drag .caja, .level-difficulty button, .titAvisoTuto, .txtCheck, .memoria #imagenes p, .Contenido p').css({//15px
      'font-size': '8.5px',
    })        
    $('.titAviso, .head, .post, #score, #times, #titleQuestion, .co, .inc, #palabras span, .txtPrg, #contratiempo, .btnBox .btnShw').css({//16px
      'font-size':'9px',
    })
    $('.lvlTitle, #LooseModal h4, #finalizado, .txtTrivia, .level-difficulty h4').css({//18px
      'font-size':'10px',
    })  
    $('.ptos, .titAudi, .drag .title, .btnLetra, #presentation, .disc #ActualQuestion').css({//20px
      'font-size':'11px',
    }) 
    $('#levelModalLabel, #TTLModal .txtTTL, .p5050, .ShowCoin').css({//26px
      'font-size':'14.5px',
    })  
    $('#WinModal h2, .boxTable .hidden-field').css({ //30px
      'font-size':'17px'
    })  
    $('.boxTable h1').css({ //36px
      'font-size':'21.2px'
    })         
    $('#LooseModal h2').css({ //40px
      'font-size':'22px',
    })
    $('#lvltxtD').css({//40px
      'font-size':'22px',
    }) 
    $('#lvltxt').css({//56px
      'font-size':'31px',
    })
    
  }, 200); 
}
})
//slider del tutorial
$(document).ready(function() {
  var cont =1;
  var divs = document.getElementsByClassName("product").length;
  divs2= divs-1
  function animateContentColor() {
    var getProductColor = $('.product .active').attr('product-color');
    $('body').css({
      background: getProductColor
    });
    $('.title').css({
      color: getProductColor
    });
    $('.btn').css({
      color: getProductColor
    });
  }
  var productItem = $('.product'),
    productCurrentItem = productItem.filter('.active'); 
  $('#next').on('click', function(e) {      
    
    if(cont<=divs2){
      $('#next').removeAttr('style') 
      $('#prev').removeAttr('style') 
      e.preventDefault();
      var nextItem = productCurrentItem.next();
      productCurrentItem.removeClass('active');
      if (nextItem.length) {
        productCurrentItem = nextItem.addClass('active');
      } else {
        productCurrentItem = productItem.first().addClass('active');
      }
      animateContentColor();
    }
    if(cont<=divs2){
      cont++
    }
    colorBtn();
  });
  $('#prev').on('click', function(e) {    
    
    if(cont>1){
      $('#next').removeAttr('style') 
      $('#prev').removeAttr('style')
      e.preventDefault();
      var prevItem = productCurrentItem.prev();
      productCurrentItem.removeClass('active');
      if (prevItem.length) {
        productCurrentItem = prevItem.addClass('active');
      } else {
        productCurrentItem = productItem.last().addClass('active');
      }
      animateContentColor();      
    }
    if(cont>1){
      cont--
    }
    colorBtn();
  });
  function colorBtn(){
    if(cont==1){
      $('#prev').css({'opacity':'.5'})
    }else if(cont==divs){
      $('#next').css({'opacity':'.5'})
    }
  }
  // Ripple
  $('[ripple]').on('click', function(e) {
    var rippleDiv = $('<div class="ripple" />'),
      rippleSize = 60,
      rippleOffset = $(this).offset(),
      rippleY = e.pageY - rippleOffset.top,
      rippleX = e.pageX - rippleOffset.left,
      ripple = $('.ripple');
    rippleDiv.css({
      top: rippleY - (rippleSize / 2),
      left: rippleX - (rippleSize / 2),
      background: $(this).attr("ripple-color")
    }).appendTo($(this));
    window.setTimeout(function() {
      rippleDiv.remove();
    }, 1900);
  });
});
// fin slider del tutorial

//para musica al minimisar
$(window).focus(function() {      
  if(localStorage.getItem('minimisar')=='1'){
    console.log('entro')
    PlayMusic('1')
    localStorage.setItem('minimisar','0') 
  }      
});
$(window).blur(function() {     
  if(localStorage.getItem('Audio')=='1'){
    console.log('salio')
    localStorage.setItem('minimisar','1') 
    PlayMusic('0')
  }                
});