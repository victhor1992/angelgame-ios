import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var $
@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  language:string;
  Titpag :string;
  Lider :string;
  Desarrollador :string;
  Disenador :string;
  Patrocinante :string;
  Version :string;
  versionNum: string;
  fechaVersion: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.language = localStorage.getItem('selectedLang');    
    this.ChangeLanguages();
  }

  ionViewDidLoad() {
    console.log(this.language);
  }
  ionViewWillEnter(){
    if (localStorage.getItem('ajustFont') == 'si'){
      this.AjustFont();
    }
    this.versionNum= localStorage.getItem('versionNum')
    this.fechaVersion= localStorage.getItem('fechaVersion')
  }
  ChangeLanguages() {
    if (this.language == "Eng") {
      this.Titpag = "Credits"
      this.Lider = "PROJECT LEADER";
      this.Desarrollador = "DEVELOPERS";
      this.Disenador = "DESIGN";
      this.Patrocinante = "SPONSOR";
      this.Version = "VERSION";
    }else if (this.language == "Por"){
      this.Titpag = "Créditos"
      this.Lider = "LÍDER DO PROJETO";
      this.Desarrollador = "COLABORADORES";
      this.Disenador = "DESENHO";
      this.Patrocinante = "PATROCINADORES";
      this.Version = "VERSÃO";
    }else{
      this.Titpag = "Créditos"
      this.Lider = "LÍDER DEL PROYECTO";
      this.Desarrollador = "DESARROLLADORES";
      this.Disenador = "DISEÑO";
      this.Patrocinante = "PATROCINANTES";
      this.Version = "VERSIÓN";
        }
    }
    
AjustFont(){
  setTimeout(function(){
    localStorage.setItem('ajustFont','si') 
    $(".vers").css({'font-size':'6px'})//10px
    $(".txtTtl, .txtCoin, .txtCoinTienda1, .txtTtl1, p, .btnSocial").css({'font-size':'9px'})//15px
    $(" h4").css({'font-size':'10px'})//18px
    $(".btnJuegos, .credit h2").css({'font-size':'12px'})//20px
    $(".titSelect").css({'font-size':'15px'})//25px
    $(".btnjugar,.iconos .icon, .titTienda, .iconsocial").css({'font-size':'18px'})//30px
  },1000) 
}
}
