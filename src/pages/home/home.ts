import { Component, ViewChild } from '@angular/core';
import { Nav} from 'ionic-angular';
import { NavController} from 'ionic-angular';
import { SettingPage } from '../setting/setting';
import { AboutPage } from '../about/about';
import { InfoPage } from '../info/info';
import { LoginPage } from '../login/login';
import { PrehomePage } from '../prehome/prehome';
import { CrucigramaPage } from '../juegos/crucigrama/crucigrama';
import { DiscosPage } from '../juegos/discos/discos';
import { DragPage } from '../juegos/drag/drag';
import { ConceptosPage } from '../juegos/conceptos/conceptos';
import { InterpretePage } from '../juegos/interprete/interprete';
import { MemoryPage } from '../juegos/memory/memory';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Device } from '@ionic-native/device/';

declare var $

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Nav) nav: Nav;
  language: string;
  Jugar: string;
  Version: string;
  classIcon: string;
  classIconName: string;
  classJugar: string;
  classSettings: string;
  classAbout: string;
  ClassModalTienda: string;
  ClassBoxTienda: string = "BoxTienda";
  Onanimated : string;
  Intro : string;
  swiche: boolean;
  login: boolean;
  classMusica: string;
  iconMusica: string;
  Userlogin: any;
  settings: any;
  about: any;
  info: any;
  loginpage: any;
  prehome:any;
  txtHome: string;
  txtMemoria: string;
  txtCrucigrama: string;
  txtDrag: string;
  txtInterprete: string;
  txtDiscos: string;
  txtConceptos:string;
  descMemoria: string;
  descCrucigrama: string;
  descDrag: string;
  descInterprete: string;
  descDiscos: string;
  selec: string;
  contStarMemoria:string;
  contAllStarDrag: any;
  contAllStarCruci: string;
  contStarTrivia: string;
  contStarDisc: string;
  ttl: any;
  ttl2:any;
  coin: any;
  comprarv:any;
  alertCompra:any;
  alertCompra2:any;
  public nttl: any;
  classDisco: string = '';
  Tienda: string;
  btnCompra: string;

  constructor(public navCtrl: NavController , private iab: InAppBrowser, private device: Device) {
    this.settings = SettingPage;
    this.about = AboutPage;
    this.info = InfoPage;
    this.loginpage = LoginPage;
    this.prehome = PrehomePage;
  }
  ionViewWillEnter(){
    //  localStorage.setItem('coin', '3000');
    //localStorage.setItem('ttl', '5');
    // console.log('Device UUID is: ' + this.device.uuid);
    // console.log('Device version is: ' + this.device.version);    
    if(this.device.version <= '4.4.4'){
      this.classDisco = "ocultar"
      $('#loading').addClass('oculto')
    }
    localStorage.setItem('goHome', 'no');
    localStorage.setItem("Next", 'no'); 
    this.contStarMemoria = localStorage.getItem('contStarMemoria');    
    this.contAllStarDrag = localStorage.getItem('contAllStarDrag');
    this.contAllStarCruci = localStorage.getItem('contAllStarCruci');
    this.contStarTrivia = localStorage.getItem('contStarTrivia');
    this.contStarDisc = localStorage.getItem('contStarDisc');
    this.ttl = localStorage.getItem('ttl');
    this.ttl2 = parseInt(localStorage.getItem('ttl'));
    this.coin = localStorage.getItem('coin');
    this.comprarv = localStorage.getItem('comprar');
    this.ClassModalTienda = "ModalTienda";
    localStorage.setItem('Next', 'no'); 
    if (localStorage.getItem('ajustFont') == 'si'){
      this.AjustFont();
    }
    if (this.comprarv == 'si'){
      this.ModalComprar();
    }
    if (this.ttl2 > 5){
      localStorage.setItem('ttl', '5');
    }
    this.Userlogin = localStorage.getItem('userName');
    this.Intro = localStorage.getItem('IntroHome');
    if (this.Intro == 'true'){
      this.swiche = true;
      this.iconMusica = "musical-notes"; 
    }else{
      this.swiche = false;
      this.iconMusica = "volume-off"; 
    }
    this.Onanimated = localStorage.getItem('Animated');
    if (this.Onanimated == 'true'){
      this.classIcon = "logo2 animated flip slow"; 
      this.classIconName = "logo1 animated tada slow"; 
      this.classJugar = "btnjugar animated fadeInUp slow"; 
      this.classMusica = "btnmusica animated fadeInRight slow"; 
      this.classSettings = "btnsetting animated fadeInRight slow"; 
      this.classAbout = "btnacerca animated fadeInLeft slow"; 
      localStorage.setItem('Animated', 'true');
    }
    if (this.Onanimated == 'false'){
      this.classIcon = "logo2 "; 
      this.classIconName = "logo1 "; 
      this.classJugar = "btnjugar "; 
      this.classMusica = "btnmusica"; 
      this.classSettings = "btnsetting "; 
      this.classAbout = "btnacerca "; 
      localStorage.setItem('Animated', 'false');
    }
    this.language = localStorage.getItem('selectedLang');
    if (this.language == undefined || this.language == null|| this.language == ''){
      this.language = "Esp"; 
    }
    this.ChangeLanguages();
    this.UserID();
    console.log('entro')
  }
  preHome(){
    this.swiche = false;
    this.iab.create('/')
  }
 SelecMusica(){

  this.swiche = !this.swiche;
  if (this.swiche){
    this.iconMusica = "musical-notes"; 
    console.log('Musica = on');
    localStorage.setItem('IntroHome', 'true');
    localStorage.setItem('Audio', '1');
  }else{
    this.iconMusica = "volume-off"; 
    console.log('Musica = off');
    localStorage.setItem('IntroHome', 'false');
    localStorage.setItem('Audio', '0');
  }
}

UserID(){
  if (this.Userlogin != null){
    this.login = true;
    console.log('logueado');
  }else{
    this.login = false;
    console.log('no logueado');
  }
}
ChangeLanguages() {
  if (this.language == "Eng") {
    this.txtHome = "Home";
    this.txtMemoria = "Memory";
    this.txtCrucigrama = "Crossword";
    this.txtDrag = "Drag";
    this.txtInterprete = "Output interpreter";
    this.txtDiscos = "Disc";
    this.txtConceptos ="Basic concepts";
    this.selec = "Select a game!";
    this.descMemoria="You must flip a letter and then get your partner. The couple corresponds to: Port <---> Protocol name.";
    this.descCrucigrama = "In this fun crossword you can test your knowledge in different aspects of the Internet world.";
    this.descDrag = "You must assemble the datagram, dragging the name of the field to its corresponding place.";
    this.descInterprete = "The goal is to answer questions regarding the output of various commands one more way to test your knowledge.";
    this.descDiscos = "The objective is to select the corresponding balloon as indicated by the screen. Another way to test your knowledge.";
    this.alertCompra= "You must enter the amount"
    this.alertCompra2="Does not have enough TTL" 
    this.Tienda="TTL Store"
    this.btnCompra="Buy"
  }else if (this.language == "Por"){
    this.txtHome = "Home";
    this.txtMemoria = "Memória";
    this.txtCrucigrama = "Palavras cruzadas";
    this.txtDrag = "Drag";
    this.txtInterprete = "Interprete de saída";
    this.txtDiscos = "Discos";
    this.txtConceptos ="Conceitos básicos";
    this.selec = "Selecione um jogo!";
    this.descMemoria="Você deve virar uma carta e depois buscar seu parceiro. O casal corresponde a: Porta <---> Nome do protocolo.";
    this.descCrucigrama = "Neste divertido jogo de palavras cruzadas, você pode testar seu conhecimento em diferentes aspectos do mundo da Internet.";
    this.descDrag = "Você deve montar o datagrama, arrastando o nome do campo para o local correspondente.";
    this.descInterprete = "O objetivo é responder a perguntas sobre a saída de vários comandos, mais uma maneira de testar seu conhecimento.";
    this.descDiscos = "O objetivo é selecionar o balão correspondente, conforme indicado na tela. Outra maneira de testar seu conhecimento.";
    this.alertCompra= "Você deve inserir o valor"
    this.alertCompra2="Não possui TTL suficiente" 
    this.Tienda="Loja TTL"
    this.btnCompra="Comprar"
  }else{
    this.txtHome = "Home";
    this.txtMemoria = "Memoria";
    this.txtCrucigrama = "Crucigrama";
    this.txtDrag = "Drag";
    this.txtInterprete = "Interprete la Salida";
    this.txtDiscos = "Discos";
    this.txtConceptos ="Conceptos básicos";
    this.selec = "¡Seleccione un Juego!";
    this.descMemoria="Debes voltear una carta y luego conseguir su pareja. La pareja corresponde a: Puerto <---> Nombre protocolo.";
    this.descCrucigrama = "En este divertido crucigrama podrás probar tus conocimientos en diferentes aspectos del mundo de Internet.";
    this.descDrag = "Debes armar el datagrama, arrastrando el nombre del campo a su lugar correspondiente.";
    this.descInterprete = "El objetivo es contestar preguntas referentes a la salida de diversos comandos una forma mas de probar tus conocimientos.";
    this.descDiscos = "El objetivo es seleccionar el globo correspondiente según te indique la pantalla. Otra manera de probar tus conocimientos.";
    this.alertCompra= "Debe ingresar la cantidad"
    this.alertCompra2="No Posee los suficientes TTL"  
    this.Tienda="Tienda TTL"
    this.btnCompra="Comprar"
  }
  }

  openPage(page) {
    if(page=="memoria"){
      this.navCtrl.push(MemoryPage);
    }
    if(page=="drag"){
      this.navCtrl.push(DragPage);
    }
    if(page=="crucigrama"){
      this.navCtrl.push(CrucigramaPage);
    }
    if(page=="trivia"){
      this.navCtrl.push(InterpretePage);
    }
    if(page=="discos"){
      this.navCtrl.push(DiscosPage);
    }
    if(page=="conceptos"){
      this.navCtrl.push(ConceptosPage);
    }
    
  }
ModalComprar(){
    localStorage.setItem('comprar', 'no');
    if (this.Onanimated == 'true'){
      this.ClassModalTienda = "ModalTienda mostrar animated fadeIn";
      this.ClassBoxTienda = "BoxTienda animated bounceIn";
    }else{
      this.ClassModalTienda = "ModalTienda mostrar"; 
      this.ClassBoxTienda = "BoxTienda ";
    }
   }
cerrarModal(){
    if (this.Onanimated == 'true'){
      this.ClassBoxTienda = "BoxTienda animated zoomOut";
      this.ClassModalTienda = "ModalTienda";
    }else{
      this.ClassModalTienda = "ModalTienda"; 
    }
}
comprar() { 
      var coinAct = Number(this.coin);
      var ttlAct = Number(this.ttl);
      var cant = Number(this.nttl);
       
      var totalCoin = cant * 100;
      if(this.nttl == null){
        alert(this.alertCompra)
      }else if(coinAct < totalCoin){
        alert(this.alertCompra2)
      }else{
        var restaCoin = coinAct - totalCoin;
        var sumaTTL= ttlAct + cant;
        var ttl = sumaTTL.toString();
        var coin = restaCoin.toString();
        if (sumaTTL == 4){
          window.localStorage.setItem('fechaRecuperarVida2', '0'); 
          window.localStorage.setItem('fechaRecuperarVida3', '0'); 
          window.localStorage.setItem('fechaRecuperarVida4', '0'); 
          window.localStorage.setItem('fechaRecuperarVida5', '0'); 
        }
        if (sumaTTL == 3){
          window.localStorage.setItem('fechaRecuperarVida3', '0'); 
          window.localStorage.setItem('fechaRecuperarVida4', '0'); 
          window.localStorage.setItem('fechaRecuperarVida5', '0');  
        }
        if (sumaTTL == 2){
          window.localStorage.setItem('fechaRecuperarVida4', '0'); 
          window.localStorage.setItem('fechaRecuperarVida5', '0'); 
        }
        if (sumaTTL == 1){
          window.localStorage.setItem('fechaRecuperarVida5', '0'); 
        }
        localStorage.setItem('coin', coin);
        localStorage.setItem('ttl', ttl);
        this.ionViewWillEnter();
        this.cerrarModal();
      }
      
}
AjustFont(){
  setTimeout(function(){
    localStorage.setItem('ajustFont','si') 
    $(".vers").css({'font-size':'6px'})//10px
    $(".txtTtl, .txtCoin, .txtCoinTienda1, .txtTtl1, p, .btnSocial").css({'font-size':'9px'})//15px
    $(" h4").css({'font-size':'10px'})//18px
    $(".btnJuegos, .credit h2").css({'font-size':'12px'})//20px
    $(".titSelect").css({'font-size':'15px'})//25px
    $(".btnjugar,.iconos .icon, .titTienda, .iconsocial").css({'font-size':'18px'})//30px
  },1000) 
}
}