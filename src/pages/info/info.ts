import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  language: string;
  cancel: string;
  accept: string;
  Cerrar:string;
  CerrarSesion: string;
  ImgUser:any;
  Userlogin: any;
  txtId: string;
  method:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.Userlogin = localStorage.getItem('userName');
    this.ImgUser = localStorage.getItem('userPic');
    this.language = localStorage.getItem('selectedLang');
    if (this.language == undefined || this.language == null|| this.language == ''){
      this.language = "Esp"; 
    }
    this.ChangeLanguages();
  }

  logout(){
    let alert = this.alertCtrl.create();
  alert.setTitle(this.Cerrar);
  alert.addButton(this.cancel);
  alert.addButton({
    text: this.accept,
    handler: data =>    
    { 
      this.navCtrl.setRoot(HomePage);
      localStorage.removeItem('userName');
      localStorage.removeItem('UserId');
      localStorage.removeItem('userPic');
      localStorage.removeItem('method');
      
    }
  });
  alert.present();
  }
  ChangeLanguages()
  {

    if(this.language == "en")
    {
      this.Cerrar = "You want to close session?";
      this.cancel = "Cancel";
      this.accept = "Accept";
      this.CerrarSesion = "Logout";
    }
    else if (this.language == "Por"){
      this.Cerrar = "Você quer fechar a sessão?";
      this.cancel = "Cancel";
      this.accept = "Aceitar";
      this.CerrarSesion = "Fechar Sessão";
    
    }else{
      this.Cerrar = "Desea cerrar sesión?";
      this.cancel = "Cancelar";
      this.accept = "Aceptar";
      this.CerrarSesion = "Cerrar Sesión";
      
    }
  }
  
}
