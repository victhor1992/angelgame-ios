import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConceptosPage } from './conceptos';

@NgModule({
  declarations: [
    ConceptosPage,
  ],
  imports: [
    IonicPageModule.forChild(ConceptosPage),
  ],
})
export class ConceptosPageModule {}
