import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


@IonicPage()
@Component({
  selector: 'page-conceptos',
  templateUrl: 'conceptos.html',
})
export class ConceptosPage {
  language: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log(this.language );
    this.language = localStorage.getItem('selectedLang');
   if (this.language == undefined || this.language == null|| this.language == ''){
     this.language = "Esp"; 
    }
   if (this.language == 'Esp'){
     this.iab.create('/assets/conceptos.html');
   }
   if (this.language == 'Eng'){
      this.iab.create('/assets/conceptos-en.html');
   }
   if (this.language == 'Por'){
     this.iab.create('/assets/conceptos-por.html');
   }
 }
}
