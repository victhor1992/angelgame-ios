import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrucigramaPage } from './crucigrama';

@NgModule({
  declarations: [
    CrucigramaPage,
  ],
  imports: [
    IonicPageModule.forChild(CrucigramaPage),
  ],
})
export class CrucigramaPageModule {}
