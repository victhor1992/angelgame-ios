import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@IonicPage()
@Component({
  selector: 'page-crucigrama',
  templateUrl: 'crucigrama.html',
})
export class CrucigramaPage {
  language: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) {
  }

 ionViewDidLoad() {
    console.log(this.language );
    this.language = localStorage.getItem('selectedLang');
   if (this.language == undefined || this.language == null|| this.language == ''){
     this.language = "Esp"; 
    }
   if (this.language == 'Esp'){
     this.iab.create('/assets/crucigramaMenu.html');
   }
   if (this.language == 'Eng'){
      this.iab.create('/assets/crucigramaMenu-en.html');
   }
   if (this.language == 'Por'){
     this.iab.create('/assets/crucigramaMenu-por.html');
   }
 }

}
