import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DragPage } from './drag';

@NgModule({
  declarations: [
    DragPage,
  ],
  imports: [
    IonicPageModule.forChild(DragPage),
  ],
})
export class DragPageModule {}
