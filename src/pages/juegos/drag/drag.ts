import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@IonicPage()
@Component({
  selector: 'page-drag',
  templateUrl: 'drag.html',
})
export class DragPage {
  language: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) {
  }

 ionViewDidLoad() {
   this.language = localStorage.getItem('selectedLang');
   console.log(this.language );
   if (this.language == undefined || this.language == null|| this.language == ''){
     this.language = "Esp"; 
   }
   if (this.language == 'Esp'){
     this.iab.create('/assets/dragMenu.html');
    }
    if (this.language == 'Eng'){
      this.iab.create('/assets/dragMenu-en.html');
    }
   if (this.language == 'Por'){
     this.iab.create('/assets/dragMenu-por.html');
   }
    
 }

}
