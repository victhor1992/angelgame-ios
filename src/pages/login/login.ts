import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { HomePage } from '../home/home';
// import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Instagram, Facebook } from "ng2-cordova-oauth/core";  
import { OauthCordova } from 'ng2-cordova-oauth/platform/cordova';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { UserService } from '../../providers/user-service/user-service';   
import { FacebookServiceProvider } from '../../providers/user-service/facebook-service';
declare var $
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  language: string;
  Jugar: string;
  Version: string;
  classIcon: string;
  classIconName: string;
  classJugar: string;
  classSettings: string;
  classAbout: string;
  Onanimated : string;
  Intro : string;
  swiche: boolean;
  login: boolean;
  classMusica: string;
  iconMusica: string;
  Userlogin: any;
  iniciarsesion:string;
  texto:string;
  ImaUser:string;
  ImaTwitter:string = 'https://angelgame.acostasite.com/Game/img/twitter.png';
  LoginConFacebook:string;
  LoginConTwitter:string;
  LoginConInstagram:string;
  connecion:string;
  remoteData = [];
  access_token: any;
  authentication_code: any;
  facebookEnable: boolean;
  photoUrl: any;
  background: any;
  ocultar:any;
  private oauth: OauthCordova = new OauthCordova();

  private instagramProvider: Instagram = new Instagram({
      clientId: "4ac68463df9b4dd8b3177c047f570cf9",      // Register you client id from https://www.instagram.com/developer/
      redirectUri: 'https://angelgame.acostasite.com/ApiAngel/Api',  // Let is be localhost for Mobile Apps
      responseType: 'token',   // Use token only 
      appScope: ['basic'] 
  });

  private facebookProvider: Facebook = new Facebook({
    clientId: "726444157854031",
    redirectUri: 'https://angelgame.acostasite.com/ApiAngel/Api',
    responseType: 'token',
    appScope: ["email"]
  })
  private apiResponse;
  twitername: string;
  LoginConApple: string;
  mostrar: string;

  constructor(private fire: AngularFireAuth,public plt: Platform, public UserService:UserService,public facebookService: FacebookServiceProvider, public http: Http, public navCtrl: NavController, public navParams: NavParams) {
    this.apiResponse = [];
  }

  ionViewWillEnter() {
    this.language = localStorage.getItem('selectedLang');
    if (this.language == undefined || this.language == null|| this.language == ''){
      this.language = "Esp"; 
    }
    if(this.plt.is('ios')){
      this.ocultar= 'ocultar';
      this.mostrar= 'mostrar';
    }else{
      this.ocultar= ' ';
      this.mostrar= ' ';
    }
    if (localStorage.getItem('ajustFont') == 'si'){
      this.AjustFont();
    }
    this.ChangeLanguages();
  }
  ChangeLanguages() {
    if (this.language == "Eng") {
      this.Jugar = "PLAY"
      this.Version = "Version";
      this.iniciarsesion ="Login"
      this.texto ="Log in to your favorite social network to keep track of your progress and share it with your friends:"
      this.LoginConFacebook = "Login with Facebook"
      this.LoginConTwitter = "Login with Twitter"
      this.LoginConApple = "Login with Apple"
      this.LoginConInstagram = "Login with Instagram"
      this.connecion = "Connection error restart app"
    }else if (this.language == "Por"){
      this.Jugar = "JOGAR"
      this.Version = "Versão";
      this.iniciarsesion ="Iniciar sessão"
      this.texto ="Faça login na sua rede social favorita para acompanhar seu progresso e compartilhe-o com seus amigos:"
      this.LoginConFacebook = "Entre com Facebook"
      this.LoginConTwitter = "Entre com Twitter"
      this.LoginConApple = "Entre com Apple"
      this.LoginConInstagram = "Entre com Instagram"
      this.connecion = "Erro de conexão reiniciar app"
    }else{
      this.Jugar = "JUGAR"
      this.Version = "Versión";
      this.iniciarsesion ="Iniciar sesión"
      this.texto ="Inicia sesión en tu red social preferida para llevar registro de tu progreso y compartirlo con tus amigos:"
      this.LoginConFacebook = "Login con Facebook"
      this.LoginConTwitter = "Login con Twitter"
      this.LoginConApple = "Login con Apple"
      this.LoginConInstagram = "Login con Instagram"
      this.connecion = "Error de conección reinicie la app"
    
    }
    }
    LoginFacebook(){
      console.log("Login facebook")
      var lobThis = this;
      var info3 = { "name" : "", "lastName"  : "", "email" : "", "id" : "","username" : "","method":"", "image":""};
        this.oauth.logInVia(this.facebookProvider).then(success => {
          lobThis.access_token = success["access_token"];
          lobThis.facebookService.setRemoteData(lobThis.access_token);
          this.facebookService.getRemoteData().subscribe(result => {
            this.remoteData = result;
            localStorage.setItem('entry.name', this.remoteData["name"]);
            localStorage.setItem('userName', this.remoteData["name"]);
            this.ImaUser = "http://graph.facebook.com/" + this.remoteData["id"] + "/picture?type=large";
            localStorage.setItem('userPic', this.ImaUser );

            var strName = this.remoteData["name"];
            var name = '';
            var lstName = '';
            switch (strName.length)
            {
                case 1:
                    name = strName[0];
                    break;
                case 2:
                    name = strName[0];
                    lstName = strName[1];
                    break;
                case 3:
                    name = strName[0] + " " + strName[1];
                    lstName = strName[2];
                    break;
                default:
                    name = strName[0] + " " + strName[1];
                    lstName = strName[2] + " " + strName[3];
                    break;
            }
            info3.lastName = lstName;
            info3.name = name;
            info3.email = this.remoteData["email"];
            info3.id= this.remoteData["id"];
            info3.username= this.remoteData["name"];
            info3.method="1";
            info3.image= this.ImaUser;
            localStorage.setItem('method', info3.method);
            this.navCtrl.push(HomePage);

            
            let headers = new Headers();
            headers.append('Content-Type','application/x-www-form-urlencoded');
            headers.append('Accept','application/json');
            console.log("enviando la data a la pagina ")
            this.http.post("https://angelgame.acostasite.com/ApiAngel/Api/register.json",
              'name='+info3.name+
              '&lastname='+info3.lastName+
              '&token='+info3.id+
              '&email='+info3.email+
              '&username='+info3.username+
              '&image='+info3.image+
              '&method='+info3.method
            ,{headers: headers}).timeout(30000)
            .map(res => res.json())
            .subscribe(data=>{
            var registrado=data.user.Id;
            console.log("id registrado= ",registrado);
            // console.log("la data es= ",data);
            localStorage.setItem('UserId', registrado);
            localStorage.setItem('method', info3.method);
            
            },
            err =>{
              alert("error send web");
            console.log(err);
            }
            );
          });
        }, error => {
          console.log("Error conection");
          console.log("ERROR: ", error);
        });
    }
    
     LoginTwitter(){
       console.log("Login twitter");
       this.fire.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider())
       .then( res => {
        console.log (res);
        var info3 = { "name" : "", "lastName"  : "", "email" : "", "id" : "","username" : "","method":"", "image":""};

         localStorage.setItem('entry.name', res.user.displayName);
         localStorage.setItem('userName', res.additionalUserInfo.username);
         localStorage.setItem('userPic', res.user.photoURL);
                  var strName = res.user.displayName.split(' ');
                  var name = '';
                  var lstName = '';
                  switch (strName.length)
                  {
                      case 1:
                          name = strName[0];
                          break;
                      case 2:
                          name = strName[0];
                          lstName = strName[1];
                          break;
                      case 3:
                          name = strName[0] + " " + strName[1];
                          lstName = strName[2];
                          break;
                      default:
                          name = strName[0] + " " + strName[1];
                          lstName = strName[2] + " " + strName[3];
                          break;
                  }
                  info3.lastName = lstName;
                  info3.name = name;
                  info3.email = res.user.email+"@twitter.com";	;
                  info3.id= res.user.providerData[0].uid;
                  info3.username= res.additionalUserInfo.username;
                  info3.method="2";
                  info3.image= res.user.photoURL;
                  localStorage.setItem('method', info3.method);
         this.navCtrl.push(HomePage);
       
         let headers = new Headers();
         headers.append('Content-Type','application/x-www-form-urlencoded');
         headers.append('Accept','application/json');
        console.log("enviando la data a la pagina ")

         this.http.post("https://angelgame.acostasite.com/ApiAngel/Api/register.json",
           'name='+info3.name+
           '&lastname='+info3.lastName+
           '&token='+info3.id+
           '&email='+info3.email+
           '&username='+info3.username+
           '&image='+info3.image+
           '&method='+info3.method
         ,{headers: headers}).timeout(30000)
         .map(res => res.json())
         .subscribe(data=>{
          var registrado=data.user.Id;
          console.log("id registrado= ",registrado);
    
          localStorage.setItem('UserId', registrado);
          localStorage.setItem('method', info3.method);
         
         },
         err =>{
           alert("error al enviar data");
          console.log(err);
         }
         );
       }  )
   }
   LoginInstagram(){
    console.log("Login Instagram");
    this.oauth.logInVia(this.instagramProvider).then((success) => {
  
      console.log(JSON.stringify(success));
  
      /* Returns User uploaded Photos */
      this.UserService.getInstagramUserInfo(success).subscribe(response => {
        this.apiResponse=response.data;
        var info3 = { "name" : "", "lastName"  : "", "email" : "", "id" : "","username" : "", "method":"", "image":"" };
            
        info3.email = this.apiResponse[0].user.username + "@instagram.com";
         console.log(this.apiResponse);
  
        localStorage.setItem('entry.name', this.apiResponse[0].user.username);
        localStorage.setItem('userName', this.apiResponse[0].user.full_name);
        localStorage.setItem('userPic', this.apiResponse[0].user.profile_picture);
        this.navCtrl.push(HomePage);
        
        var username = this.apiResponse[0].user.full_name;
        var strName = username.split(' ');
        var name = '';
        var lstName = '';
        switch (strName.length)
        {
          case 1:
            name = strName[0];
            break;
          case 2:
            name = strName[0];
            lstName = strName[1];
            break;
          case 3:
            name = strName[0] + " " + strName[1];
            lstName = strName[2];
            break;
          default:
            name = strName[0] + " " + strName[1];
            lstName = strName[2] + " " + strName[3];
            break;
        }
  
        info3.lastName = lstName;
        info3.name = name;
        info3.username = this.apiResponse[0].user.username;	
        info3.email = this.apiResponse[0].data.id + "@instagram.com"; 
        info3.id = this.apiResponse[0].user.id;
        info3.method="3";
        info3.image= this.apiResponse[0].user.profile_picture;
        localStorage.setItem('method', info3.method);

        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded');
        headers.append('Accept','application/json');
       console.log("enviando la data a la pagina ")

        this.http.post("https://angelgame.acostasite.com/ApiAngel/Api/register.json",
          'name='+info3.name+
          '&lastname='+info3.lastName+
          '&token='+info3.id+
          '&email='+info3.email+
          '&username='+info3.username+
          '&image='+info3.image+
          '&method='+info3.method
        ,{headers: headers}).timeout(30000)
        .map(res => res.json())
        .subscribe(data=>{
         var registrado=data.user.Id;
         console.log("id registrado= ",registrado);
    
         localStorage.setItem('UserId', registrado);
         localStorage.setItem('method', info3.method);
         
        },
        err =>{
          alert("error al enviar data");
         console.log(err);
        }
        );

      })
      }, (error) => {
        alert("error al enviar data ");
          console.log(JSON.stringify(error));
      });
  
  }
    
  LoginApple(){
 console.log('login apple')
  }
  AjustFont(){
    setTimeout(function(){
      localStorage.setItem('ajustFont','si') 
      $(".vers").css({'font-size':'6px'})//10px
      $(".txtTtl, .txtCoin, .txtCoinTienda1, .txtTtl1, p, .btnSocial").css({'font-size':'9px'})//15px
      $(" h4").css({'font-size':'10px'})//18px
      $(".btnJuegos, .credit h2").css({'font-size':'12px'})//20px
      $(".titSelect").css({'font-size':'15px'})//25px
      $(".btnjugar,.iconos .icon, .titTienda, .iconsocial").css({'font-size':'18px'})//30px
    },1000) 
  }
}
