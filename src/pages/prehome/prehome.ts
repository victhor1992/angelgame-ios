import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav } from 'ionic-angular';
import { SettingPage } from '../setting/setting';
import { AboutPage } from '../about/about';
import { InfoPage } from '../info/info';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { Instagram } from '@ionic-native/instagram';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device/';

@IonicPage()
@Component({
  selector: 'page-prehome',
  templateUrl: 'prehome.html',
})

export class PrehomePage {
  @ViewChild(Nav) nav: Nav;
  language: string;
  Jugar: string;
  Version: string;
  classIcon: string;
  classIconName: string;
  classJugar: string;
  classSettings: string;
  classAbout: string;
  classfacebook: string;
  classtwitter: string;
  classinstagram: string;
  Onanimated : string;
  Intro : string;
  swiche: boolean;
  login: boolean;
  classMusica: string;
  iconMusica: string;
  Userlogin: any;
  settings: any;
  homepage:any;
  about: any;
  info: any;
  loginpage: any;
  Nperdidas:any;
  imgShare: string = 'https://angelgame.acostasite.com/Game/img/desAngel.png';
  mensajeShare: string = 'A.N.G.E.L GAME - ipv4 - ipv6 - tcp - udp -ethernet';
  linkShare:string = 'https://angelgame.acostasite.com/Game/index.html';
  contStarMemoria:any;
  contAllStarDrag: any;
  contAllStarCruci: any;
  contStarTrivia: string;
  contStarDisc: any;
  ttl: any;
  coin: string;
  comprar: string;
  timeNewVida: any;
  Efectos: any;
  gohome:any;
  urlogo:string;
  noShow:string;
  ocultar:string;
  versionNum: string;
  fechaVersion: string;
  constructor(public navCtrl: NavController,public plt: Platform, private iab: InAppBrowser, public navParams: NavParams, private instagram: Instagram, private device: Device) {
    this.settings = SettingPage;
    this.about = AboutPage;
    this.info = InfoPage;
    this.loginpage = LoginPage;
  }
  ionViewWillEnter(){    
    localStorage.setItem('versionNum' , '"3.631"')
    localStorage.setItem('fechaVersion' , 'Comp=29-09-2020')
    this.versionNum= localStorage.getItem('versionNum')
    this.fechaVersion= localStorage.getItem('fechaVersion')
    console.log('entro ajustes')
    if(this.device.version <= '4.4.4'){
      console.log('entro ajustes')
      window.localStorage.setItem('ajustes', 'si');
    }else{
      window.localStorage.setItem('ajustes', 'no');  
    }
    this.noShow = localStorage.getItem('noShow');
    this.contStarMemoria = localStorage.getItem('contStarMemoria');
    this.contAllStarDrag = localStorage.getItem('contAllStarDrag');
    this.contAllStarCruci = localStorage.getItem('contAllStarCruci');
    this.contStarTrivia = localStorage.getItem('contStarTrivia');
    this.contStarDisc = localStorage.getItem('contStarDisc');
    this.comprar = localStorage.getItem('comprar');
    this.ttl = localStorage.getItem('ttl');
    this.coin = localStorage.getItem('coin');
    this.Nperdidas = localStorage.getItem('Nperdidas');
    this.Userlogin = localStorage.getItem('userName');
    this.Intro = localStorage.getItem('IntroHome');
    this.gohome = localStorage.getItem('goHome');
    this.Efectos = localStorage.getItem('sound-acert');
    
    if(localStorage.getItem('ajusText')=='si'){
      console.log('si entro')
    }
    if(this.Intro==undefined || this.Intro == null || this.Intro == ''){
      window.localStorage.setItem('audio', '0');  
    }
    if (this.noShow == undefined || this.noShow == null|| this.noShow == ''){
      window.localStorage.setItem('noShow', 'false');
      window.localStorage.setItem('noShowmemoria', 'false');
      window.localStorage.setItem('noShowdrag', 'false');
      window.localStorage.setItem('noShowcrucigrama', 'false');
      window.localStorage.setItem('noShowdisco', 'false');
      window.localStorage.setItem('noShowtrivia', 'false');
      window.localStorage.setItem('noShowconceptos', 'false');
    }
    if (this.comprar == 'si' || this.gohome == 'si'){
      this.navCtrl.push(HomePage);      
      this.SelecMusica('si');
    }
    if (this.ttl == null || this.ttl < 0){
      localStorage.setItem('ttl', '5');
      window.localStorage.setItem('fechaRecuperarVida1', '0');
      window.localStorage.setItem('fechaRecuperarVida2', '0'); 
      window.localStorage.setItem('fechaRecuperarVida3', '0'); 
      window.localStorage.setItem('fechaRecuperarVida4', '0'); 
      window.localStorage.setItem('fechaRecuperarVida5', '0');  
    }
    if (this.coin == null){ 
      localStorage.setItem('coin', '500');
    }
    if (this.Intro == 'true'){
      this.swiche = true;
      this.iconMusica = "musical-notes"; 
    }else{
      this.swiche = false;
      this.iconMusica = "volume-off"; 
    }
    
    this.Onanimated = localStorage.getItem('Animated');
    if (this.Onanimated == 'true'){

      this.classIcon = "logo2 animated flip slow"; 
      this.classIconName = "logo1 animated tada slow"; 
      this.classJugar = "btnjugar animated infinite fadeIn slow"; 
      this.classMusica = "btnmusica iconos animated fadeInUp slow"; 
      this.classSettings = "btnsetting iconos animated fadeInUp slow"; 
      this.classAbout = "btnacerca iconos animated fadeInUp slow"; 
      this.classfacebook = "iconFace iconos animated fadeInUp slow"; 
      this.classtwitter = "icontwitter iconos animated fadeInUp slow"; 
      this.classinstagram = "iconinstagram iconos animated fadeInUp slow"; 
      localStorage.setItem('Animated', 'true');
    }
    if (this.Onanimated == 'false'){
      this.classIcon = "logo2 "; 
      this.classIconName = "logo1 "; 
      this.classJugar = "btnjugar"; 
      this.classMusica = "btnmusica iconos"; 
      this.classSettings = "btnsetting iconos"; 
      this.classAbout = "btnacerca iconos"; 
      this.classfacebook = "iconFace iconos"; 
      this.classtwitter = "icontwitter iconos"; 
      this.classinstagram = "iconinstagram iconos"; 
      localStorage.setItem('Animated', 'false');
    }
    this.language = localStorage.getItem('selectedLang');
    if (this.language == undefined || this.language == null|| this.language == ''){
      this.language = "Esp"; 
    }
    if(this.plt.is('android')){
      // console.log('android') 
      window.localStorage.setItem('platform', 'android');
    }
    if( this.plt.is('ios')){
      // console.log('ios')
      window.localStorage.setItem('platform', 'ios');
      this.classinstagram = "displaynone"; 
    }
    this.plt.pause.subscribe(async () => {
      console.log('pausado')
    });
    this.ChangeLanguages();
    this.UserID();
    this.star();
  }

 SelecMusica(goHome){
  if(goHome == 'si'){
    this.swiche = false;
  }else{
    this.swiche = !this.swiche;
    if (this.swiche){
      this.iconMusica = "musical-notes"; 
      console.log('Musica = on');
      localStorage.setItem('IntroHome', 'true');
      localStorage.setItem('Audio', '1');      
    }else{
      this.iconMusica = "volume-off"; 
      console.log('Musica = off');
      localStorage.setItem('IntroHome', 'false');
      localStorage.setItem('Audio', '0');
    }
  }  
}
home(){ 
  this.swiche = false;
  this.navCtrl.push(HomePage);
}

UserID(){
  if (this.Userlogin != null){
    this.login = true;
    // console.log('logueado');
  }else{
    this.login = false;
    // console.log('no logueado');
  }
}

  ChangeLanguages() {
    if (this.language == "Eng") {
      this.urlogo='/assets/imgs/logoangelN-en.png'
      this.Jugar = "Touch To Start"
      this.Version = "Version";
    }else if (this.language == "Por"){
      this.urlogo='/assets/imgs/logoangelN-por.png'
      this.Jugar = "Toque para começar"
      this.Version = "Versão";
    }else{
      this.urlogo='/assets/imgs/logoangelN.png'
      this.Jugar = "Toque Para Comenzar"
      this.Version = "Versión";
        }
    }
    shareFacebook(){
      console.log('share Facebook');
      this.iab.create('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fangelgame.acostasite.com%2FGame%2Findex.html?description='+this.mensajeShare+'&method=1&amp;src=sdkpreparse');
     }
     shareTwitter(){
      console.log('share Twitter');
      this.iab.create('https://twitter.com/share?text='+this.mensajeShare+'&url=https://angelgame.acostasite.com/Game/index-es.html');
     }
     shareInstagram(){
      console.log('share Instagram');
      this.instagram.share(this.getBase64Image(document.getElementById("capImg")), this.mensajeShare).then(() => {
     
      }).catch(() =>{
     
      }) 
      }
      getBase64Image(img) {
       var canvas = document.createElement("canvas");
       canvas.width = img.naturalWidth;
       canvas.height = img.naturalHeight;
       var ctx = canvas.getContext("2d");
       ctx.drawImage(img, 0, 0);
       var dataURL = canvas.toDataURL("image/png");
       return dataURL;
     }
 
star(){
  if(this.contStarMemoria == null){
    localStorage.setItem('contStarMemoria', '0');
  }
  if(this.contAllStarDrag == null){
    localStorage.setItem('contAllStarDrag', '0');
  }  
  if(this.contAllStarCruci == null){
    localStorage.setItem('contAllStarCruci', '0');
  } 
  if(this.contStarTrivia == null){
    localStorage.setItem('contStarTrivia', '0');
  } 
  if(this.contStarDisc == null){
    localStorage.setItem('contStarDisc', '0');
  }     
}

}

