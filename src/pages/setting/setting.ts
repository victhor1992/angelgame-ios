import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { LoginPage } from '../login/login';



@Injectable()
@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  Espanol: string;
  Ingles: string;
  Portugues: string;
  TitPag: string;
  txtMusica: string;
  txtEfectos: string;
  txtAnimacion: string;
  Musica: any;
  Efectos: any;
  Animacion: any;
  txtIdioma: string;
  language: string;
  Autentic: string;
  Onanimated : string;
  Boleano : string;
  Intro : string;
  Login: boolean;
  ImgUser:any;
  Userlogin: any;
  EmailUser:any;
  txtId: string;
  cancelar:string;
  cerrarsesion:string;
  Idioma:any;
  method:any;
  iniciarsesion:string;
  selectLanguage:string;
  cancel:string;
  accept:string;
  Lan:string;

 
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,public actionSheetCtrl: ActionSheetController) {
  }
  ionViewWillEnter(){

    // if (this.Musica == undefined || this.Musica == null|| this.Musica == ''){
    //   this.Boleano = localStorage.getItem('Audio');
    //   if ( this.Boleano == '0'){
    //     this.Musica = 'false';
    // }else{
    //     this.Musica = 'true';
    //     }
    // }
    this.Userlogin = localStorage.getItem('userName');
    this.txtId = localStorage.getItem('userid');
    this.method = localStorage.getItem('method');
    this.ImgUser = localStorage.getItem('userPic');
    if (this.Efectos == undefined || this.Efectos == null|| this.Efectos == ''){
      this.Boleano = localStorage.getItem('sound-acert');
      if ( this.Boleano == '0'){
        this.Efectos = 'false';
        }else{
        this.Efectos = 'true';
        }
    }
    if (this.Animacion == undefined || this.Animacion == null|| this.Animacion == ''){
      this.Animacion = localStorage.getItem('Animated');
    }
    this.language = localStorage.getItem('selectedLang');
    if (this.language == undefined || this.language == null|| this.language == ''){
      this.language = "Esp"; 
    }
    // this.SelecIdioma();
    this.UserID();
    this.ChangeLanguages()
  } 

  public SelecEfectos():void {
    
    if (!this.Efectos){
    console.log('efectos = off');
    localStorage.setItem('sound-acert', '0');
    }
    else{
    console.log('efectos = on');  
    localStorage.setItem('sound-acert', '1');
    }
  }
  public SelecAnimacion():void {
    // console.log(this.Animacion);
    if (!this.Animacion){
      console.log('Animacion = off');  
      localStorage.setItem('Animated', "false");
    }
    else{
      console.log('Animacion = on');  
      localStorage.setItem('Animated', "true");
    }
    
  }

  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle(this.selectLanguage);
    if (this.language == "Esp")
    {
      alert.addInput({
        type: 'radio',
        label: 'Español',
        value: 'Esp',
        checked: true
      });
      alert.addInput({
        type: 'radio',
        label: 'English',
        value: 'Eng'
       });
       alert.addInput({
        type: 'radio',
        label: 'Portugués',
        value: 'Por'
       });
    }
    else if(this.language == "Eng" )
    {
      alert.addInput({
        type: 'radio',
        label: 'Español',
        value: 'Esp',
        
      });
      alert.addInput({
        type: 'radio',
        label: 'English',
        value: 'Eng',
        checked: true
       });
       alert.addInput({
        type: 'radio',
        label: 'Portugués',
        value: 'Por'
        
       });
    }else{
      alert.addInput({
        type: 'radio',
        label: 'Español',
        value: 'Esp',
      });
      alert.addInput({
        type: 'radio',
        label: 'English',
        value: 'Eng'
       });
       alert.addInput({
        type: 'radio',
        label: 'Portugués',
        value: 'Por',
        checked: true
       });
    }
    alert.addButton(this.cancel);
    alert.addButton({
      text: this.accept,
      handler: data =>
      {
        localStorage.setItem('selectedLang',data);
        this.language = localStorage.getItem('selectedLang');
        this.ChangeLanguages();
      }
    });
    alert.present();
  }
  // public SelecIdioma():void {
  //   if (this.Idioma == undefined || this.Idioma == null|| this.Idioma == ''){
  //     this.Idioma = this.language ;
  //   }
  //   console.log('Idioma =', this.Idioma);
  //   this.language = this.Idioma;
  //   localStorage.setItem('selectedLang', this.language);
   
  //   this.ChangeLanguages();
  // }

  UserID(){
    if (this.Userlogin != null){
      this.Login = true;
      console.log('logueado');
    }else{
      this.Login = false;
      console.log('no logueado');
    }
  }
  LogIN(){
    this.navCtrl.push(LoginPage);
  }

  ChangeLanguages() {
    if (this.language == "Eng") {
      this.TitPag = "Settings";
      this.txtMusica = "Music";
      this.txtEfectos = "Sound effects";
      this.txtAnimacion = "Animation";
      this.txtIdioma = "Language";
      this.Espanol = "Spanish";
      this.Ingles = "Inglish";
      this.Portugues = "Portuguese";
      this.Autentic = "Authentication"
      this.cancelar = "Cancel";
      this.cerrarsesion ="Logout"
      this.iniciarsesion ="Login"
      this.selectLanguage = "Select a language";
      this.cancel = "Cancel";
      this.accept = "Accept";
      this.Lan = "English";
    }else if (this.language == "Por"){
      this.TitPag = "Opcional";
      this.txtMusica = "Música";
      this.txtEfectos = "Efeitos Sonoros";
      this.txtAnimacion = "Animação";
      this.txtIdioma = "Idioma";
      this.Espanol = "Espanhol";
      this.Ingles = "Inglês";
      this.Portugues = "Portugués";
      this.Autentic = "Autenticação"
      this.cancelar = "Cancelar";
      this.cerrarsesion ="Fechar Sessão"
      this.iniciarsesion ="Iniciar sessão"
      this.selectLanguage = "Selecione uma lenguaje";
      this.cancel = "Cancelar";
      this.accept = "Aceitar";
      this.Lan = "Portugues";
    }else{
      this.TitPag = "Opciones";
      this.txtMusica = "Música";
      this.txtEfectos = "Efectos";
      this.txtAnimacion = "Animación";
      this.txtIdioma = "Idioma";
      this.Espanol = "Español";
      this.Ingles = "Ingles";
      this.Portugues = "Portugués";
      this.Autentic = "Autenticación"
      this.cancelar = "Cancelar";
      this.cerrarsesion ="Cerrar sesion"
      this.iniciarsesion ="Iniciar sesión"
      this.selectLanguage = "Seleccione un lenguaje";
      this.cancel = "Cancelar";
      this.accept = "Aceptar";
      this.Lan = "Español";
        }
    } 
}
